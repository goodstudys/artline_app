import 'package:artline/bloc/global/BlocProvider.dart';
import 'package:artline/bloc/cart/CartBloc.dart';

class GlobalBloc implements BlocBase {
  CartBloc cartBloc;

  GlobalBloc() {
    cartBloc = CartBloc();
  }

  void dispose() {
    cartBloc.dispose();
  }
}
