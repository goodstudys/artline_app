import 'package:artline/model/dataAddress.dart';
import 'package:artline/model/dataProduct.dart';
// import 'package:artline/model/dataitems.dart';
import 'package:artline/model/dataCart.dart';
import 'package:artline/model/dataService.dart';
import 'package:artline/model/group.dart';
import 'package:artline/storage/storage.dart';
import 'package:flutter/material.dart';

class CartData {
  DataStore storage = DataStore();
  // List<DataItems> list = [];
  List<CartItem> listCart = [];
  DataAddress saveAddress;
  int totalItems = 0;
  int subTotal = 0;
  int totalPay = 0;
  int totalQty = 0;
  int totalWeight = 0;
  int totalDiscounts = 0;
  GroupSelected getGroup;
  DataService saveService;
  String saveKurir;

  void addKurir(String p) {
    saveKurir = p;
  }

  void addService(DataService p) {
    saveService = p;
  }

  void clearPengiriman(a) {
    saveKurir = a;
    saveService = a;
  }


  void addAddress(DataAddress p) {
    saveAddress = p;
  }

  void addGroup(GroupSelected p) {
    getGroup = p;
  }

  void addCartItem(DataProduct p) {
    var id = listCart.indexWhere((item) => item.id == p.id);
    String namanya = '';
    listCart.forEach((f) {
      if (f.title.toLowerCase() == p.name.toLowerCase()) {
        namanya = p.name;
      }
    });
    int price = calculatePriceItem(p);
    int diskonPersen;
    int diskonNominal;
    if (p.discAmount == '0') {
      diskonPersen = (int.parse(p.price) -
          (int.parse(p.price) * int.parse(p.discPercent) ~/ 100));
      if (id == -1 || namanya == '') {
        // p.qty = 1;
        // p.netprice = price;
        // p.total = price;
        // list.add(p);
        listCart.add(CartItem(
          id: p.id,
          img: p.image1,
          price: diskonPersen,
          priceAsli: p.price,
          qty: p.qty,
          discAmount: int.parse(p.discAmount),
          title: p.name,
          color: p.colorHex,
          colorName: p.colorName,
          sku: p.sku,
          category: p.category,
          subcategory: p.subcategory,
          tags: p.tags,
          brand: p.brand,
          dimension: p.dimensions,
          discPercent: int.parse(p.discPercent),
          rowPointer: p.rowPointer,
          weight: int.parse(p.weight),
          totalPrice: diskonPersen * p.qty,
          wholesalePrice: p.wholesalePrice,
        ));
        getTotalItems();
        getTotalPay();
      } else {
        debugPrint('berbedaaaaaaaaaaaa');
        listCart.forEach((f) {
          if (f.title.toLowerCase() == p.name.toLowerCase()) {
            f.price = price;
            f.qty += p.qty;
            f.totalPrice = f.qty * price;
          }
        });
      }
      debugPrint(diskonPersen.toString());
    } else if (p.discPercent == '0') {
      diskonNominal = int.parse(p.price) - int.parse(p.discAmount);
      if (id == -1 || namanya == '') {
        // p.qty = 1;
        // p.netprice = price;
        // p.total = price;
        // list.add(p);
        listCart.add(CartItem(
            id: p.id,
            img: p.image1,
            price: diskonNominal,
            priceAsli: p.price,
            qty: p.qty,
            discAmount: int.parse(p.discAmount),
            title: p.name,
            color: p.colorHex,
            colorName: p.colorName,
            sku: p.sku,
            category: p.category,
            subcategory: p.subcategory,
            tags: p.tags,
            brand: p.brand,
            dimension: p.dimensions,
            discPercent: int.parse(p.discPercent),
            wholesalePrice: p.wholesalePrice,
            wholesaleType: p.wholesaleType,
            rowPointer: p.rowPointer,
            weight: int.parse(p.weight),
            totalPrice: diskonNominal * p.qty));
        getTotalItems();
        getTotalPay();
      } else {
        debugPrint('berbedaaaaaaaaaaaa');
        listCart.forEach((f) {
          if (f.title.toLowerCase() == p.name.toLowerCase()) {
            f.price = price;
            f.qty += p.qty;
            f.totalPrice = f.qty * price;
          }
        });
      }
      debugPrint(diskonNominal.toString());
    } else if (p.discAmount == '0' && p.discPercent == '0') {
      if (id == -1 || namanya == '') {
        // p.qty = 1;
        // p.netprice = price;
        // p.total = price;
        // list.add(p);
        listCart.add(CartItem(
          id: p.id,
          img: p.image1,
          price: int.parse(p.price),
          priceAsli: p.price,
          qty: p.qty,
          discAmount: int.parse(p.discAmount),
          title: p.name,
          color: p.colorHex,
          colorName: p.colorName,
          sku: p.sku,
          category: p.category,
          subcategory: p.subcategory,
          tags: p.tags,
          brand: p.brand,
          dimension: p.dimensions,
          discPercent: int.parse(p.discPercent),
          rowPointer: p.rowPointer,
          weight: int.parse(p.weight),
          wholesalePrice: p.wholesalePrice,
          wholesaleType: p.wholesaleType,
          totalPrice: int.parse(p.price) * p.qty,
        ));
        getTotalItems();
        getTotalPay();
      } else {
        debugPrint('berbedaaaaaaaaaaaa');
        listCart.forEach((f) {
          if (f.title.toLowerCase() == p.name.toLowerCase()) {
            f.price = price;
            f.qty += p.qty;
            f.totalPrice = f.qty * price;
          }
        });
      }
      debugPrint(p.price);
    }
    debugPrint(id.toString());

    getTotalItems();
    getTotalPay();
    debugPrint(listCart.toString());
  }

  void addCartQty(CartItem p) {
    var id = listCart
        .indexWhere((item) => item.sku.toLowerCase() == p.sku.toLowerCase());
    int price = calculatePriceQty(p);
    debugPrint('aidiii' + id.toString());
    listCart[id].qty += 1;
    listCart[id].totalPrice = listCart[id].qty * price;
    getTotalItems();
    getTotalPay();
  }

  void reduceCartQty(CartItem p) {
    var id = listCart
        .indexWhere((item) => item.sku.toLowerCase() == p.sku.toLowerCase());
    int price = calculatePriceQty(p);
    debugPrint('aidiiinya' + id.toString());
    if (listCart[id].qty == 1) {
      // list.remove(p);
      listCart.remove(p);
    } else {
      // list[id].qty -= 1;
      // list[id].netprice = price;
      // list[id].total = list[id].qty * price;
      listCart[id].qty -= 1;
      listCart[id].price = price;
      listCart[id].totalPrice = listCart[id].qty * price;
    }
    getTotalItems();
    getTotalPay();
  }

  // void reduceCart(DataItems p) {
  //   var id = listCart.indexWhere((item) => item.id == p.id);
  //   int price = calculatePrice(p);
  //   if (id > -1) {
  //     if (listCart[id].qty == 1) {
  //       // list.remove(p);
  //       listCart.remove(CartItem(
  //         id: p.id,
  //         discAmount: p.discAmount,
  //         img: p.gambar,
  //         price: p.price,
  //       ));
  //     } else {
  //       // list[id].qty -= 1;
  //       // list[id].netprice = price;
  //       // list[id].total = list[id].qty * price;
  //       listCart[id].qty -= 1;
  //       listCart[id].price = price;
  //       listCart[id].totalPrice = listCart[id].qty * price;
  //     }
  //   }
  //   getTotalItems();
  //   getTotalDisc();
  //   getTotalPay();
  //   debugPrint(list.toString());
  // }

  void removeCartQty(CartItem p) {
    // var id = listCart.indexWhere((item) => item.id == p.id);
    listCart.remove(p);
    getTotalItems();
    // getTotalDisc();
    getTotalPay();
    // debugPrint(list.toString());
  }

  // void remCart(DataItems p) {
  //   var id = listCart.indexWhere((item) => item.id == p.id);
  //   if (id > -1) {
  //     // list[id].qty = 0;
  //     list[id].netprice = 0;
  //     list[id].total = 0;
  //     listCart[id].qty = 0;
  //     // list.remove(p);
  //     listCart.remove(CartItem(
  //         id: p.id,
  //         discAmount: p.discAmount,
  //         img: p.gambar,
  //         price: p.price,
  //         size: p.ukuran,
  //         title: p.nama));
  //   }
  //   getTotalItems();
  //   getTotalDisc();
  //   getTotalPay();
  //   debugPrint(list.toString());
  // }

  // calculatePrice(DataItems p) {
  //   int pricing = p.price;
  //   int disc = 0;
  //   if (p.discPercent > 0) {
  //     disc = (p.price * p.discPercent) ~/ 100;
  //     pricing = p.price - disc;
  //   } else if (p.discAmount > 0) {
  //     pricing = p.price - p.discAmount;
  //   }
  //   return pricing;
  // }

  calculatePriceItem(DataProduct p) {
    int pricing;
    if (p.discAmount == '0') {
      pricing = (int.parse(p.price) -
          (int.parse(p.price) * int.parse(p.discPercent) ~/ 100));
    } else if (p.discPercent == '0') {
      pricing = int.parse(p.price) - int.parse(p.discAmount);
    } else if (p.discAmount == '0' && p.discPercent == '0') {
      pricing = int.parse(p.price);
    }

    return pricing;
  }

  calculatePriceQty(CartItem p) {
    int pricing = p.price;
    return pricing;
  }

  // void getTotalDisc() {
  //   totalDiscounts = 0;
  //   list.forEach((p) {
  //     if (p.discPercent > 0) {
  //       totalDiscounts += p.price * (p.discPercent ~/ 100);
  //     } else if (p.discAmount > 0) {
  //       totalDiscounts += p.discAmount;
  //     }
  //   });
  // }

  void getTotalPay() {
    gettotalQty();
    gettotalWeight();
    totalPay = 0;
    listCart.forEach((p) {
      totalPay += p.totalPrice;
    });
    subTotal = 0;
    listCart.forEach((p) {
      subTotal += p.price;
    });
    storage.storeObject('listcart', {'cart': listCart.map((e) => e.toJson()).toList()});
  }

  void gettotalQty() {
    totalQty = 0;
    listCart.forEach((f) {
      totalQty += f.qty;
    });
  }

  void gettotalWeight() {
    totalWeight = 0;
    listCart.forEach((f) {
      totalWeight += f.weight;
    });
  }

  void getTotalItems() {
    totalItems = listCart.length;
  }

  void clear() {
    // list = [];
    listCart = [];
    totalItems = 0;
    totalPay = 0;
    totalQty = 0;
    totalWeight = 0;
    getGroup = null;
    saveKurir = null;
    saveService = null;
    saveAddress = null;
  }

  void updateCart(data) {
    listCart = data;
    getTotalItems();
    getTotalPay();
  }
}
