import 'dart:async';

import 'package:artline/bloc/global/BlocProvider.dart';
import 'package:artline/model/dataAddress.dart';
import 'package:artline/model/dataCart.dart';
import 'package:artline/model/dataProduct.dart';
import 'package:artline/bloc/cart/Cart.dart';
import 'package:artline/model/dataService.dart';
import 'package:artline/model/group.dart';
import 'package:rxdart/rxdart.dart';

class CartBloc implements BlocBase {
  CartData cart = CartData();

  /// Sinks
  // Sink<DataItems> get addition => itemAdditionController.sink;
  // final itemAdditionController = StreamController<DataItems>();

  Sink<DataProduct> get addItem => itemAddItemController.sink;
  final itemAddItemController = StreamController<DataProduct>();

  Sink<DataAddress> get addAddress => itemAddAddressController.sink;
  final itemAddAddressController = StreamController<DataAddress>();


  Sink<DataService> get addService => itemAddServiceController.sink;
  final itemAddServiceController = StreamController<DataService>();

  Sink<CartItem> get tambahQty => itemtambahQtyController.sink;
  final itemtambahQtyController = StreamController<CartItem>();
  Sink<GroupSelected> get tambahGroup => itemtambahGroupController.sink;
  final itemtambahGroupController = StreamController<GroupSelected>();

  Sink<String> get addKurir => itemaddKurirController.sink;
  final itemaddKurirController = StreamController<String>();

  Sink<dynamic> get clearPengiriman => itemclearPengirimanController.sink;
  final itemclearPengirimanController = StreamController<dynamic>();

  Sink<CartItem> get removeCartItem => itemRemoveCartItemController.sink;
  final itemRemoveCartItemController = StreamController<CartItem>();

  Sink<CartItem> get reduceCartItem => itemReduceCartItemController.sink;
  final itemReduceCartItemController = StreamController<CartItem>();

  /// Streams
  Stream<CartData> get cartStream => _cart.stream;
  final _cart = BehaviorSubject<CartData>();

  CartBloc() {
    // itemAdditionController.stream.listen(itemAdd);
    itemAddItemController.stream.listen(addCartItem);
    itemtambahQtyController.stream.listen(addQty);
    itemtambahGroupController.stream.listen(addGroup);
    itemRemoveCartItemController.stream.listen(removeCartQty);
    itemReduceCartItemController.stream.listen(reduceCartQty);
    itemAddAddressController.stream.listen(addAddressItem);
    itemAddServiceController.stream.listen(addServiceItem);
    itemaddKurirController.stream.listen(addKurirItem);
    itemclearPengirimanController.stream.listen(clearOpsiPengiriman);
  }

  clearOpsiPengiriman(dynamic a) {
    cart.clearPengiriman(a);
    _cart.add(cart);
  }

  addKurirItem(String item) {
    cart.addKurir(item);
    _cart.add(cart);
  }

  addServiceItem(DataService item) {
    cart.addService(item);
    _cart.add(cart);
  }
  addAddressItem(DataAddress item) {
    cart.addAddress(item);
    _cart.add(cart);
  }

  addCartItem(DataProduct item) {
    cart.addCartItem(item);
    _cart.add(cart);
  }

  addQty(CartItem item) {
    cart.addCartQty(item);
    _cart.add(cart);
  }
  addGroup(GroupSelected item) {
    cart.addGroup(item);
    _cart.add(cart);
  }
  // itemReduce(DataItems item) {
  //   cart.reduceCart(item);
  //   _cart.add(cart);
  // }

  // itemRem(DataItems item) {
  //   cart.remCart(item);
  //   _cart.add(cart);
  // }

  removeCartQty(CartItem item) {
    cart.removeCartQty(item);
    _cart.add(cart);
  }

  reduceCartQty(CartItem item) {
    cart.reduceCartQty(item);
    _cart.add(cart);
  }

  void clearCart() {
    cart.clear();
    _cart.add(cart);
  }

  void updateCart(List<CartItem> data) {
    cart.updateCart(data);
    _cart.add(cart);
  }

  @override
  void dispose() {
    // itemAdditionController.close();
    itemtambahQtyController.close();
    itemtambahGroupController.close();
    itemAddAddressController.close();
    itemReduceCartItemController.close();
    // itemSubtractionController.close();
    itemRemoveCartItemController.close();
    // itemRemovingController.close();
    itemAddItemController.close();
    itemaddKurirController.close();
    itemclearPengirimanController.close();
    itemAddServiceController.close();
  }
}
