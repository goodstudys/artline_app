import 'package:artline/components/discTag.dart';
import 'package:artline/env.dart';
import 'package:artline/model/dataProduct.dart';
import 'package:artline/theme/style.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

/// Class Component a Item Discount Card
class DiscountItem extends StatelessWidget {
  final DataProduct item;
  final Function tap;
  final String userLevel;

  DiscountItem({this.item, this.tap, this.userLevel});
  
  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQD = MediaQuery.of(context);
    double amountFromPercent;
    if (item.discPercent != '0') {
      double awal = int.parse(item.price) * int.parse(item.discPercent) / 100;
      amountFromPercent = int.parse(item.price) - awal;
    }
    // double presentase = (int.parse(item.discAmount) / int.parse(item.price));
    // int total = (presentase * 100.0).toInt();
    // int menjadi = (-total + 100);
    final NumberFormat formatter = NumberFormat.currency(
        symbol: 'Rp. ',
        decimalDigits: 0,
        locale: Localizations.localeOf(context).toString());
    return Padding(
      padding: EdgeInsets.only(top: 10.0, left: 10.0, bottom: 10.0),
      child: InkWell(
        onTap: tap,
        child: Container(
          width: mediaQD.size.width / 2.3,
          padding: EdgeInsets.only(bottom: 10.0),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
              boxShadow: [
                BoxShadow(
                  color: Color(0xFF656565).withOpacity(0.15),
                  blurRadius: 2.0,
                  spreadRadius: 1.0,
//           offset: Offset(4.0, 10.0)
                )
              ]),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Container(
                        height: mediaQD.size.width / 2.2,
                        /*
                        height: item.wholesalePrice == null
                            ? 185
                            : userLevel == '4' ||
                                    userLevel == '5' &&
                                        item.wholesalePrice != null
                                ? 140.0
                                : 185,
                                */
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(7.0),
                            topRight: Radius.circular(7.0)
                          ),
                          image: DecorationImage(
                            image: AssetImage('assets/img/product-placeholder.jpg'),
                            fit: BoxFit.contain,
                            alignment: Alignment.topCenter
                          )
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(7.0),
                            topRight: Radius.circular(7.0)
                          ),
                          child: FadeInImage.assetNetwork(
                            placeholder: 'assets/img/product-placeholder.jpg',
                            image: host + 'upload/users/products/' + item.image1,
                            fit: BoxFit.contain,
                            alignment: Alignment.topCenter
                          )
                        )
                      ),
                      item.discAmount == '0' && item.discPercent == '0' ? Container() : DiscTag(data: item),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                    child: Text(
                      item.name,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 3,
                      style: TextStyle(
                          letterSpacing: 0.5,
                          color: Colors.black54,
                          fontFamily: "Sans",
                          fontWeight: FontWeight.w500,
                          fontSize: 13.0),
                    ),
                  ),
                ]
              ),
              Padding(padding: EdgeInsets.only(top: 1.0)),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  item.discAmount == '0' && item.discPercent == '0' ? Padding(
                    padding: EdgeInsets.only(left: 10.0, right: 10.0),
                    child: Text(
                      formatter.format(int.parse(item.price)),
                      style: TextStyle(
                        fontFamily: "Sans",
                        fontWeight: FontWeight.w600,
                        fontSize: 14.0
                      ),
                    ),
                  ) : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 10.0, right: 10.0),
                        child: Text(
                          formatter.format(int.parse(item.price)),
                          style: TextStyle(
                              fontSize: 11.5,
                              decoration: TextDecoration.lineThrough,
                              color: ColorStyle.primaryColor,
                              fontWeight: FontWeight.w500,
                              fontFamily: "Sans"),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 1.0)),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 10.0, right: 10.0),
                        child: Text(
                          item.discAmount != '0'
                              ? formatter.format(int.parse(item.price) -
                                  int.parse(item.discAmount))
                              : formatter.format(amountFromPercent),
                          style: TextStyle(
                              fontSize: 14.0,
                              color: Colors.black,
                              fontWeight: FontWeight.w600,
                              fontFamily: "Sans"),
                        ),
                      ),
                    ],
                  ),
                  userLevel == '4' || userLevel == '5' ? item.wholesalePrice == null ? Container() : Container(
                    padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 6.0),
                    margin: EdgeInsets.only(top: 3.0, left: 10.0, right: 10.0),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: ColorStyle.primaryColor
                            .withOpacity(0.1)),
                    child: Text(
                      'Grosir',
                      style: TextStyle(
                          fontSize: 12.0,
                          color: ColorStyle.primaryColor),
                    ),
                  ): Container(),
                ]
              )
            ],
          ),
        ),
      ),
    );
  }
}
