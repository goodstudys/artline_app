import 'package:artline/components/discTag.dart';
import 'package:artline/components/textFormatIDR.dart';
import 'package:artline/model/dataProduct.dart';
import 'package:flutter/material.dart';

class FavoriteItem extends StatelessWidget {
  //String image, Rating, Salary, title/* sale*/;
  final String image, salary, title, id;
  final tekan;
  final Function tap;
  final DataProduct item;

  final double rating;
  FavoriteItem(
      {this.image,
      this.rating,
      this.salary,
      this.id,
      this.tap,
      this.title,
      this.item,
      this.tekan /*this.sale*/});

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQD = MediaQuery.of(context);
    return InkWell(
      onTap: tap,
      child: Padding(
        padding: EdgeInsets.only(top: 10.0, left: 10.0, bottom: 10.0),
        child: Container(
          width: mediaQD.size.width / 2.3,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              boxShadow: [
                BoxShadow(
                  color: Color(0xFF656565).withOpacity(0.15),
                  blurRadius: 4.0,
                  spreadRadius: 1.0,
//           offset: Offset(4.0, 10.0)
                )
              ]),
          child: Wrap(
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Stack(
                    children: [
                      Container(
                        height: mediaQD.size.width / 2.3,
                        width: mediaQD.size.width / 2.3,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(7.0),
                            topRight: Radius.circular(7.0)
                          ),
                          image: DecorationImage(
                            image: AssetImage('assets/img/product-placeholder.jpg'),
                            fit: BoxFit.contain,
                            alignment: Alignment.topCenter
                          )
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(7.0),
                            topRight: Radius.circular(7.0)
                          ),
                          child: FadeInImage.assetNetwork(
                            placeholder: 'assets/img/product-placeholder.jpg',
                            image: image,
                            fit: BoxFit.contain,
                            alignment: Alignment.topCenter
                          )
                        )
                      ),
                      item.discAmount == '0' && item.discPercent == '0' ? Container() : DiscTag(data: item),
                    ],
                  ),
                  Padding(padding: EdgeInsets.only(top: 15.0)),
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                    child: Text(
                      title,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 3,
                      style: TextStyle(
                        letterSpacing: 0.5,
                        color: Colors.black54,
                        fontFamily: "Sans",
                        fontWeight: FontWeight.w500,
                        fontSize: 13.0
                      ),
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 1.0)),
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                    child: TextFormatIDR(
                      thisText: int.parse(salary),
                      thisStyle: TextStyle(
                          fontFamily: "Sans",
                          fontWeight: FontWeight.w500,
                          fontSize: 12.0),
                    )
                  ),
                  /*
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 15.0, right: 15.0, top: 5.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text(
                              rating.toString(),
                              style: TextStyle(
                                  fontFamily: "Sans",
                                  color: Colors.black26,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 12.0),
                            ),
                            Icon(
                              Icons.star,
                              color: Colors.yellow,
                              size: 14.0,
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  */
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
