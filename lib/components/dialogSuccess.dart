import 'package:flutter/material.dart';
import 'package:artline/theme/style.dart';

class DialogSuccess extends StatelessWidget {
  final String txtHead, txtSub;
  final bool type;
  DialogSuccess({this.txtHead, this.txtSub, @required this.type});
  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(top: 30.0, right: 60.0, left: 60.0),
          color: Colors.white,
          child: Image.asset(
            type == true ? "assets/img/centang.jpg" : "assets/img/silang.jpg",
            height: 110.0,
            //color: Colors.lightGreen,
          ),
        ),
        Center(
            child: Padding(
          padding: const EdgeInsets.only(top: 16.0),
          child: Text(
            txtHead + '!!!',
            style: TxtStyle.txtCustomHead,
          ),
        )),
        Center(
            child: Padding(
          padding: const EdgeInsets.only(top: 30.0, bottom: 40.0),
          child: Text(
            txtSub,
            style: TxtStyle.txtCustomSub,
          ),
        )),
      ],
    );
  }
}
