import 'package:artline/model/dataCity.dart';
import 'package:artline/model/dataProvince.dart';
import 'package:artline/theme/style.dart';
import 'package:flutter/material.dart';

class CardAddress extends StatelessWidget {
  CardAddress(
      {this.type,
      this.name,
      this.phone,
      this.cityName,
      this.address,
      this.city,
      this.tap,
      this.tap2,
      this.provinceName,
      this.kecamatan,
      this.province,
      this.kodePos});
  final String type, name, phone, address, city, kecamatan, province, kodePos;
  final Function tap, tap2;
  final DataCity cityName;
  final DataProvince provinceName;
  @override
  Widget build(BuildContext context) {
    return Container(
        margin:
            EdgeInsets.only(bottom: 8.0, left: 13.0, right: 13.0, top: 13.0),
        padding: EdgeInsets.all(13.0),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                  color: Colors.black12,
                  blurRadius: 5,
                  spreadRadius: 2,
                  offset: Offset(5, 5))
            ]),
        child: InkWell(
          onTap: tap,
          child: Material(
              color: Colors.transparent,
              child: InkWell(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          name,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        type == 'pilih'
                            ? Icon(
                                Icons.arrow_right,
                                size: 40,
                              )
                            : type == 'pilihAlamat'
                                ? Text('')
                                : InkWell(
                                    onTap: tap2,
                                    child: Text(
                                      'Edit',
                                      style: TextStyle(
                                          color: ColorStyle.primaryColor),
                                    ),
                                  )
                      ],
                    ),
                    SizedBox(
                      height: 16.0,
                    ),
                    Text(phone),
                    Text(address),
                    type == 'pilihAlamat' || type == 'myAddress'
                        ? Text(cityName.cityName + ' - ' + kecamatan)
                        : Text(city + ' - ' + kecamatan),
                    type == 'pilihAlamat' || type == 'myAddress'
                        ? Text(provinceName.province)
                        : Text(province),
                    Text(kodePos)
                  ],
                ),
              )),
        ));
  }
}
