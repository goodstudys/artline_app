import 'package:flutter/material.dart';

class InputTextForm extends StatelessWidget {
  // To get passed in arg
  InputTextForm({
    this.label,
    //this.icon = Icons.input,
    this.inputType = TextInputType.text,
    this.textController,
    this.initialValuea,
    this.obscure = false,
    //this.obscureButton,
    this.type = 'default',
    this.autovalidate = false,
    this.confirm = false,
    this.confirmValue,
    this.password = false,
    this.hint,
    this.line,
    //this.distinct = false,
    //this.distinctValue,
    //this.distinctLabel,
  });
  // need to create a variable to hold greeting
  final String label, hint;
  //final IconData icon;
  final int line;
  final TextInputType inputType;
  final TextEditingController textController;
  final String initialValuea;
  final bool obscure;
  final bool password;
  //final dynamic obscureButton;
  final String type;
  final bool autovalidate;
  //static final Pattern patternEmail =
  //  r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  static final Pattern patternMobile = r'^08[0-9]{9,}$';
  //final RegExp regexEmail = new RegExp(patternEmail);
  final RegExp regexMobile = new RegExp(patternMobile);
  final bool confirm;
  final String confirmValue;
  //final bool distinct;
  //final String distinctValue;
  //final String distinctLabel;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
        controller: textController,
        autovalidateMode: autovalidate ? AutovalidateMode.always : AutovalidateMode.disabled,
        // style: TextStyle(
        //   fontWeight: FontWeight.bold,
        //   fontSize: 18.0,
        //   color: Colors.black,
        // ),
        maxLines: password == false ? line : 1,
        autocorrect: true,
        obscureText: password,
        decoration: InputDecoration(
          labelText: label,
          hintStyle: TextStyle(color: Colors.black54),
          hintText: hint,
          // labelStyle: TextStyle(color: Colors.grey),
          // border: InputBorder.none
          //hintText: 'Type Text Here',
          // enabledBorder: UnderlineInputBorder(
          //   borderSide: BorderSide(color: Colors.red),
          // ),
          // focusedBorder: UnderlineInputBorder(
          //   borderSide: BorderSide(color: Colors.green),
          // ),
        ),
        keyboardType: inputType,
        validator: (input) {
          if (input.isEmpty && type != 'not required') {
            return 'Silahkan isi ' + label.toLowerCase() + ' anda';
          } else if (type == 'mobile' && !regexMobile.hasMatch(input)) {
            return 'Harus dimulai dengan 08 dan minimal 9 angka';
          } else {
            return null;
          }
        },
        onChanged: (String val) {});

    //contentPadding: EdgeInsets.only(bottom: 0)),
    // keyboardType: inputType,
    // validator: (input) {
    //   if (input.isEmpty) {
    //     return 'Please type your ' + label.toLowerCase();
    //   } else {
    //     return null;
    //   }
    // },
    // onChanged: (String val) {});
  }
}
