import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class TextFormatIDR extends StatelessWidget {
  final dynamic thisText;
  final TextStyle thisStyle;
  final TextOverflow overflow;
  TextFormatIDR({
    this.thisText,
    this.thisStyle,
    this.overflow,
  });

  @override
  Widget build(BuildContext context) {
    final NumberFormat formatter = NumberFormat.currency(
        symbol: 'Rp. ',
        decimalDigits: 0,
        locale: Localizations.localeOf(context).toString());
    return Text(
      formatter.format(thisText),
      overflow: overflow,
      style: thisStyle,
    );
  }
}
