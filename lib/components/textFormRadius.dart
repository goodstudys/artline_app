import 'package:flutter/material.dart';

/// textfromfield custom class
class TextFromField extends StatelessWidget {
  final bool password, autovalidate, obscure;
  final String email;
  final IconData icon;
  final TextInputType inputType;
  final TextEditingController controller;
  final Function tapHide;

  TextFromField(
      {this.email,
      this.icon,
      this.tapHide,
      this.inputType,
      this.password = false,
      this.obscure = false,
      this.controller,
      this.autovalidate});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30.0),
      child: Container(
        height: 60.0,
        alignment: AlignmentDirectional.center,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(14.0),
            color: Colors.white,
            boxShadow: [BoxShadow(blurRadius: 10.0, color: Colors.black12)]),
        padding:
            EdgeInsets.only(left: 20.0, right: 15.0, top: 0.0, bottom: 0.0),
        child: Theme(
          data: ThemeData(
            hintColor: Colors.transparent,
          ),
          child: TextFormField(
            validator: (input) {
              if (input.isEmpty) {
                return 'Silahkan isi ' + email.toLowerCase() + ' anda';
              } else {
                return null;
              }
            },
            controller: controller,
            autovalidateMode: AutovalidateMode.always,
            obscureText: obscure,
            decoration: InputDecoration(
                suffixIcon: password == true
                    ? InkWell(
                        onTap: tapHide,
                        child: obscure
                            ? Icon(
                                Icons.remove_red_eye,
                                color: Colors.grey,
                              )
                            : Icon(Icons.remove_red_eye))
                    : Text(''),
                border: InputBorder.none,
                labelText: email,
                icon: Icon(
                  icon,
                  color: Colors.black38,
                ),
                labelStyle: TextStyle(
                    fontSize: 15.0,
                    fontFamily: 'Sans',
                    letterSpacing: 0.3,
                    color: Colors.black38,
                    fontWeight: FontWeight.w600)),
            keyboardType: inputType,
          ),
        ),
      ),
    );
  }
}
