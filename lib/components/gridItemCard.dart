import 'package:artline/components/discTag.dart';
import 'package:artline/components/textFormatIDR.dart';
import 'package:artline/env.dart';
import 'package:artline/model/dataProduct.dart';
import 'package:artline/theme/style.dart';
import 'package:flutter/material.dart';

class GridItemCard extends StatelessWidget {
  final String userLevel;
  final Function tap;
  final DataProduct item;
  GridItemCard({this.userLevel, this.tap, this.item});
  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQD = MediaQuery.of(context);
    double amountFromPercent;
    if (item.discPercent != '0') {
      double awal = int.parse(item.price) * int.parse(item.discPercent) / 100;
      amountFromPercent = int.parse(item.price) - awal;
    }
    double tinggiGrid;
    tinggiGrid = mediaQD.size.width / 2.2;
    /*
    if (userLevel == '4' || userLevel == '5') {
      if (item.wholesalePrice == null &&
          item.discAmount == '0' &&
          item.discPercent == '0') {
        tinggiGrid = mediaQD.size.height / 3.6;
      } else if (item.wholesalePrice == null &&
          item.discAmount != '0' &&
          item.discPercent == '0') {
        tinggiGrid = mediaQD.size.height / 3.9;
      } else if (item.wholesalePrice == null&&
          item.discAmount == '0' &&
          item.discPercent != '0') {
        tinggiGrid = mediaQD.size.height / 3.9;
      } else if (item.wholesalePrice != null &&
          item.discAmount == '0' &&
          item.discPercent == '0') {
        tinggiGrid = mediaQD.size.height / 4.4;
      } else if (item.wholesalePrice != null &&
          item.discAmount != '0' &&
          item.discPercent == '0') {
        tinggiGrid = mediaQD.size.height / 4.7;
      } else if (item.wholesalePrice != null &&
          item.discAmount == '0' &&
          item.discPercent != '0') {
        tinggiGrid = mediaQD.size.height / 4.7;
      } else {
        tinggiGrid = mediaQD.size.height / 4.9;
      }
    } else {
      if (item.discAmount == '0' && item.discPercent == '0') {
        tinggiGrid = mediaQD.size.height / 3.6;
      } else if (item.discAmount != '0' && item.discPercent == '0') {
        tinggiGrid = mediaQD.size.height / 3.9;
      } else if (item.discAmount == '0' && item.discPercent != '0') {
        tinggiGrid = mediaQD.size.height / 3.9;
      } else {
        tinggiGrid = mediaQD.size.height / 3.9;
      }
    }
    */

    return InkWell(
      onTap: tap,
      child: Container(
        padding: EdgeInsets.only(bottom: 10.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          boxShadow: [
            BoxShadow(
              color: Color(0xFF656565).withOpacity(0.15),
              blurRadius: 4.0,
              spreadRadius: 1.0,
              //          offset: Offset(4.0, 10.0)
            )
          ]
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            /// Set Animation image to detailProduk layout
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Stack(
                  children: [
                    Hero(
                      tag: "hero-grid-${item.id.toString()}",
                      child: Container(
                        height: tinggiGrid,
                        width: double.maxFinite,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(7.0),
                            topRight: Radius.circular(7.0)
                          ),
                          image: DecorationImage(
                            image: AssetImage('assets/img/product-placeholder.jpg'),
                            fit: BoxFit.contain,
                            alignment: Alignment.topCenter
                          )
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(7.0),
                            topRight: Radius.circular(7.0)
                          ),
                          child: FadeInImage.assetNetwork(
                            placeholder: 'assets/img/product-placeholder.jpg',
                            image: host + 'upload/users/products/' + item.image1,
                            fit: BoxFit.contain,
                            alignment: Alignment.topCenter
                          )
                        )
                      )
                    ),
                    item.discAmount == '0' && item.discPercent == '0' ? Container() : DiscTag(data: item),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 5.0, left: 10.0, right: 10.0),
                  child: Text(
                    item.name,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 3,
                    style: TextStyle(
                        letterSpacing: 0.5,
                        color: Colors.black54,
                        fontFamily: "Sans",
                        fontWeight: FontWeight.w500,
                        fontSize: 13.0),
                  ),
                ),
              ],
            ),
            /*
            Hero(
              tag: "hero-grid-${item.id.toString()}",
              child: Material(
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).push(PageRouteBuilder(
                        opaque: false,
                        pageBuilder: (BuildContext context, _, __) {
                          return new Material(
                            color: Colors.black54,
                            child: Container(
                              padding: EdgeInsets.all(30.0),
                              child: InkWell(
                                child: Hero(
                                    tag: "hero-grid-${item.id.toString()}",
                                    child: Image.network(
                                      host +
                                          'upload/users/products/' +
                                          item.image1,
                                      width: 300.0,
                                      height: 300.0,
                                      alignment: Alignment.center,
                                      fit: BoxFit.contain,
                                    )),
                                onTap: () {
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                          );
                        },
                        transitionDuration: Duration(milliseconds: 500)));
                  },
                  child: Container(
                    height: tinggiGrid,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(7.0),
                            topRight: Radius.circular(7.0)),
                        image: DecorationImage(
                            image: NetworkImage(host +
                                'upload/users/products/' +
                                item.image1),
                            fit: BoxFit.contain,
                            alignment: Alignment.topCenter)),
                  ),
                ),
              ),
            ),
            */
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                item.discAmount == '0' && item.discPercent == '0' ? Padding(
                  padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                  child: TextFormatIDR(
                    thisText: int.parse(item.price),
                    thisStyle: TextStyle(
                      fontFamily: "Sans",
                      fontWeight: FontWeight.w600,
                      fontSize: 15.0
                    ),
                  )
                ) : Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 10.0, right: 10.0),
                        child: TextFormatIDR(
                          thisText: int.parse(item.price),
                          thisStyle: TextStyle(
                              fontSize: 11.5,
                              decoration: TextDecoration.lineThrough,
                              color: ColorStyle.primaryColor,
                              fontWeight: FontWeight.w500,
                              fontFamily: "Sans"),
                        )),
                    Padding(padding: EdgeInsets.only(top: 1.0)),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 10.0, right: 10.0),
                        child: TextFormatIDR(
                          thisText: item.discAmount != '0'
                              ? int.parse(item.price) -
                                      int.parse(item.discAmount)
                              : amountFromPercent,
                          thisStyle: TextStyle(
                              fontSize: 15.0,
                              color: Colors.black,
                              fontWeight: FontWeight.w600,
                              fontFamily: "Sans"),
                        )),
                  ],
                ),
                userLevel == '4' || userLevel == '5' ? item.wholesalePrice == null ? Container() : Container(
                  padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 6.0),
                  margin: EdgeInsets.only(top: 3.0, left: 10.0, right: 10.0),
                  // margin: EdgeInsets.all(8.0),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      color:
                          ColorStyle.primaryColor.withOpacity(0.1)),
                  child: Text(
                    'Grosir',
                    style: TextStyle(
                        fontSize: 12.0,
                        color: ColorStyle.primaryColor
                            .withOpacity(0.5)),
                  ),
                ) : Container(),
              ]
            ),
          ],
        ),
      ),
    );
  }
}
