import 'package:artline/theme/style.dart';
import 'package:flutter/material.dart';
import 'package:artline/components/textFormatIDR.dart';

class CardInventory extends StatelessWidget {
  CardInventory(
      {this.image,
      this.name,
      this.price,
      this.netPrice,
      this.qty,
      this.flag,
      this.totalPrice,
      this.discPercent,
      @required this.type,
      //this.asli,
      this.discAmount});
  final String image, name, type, flag, discAmount, discPercent, price;
  final int qty, totalPrice, netPrice;
  //int asli;

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    int totalQTYPRICE;
    totalQTYPRICE = int.parse(price) * qty;
    int hasilKalkulasi;
    if (discPercent == '0') {
      hasilKalkulasi = int.parse(price) - int.parse(discAmount);
    } else if (discAmount == '0') {
      hasilKalkulasi = (int.parse(price) -
          (int.parse(price) * int.parse(discPercent) ~/ 100));
    } else {
      hasilKalkulasi = int.parse(price);
    }
    return Container(
        margin: EdgeInsets.only(bottom: 8.0, left: 13.0, right: 13.0),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                  color: Colors.black12,
                  blurRadius: 5,
                  spreadRadius: 2,
                  offset: Offset(5, 5))
            ]),
        child: Material(
            color: Colors.transparent,
            child: InkWell(
                //onTap: action,
                child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              width: 60.0,
                              height: 60.0,
                              decoration: BoxDecoration(
                                  color: Color(0xFFe0e0e0),
                                  borderRadius: BorderRadius.circular(10.0)),
                              child: Image.network(
                                image,
                                width: 80.0,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 15.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    width: mediaQueryData.size.width / 3,
                                    child: Text(
                                      name,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          color: Colors.black87,
                                          fontSize: 15.0,
                                          fontWeight: FontWeight.w700,
                                          letterSpacing: 0.7),
                                      textAlign: TextAlign.start,
                                    ),
                                  ),
                                  Container(
                                    //color: Colors.red,
                                    width: mediaQueryData.size.width / 3,
                                    child: type == 'inventory'
                                        ? Text(
                                            'Jumlah Stok : ' + qty.toString(),
                                            style: TextStyle(
                                                color: Colors.grey,
                                                letterSpacing: 0.5),
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            textAlign: TextAlign.start,
                                          )
                                        : Text(
                                            'Jumlah : ' + qty.toString(),
                                            style: TextStyle(
                                                color: Colors.grey,
                                                letterSpacing: 0.5),
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            textAlign: TextAlign.start,
                                          ),
                                  ),
                                  type != 'inventory'
                                      ? Container(
                                          child: Row(
                                          children: <Widget>[
                                            Text(
                                              'Price : ',
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  letterSpacing: 0.5),
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              textAlign: TextAlign.start,
                                            ),
                                            hasilKalkulasi.toString() == price
                                                ? TextFormatIDR(
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    thisStyle: TextStyle(
                                                        color: Colors.grey,
                                                        letterSpacing: 0.5),
                                                    thisText: int.parse(price),
                                                  )
                                                : TextFormatIDR(
                                                  overflow: TextOverflow
                                                      .ellipsis,
                                                  thisStyle: TextStyle(
                                                      fontSize: 12,
                                                      color: Colors.grey,
                                                      fontWeight:
                                                          FontWeight
                                                              .w500),
                                                  thisText: hasilKalkulasi,
                                                )
                                          ],
                                        ))
                                      : Container(
                                          child: Text(''),
                                        )
                                ],
                              ),
                            ),
                          ],
                        ),
                        Flexible(
                          child: Container(
                            margin: EdgeInsets.all(4.0),
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15.0)),
                              // boxShadow: [
                              //   BoxShadow(
                              //       color: Colors.black54,
                              //       blurRadius: 15.0,
                              //       offset: Offset(0.0, 0.75))
                              // ],
                              gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: hasilKalkulasi.toString() == price
                                    ? <Color>[
                                        ColorStyle.primaryColor,
                                        Colors.black,
                                        //ColorStyle.secondaryColor
                                        // Color(0xffc9741a),
                                        // Color(0xffa09716),
                                      ]
                                    : <Color>[
                                        Color(0xffc9741a),
                                        Color(0xffa09716),
                                      ],
                              ),
                            ),
                            child: Padding(
                                padding: const EdgeInsets.only(
                                    top: 13.0,
                                    bottom: 13.0,
                                    left: 4.0,
                                    right: 4.0),
                                child: type == 'listbarang'
                                    ? hasilKalkulasi.toString() == price
                                        ? TextFormatIDR(
                                            overflow: TextOverflow.ellipsis,
                                            thisStyle: TextStyle(
                                                fontSize: 12,
                                                color: Colors.white,
                                                fontWeight: FontWeight.w500),
                                            thisText: totalQTYPRICE,
                                          )
                                        : Column(
                                            children: <Widget>[
                                              // TextFormatIDR(
                                              //   overflow: TextOverflow.ellipsis,
                                              //   thisStyle: TextStyle(
                                              //       fontSize: 10,
                                              //       color: Colors.white,
                                              //       decoration: TextDecoration
                                              //           .lineThrough,
                                              //       fontWeight:
                                              //           FontWeight.w400),
                                              //   thisText: price,
                                              // ),
                                             totalPrice == null? TextFormatIDR(
                                                overflow: TextOverflow.ellipsis,
                                                thisStyle: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.white,
                                                    fontWeight:
                                                        FontWeight.w500),
                                                thisText: hasilKalkulasi * qty,
                                              ):TextFormatIDR(
                                                overflow: TextOverflow.ellipsis,
                                                thisStyle: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.white,
                                                    fontWeight:
                                                        FontWeight.w500),
                                                thisText: totalPrice,
                                              )
                                            ],
                                          )
                                    : hasilKalkulasi.toString() == price
                                        ? TextFormatIDR(
                                            overflow: TextOverflow.ellipsis,
                                            thisStyle: TextStyle(
                                                fontSize: 12,
                                                color: Colors.white,
                                                fontWeight: FontWeight.w500),
                                            thisText: int.parse(price),
                                          )
                                        : Column(
                                            children: <Widget>[
                                              TextFormatIDR(
                                                overflow: TextOverflow.ellipsis,
                                                thisStyle: TextStyle(
                                                    fontSize: 10,
                                                    color: Colors.white,
                                                    decoration: TextDecoration
                                                        .lineThrough,
                                                    fontWeight:
                                                        FontWeight.w400),
                                                thisText: int.parse(price),
                                              ),
                                              TextFormatIDR(
                                                overflow: TextOverflow.ellipsis,
                                                thisStyle: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.white,
                                                    fontWeight:
                                                        FontWeight.w500),
                                                thisText: hasilKalkulasi,
                                              )
                                            ],
                                          )),
                          ),
                        ),
                      ],
                    )))));
  }
}
