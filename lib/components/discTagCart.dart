import 'package:artline/model/dataCart.dart';
import 'package:flutter/material.dart';

class DiscTagCart extends StatelessWidget {
  final CartItem data;

  DiscTagCart({
    @required this.data,
  });

  @override
  Widget build(BuildContext context) {
    int percentAmount;
    if (data.discAmount != 0) {
      double presentase = (data.discAmount / data.price);
      percentAmount = (presentase * 100.0).toInt();
    }
    return Container(
      height: 25.5,
      width: 55.0,
      decoration: BoxDecoration(
        color: Color(0xFFD7124A),
        borderRadius: BorderRadius.only(
          bottomRight: Radius.circular(20.0),
          topLeft: Radius.circular(5.0)
        )
      ),
      child: Center(
        child: Text(
          data.discAmount != 0 ? percentAmount.toString() + " %" : data.discPercent.toString() + " %",
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w600
          ),
        )
      ),
    );
  }
}