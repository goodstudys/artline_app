import 'package:artline/components/textFormatIDR.dart';
import 'package:artline/env.dart';
import 'package:artline/theme/style.dart';
import 'package:flutter/material.dart';

class CardOrder extends StatelessWidget {
  CardOrder(
      {this.orderNumber,
      this.tap,
      this.konfirm,
      this.name,
      this.qty,
      this.resi,
      this.discAmount,
      this.discPercent,
      this.tapCancel,
      this.jumlah,
      this.type = 'default',
      this.tapdetail,
      this.price,
      this.image});
  final String orderNumber,
      name,
      qty,
      price,
      image,
      konfirm,
      discAmount,
      discPercent,
      resi;
  final String type;
  final int jumlah;
  final Function tap, tapdetail, tapCancel;
  @override
  Widget build(BuildContext context) {
    int amopuntCalculate;
    if (discAmount != '0') {
      amopuntCalculate = int.parse(price) - int.parse(discAmount);
    } else if (discPercent != '0') {
      amopuntCalculate = (int.parse(price) -
          (int.parse(price) * int.parse(discPercent) ~/ 100));
    }
    return Container(
        margin:
            EdgeInsets.only(bottom: 8.0, left: 13.0, right: 13.0, top: 13.0),
        padding: EdgeInsets.all(13.0),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                  color: Colors.black12,
                  blurRadius: 5,
                  spreadRadius: 2,
                  offset: Offset(5, 5))
            ]),
        child: InkWell(
          onTap: tapdetail,
          child: Material(
              color: Colors.transparent,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 7.0, bottom: 20),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(orderNumber),
                        type == 'NEW'
                            ? konfirm == 'null'
                                ? Text(
                                    'Belum Bayar',
                                    style: TextStyle(
                                        color: ColorStyle.primaryColor),
                                  )
                                : Text('Konfirmasi Terkirim',
                                    overflow: TextOverflow.ellipsis,
                                    // maxLines: 2,
                                    style: TextStyle(
                                        color: ColorStyle.primaryColor))
                            : type == 'proses'
                                ? resi == null
                                    ? Text('Proses',
                                        style: TextStyle(
                                            color: ColorStyle.primaryColor))
                                    : Text('Sedang dikirim',
                                        style: TextStyle(
                                            color: ColorStyle.primaryColor))
                                : type == 'selesai'
                                    ? Text('Selesai',
                                        style: TextStyle(
                                            color: ColorStyle.primaryColor))
                                    : Text('cancel',
                                        style: TextStyle(
                                            color: ColorStyle.primaryColor))
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Image.network(
                          host + 'upload/users/products/' + image,
                          width: 100.0,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                name,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 3,
                              ),
                              SizedBox(
                                height: 8.0,
                              ),
                              Text(qty + 'x'),
                              SizedBox(
                                height: 8.0,
                              ),
                              TextFormatIDR(
                                thisText: discAmount != '0'
                                    ? amopuntCalculate
                                    : discPercent != '0'
                                        ? amopuntCalculate
                                        : int.parse(price),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  jumlah >= 1
                      ? Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10.0),
                          child: Divider(
                            height: 0.5,
                            color: Colors.black12,
                          ),
                        )
                      : Container(),
                  jumlah >= 1
                      ? Padding(
                          padding:
                              const EdgeInsets.only(top: 13.0, bottom: 13.0),
                          child: Text(
                            'Tampilkan ' + jumlah.toString() + ' Produk Lagi',
                            style: TxtStyle.txtCustomSub
                                .copyWith(color: Colors.black54, fontSize: 12),
                          ),
                        )
                      : Container(),
                  jumlah >= 1
                      ? Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10.0),
                          child: Divider(
                            height: 0.5,
                            color: Colors.black12,
                          ),
                        )
                      : Container(),
                  type == 'NEW'
                      ? Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              child: FlatButton(
                                  padding: EdgeInsets.all(8.0),
                                  color: Colors.white,
                                  onPressed: tapCancel,
                                  child: Text(
                                    'Membatalkan',
                                    style: TextStyle(color: Colors.red),
                                  )),
                            ),
                            Container(
                              child: FlatButton(
                                  padding: EdgeInsets.all(8.0),
                                  color: ColorStyle.primaryColor,
                                  onPressed: tap,
                                  child: Text(
                                    'Konfirmasi Pembayaran',
                                    style: TextStyle(color: Colors.white),
                                  )),
                            )
                          ],
                        )
                      // : type == 'proses'
                      //     ? Row(
                      //         crossAxisAlignment: CrossAxisAlignment.end,
                      //         mainAxisAlignment: MainAxisAlignment.end,
                      //         children: <Widget>[
                      //           // Container(
                      //           //   width: MediaQuery.of(context).size.width / 3,
                      //           //   child: Text(
                      //           //       'Bayar Sebelum 28-04-2020 dengan transfer bank bca'),
                      //           // ),
                      //           Container(
                      //             child: FlatButton(
                      //                 padding: EdgeInsets.all(8.0),
                      //                 color: ColorStyle.primaryColor,
                      //                 onPressed: tap,
                      //                 child: Text(
                      //                   'Cek No. Resi',
                      //                   style: TextStyle(color: Colors.white),
                      //                 )),
                      //           )
                      //         ],
                      //       )
                      : Container()
                ],
              )),
        ));
  }
}
