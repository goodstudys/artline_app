import 'package:flutter/material.dart';

class ColorStyle {
  static final primaryColor = Color(0xff39569b);
  static final primarybutton = Color(0xFF4b0082);
  static final firstColor = Color(0xFF220051);
  static final secondaryColor = Color(0xFF690095);
  static final alternativeColor = Color(0xFF3b006f);
  static final background = Color(0xFF111121);
  static final cardColorLight = Colors.white;
  static final cardColorDark = Colors.black;
  static final fontColorLight = Colors.black;
  static final fontColorDark = Colors.white;
  static final fontSecondaryColorLight = Colors.black26;
  static final fontSecondaryColorDark = Colors.white;
  static final iconColorLight = Colors.black;
  static final iconColorDark = Colors.white;
  static final fontColorDarkTitle = Color(0xFF32353E);
  static final redDarkBackground = Color(0xFF30191A);
  static final grayBackground = Color(0xFF808080);
  static final whiteBackground = Color(0xFFF4F5F7);
  static final blackBackground = Color(0xFF12151C);
  static final modalBackground = Colors.black87;
  static final bottomBarDark = Color(0xFF202833);
  static final linkColor = Color(0xFFFF0033);
  static final shimmerBase = Colors.grey[500].withOpacity(0.5);
  static final shimmerHighlight = Colors.grey[100].withOpacity(0.5);
  static final successColor = Color(0xFF00be13);
  static final darkSlateGray = Color(0xFF5D5D5D);
}

class TxtStyle {
  static final headerStyle = TextStyle(
    color: Colors.white,
    fontSize: 18.0,
    fontWeight: FontWeight.w500,
    letterSpacing: 1.3,
  );
  static final txtCustomHead = TextStyle(
    color: Colors.black54,
    fontSize: 23.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Gotik",
  );
  static final txtCustomSub = TextStyle(
    color: Colors.black38,
    fontSize: 15.0,
    fontWeight: FontWeight.w500,
    fontFamily: "Gotik",
  );
  static final headerAltStyle = TextStyle(
    fontSize: 28.0,
    fontWeight: FontWeight.w800,
    color: Colors.black87,
    letterSpacing: 1,
    height: 0.9,
  );

  static final headerBigStyle = TextStyle(
    fontSize: 43.0,
    fontWeight: FontWeight.w900,
    color: Colors.black87,
    letterSpacing: 1,
    height: 0.9,
  );

  static final descriptionStyle = TextStyle(
    color: Colors.black,
    fontSize: 13.0,
    fontWeight: FontWeight.w400,
    letterSpacing: 1,
  );

  static final buttonText = TextStyle(
      color: Colors.white,
      letterSpacing: 0.2,
      fontSize: 14.0,
      fontWeight: FontWeight.w800);

  static final buttonModalText = TextStyle(
      color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.w600);

  static final cardHeaderStyle = TextStyle(
    color: Colors.white,
    fontSize: 26.0,
    fontWeight: FontWeight.w800,
    letterSpacing: 0.9,
  );

  static final linkText = TextStyle(
      color: ColorStyle.linkColor,
      letterSpacing: 0.2,
      fontSize: 13.0,
      fontWeight: FontWeight.w400);

  static final cartCardStyle = TextStyle(
    color: Colors.white,
    fontSize: 15.0,
    fontWeight: FontWeight.w500,
    letterSpacing: 1,
  );
  static final cartPriceStyle = TextStyle(
    color: Colors.black54,
    fontSize: 13.0,
    fontWeight: FontWeight.w800,
    letterSpacing: 1,
  );
  static final subHeaderCustomStyle = TextStyle(
      color: Colors.black54,
      fontWeight: FontWeight.w700,
      fontFamily: "Gotik",
      fontSize: 16.0);
}
