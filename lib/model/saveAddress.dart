class SaveAddress {
  int id;
  String userId;
  String label;
  String type;
  String name;
  String address;
  String province;
  String city;
  String kecamatan;
  String kelurahan;
  String phone;
  String kodePos;
  String dateCreated;
  String rowPointer;

  SaveAddress({
    this.id,
    this.userId,
    this.label,
    this.type,
    this.name,
    this.address,
    this.province,
    this.city,
    this.kecamatan,
    this.kelurahan,
    this.phone,
    this.kodePos,
    this.dateCreated,
    this.rowPointer,
  });
}
