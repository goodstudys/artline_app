class DataProduct {
  int id;
  String name;
  String description;
  String colorHex;
  String colorName;
  String sku;
  String category;
  String subcategory;
  String tags;
  String wholesalePrice;
  String wholesaleType;
  String brand;
  String weight;
  String dimensions;
  String price;
  String discAmount;
  String discPercent;
  String image1;
  String image2;
  String image3;
  String image4;
  String flag;
  String dateCreated;
  String rowPointer;
  int qty;
  // int isActive;
  // Null isFeatured;
  // int isObsolete;

  DataProduct(
      {this.id,
      this.name,
      this.description,
      this.colorHex,
      this.colorName,
      this.sku,
      this.category,
      this.subcategory,
      this.tags,
      this.brand,
      this.weight,
      this.dimensions,
      this.price,
      this.wholesalePrice,
      this.wholesaleType,
      this.discAmount,
      this.discPercent,
      this.image1,
      this.image2,
      this.image3,
      this.image4,
      this.flag,
      this.dateCreated,
      this.rowPointer,
      this.qty
      // this.isActive,
      // this.isFeatured,
      // this.isObsolete
      });

  DataProduct.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    colorHex = json['colorHex'];
    colorName = json['colorName'];
    sku = json['sku'];
    category = json['category'];
    subcategory = json['subcategory'];
    tags = json['tags'];
    brand = json['brand'];
    weight = json['weight'];
    dimensions = json['dimensions'];
    price = json['price'];
    wholesalePrice = json['wholesale_price'];
    wholesaleType = json['wholesale_type'];
    discAmount = json['discAmount'];
    discPercent = json['discPercent'];
    image1 = json['image1'];
    image2 = json['image2'];
    image3 = json['image3'];
    image4 = json['image4'];
    flag = json['flag'];
    dateCreated = json['dateCreated'];
    rowPointer = json['rowPointer'];
    // isActive = json['isActive'];
    // isFeatured = json['isFeatured'];
    // isObsolete = json['isObsolete'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    data['colorHex'] = this.colorHex;
    data['colorName'] = this.colorName;
    data['sku'] = this.sku;
    data['category'] = this.category;
    data['subcategory'] = this.subcategory;
    data['tags'] = this.tags;
    data['brand'] = this.brand;
    data['weight'] = this.weight;
    data['dimensions'] = this.dimensions;
    data['price'] = this.price;
    data['wholesale_price'] = this.wholesalePrice;
    data['wholesale_type'] = this.wholesaleType;
    data['discAmount'] = this.discAmount;
    data['discPercent'] = this.discPercent;
    data['image1'] = this.image1;
    data['image2'] = this.image2;
    data['image3'] = this.image3;
    data['image4'] = this.image4;
    data['flag'] = this.flag;
    data['dateCreated'] = this.dateCreated;
    data['rowPointer'] = this.rowPointer;
    // data['isActive'] = this.isActive;
    // data['isFeatured'] = this.isFeatured;
    // data['isObsolete'] = this.isObsolete;
    return data;
  }
}
