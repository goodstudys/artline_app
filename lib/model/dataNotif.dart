class DataNotif {
  String id;
  String title;
  String message;
  String image;
  String topic;
  String uuid;

  DataNotif({
    this.id,
    this.title,
    this.message,
    this.image,
    this.topic,
    this.uuid
  });

  DataNotif.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    message = json['message'];
    image = json['image'];
    topic = json['topic'];
    uuid = json['uuid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['message'] = this.message;
    data['image'] = this.image;
    data['topic'] = this.topic;
    data['uuid'] = this.uuid;
    return data;
  }
}
