class CartResponse {
  final List<CartItem> results;
  final String error;
  CartResponse(this.results, this.error);
  CartResponse.fromJson(List<dynamic> parsedJson)
    : results = parsedJson.map((i) => CartItem.fromJson(i)).toList(), error = "";
  CartResponse.withError(String errorValue)
    : results = List(), error = errorValue;
}

class CartItem {
  String img,
      title,
      desc,
      color,
      colorName,
      sku,
      category,
      subcategory,
      tags,
      brand,
      dimension,
      rowPointer,
      wholesalePrice,
      wholesaleType,
      priceAsli;
  int id, totalPrice, qty, price, discAmount, discPercent, weight;
  CartItem({
    this.id,
    this.img,
    this.title,
    this.qty,
    this.weight,
    this.price,
    this.priceAsli,
    this.discAmount,
    this.totalPrice,
    this.color,
    this.colorName,
    this.sku,
    this.category,
    this.subcategory,
    this.tags,
    this.brand,
    this.dimension,
    this.discPercent,
    this.wholesalePrice,
    this.wholesaleType,
    this.rowPointer
  });
  factory CartItem.fromJson(Map<String, dynamic> json){
    return CartItem(
      id: json['id'],
      img: json['img'],
      title: json['title'],
      qty: json['qty'],
      weight: json['size'],
      discAmount: json['discAmount'],
      priceAsli: json['price'],
      totalPrice: json['totalPrice'],
      color: json['color'],
      colorName: json['colorName'],
      sku: json['sku'],
      category: json['category'],
      subcategory: json['subcategory'],
      tags: json['tags'],
      brand: json['brand'],
      price: json['priceCalculate'],
      dimension: json['dimension'],
      discPercent: json['discPercent'],
      wholesalePrice: json['wholesalePrice'],
      wholesaleType: json['wholesaleType'],
      rowPointer: json['rowPointer'],
    );
  }

  Map<String, dynamic> toJson() => {
      'id': id,
      'img': img,
      'title': title,
      'qty': qty,
      'size': weight,
      'discAmount': discAmount,
      'price': priceAsli,
      'totalPrice': totalPrice,
      'color': color,
      'colorName': colorName,
      'sku': sku,
      'category': category,
      'subcategory': subcategory,
      'tags': tags,
      'brand': brand,
      'priceCalculate' : price,
      'dimension': dimension,
      'discPercent': discPercent,
      'wholesalePrice': wholesalePrice,
      'wholesaleType': wholesaleType,
      'rowPointer': rowPointer
    };
}
