class DataAddress {
  int id;
  String userId;
  String label;
  String type;
  String name;
  String address;
  String province;
  String city;
  String kecamatan;
  String kelurahan;
  String phone;
  String kodePos;
  String dateCreated;
  String rowPointer;

  DataAddress(
      {this.id,
      this.userId,
      this.label,
      this.type,
      this.name,
      this.address,
      this.province,
      this.city,
      this.kecamatan,
      this.kelurahan,
      this.phone,
      this.kodePos,
      this.dateCreated,
      this.rowPointer,});

  DataAddress.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['userId'];
    label = json['label'];
    type = json['type'];
    name = json['name'];
    address = json['address'];
    province = json['province'];
    city = json['city'];
    kecamatan = json['kecamatan'];
    kelurahan = json['kelurahan'];
    phone = json['phone'];
    kodePos = json['kodePos'];
    dateCreated = json['dateCreated'];
    rowPointer = json['rowPointer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['userId'] = this.userId;
    data['label'] = this.label;
    data['type'] = this.type;
    data['name'] = this.name;
    data['address'] = this.address;
    data['province'] = this.province;
    data['city'] = this.city;
    data['kecamatan'] = this.kecamatan;
    data['kelurahan'] = this.kelurahan;
    data['phone'] = this.phone;
    data['kodePos'] = this.kodePos;
    data['dateCreated'] = this.dateCreated;
    data['rowPointer'] = this.rowPointer;
    return data;
  }
}