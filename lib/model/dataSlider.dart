class DataSlider {
  int id;
  String category;
  String image;
  String dateCreated;
  String rowPointer;

  DataSlider({
    this.id,
    this.category,
    this.image,
    this.dateCreated,
    this.rowPointer,
  });

  DataSlider.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    category = json['category'];
    image = json['image'];
    dateCreated = json['dateCreated'];
    rowPointer = json['rowPointer'];
  }
}
