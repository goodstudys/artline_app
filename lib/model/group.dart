class GroupSelected {
  String productId;
  int groupId1;
  int detailId;
  int groupDetail;
  String name;
  String price;
  String discAmount;
  String discPercent;
  String pointer;

  GroupSelected(
      {this.productId,
      this.groupId1,
      this.detailId,
      this.groupDetail,
      this.name,
      this.price,
      this.discAmount,
      this.discPercent,
      this.pointer});

  GroupSelected.fromJson(Map<String, dynamic> json) {
    productId = json['productId'];
    groupId1 = json['groupId1'];
    detailId = json['detailId'];
    groupDetail = json['groupDetail'];
    name = json['name'];
    price = json['price'];
    discAmount = json['discAmount'];
    discPercent = json['discPercent'];
    pointer = json['Pointer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['productId'] = this.productId;
    data['groupId1'] = this.groupId1;
    data['detailId'] = this.detailId;
    data['groupDetail'] = this.groupDetail;
    data['name'] = this.name;
    data['price'] = this.price;
    data['discAmount'] = this.discAmount;
    data['discPercent'] = this.discPercent;
    data['Pointer'] = this.pointer;
    return data;
  }
}