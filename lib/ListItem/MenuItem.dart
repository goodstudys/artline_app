class MenuItem{
final String image, title, salary, rating, sale;

MenuItem({
  this.image,
  this.title,
  this.salary,
  this.rating,
  this.sale
});
}

List<MenuItem> itemDiscount =[
  MenuItem(
    image: "assets/imgCamera/cameraItem1.jpg",
    title: "Artline Supreme Marker 1.0mm Bullet Green",
    salary: "Rp. 16.000",
    rating: "4.8",
    sale: "923 Sale",
  ),
  MenuItem(
    image: "assets/imgCamera/cameraItem2.jpg",
    title: "Artline Supreme Marker 1.0mm Bullet Grey",
    salary: "Rp. 19.000",
    rating: "4.2",
    sale: "892 Sale",
  ),
  MenuItem(
    image: "assets/imgCamera/cameraItem3.jpg",
    title: "Artline Supreme Whiteboard Marker 1.5mm Red",
    salary: "Rp. 25.000",
    rating: "4.7",
    sale: "1422 Sale",
  ),
  MenuItem(
    image: "assets/imgCamera/cameraItem4.jpg",
    title: "Artline Supreme Whiteboard Marker 1.5mm Green",
    salary: "Rp. 15.000",
    rating: "4.4",
    sale: "531 Sale",
  ),
  MenuItem(
    image: "assets/imgCamera/CameraDigital.jpg",
    title: "Artline Supreme Marker 1.0mm Bullet Pink",
    salary: "Rp. 9.000",
    rating: "4.5",
    sale: "130 Sale",
  ),
];

List<MenuItem> itemPopularData = [
  MenuItem(
    image: "assets/imgCamera/cameraItem4.jpg",
    title: "Artline Supreme Whiteboard Marker 1.5mm Green",
    salary: "Rp. 16.000",
    rating: "4.8",
    sale: "923 Sale",
  ),
  MenuItem(
    image: "assets/imgCamera/cameraItem3.jpg",
    title: "Artline Supreme Whiteboard Marker 1.5mm Red",
    salary: "Rp. 19.000",
    rating: "4.2",
    sale: "892 Sale",
  ),
  MenuItem(
    image: "assets/imgCamera/cameraItem2.jpg",
    title: "Artline Supreme Marker 1.0mm Bullet Grey",
    salary: "Rp. 25.000",
    rating: "4.7",
    sale: "1422 Sale",
  ),
  MenuItem(
    image: "assets/imgCamera/cameraItem1.jpg",
    title: "Artline Supreme Marker 1.0mm Bullet Green",
    salary: "Rp. 15.000",
    rating: "4.4",
    sale: "531 Sale",
  ),
  MenuItem(
    image: "assets/imgCamera/CameraDigital.jpg",
    title: "Artline Supreme Marker 1.0mm Bullet Pink",
    salary: "Rp. 9.000",
    rating: "4.5",
    sale: "130 Sale",
  ),
];

List<MenuItem> newItems = [
  MenuItem(
    image: "assets/imgCamera/CameraDigital.jpg",
    title: "Artline Supreme Marker 1.0mm Bullet Pink",
    salary: "Rp. 16.000",
    rating: "4.8",
    sale: "923 Sale",
  ),
  MenuItem(
    image: "assets/imgCamera/CompactCamera.jpg",
    title: "Artline Supreme Whiteboard Marker 1.5mm Blue",
    salary: "Rp. 19.000",
    rating: "4.2",
    sale: "892 Sale",
  ),
  MenuItem(
    image: "assets/imgCamera/ActionCamera.jpg",
    title: "Artline Supreme Marker 1.0mm Bullet Light Blue",
    salary: "Rp. 25.000",
    rating: "4.7",
    sale: "1422 Sale",
  ),
  MenuItem(
    image: "assets/imgCamera/cameraItem4.jpg",
    title: "Artline Supreme Whiteboard Marker 1.5mm Green",
    salary: "Rp. 15.000",
    rating: "4.4",
    sale: "531 Sale",
  ),
  MenuItem(
    image: "assets/imgCamera/CameraDigital.jpg",
    title: "Artline Supreme Marker 1.0mm Bullet Pink",
    salary: "Rp. 9.000",
    rating: "4.5",
    sale: "130 Sale",
  ),
];