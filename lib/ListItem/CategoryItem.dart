class CategoryItem{
  final String image, title, salary, rating, sale;

  CategoryItem({
    this.image,
    this.title,
    this.salary,
    this.rating,
    this.sale
  });
}

List<CategoryItem> itemDiscount =[
  CategoryItem(
    image: "assets/imgCategoryPermanents/pic5.jpg",
    title: "Artline 900XF Metallic Paint Marker Gold",
    salary: "Rp. 7.000",
    rating: "4.8",
    sale: "923 Sale",
  ),
  CategoryItem(
    image: "assets/imgCategoryPermanents/pic6.jpg",
    title: "Artline 900XF Metallic Paint Marker Silver",
    salary: "Rp. 16.000",
    rating: "4.2",
    sale: "892 Sale",
  ),
  CategoryItem(
    image: "assets/imgCategoryPermanents/pic7.jpg",
    title: "Artline 400XF Paint Marker 2.3mm Yellow",
    salary: "Rp. 4.500",
    rating: "4.7",
    sale: "1422 Sale",
  ),
  CategoryItem(
    image: "assets/imgCategoryPermanents/pic8.jpg",
    title: "Artline 400XF Paint Marker 2.3mm Black",
    salary: "Rp. 15.000",
    rating: "4.4",
    sale: "531 Sale",
  ),
  CategoryItem(
    image: "assets/imgCategoryPermanents/pic9.jpg",
    title: "Artline 400XF Paint Marker 2.3mm Red",
    salary: "Rp. 9.000",
    rating: "4.5",
    sale: "130 Sale",
  ),
  CategoryItem(
    image: "assets/imgCategoryPermanents/pic21.jpg",
    title: "Artline 577 Whiteboard Marker 2mm Orange",
    salary: "Rp. 4.000",
    rating: "4.8",
    sale: "110 Sale",
  ),
];

List<CategoryItem> itemPopularData = [
  CategoryItem(
    image: "assets/imgCategoryPermanents/pic21.jpg",
    title: "Artline 577 Whiteboard Marker 2mm Orange",
    salary: "Rp. 7.000",
    rating: "4.8",
    sale: "923 Sale",
  ),
  CategoryItem(
    image: "assets/imgCategoryPermanents/pic6.jpg",
    title: "Artline 900XF Metallic Paint Marker Silver",
    salary: "Rp. 16.000",
    rating: "4.2",
    sale: "892 Sale",
  ),
  CategoryItem(
    image: "assets/imgCategoryPermanents/pic8.jpg",
    title: "Artline 400XF Paint Marker 2.3mm Black",
    salary: "Rp. 4.500",
    rating: "4.7",
    sale: "1422 Sale",
  ),
  CategoryItem(
    image: "assets/imgCategoryPermanents/pic7.jpg",
    title: "Artline 400XF Paint Marker 2.3mm Yellow",
    salary: "Rp. 15.000",
    rating: "4.4",
    sale: "531 Sale",
  ),
  CategoryItem(
    image: "assets/imgCategoryPermanents/pic5.jpg",
    title: "Artline 900XF Metallic Paint Marker Gold",
    salary: "Rp. 9.000",
    rating: "4.5",
    sale: "130 Sale",
  ),
  CategoryItem(
    image: "assets/imgCategoryPermanents/pic6.jpg",
    title: "Artline 900XF Metallic Paint Marker Silver",
    salary: "Rp. 16.000",
    rating: "4.2",
    sale: "892 Sale",
  ),
];

List<CategoryItem> newItems = [
  CategoryItem(
    image: "assets/imgCategoryPermanents/pic9.jpg",
    title: "Artline 400XF Paint Marker 2.3mm Red",
    salary: "Rp. 7.000",
    rating: "4.8",
    sale: "923 Sale",
  ),
  CategoryItem(
    image: "assets/imgCategoryPermanents/pic8.jpg",
    title: "Artline 400XF Paint Marker 2.3mm Black",
    salary: "Rp. 16.000",
    rating: "4.2",
    sale: "892 Sale",
  ),
  CategoryItem(
    image: "assets/imgCategoryPermanents/pic6.jpg",
    title: "Artline 900XF Metallic Paint Marker Silver",
    salary: "Rp. 4.500",
    rating: "4.7",
    sale: "1422 Sale",
  ),
  CategoryItem(
    image: "assets/imgCategoryPermanents/pic5.jpg",
    title: "Artline 900XF Metallic Paint Marker Gold",
    salary: "Rp. 15.000",
    rating: "4.4",
    sale: "531 Sale",
  ),
  CategoryItem(
    image: "assets/imgCategoryPermanents/pic7.jpg",
    title: "Artline 400XF Paint Marker 2.3mm Yellow",
    salary: "Rp. 7.000",
    rating: "4.8",
    sale: "923 Sale",
  ),
    CategoryItem(
    image: "assets/imgCategoryPermanents/pic6.jpg",
    title: "Artline 900XF Metallic Paint Marker Silver",
    salary: "Rp. 4.500",
    rating: "4.7",
    sale: "1422 Sale",
  ),
];