class Brand {
  final String id;
  final String name;
  final String img;
  final String desc;
  final Items item;

  const Brand({
    this.img,
    this.id,
    this.name,
    this.desc,
    this.item,
  });
}

class Items {
  final String itemName;
  final String itemPrice;
  final String itemRatting;
  /*final String itemSale;*/
  final String itemId;
  final String itemImg;

  const Items(
      {this.itemName,
        this.itemPrice,
        this.itemRatting,
        /*this.Itemsale,*/
        this.itemId,
        this.itemImg});
}

List<Brand> brandData = [
  const Brand(
      name: "Whiteboard",
      id: "1",
      img: "assets/imgItem/whiteboard.jpg",
      desc:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      item: Items(
          itemImg: "assets/imgItem/pen1.jpg",
          itemId: "1",
          itemName: "Artline 200 Fineliner Pen 0.4mm Blue",
          itemPrice: "Rp. 12.000",
          itemRatting: "4.5",
          /*Itemsale: "200 Sale"*/)),
  const Brand(
      name: "Signature",
      id: "2",
      img: "assets/imgItem/signature.jpg",
      desc:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      item: Items(
          itemImg: "assets/imgItem/pen2.jpg",
          itemId: "1",
          itemName: "Artline 200 Fineliner Pen 0.4mm Red",
          itemPrice: "Rp. 15.000",
          itemRatting: "4.5",
          /*Itemsale: "250 Sale"*/)),
  const Brand(
      name: "Fineliners",
      id: "3",
      img: "assets/imgItem/fineliners.jpg",
      desc:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      item: Items(
          itemImg: "assets/imgItem/pic1.jpg",
          itemId: "1",
          itemName: "Artline Supreme Marker 1.0mm Bullet Light Blue",
          itemPrice: "\$ 250",
          itemRatting: "4.5",
          /*Itemsale: "200 Sale"*/)),
  const Brand(
      name: "Highlighters",
      id: "4",
      img: "assets/imgItem/highlighter.png",
      desc:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      item: Items(
          itemImg: "assets/imgItem/pic2.jpg",
          itemId: "1",
          itemName: "Artline Supreme Marker 1.0mm Bullet Pink",
          itemPrice: "Rp. 12.000",
          itemRatting: "4.5",
          /*Itemsale: "200 Sale"*/)),
  const Brand(
      name: "General Pens",
      id: "5",
      img: "assets/imgItem/general-pens.jpg",
      desc:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      item: Items(
          itemImg: "assets/imgItem/pic12.jpg",
          itemId: "1",
          itemName: "Artline Supreme Whiteboard Marker 1.5mm Red",
          itemPrice: "Rp. 31.000",
          itemRatting: "4.5",
          /*Itemsale: "200 Sale"*/)),
  const Brand(
      name: "Permanents",
      id: "6",
      img: "assets/imgItem/permanents-markers.jpg",
      desc:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      item: Items(
          itemImg: "assets/imgItem/pic13.jpg",
          itemId: "1",
          itemName: "Artline Supreme Whiteboard Marker 1.5mm Black",
          itemPrice: "Rp. 12.000",
          itemRatting: "4.5",
          /*itemSale: "200 Sale"*/)),
];
