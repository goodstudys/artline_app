class ListItem{
  final String image, title;

  ListItem({
    this.image,
    this.title,
  });
}

List<ListItem> itemList =[
  ListItem(
    image: "assets/icon/warehouse.png",
    title: "Factory",
  ),
  ListItem(
    image: "assets/icon/office-material.png",
    title: "Office",
  ),
  ListItem(
    image: "assets/icon/pencil-case.png",
    title: "School",
  ),
  ListItem(
    image: "assets/icon/craft.png",
    title: "Arts & Crafts",
  ),
];
