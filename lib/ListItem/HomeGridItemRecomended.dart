class GridItem {
  final String id;
  final String img;
  final String title;
  final String price;
  final String rattingValue;
   final String itemSale; 
  final String description;

  GridItem(
      {this.id,
        this.img,
        this.title,
        this.price,
        this.rattingValue,
        this.itemSale, 
        this.description});
}

List<GridItem> gridItemArray = [
  GridItem(
    id: "1",
    img: "assets/imgItem/pic19.jpg",
    title: "Artline 527 Eco Whiteboard Marker 2mm Green",
    price: "Rp. 7.000",
    itemSale: "", 
    rattingValue: "4.8",
    description:
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
  ),
  GridItem(
    id: "2",
    img: "assets/imgItem/pic20.jpg",
    title: "Artline 527 Eco Whiteboard Marker 2mm Red",
    price: "Rp. 16.000",
     itemSale: "", 
    rattingValue: "4.2",
    description:
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
  ),
  GridItem(
    id: "3",
    img: "assets/imgItem/pic1.jpg",
    title: "Artline Supreme Marker 1.0mm Bullet Light Blue",
    price: "Rp. 4.500",
    itemSale: "", 
    rattingValue: "4.7",
    description:
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
  ),
  GridItem(
    id: "4",
    img: "assets/imgItem/pic2.jpg",
    title: "Artline Supreme Marker 1.0mm Bullet Light Pink",
    price: "Rp. 15.000",
    itemSale: "", 
    rattingValue: "4.4",
    description:
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
  ),
  GridItem(
    id: "5",
    img: "assets/imgItem/pic12.jpg",
    title: "Artline Supreme Whiteboard Marker 1.5mm Red",
    price: "Rp. 9.000",
    itemSale: "", 
    rattingValue: "4.5",
    description:
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
  ),
  GridItem(
    id: "6",
    img: "assets/imgItem/pic13.jpg",
    title: "Artline Supreme Whiteboard Marker 1.5mm Black",
    price: "Rp. 4.000",
    itemSale: "", 
    rattingValue: "4.8",
    description:
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
  ),
  GridItem(
    id: "7",
    img: "assets/imgItem/pen1.jpg",
    title: "Artline 200 Fineliner Pen 0.4mm Purple",
    price: "Rp. 15.000",
    itemSale: "", 
    rattingValue: "4.1",
    description:
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
  ),
  GridItem(
    id: "8",
    img: "assets/imgItem/pen2.jpg",
    title: "Artline 200 Fineliner Pen 0.4mm Red",
    price: "Rp. 12.000",
    itemSale: "", 
    rattingValue: "4.1",
    description:
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
  ),
];
