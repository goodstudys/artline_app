class ItemWeek{
  final String image, title;

  ItemWeek({
    this.image,
    this.title,
  });
}

List<ItemWeek> listWeek =[
  ItemWeek(
    image: "assets/imgPromo/Discount1.png",
    title: "pertama",
  ),
  ItemWeek(
    image: "assets/imgPromo/Discount3.png",
    title: "kedua",
  ),
  ItemWeek(
    image: "assets/imgPromo/Discount2.png",
    title: "ketiga",
  ),
  ItemWeek(
    image: "assets/imgPromo/Discount4.png",
    title: "keempat",
  ),
  ItemWeek(
    image: "assets/imgPromo/Discount5.png",
    title: "kelima",
  ),
  ItemWeek(
    image: "assets/imgPromo/Discount6.png",
    title: "keenam",
  ),
];
