class Product {
  int id;
  String img;
  String title;
  int price;
  int qty;
  int discountprice;
  double rattingValue;
  int itemSale;
  String description;
  String kategori;
  // int cartlist_id;
  List warna;

  Product(
      {this.id,
      this.img,
      this.title,
      this.price,
      this.qty,
      this.discountprice,
      this.warna,
      this.kategori,
      this.rattingValue,
      this.itemSale,
      this.description});
}

List<Product> loadProducts = [
  Product(
    id: 1,
    qty: 1,
    img: "assets/imgItem/pic19.jpg",
    title: "Artline 527 Eco Whiteboard Marker 2mm Green",
    price: 18000,
    discountprice: 14000,
    itemSale: 932,
    warna: ["biru", "hitam", "hijau"],
    rattingValue: 4.8,
    kategori: "Factory",
    description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
  ),
  Product(
    id: 2,
    qty: 1,
    img: "assets/imgItem/pic20.jpg",
    title: "Artline 527 Eco Whiteboard Marker 2mm Red",
    price: 15000,
    discountprice: 0,
    itemSale: 892,
    warna: ["biru", "", "hijau"],
    rattingValue: 4.2,
    kategori: "Factory",
    description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
  ),
  Product(
    id: 3,
    qty: 1,
    img: "assets/imgItem/pic1.jpg",
    title: "Artline Supreme Marker 1.0mm Bullet Light Blue",
    price: 14000,
    discountprice: 10000,
    itemSale: 1422,
    warna: ["biru", "hitam", ""],
    rattingValue: 4.7,
    kategori: "Office",
    description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
  ),
  Product(
    id: 4,
    qty: 1,
    img: "assets/imgItem/pic2.jpg",
    title: "Artline Supreme Marker 1.0mm Bullet Light Pink",
    price: 16000,
    discountprice: 0,
    itemSale: 523,
    warna: ["biru", "hitam", ""],
    rattingValue: 4.4,
    kategori: "Office",
    description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
  ),
  Product(
    id: 5,
    qty: 1,
    img: "assets/imgItem/pic12.jpg",
    title: "Artline Supreme Whiteboard Marker 1.5mm Red",
    price: 20000,
    discountprice: 0,
    itemSale: 130,
    warna: ["biru", "", "hijau"],
    rattingValue: 4.5,
    kategori: "School",
    description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
  ),
  Product(
    id: 6,
    qty: 1,
    img: "assets/imgItem/pic13.jpg",
    title: "Artline Supreme Whiteboard Marker 1.5mm Black",
    price: 13000,
    discountprice: 0,
    itemSale: 110,
    warna: ["", "hitam", "hijau"],
    rattingValue: 4.8,
    kategori: "School",
    description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
  ),
  Product(
    id: 7,
    qty: 1,
    img: "assets/imgItem/pen1.jpg",
    title: "Artline 200 Fineliner Pen 0.4mm Purple",
    price: 15000,
    discountprice: 7000,
    itemSale: 654,
    warna: ["biru", "hitam", ""],
    rattingValue: 4.1,
    kategori: "Art",
    description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
  ),
  Product(
    id: 8,
    qty: 1,
    img: "assets/imgItem/pen2.jpg",
    title: "Artline 200 Fineliner Pen 0.4mm Red",
    price: 10000,
    discountprice: 0,
    itemSale: 1542,
    warna: ["", "hitam", "hijau"],
    rattingValue: 4.1,
    kategori: "Art",
    description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
  ),
  Product(
    id: 9,
    qty: 1,    
    img: "assets/imgCamera/CameraDigital.jpg",
    title: "Artline Supreme Marker 1.0mm Bullet Pink",
    price: 16000,
    discountprice: 13000,
    itemSale: 1542,
    warna: ["", "hitam", ""],
    rattingValue: 4.5,
    kategori: "Factory",
    description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
  ),
  Product(
    id: 10,
    qty: 1,
    img: "assets/imgCamera/CompactCamera.jpg",
    title: "Artline Supreme Whiteboard Marker 1.5mm Blue",
    price: 19000,
    discountprice: 13000,
    itemSale: 456,
    warna: ["biru", "hitam", ""],
    rattingValue: 4.1,
    kategori: "Office",
    description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
  ),
  Product(
    id: 11,
    qty: 1,
    img: "assets/imgCamera/ActionCamera.jpg",
    title: "Artline Supreme Marker 1.0mm Bullet Light Blue",
    price: 25000,
    discountprice: 12000,
    itemSale: 765,
    warna: ["biru", "hitam", "hijau"],
    rattingValue: 4.1,
    kategori: "School",
    description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
  ),
  Product(
    id: 12,
    qty: 1,
    img: "assets/imgCamera/cameraItem4.jpg",
    title: "Artline Supreme Whiteboard Marker 1.5mm Green",
    price: 15000,
    discountprice: 0,
    itemSale: 325,
    warna: ["", "", "hijau"],
    rattingValue: 4.1,
    kategori: "Art",
    description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
  ),
  Product(
    id: 13,
    qty: 1,
    img: "assets/imgCamera/CameraDigital.jpg",
    title: "Artline Supreme Marker 1.0mm Bullet Pink",
    price: 9000,
    discountprice: 0,
    itemSale: 896,
    warna: ["biru", "", "hijau"],
    rattingValue: 4.9,
    kategori: "Factory",
    description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen.....",
  ),
];
