import 'dart:async';
import 'package:artline/storage/storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:artline/bloc/global/BlocProvider.dart';
import 'package:artline/bloc/global/GlobalBloc.dart';
import 'package:artline/pages/BottomNavigationBar.dart';
import 'package:artline/pages/onboard/intro_review.dart';

/// Run first apps open
void main() {
  runApp(MyApp());
}

/// Set orienttation
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    ///Set color status bar
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
      statusBarColor: Colors.transparent, //or set color with: Color(0xFF0000FF)
    ));
    return BlocProvider<GlobalBloc>(
      bloc: GlobalBloc(),
      child: MaterialApp(
        title: "Artline Shop",
        theme: ThemeData(
          brightness: Brightness.light,
          backgroundColor: Colors.white,
          primaryColorLight: Colors.white,
          primaryColorBrightness: Brightness.light,
          primaryColor: Colors.white
        ),
        debugShowCheckedModeBanner: false,
        home: SplashScreen(),
        routes: {
          '/home': (context) => BottomNavigation(),
        },
      )
    );
  }
}

/// Component UI
class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

/// Component UI
class _SplashScreenState extends State<SplashScreen> {
  Timer timer;
  String _name;

  void navigationPage() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => IntroPage()),
    );
  }

  void dashboard() {
    Navigator.pushReplacement(context,
      MaterialPageRoute(builder: (BuildContext context) => BottomNavigation())
    );
  }

  Future<Null> getSharedPrefs() async {
    DataStore dataStore = new DataStore();
    _name = await dataStore.getDataString("name");

    if (_name == 'Tidak ditemukan') {
      timer = Timer.periodic(
          Duration(seconds: 3),
          (Timer t) => setState(() {
                navigationPage();
              }));
    } else {
      timer = Timer.periodic(
          Duration(seconds: 3),
          (Timer t) => setState(() {
                dashboard();
              }));
    }
  }

  @override
  void initState() {
    super.initState();
    getSharedPrefs();
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/img/man.png'), fit: BoxFit.cover)),
        child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [
                Color.fromRGBO(0, 0, 0, 0),
                Color.fromRGBO(0, 0, 0, 0)
              ],
                  begin: FractionalOffset.topCenter,
                  end: FractionalOffset.bottomCenter)),
          child: Center(
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 30.0),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
