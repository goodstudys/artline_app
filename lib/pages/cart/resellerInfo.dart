import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:artline/bloc/global/BlocProvider.dart';
import 'package:artline/bloc/global/GlobalBloc.dart';
import 'package:artline/env.dart';
import 'package:artline/pages/cart/Invoice.dart';
import 'package:artline/storage/storage.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class ResellerInfo extends StatefulWidget {
  @override
  _ResellerInfoState createState() => _ResellerInfoState();
}

class _ResellerInfoState extends State<ResellerInfo> {
  bool isLoading = false;
  String ongkir;
  int mustPay;
  int parse;
  int total, totalQty;
  bool dropship, isBook;
  String product,
      billName,
      billProvince,
      cityname,
      billAddress,
      billKecamatan,
      billKelurahan,
      billPhone,
      billKodePos,
      billEmail;
  String deliveryName, deliveryPhone, kodeBooking, reseller;
  DataStore storage = DataStore();

  startTime() async {
    return Timer(Duration(milliseconds: 1450), navigator);
  }

  /// Navigation to route after user succes payment
  void navigator() {
    // Navigator.of(context).pushAndRemoveUntil(
    //   MaterialPageRoute(builder: (BuildContext context) => InvoiceLayout()),
    //   ModalRoute.withName('/home'),
    // );
    // Navigator.pushAndRemoveUntil(
    //     context,
    //     MaterialPageRoute(builder: (context) => InvoiceLayout()),
    //     ModalRoute.withName("/cart"));
    Navigator.of(context).pushReplacement(
        PageRouteBuilder(pageBuilder: (_, __, ___) => new InvoiceLayout()));
  }

  submit() async {
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    if (dropship == true) {
      if (isBook == true) {
        var res = await http.post(url('api/createOrder'), headers: {
          'Authorization': $tokenType + ' ' + $accesToken
        }, body: {
          'product': product,
          'billName': billName,
          'billProvince': billProvince,
          'cityname': cityname,
          'billAddress': billAddress,
          'billKecamatan': billKecamatan,
          'billKelurahan': billKelurahan,
          'billPhone': billPhone,
          'billKodePos': billKodePos,
          'billEmail': billEmail,
          'ongkir': ongkir,
          'payment': tapvalue4.toString(),
          'totalqtys': totalQty.toString(),
          'gettotal': total.toString(),
          'totalorders': mustPay.toString(),
          'deliveryName': deliveryName,
          'deliveryPhone': deliveryPhone,
          'kodeBooking': kodeBooking,
          'reseller': reseller
        });
        var data = json.decode(res.body);
        setState(() {
          print(data);
        });
      } else {
        var res = await http.post(url('api/createOrder'), headers: {
          'Authorization': $tokenType + ' ' + $accesToken
        }, body: {
          'product': product,
          'billName': billName,
          'billProvince': billProvince,
          'cityname': cityname,
          'billAddress': billAddress,
          'billKecamatan': billKecamatan,
          'billKelurahan': billKelurahan,
          'billPhone': billPhone,
          'billKodePos': billKodePos,
          'billEmail': billEmail,
          'ongkir': ongkir,
          'payment': tapvalue4.toString(),
          'totalqtys': totalQty.toString(),
          'gettotal': total.toString(),
          'totalorders': mustPay.toString(),
          'deliveryName': deliveryName,
          'deliveryPhone': deliveryPhone,
          'reseller': reseller
        });
        var data = json.decode(res.body);
        setState(() {
          print(data);
        });
      }
    } else {
      var res = await http.post(url('api/createOrder'), headers: {
        'Authorization': $tokenType + ' ' + $accesToken
      }, body: {
        'product': product,
        'billName': billName,
        'billProvince': billProvince,
        'cityname': cityname,
        'billAddress': billAddress,
        'billKecamatan': billKecamatan,
        'billKelurahan': billKelurahan,
        'billPhone': billPhone,
        'billKodePos': billKodePos,
        'billEmail': billEmail,
        'ongkir': ongkir,
        'payment': tapvalue4.toString(),
        'totalqtys': totalQty.toString(),
        'gettotal': total.toString(),
        'totalorders': mustPay.toString(),
        'reseller': reseller
      });
      var data = json.decode(res.body);
      setState(() {
        print(data);
      });
    }
  }

  getUserData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      ongkir = prefs.getString('ongkir') ?? '';
      total = BlocProvider.of<GlobalBloc>(context).cartBloc.cart.totalPay;
      totalQty = BlocProvider.of<GlobalBloc>(context).cartBloc.cart.totalQty;
      billName = prefs.getString('billNama');
      billProvince = prefs.getString('billProvinsi');
      cityname = prefs.getString('billKota');
      billAddress = prefs.getString('billAlamat');
      billKecamatan = prefs.getString('billKecamatan');
      billKelurahan = prefs.getString('billKelurahan');
      billKodePos = prefs.getString('billKodePos');
      billPhone = prefs.getString('billTelp');
      billEmail = prefs.getString('billEmail');
      product = prefs.getString('billProduct');
      dropship = prefs.getBool('dropship');
      isBook = prefs.getBool('isBooking');
      reseller = prefs.getString('reseller');
      if (dropship == true) {
        deliveryName = prefs.getString('dropName');
        deliveryPhone = prefs.getString('dropPhone');
        kodeBooking = prefs.getString('kodeBooking');
      }
      print('iniiii ongkir' + ongkir);
    });
  }

  savePay() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('mustPay', mustPay.toString());
      prefs.setString('payOngkir', ongkir.toString());
      prefs.setString('payment', tapvalue4.toString());
    });
  }

  @override
  void initState() {
    getUserData().then((s) => setState(() {
          isLoading = true;
        }));
    super.initState();
  }

  int tapvalue4 = 0;

  Widget build(BuildContext context) {
    return Scaffold(
      /// Appbar
      appBar: AppBar(
        elevation: 0.0,
        title: Text(
          "Pre Order",
          style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 18.0,
              color: Colors.black54,
              fontFamily: "Gotik"),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Color(0xFF6991C7)),
      ),
      body: isLoading
          ? SingleChildScrollView(
              child: Container(
                color: Colors.white,
                child: Padding(
                  padding:
                      const EdgeInsets.only(top: 30.0, left: 20.0, right: 20.0),
                  child: Column(
                    children: <Widget>[
                      Text(
                        "Informasi Tentang Pre Order",
                        style: TextStyle(
                            letterSpacing: 0.1,
                            fontWeight: FontWeight.w600,
                            fontSize: 25.0,
                            color: Colors.black54,
                            fontFamily: "Gotik"),
                      ),
                      Padding(padding: EdgeInsets.only(top: 60.0)),

                      /// For RadioButton if selected or not selected
                      // InkWell(
                      //   onTap: () {
                      //     setState(() {
                      //       if (tapvalue == 0) {
                      //         tapvalue++;
                      //       } else {
                      //         tapvalue--;
                      //       }
                      //     });
                      //   },
                      //   child: Row(
                      //     children: <Widget>[
                      //       Radio(
                      //         value: 1,
                      //         groupValue: tapvalue,
                      //         onChanged: null,
                      //       ),
                      //       Text(
                      //         "Credit / Debit Card",
                      //         style: _customStyle,
                      //       ),
                      //       Padding(
                      //         padding: const EdgeInsets.only(left: 40.0),
                      //         child: Image.asset(
                      //           "assets/img/credit.png",
                      //           height: 25.0,
                      //         ),
                      //       )
                      //     ],
                      //   ),
                      // ),
                      // Padding(padding: EdgeInsets.only(top: 15.0)),
                      // Divider(
                      //   height: 1.0,
                      //   color: Colors.black26,
                      // ),
                      // Padding(padding: EdgeInsets.only(top: 15.0)),
                      // InkWell(
                      //   onTap: () {
                      //     setState(() {
                      //       if (tapvalue2 == 0) {
                      //         tapvalue2++;
                      //       } else {
                      //         tapvalue2--;
                      //       }
                      //     });
                      //   },
                      //   child: Row(
                      //     children: <Widget>[
                      //       Radio(
                      //         value: 1,
                      //         groupValue: tapvalue2,
                      //         onChanged: null,
                      //       ),
                      //       Text("Cash On Delivery", style: _customStyle),
                      //       Padding(
                      //         padding: const EdgeInsets.only(left: 50.0),
                      //         child: Image.asset(
                      //           "assets/img/handshake.png",
                      //           height: 25.0,
                      //         ),
                      //       )
                      //     ],
                      //   ),
                      // ),
                      // Padding(padding: EdgeInsets.only(top: 15.0)),
                      // Divider(
                      //   height: 1.0,
                      //   color: Colors.black26,
                      // ),
                      // Padding(padding: EdgeInsets.only(top: 15.0)),
                      // InkWell(
                      //   onTap: () {
                      //     setState(() {
                      //       if (tapvalue3 == 0) {
                      //         tapvalue3++;
                      //       } else {
                      //         tapvalue3--;
                      //       }
                      //     });
                      //   },
                      //   child: Row(
                      //     children: <Widget>[
                      //       Radio(
                      //         value: 1,
                      //         groupValue: tapvalue3,
                      //         onChanged: null,
                      //       ),
                      //       Text("Paypal", style: _customStyle),
                      //       Padding(
                      //         padding: const EdgeInsets.only(left: 130.0),
                      //         child: Image.asset(
                      //           "assets/img/paypal.png",
                      //           height: 25.0,
                      //         ),
                      //       )
                      //     ],
                      //   ),
                      // ),
                      // Padding(padding: EdgeInsets.only(top: 15.0)),
                      // Divider(
                      //   height: 1.0,
                      //   color: Colors.black26,
                      // ),
                      // Padding(padding: EdgeInsets.only(top: 15.0)),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 130.0,
                          decoration: BoxDecoration(
                              color: Colors.grey[300],
                              boxShadow: [
                                BoxShadow(color: Colors.black38, blurRadius: 5)
                              ]),
                          child: Padding(
                            padding: const EdgeInsets.only(
                                top: 15.0,
                                left: 25.0,
                                right: 25.0,
                                bottom: 15.0),
                            child: Text(
                              //  \n\njumlah barang dan transaksi akan menyesuaikan dengan stok yang ada
                              'Thank you for shopping at Artline.co.id your order will be processed within 2 days maximum.\nFor further info, please contact our customer service',
                              textAlign: TextAlign.justify,
                            ),
                          ),
                        ),
                      ),
                      // InkWell(
                      //   onTap: () {
                      //     setState(() {
                      //       parse = int.parse(ongkir);
                      //       mustPay = parse + total;
                      //       if (tapvalue4 == 0) {
                      //         tapvalue4++;
                      //       } else {
                      //         tapvalue4--;
                      //       }
                      //     });
                      //   },
                      //   child: Row(
                      //     children: <Widget>[
                      //       Radio(
                      //         value: 1,
                      //         groupValue: tapvalue4,
                      //         onChanged: null,
                      //       ),
                      //       Column(
                      //         mainAxisAlignment: MainAxisAlignment.start,
                      //         crossAxisAlignment: CrossAxisAlignment.start,
                      //         children: <Widget>[
                      //           Text("Bank Transfer", style: _customStyle),
                      //           Text(
                      //             "65236745",
                      //             textAlign: TextAlign.left,
                      //           ),
                      //         ],
                      //       ),
                      //       Padding(
                      //         padding: const EdgeInsets.only(left: 65.0),
                      //         child: Image.asset(
                      //           "assets/img/bca.png",
                      //           height: 25.0,
                      //         ),
                      //       )
                      //     ],
                      //   ),
                      // ),
                      Padding(padding: EdgeInsets.only(top: 110.0)),

                      /// Button pay
                      InkWell(
                        onTap: () {
                          setState(() {
                            parse = int.parse(ongkir);
                            mustPay = parse + total;
                            tapvalue4 = 1;
                          });
                          if (tapvalue4 == 1) {
                            submit();
                            savePay();
                            _showDialog(context);
                            startTime();
                          } else {
                            return;
                          }

                          // Navigator.of(context).pushReplacement(PageRouteBuilder(
                          //     pageBuilder: (_, __, ___) => InvoiceLayout()));
                        },
                        child: Container(
                          height: 55.0,
                          width: 300.0,
                          decoration: BoxDecoration(
                              color: Colors.indigoAccent,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(40.0))),
                          child: Center(
                            child: Text(
                              "Checkout",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 16.5,
                                  letterSpacing: 2.0),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      )
                    ],
                  ),
                ),
              ),
            )
          : Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
    );
  }
}

/// Custom Text Header for Dialog after user succes payment
var _txtCustomHead = TextStyle(
  color: Colors.black54,
  fontSize: 23.0,
  fontWeight: FontWeight.w600,
  fontFamily: "Gotik",
);

/// Custom Text Description for Dialog after user succes payment
var _txtCustomSub = TextStyle(
  color: Colors.black38,
  fontSize: 15.0,
  fontWeight: FontWeight.w500,
  fontFamily: "Gotik",
);
_showDialog(BuildContext context) {
  showDialog(
    context: context,
    builder: (context) => SimpleDialog(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(top: 30.0, right: 60.0, left: 60.0),
          color: Colors.white,
          child: Image.asset(
            "assets/img/checklist.png",
            height: 110.0,
            color: Colors.lightGreen,
          ),
        ),
        Center(
            child: Padding(
          padding: const EdgeInsets.only(top: 16.0),
          child: Text(
            "Yuppy!!",
            style: _txtCustomHead,
          ),
        )),
        Center(
            child: Padding(
          padding: const EdgeInsets.only(top: 30.0, bottom: 40.0),
          child: Text(
            "Your Payment Receive to Seller",
            style: _txtCustomSub,
          ),
        )),
      ],
    ),
  );
}

/// Card Popup if success payment
// _showDialog(BuildContext ctx) {
//   showDialog(
//     context: ctx,
//     barrierDismissible: true,
//     child: SimpleDialog(
//       children: <Widget>[
//         Container(
//           padding: EdgeInsets.only(top: 30.0, right: 60.0, left: 60.0),
//           color: Colors.white,
//           child: Image.asset(
//             "assets/img/checklist.png",
//             height: 110.0,
//             color: Colors.lightGreen,
//           ),
//         ),
//         Center(
//             child: Padding(
//           padding: const EdgeInsets.only(top: 16.0),
//           child: Text(
//             "Yuppy!!",
//             style: _txtCustomHead,
//           ),
//         )),
//         Center(
//             child: Padding(
//           padding: const EdgeInsets.only(top: 30.0, bottom: 40.0),
//           child: Text(
//             "Your Payment Receive to Seller",
//             style: _txtCustomSub,
//           ),
//         )),
//       ],
//     ),
//   );
// }
