import 'dart:convert';
import 'package:artline/bloc/cart/Cart.dart';
import 'package:artline/bloc/global/BlocProvider.dart';
import 'package:artline/bloc/global/GlobalBloc.dart';
import 'package:artline/components/discTagCart.dart';
import 'package:artline/components/textFormatIDR.dart';
import 'package:artline/env.dart';
import 'package:artline/model/dataCart.dart';
import 'package:artline/storage/storage.dart';
import 'package:artline/theme/style.dart';
import 'package:flutter/material.dart';
import 'package:artline/pages/cart/Delivery.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CartLayout extends StatefulWidget {
  @override
  _CartLayoutState createState() => _CartLayoutState();
}

class _CartLayoutState extends State<CartLayout> {
  CartData cart;
  List<CartItem> shopcart = [];
  DataStore storage = DataStore();
  String userLevel;
  _saveBillProduct() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      pref.setString('billProduct', jsonEncode(cart.listCart));
      print(jsonEncode(cart.listCart));
    });
  }

  void loadCart() async {
    var cartbloc = BlocProvider.of<GlobalBloc>(context).cartBloc;
    if (cartbloc.cart.listCart.length == 0) {
      var listcart = await storage.fetchObject('listcart');
      var shopcart = CartResponse.fromJson(listcart['cart']).results;
      cartbloc.updateCart(shopcart);
    }
  }

  void loadUser() async {
    var data = await storage.getDataString('userLevel');
    setState(() {
      userLevel = data;
    });
  }

  @override
  void initState() {
    loadUser();
    loadCart();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQD = MediaQuery.of(context);
    double _gridSize = mediaQD.size.height * 0.88;
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Color(0xFF6991C7)),
          centerTitle: true,
          backgroundColor: Colors.white,
          title: Text(
            "Shopping Cart",
            style: TextStyle(
                fontFamily: "Gotik",
                fontSize: 18.0,
                color: Colors.black54,
                fontWeight: FontWeight.w700),
          ),
          elevation: 1.0,
        ),

        ///
        ///
        /// Checking item value of cart
        ///
        ///
        body: Container(
          child: StreamBuilder(
              stream: BlocProvider.of<GlobalBloc>(context).cartBloc.cartStream,
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return Center(child: NoItemCart());
                } else {
                  cart = snapshot.data;
                  if (cart.listCart.length == 0) {
                    return Center(child: NoItemCart());
                  }
                }
                return Container(
                    height: double.maxFinite,
                    child: Stack(
                      children: <Widget>[
                        Positioned(
                          child: Container(
                            height: _gridSize * 0.92,
                            child: ListView.builder(
                              itemCount: cart.listCart.length,
                              itemBuilder: (context, index) {
                                var item = cart.listCart[index];
                                double amountFromPercent;
                                if (item.discPercent != 0) {
                                  double awal = item.price * item.discPercent / 100;
                                  amountFromPercent = item.price - awal;
                                }
                                ///
                                /// Widget for list view slide delete
                                ///
                                return Slidable(
                                  actionPane: SlidableDrawerActionPane(),
                                  actionExtentRatio: 0.25,
                                  // actions: <Widget>[
                                  //   new IconSlideAction(
                                  //     caption: 'Archive',
                                  //     color: Colors.blue,
                                  //     icon: Icons.archive,
                                  //     onTap: () {
                                  //       ///
                                  //       /// SnackBar show if cart Archive
                                  //       ///
                                  //       Scaffold.of(context)
                                  //           .showSnackBar(SnackBar(
                                  //         content: Text("Items Cart Archive"),
                                  //         duration: Duration(seconds: 1),
                                  //         backgroundColor: Colors.blue,
                                  //       ));
                                  //     },
                                  //   ),
                                  // ],
                                  secondaryActions: <Widget>[
                                    new IconSlideAction(
                                      key: Key(item.id.toString()),
                                      caption: 'Delete',
                                      color: Colors.red,
                                      icon: Icons.delete,
                                      onTap: () {
                                        BlocProvider.of<GlobalBloc>(context).cartBloc.removeCartItem.add(cart.listCart[index]);
                                        ///
                                        /// SnackBar show if cart delet
                                        ///
                                        Scaffold.of(context)
                                            .showSnackBar(SnackBar(
                                          content: Text("Items Cart Deleted"),
                                          duration: Duration(seconds: 1),
                                          backgroundColor: Colors.redAccent,
                                        ));
                                      },
                                    ),
                                  ],
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color:
                                              Colors.black12.withOpacity(0.1),
                                          blurRadius: 3.5,
                                          spreadRadius: 0.4,
                                        )
                                      ],
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Padding(
                                                padding: EdgeInsets.all(10.0),
                                                /// Image item
                                                child: Stack(
                                                  children: [
                                                    Container(
                                                      decoration: BoxDecoration(
                                                        image: DecorationImage(
                                                          image: AssetImage('assets/img/product-placeholder.jpg'),
                                                          fit: BoxFit.contain,
                                                          alignment: Alignment.topCenter
                                                        )
                                                      ),
                                                      child: ClipRRect(
                                                        borderRadius: BorderRadius.only(
                                                          topLeft: Radius.circular(7.0),
                                                          topRight: Radius.circular(7.0)
                                                        ),
                                                        child: FadeInImage.assetNetwork(
                                                          placeholder: 'assets/img/product-placeholder.jpg',
                                                          image: host + 'upload/users/products/' + item.img,
                                                          height: mediaQD.size.width / 4,
                                                          width: mediaQD.size.width / 4,
                                                          fit: BoxFit.cover,
                                                          alignment: Alignment.topCenter
                                                        )
                                                      )
                                                    ),
                                                    item.discAmount == 0 && item.discPercent == 0 ? Container() : DiscTagCart(data: item),
                                                  ],
                                                )
                                                ),
                                            Flexible(
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.only(
                                                        top: 10.0,
                                                        left: 10.0,
                                                        bottom: 20.0,
                                                        right: 5.0),
                                                child: Column(
                                                  /// Text Information Item
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment
                                                          .start,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.spaceBetween,
                                                  children: <Widget>[
                                                    Text(
                                                      cart.listCart[index].title,
                                                      maxLines: 3,
                                                      style: TextStyle(
                                                        fontFamily: "Sans",
                                                        color: Colors.black87,
                                                      ),
                                                      overflow: TextOverflow
                                                          .ellipsis,
                                                    ),
                                                    // Padding(
                                                    //     padding:
                                                    //         EdgeInsets.only(
                                                    //             top: 10.0)),
                                                    // Text(
                                                    //   '122' + "Sale",
                                                    //   style: TextStyle(
                                                    //     color: Colors.black54,
                                                    //     fontWeight:
                                                    //         FontWeight.w500,
                                                    //     fontSize: 12.0,
                                                    //   ),
                                                    // ),
                                                    Column(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      children: <Widget>[
                                                        item.discAmount == 0 && item.discPercent == 0 ? TextFormatIDR(
                                                          thisText: item.price,
                                                          thisStyle: TextStyle(
                                                            fontFamily: "Sans",
                                                            fontWeight: FontWeight.w600,
                                                            fontSize: 15.0
                                                          ),
                                                        ) : Column(
                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                          mainAxisAlignment: MainAxisAlignment.start,
                                                          children: <Widget>[
                                                            TextFormatIDR(
                                                              thisText: item.price,
                                                              thisStyle: TextStyle(
                                                                  fontSize: 11.5,
                                                                  decoration: TextDecoration.lineThrough,
                                                                  color: ColorStyle.primaryColor,
                                                                  fontWeight: FontWeight.w500,
                                                                  fontFamily: "Sans"),
                                                            ),
                                                            Padding(padding: EdgeInsets.only(top: 1.0)),
                                                            TextFormatIDR(
                                                              thisText: item.discAmount != 0
                                                                  ? item.price - item.discAmount
                                                                  : amountFromPercent,
                                                              thisStyle: TextStyle(
                                                                  fontSize: 15.0,
                                                                  color: Colors.black,
                                                                  fontWeight: FontWeight.w600,
                                                                  fontFamily: "Sans"),
                                                            ),
                                                          ],
                                                        ),
                                                        userLevel == '4' || userLevel == '5' ? item.wholesalePrice == null ? Container() : Container(
                                                          padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 6.0),
                                                          margin: EdgeInsets.only(top: 3.0),
                                                          // margin: EdgeInsets.all(8.0),
                                                          decoration: BoxDecoration(
                                                              borderRadius: BorderRadius.circular(10.0),
                                                              color:
                                                                  ColorStyle.primaryColor.withOpacity(0.1)),
                                                          child: Text(
                                                            'Grosir',
                                                            style: TextStyle(
                                                                fontSize: 12.0,
                                                                color: ColorStyle.primaryColor
                                                                    .withOpacity(0.5)),
                                                          ),
                                                        ) : Container(),
                                                      ]
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets
                                                                  .only(
                                                              top: 18.0,
                                                              left: 0.0),
                                                      child: Container(
                                                        width: 130.0,
                                                        decoration: BoxDecoration(
                                                            color: Colors
                                                                .white70,
                                                            border: Border.all(
                                                                color: Colors
                                                                    .black12
                                                                    .withOpacity(
                                                                        0.1))),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceAround,
                                                          children: <Widget>[
                                                            /// Decrease of value item
                                                            InkWell(
                                                              onTap: () {
                                                                BlocProvider.of<
                                                                            GlobalBloc>(
                                                                        context)
                                                                    .cartBloc
                                                                    .reduceCartItem
                                                                    .add(cart
                                                                            .listCart[
                                                                        index]);
                                                                Scaffold.of(
                                                                        context)
                                                                    .showSnackBar(
                                                                        SnackBar(
                                                                  content: Text("Item " +
                                                                      cart.listCart[index]
                                                                          .title +
                                                                      " berhasil dikurangi"),
                                                                  duration: Duration(
                                                                      seconds:
                                                                          1),
                                                                  backgroundColor:
                                                                      Colors
                                                                          .orange,
                                                                ));
                                                              },
                                                              child:
                                                                  Container(
                                                                height: 30.0,
                                                                width: 30.0,
                                                                decoration: BoxDecoration(
                                                                    border: Border(
                                                                        right:
                                                                            BorderSide(color: Colors.black12.withOpacity(0.1)))),
                                                                child: Center(
                                                                    child: Text(
                                                                        "-")),
                                                              ),
                                                            ),
                                                            Padding(
                                                                padding: const EdgeInsets
                                                                        .symmetric(
                                                                    horizontal:
                                                                        18.0),
                                                                child: Text(cart
                                                                    .listCart[
                                                                        index]
                                                                    .qty
                                                                    .toString())),

                                                            /// Increasing value of item
                                                            InkWell(
                                                              onTap: () {
                                                                BlocProvider.of<
                                                                            GlobalBloc>(
                                                                        context)
                                                                    .cartBloc
                                                                    .tambahQty
                                                                    .add(cart
                                                                            .listCart[
                                                                        index]);
                                                                Scaffold.of(
                                                                        context)
                                                                    .showSnackBar(
                                                                        SnackBar(
                                                                  content: Text("Item " +
                                                                      cart.listCart[index]
                                                                          .title +
                                                                      " berhasil ditambahkan"),
                                                                  duration: Duration(
                                                                      seconds:
                                                                          1),
                                                                  backgroundColor:
                                                                      Colors
                                                                          .blueAccent,
                                                                ));
                                                              },
                                                              child:
                                                                  Container(
                                                                height: 30.0,
                                                                width: 28.0,
                                                                decoration: BoxDecoration(
                                                                    border: Border(
                                                                        left:
                                                                            BorderSide(color: Colors.black12.withOpacity(0.1)))),
                                                                child: Center(
                                                                    child: Text(
                                                                        "+")),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        /*Padding(padding: EdgeInsets.only(top: 8.0)),
                                Divider(
                                  height: 2.0,
                                  color: Colors.black12,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 9.0, left: 10.0, right: 10.0),
                                  child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(left: 10.0),

                                      /// Total price of item buy
                                      child: Text(
                                        "Total : Rp. " + pay.toString(),
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 15.5,
                                            fontFamily: "Sans"),
                                      ),
                                    ),
                                    InkWell(
                                      onTap: () {
                                        Navigator.of(context).push(PageRouteBuilder(
                                            pageBuilder: (_, __, ___) => delivery()));
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.only(right: 10.0),
                                        child: Container(
                                          height: 40.0,
                                          width: 120.0,
                                          decoration: BoxDecoration(
                                            color: Color(0xFFA3BDED),
                                          ),
                                          child: Center(
                                            child: Text(
                                              "Pay",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontFamily: "Sans",
                                                  fontWeight: FontWeight.w600),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                  ),
                                ),*/
                                      ],
                                    ),
                                  ),
                                );
                              },
                              scrollDirection: Axis.vertical,
                            ),
                          ),
                        ),
                        Positioned(
                          child: Align(
                            alignment: FractionalOffset.bottomCenter,
                            child: Container(
                              color: Colors.grey[200],
                              padding: const EdgeInsets.only(
                                  top: 10.0,
                                  left: 10.0,
                                  right: 10.0,
                                  bottom: 10.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Padding(
                                      padding:
                                          const EdgeInsets.only(left: 10.0),

                                      /// Total price of item buy
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            'Total : ',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.w800,
                                                fontSize: 15.5,
                                                fontFamily: "Sans"),
                                          ),
                                          TextFormatIDR(
                                            thisText: cart.totalPay,
                                            thisStyle: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.w600,
                                                fontSize: 15.5,
                                                fontFamily: "Sans"),
                                          )
                                        ],
                                      )
                                      // Text(
                                      //   "Total : " +
                                      //       formatter.format(cart.totalPay),
                                      //   style: TextStyle(
                                      //       color: Colors.black,
                                      //       fontWeight: FontWeight.w500,
                                      //       fontSize: 15.5,
                                      //       fontFamily: "Sans"),
                                      // ),
                                      ),
                                  InkWell(
                                    onTap: () {
                                      _saveBillProduct();
                                      Navigator.of(context).pushReplacement(
                                          MaterialPageRoute(
                                              builder: (BuildContext context) =>
                                                  Delivery()));
                                    },
                                    child: Padding(
                                      padding:
                                          const EdgeInsets.only(right: 10.0),
                                      child: Container(
                                        height: 40.0,
                                        width: 120.0,
                                        decoration: BoxDecoration(
                                          color: Colors.indigoAccent
                                        ),
                                        child: Center(
                                          child: Text(
                                            "Checkout",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontFamily: "Sans",
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ));
              }),
        ));
  }
}

class NoItemCart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQD = MediaQuery.of(context);
    return Container(
      width: 500.0,
      color: Colors.white,
      height: double.infinity,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
                padding:
                    EdgeInsets.only(top: mediaQD.padding.top + 50.0)),
            Image.asset(
              "assets/imgIllustration/IlustrasiCart.png",
              height: 300.0,
            ),
            Padding(padding: EdgeInsets.only(bottom: 10.0)),
            Text(
              "Not Have Item",
              style: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 18.5,
                  color: Colors.black26.withOpacity(0.2),
                  fontFamily: "Popins"),
            ),
          ],
        ),
      ),
    );
  }
}
