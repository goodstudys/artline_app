import 'dart:convert';

import 'package:artline/bloc/global/BlocProvider.dart';
import 'package:artline/bloc/global/GlobalBloc.dart';
import 'package:artline/components/cardAddress.dart';
import 'package:artline/components/inputTextForm.dart';
import 'package:artline/env.dart';
import 'package:artline/model/dataAddress.dart';
import 'package:artline/model/dataCity.dart';
import 'package:artline/model/dataProvince.dart';
import 'package:artline/model/dataService.dart';
import 'package:artline/pages/cart/opsiPengiriman.dart';
import 'package:artline/pages/cart/pilihAlamat.dart';
import 'package:artline/pages/cart/resellerInfo.dart';
import 'package:artline/storage/storage.dart';
import 'package:artline/theme/style.dart';
import 'package:flutter/material.dart';
import 'package:artline/pages/cart/Payment.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Delivery extends StatefulWidget {
  @override
  _DeliveryState createState() => _DeliveryState();
}

class _DeliveryState extends State<Delivery> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _formKeyDrop = GlobalKey<FormState>();

  final TextEditingController _nama = TextEditingController();
  final TextEditingController _telp = TextEditingController();
  final TextEditingController _kecamatan = TextEditingController();
  final TextEditingController _kelurahan = TextEditingController();
  final TextEditingController _kodepos = TextEditingController();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _alamat = TextEditingController();
  final TextEditingController _dropName = TextEditingController();
  final TextEditingController _dropPhone = TextEditingController();
  final TextEditingController _kodeBooking = TextEditingController();

  DataStore storage = DataStore();

  bool _autovalidate = false;
  bool isLoading = false;
  bool isSelected = false;
  bool kodeBoking = false;
  bool reseller = false;
  String valueReseller = 'off';

  String totalBerat;
  void _completeLogin() {
    Navigator.pushReplacement(context,
        MaterialPageRoute(builder: (BuildContext context) => Payment()));
  }

  void saveKotaProv(String kota, String prov, String kurir) async {
    storage.setDataString('simpanKota', kota);
    storage.setDataString('simpanProv', prov);
    storage.setDataString('simpanKurir', kurir);
  }

  void _resellerPage() {
    Navigator.pushReplacement(context,
        MaterialPageRoute(builder: (BuildContext context) => ResellerInfo()));
  }

  DataCity _city;
  DataProvince _province;
  int totalPay;
  String userLevel;
  List<DataProvince> listApiProvince;
  List<DataCity> listApiCity;
  List<DataCity> listCity = [];
  submit(String val, kurirName, kurirService) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      pref.setString('ongkir', val);
      pref.setString('kurirName', kurirName);
      pref.setString('kurirService', kurirService);
      // print('total berat : $totalBerat, tujuan : ' + _city.cityId + ', ' + data.toString());
    });
  }

  _submitStore() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      totalBerat = BlocProvider.of<GlobalBloc>(context)
          .cartBloc
          .cart
          .totalWeight
          .toString();
      pref.setString('billNama', _nama.text);
      pref.setString('billProvinsi', _province.province);
      pref.setString('billKota', _city.cityName);
      pref.setString('billAlamat', _alamat.text);
      pref.setString('billKecamatan', _kecamatan.text);
      pref.setString('billKelurahan', _kelurahan.text);
      pref.setString('billKodePos', _kodepos.text);
      pref.setString('billTelp', _telp.text);
      pref.setString('billEmail', _email.text);
      pref.setString('tujuan', _city.cityId);
      pref.setString('reseller', valueReseller);
      pref.setString(
          'berat',
          BlocProvider.of<GlobalBloc>(context)
              .cartBloc
              .cart
              .totalWeight
              .toString());
      if (isSelected == true) {
        pref.setString('dropName', _dropName.text);
        pref.setString('dropPhone', _dropPhone.text);
        pref.setBool('dropship', isSelected);
        if (kodeBoking == true) {
          pref.setString('kodeBooking', _kodeBooking.text);
          pref.setBool('isBooking', kodeBoking);
        } else {
          pref.setBool('isBooking', kodeBoking);
        }
      } else {
        pref.setBool('dropship', isSelected);
      }
    });
  }

  _submitStore2() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      totalBerat = BlocProvider.of<GlobalBloc>(context)
          .cartBloc
          .cart
          .totalWeight
          .toString();
      pref.setString('billNama',
          BlocProvider.of<GlobalBloc>(context).cartBloc.cart.saveAddress.name);
      pref.setString(
          'billProvinsi',
          BlocProvider.of<GlobalBloc>(context)
              .cartBloc
              .cart
              .saveAddress
              .province);
      pref.setString('billKota', cit.cityName);
      pref.setString(
          'billAlamat',
          BlocProvider.of<GlobalBloc>(context)
              .cartBloc
              .cart
              .saveAddress
              .address);
      pref.setString(
          'billKecamatan',
          BlocProvider.of<GlobalBloc>(context)
              .cartBloc
              .cart
              .saveAddress
              .kecamatan);
      pref.setString(
          'billKelurahan',
          BlocProvider.of<GlobalBloc>(context)
              .cartBloc
              .cart
              .saveAddress
              .kelurahan);
      pref.setString(
          'billKodePos',
          BlocProvider.of<GlobalBloc>(context)
              .cartBloc
              .cart
              .saveAddress
              .kodePos);
      pref.setString('billTelp',
          BlocProvider.of<GlobalBloc>(context).cartBloc.cart.saveAddress.phone);
      pref.setString('billEmail', '');
      pref.setString('tujuan',
          BlocProvider.of<GlobalBloc>(context).cartBloc.cart.saveAddress.city);
      pref.setString('reseller', valueReseller);
      pref.setString(
          'berat',
          BlocProvider.of<GlobalBloc>(context)
              .cartBloc
              .cart
              .totalWeight
              .toString());
      if (isSelected == true) {
        pref.setString('dropName', _dropName.text);
        pref.setString('dropPhone', _dropPhone.text);
        pref.setBool('dropship', isSelected);
        if (kodeBoking == true) {
          pref.setString('kodeBooking', _kodeBooking.text);
          pref.setBool('isBooking', kodeBoking);
        }
      } else {
        pref.setBool('dropship', isSelected);
        pref.setBool('isBooking', kodeBoking);
      }
    });
  }

  List data;
  DataCity cit;
  DataProvince prov;
  Future<String> getDataAlamat() async {
    // setState(() {
    //   isLoading = true;
    // });
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var res = await http.get(url('api/getAddress'), headers: {
      'Authorization': $tokenType + ' ' + $accesToken,
      'Accept': 'application/json'
    });
    setState(() {
      var content = json.decode(res.body);
      data = content['address'];
      detailAddress =
          BlocProvider.of<GlobalBloc>(context).cartBloc.cart.saveAddress;
      if (detailAddress != null) {
        listApiProvince.forEach((f) {
          if (f.provinceId == detailAddress.province) {
            prov = f;
          }
        });
        listApiCity.forEach((p) {
          if (p.cityId == detailAddress.city) {
            cit = p;
          }
        });
      }
    });
    // setState(() {
    //   isLoading = false;
    // });
    return 'success!';
  }

  Future<String> getData() async {
    // setState(() {
    //   isLoading = true;
    // });
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var $userLevel = await storage.getDataString('userLevel');
    var res = await http.get(url('api/formDelivery'),
        headers: {'Authorization': $tokenType + ' ' + $accesToken});

    setState(() {
      var content = json.decode(res.body);

      // print(content['provinces']['rajaongkir']['results']);
      listApiProvince = List<DataProvince>.from(content['provinces']
              ['rajaongkir']['results']
          .map((item) => DataProvince.fromJson(item)));
      listApiCity = List<DataCity>.from(content['city']['rajaongkir']['results']
          .map((item) => DataCity.fromJson(item)));
      getDataAlamat();
      userLevel = $userLevel;
      if ($userLevel == '4') {
        valueReseller = 'on';
        reseller = true;
      }
    });
    // setState(() {
    //   isLoading = false;
    // });
    //print(data);
    return 'success!';
  }

  DataAddress detailAddress;

  @override
  void initState() {
    super.initState();
    getData().then((s) => setState(() {
          isLoading = true;
        }));
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQD = MediaQuery.of(context);
    return StreamBuilder(
        stream: BlocProvider.of<GlobalBloc>(context).cartBloc.cartStream,
        builder: (context, snapshot) {
          DataProvince namaProvinsi;
          DataCity namaKota;
          if (BlocProvider.of<GlobalBloc>(context).cartBloc.cart.saveAddress !=
              null) {
            listApiProvince.forEach((f) {
              if (f.provinceId ==
                  BlocProvider.of<GlobalBloc>(context)
                      .cartBloc
                      .cart
                      .saveAddress
                      .province) {
                namaProvinsi = f;

                prov = f;
              }
            });
            listApiCity.forEach((p) {
              if (p.cityId ==
                  BlocProvider.of<GlobalBloc>(context)
                      .cartBloc
                      .cart
                      .saveAddress
                      .city) {
                namaKota = p;

                cit = p;
              }
            });
          }
          return Scaffold(
            appBar: AppBar(
              // leading: InkWell(
              //     onTap: () {
              //       Navigator.of(context).pop(false);
              //     },
              //     child: Icon(Icons.arrow_back)),
              elevation: 0.0,
              title: Text(
                "Delivery",
                style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 18.0,
                    color: Colors.black54,
                    fontFamily: "Gotik"),
              ),
              centerTitle: true,
              backgroundColor: Colors.white,
              iconTheme: IconThemeData(color: ColorStyle.primaryColor),
            ),
            body: isLoading ||
                    BlocProvider.of<GlobalBloc>(context)
                            .cartBloc
                            .cart
                            .saveAddress !=
                        null
                ? SingleChildScrollView(
                    child: Container(
                      color: Colors.white,
                      child: Padding(
                          padding: const EdgeInsets.only(top: 30.0),
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(left: 20.0, right: 20.0),
                                child: Text(
                                  "Where are your ordered items shipped ?",
                                  style: TextStyle(
                                      letterSpacing: 0.1,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 25.0,
                                      color: Colors.black54,
                                      fontFamily: "Gotik"),
                                ),
                              ),
                              BlocProvider.of<GlobalBloc>(context)
                                          .cartBloc
                                          .cart
                                          .saveAddress !=
                                      null
                                  ? CardAddress(
                                      type: 'pilih',
                                      city: namaKota.cityName,
                                      province: namaProvinsi.province,
                                      address:
                                          BlocProvider.of<GlobalBloc>(context)
                                              .cartBloc
                                              .cart
                                              .saveAddress
                                              .address,
                                      //city: BlocProvider.of<GlobalBloc>(context).cartBloc.cart.saveAddress.city,
                                      kecamatan:
                                          BlocProvider.of<GlobalBloc>(context)
                                              .cartBloc
                                              .cart
                                              .saveAddress
                                              .kecamatan,
                                      kodePos:
                                          BlocProvider.of<GlobalBloc>(context)
                                              .cartBloc
                                              .cart
                                              .saveAddress
                                              .kodePos,
                                      name: BlocProvider.of<GlobalBloc>(context)
                                          .cartBloc
                                          .cart
                                          .saveAddress
                                          .name,
                                      phone:
                                          BlocProvider.of<GlobalBloc>(context)
                                              .cartBloc
                                              .cart
                                              .saveAddress
                                              .phone,
                                      // province: BlocProvider.of<GlobalBloc>(context).cartBloc.cart.saveAddress.province,
                                      tap: () {
                                        Navigator.of(context).push(
                                            PageRouteBuilder(
                                                pageBuilder: (_, __, ___) =>
                                                    new PilihAlamat()));
                                      },
                                    )
                                  : Container(
                                      margin: EdgeInsets.only(
                                          bottom: 0.0,
                                          left: 20.0,
                                          right: 20.0,
                                          top: 13.0),
                                      padding: EdgeInsets.all(13.0),
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                          boxShadow: [
                                            BoxShadow(
                                                color: Colors.black12,
                                                blurRadius: 5,
                                                spreadRadius: 2,
                                                offset: Offset(5, 5))
                                          ]),
                                      child: Material(
                                          color: Colors.transparent,
                                          child: InkWell(
                                            onTap: () {
                                              Navigator.of(context).push(
                                                  PageRouteBuilder(
                                                      pageBuilder: (_, __,
                                                              ___) =>
                                                          new PilihAlamat()));
                                            },
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: <Widget>[
                                                Text(
                                                  'Pilih Alamat',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                Text(
                                                  '+',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold),
                                                )
                                              ],
                                            ),
                                          ))),
                              BlocProvider.of<GlobalBloc>(context)
                                          .cartBloc
                                          .cart
                                          .saveAddress ==
                                      null
                                  ? Padding(
                                    padding: EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
                                    child: Form(
                                        key: _formKey,
                                        child: Column(
                                          children: <Widget>[
                                            InputTextForm(
                                              autovalidate: _autovalidate,
                                              textController: _nama,
                                              label: 'Nama',
                                              hint: 'Nama',
                                            ),
                                            // listApiProvince == null
                                            //     ? Text('')
                                            //     :
                                                 DropdownButtonFormField(
                                                    autovalidate: _autovalidate,
                                                    value: _province,
                                                    validator: (value) {
                                                      if (value == null) {
                                                        return 'Silahkan pilih Provinsi';
                                                      } else {
                                                        return null;
                                                      }
                                                    },
                                                    decoration: InputDecoration(
                                                        hintStyle: TextStyle(
                                                            color:
                                                                Colors.black54),
                                                        filled: false,
                                                        hintText:
                                                            'Pilih Provinsi',
                                                        labelText:
                                                            _province == null
                                                                ? 'Pilih Provinsi'
                                                                : 'Provinsi',
                                                        // border: InputBorder.none,
                                                        hoverColor: Colors.white),
                                                    items: listApiProvince.map(
                                                        (DataProvince value) {
                                                      return DropdownMenuItem<
                                                          DataProvince>(
                                                        value: value,
                                                        child: Text(
                                                          value.province,
                                                        ),
                                                      );
                                                    }).toList(),
                                                    onChanged: (value) {
                                                      setState(() {
                                                        _province = value;
                                                        listCity = [];
                                                        _city = null;
                                                      });
                                                      setState(() {
                                                        listApiCity.forEach((f) {
                                                          if (f.provinceId
                                                                  .toString() ==
                                                              value.provinceId
                                                                  .toString()) {
                                                            listCity.add(f);
                                                          }
                                                        });
                                                      });
                                                    },
                                                  ),
                                            // listApiProvince == null
                                            //     ? Text('')
                                            //     : 
                                                DropdownButtonFormField(
                                                    autovalidate: _autovalidate,
                                                    value: _city,
                                                    validator: (value) {
                                                      if (value == null) {
                                                        return 'Silahkan pilih Kota';
                                                      } else {
                                                        return null;
                                                      }
                                                    },
                                                    decoration: InputDecoration(
                                                        hintStyle: TextStyle(
                                                            color:
                                                                Colors.black54),
                                                        filled: false,
                                                        hintText: 'Pilih Kota',
                                                        labelText: _city == null
                                                            ? 'Pilih Kota'
                                                            : 'Kota',
                                                        hoverColor: Colors.white),
                                                    items: listCity
                                                        .map((DataCity value) {
                                                      return DropdownMenuItem<
                                                          DataCity>(
                                                        value: value,
                                                        child: Text(
                                                          value.cityName,
                                                          // style: TextStyle(
                                                          //   fontWeight: FontWeight.bold,
                                                          //   fontSize: 18.0,
                                                          //   color: Colors.black,
                                                          // ),
                                                        ),
                                                      );
                                                    }).toList(),
                                                    onChanged: (value) {
                                                      setState(() {
                                                        BlocProvider.of<
                                                                    GlobalBloc>(
                                                                context)
                                                            .cartBloc
                                                            .clearOpsiPengiriman(
                                                                null);
                                                        _city = value;
                                                      });
                                                    },
                                                  ),
                                            InputTextForm(
                                              autovalidate: _autovalidate,
                                              textController: _alamat,
                                              label: 'Alamat Lengkap',
                                              hint: 'Alamat Lengkap',
                                              line: 3,
                                            ),
                                            InputTextForm(
                                              autovalidate: _autovalidate,
                                              textController: _kecamatan,
                                              label: 'Kecamatan',
                                              hint: 'Kecamatan',
                                            ),
                                            InputTextForm(
                                              autovalidate: _autovalidate,
                                              textController: _kelurahan,
                                              label: 'Kelurahan',
                                              hint: 'Kelurahan',
                                            ),
                                            InputTextForm(
                                              autovalidate: _autovalidate,
                                              textController: _kodepos,
                                              label: 'Kode Pos',
                                              hint: 'Kode Pos',
                                              inputType: TextInputType.number,
                                            ),
                                            InputTextForm(
                                              autovalidate: _autovalidate,
                                              textController: _telp,
                                              label: 'No. Telepon',
                                              hint: 'No. Telepon',
                                              type: 'mobile',
                                              inputType: TextInputType.number,
                                            ),
                                            InputTextForm(
                                              autovalidate: _autovalidate,
                                              textController: _email,
                                              label: 'Alamat Email',
                                              hint: 'Alamat Email',
                                            ),
                                          ],
                                        ),
                                      ),
                                  )
                                  : Text(''),
                              _city != null ||
                                      BlocProvider.of<GlobalBloc>(context)
                                              .cartBloc
                                              .cart
                                              .saveAddress !=
                                          null
                                  ? Padding(
                                      padding: const EdgeInsets.only(top: 10.0),
                                      child: InkWell(
                                        onTap: () {},
                                        child: Container(
                                          padding: EdgeInsets.all(13.0),
                                          //height: 50.0,
                                          // width: 600.0,
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xFF656565)
                                                      .withOpacity(0.15),
                                                  blurRadius: 1.0,
                                                  spreadRadius: 0.2,
                                                )
                                              ]),
                                          child: Column(
                                            children: <Widget>[
                                              Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Row(
                                                    children: <Widget>[
                                                      Text(
                                                        "Opsi Pengiriman",
                                                        style: TxtStyle
                                                            .subHeaderCustomStyle,
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                              InkWell(
                                                onTap: () {
                                                  if (BlocProvider.of<
                                                                  GlobalBloc>(
                                                              context)
                                                          .cartBloc
                                                          .cart
                                                          .saveAddress !=
                                                      null) {
                                                    saveKotaProv(
                                                        BlocProvider.of<
                                                                    GlobalBloc>(
                                                                context)
                                                            .cartBloc
                                                            .cart
                                                            .saveAddress
                                                            .city,
                                                        BlocProvider.of<
                                                                    GlobalBloc>(
                                                                context)
                                                            .cartBloc
                                                            .cart
                                                            .saveAddress
                                                            .province,
                                                        'jne');
                                                    Navigator.of(context).push(
                                                        PageRouteBuilder(
                                                            pageBuilder: (_, __,
                                                                    ___) =>
                                                                new OpsiPengiriman()));
                                                  } else {
                                                    saveKotaProv(
                                                        _city.cityId,
                                                        _province.provinceId,
                                                        'jne');
                                                    Navigator.of(context).push(
                                                        PageRouteBuilder(
                                                            pageBuilder: (_, __,
                                                                    ___) =>
                                                                new OpsiPengiriman()));
                                                  }
                                                },
                                                child: Container(
                                                  margin: EdgeInsets.only(
                                                      top: 10.0, left: 20.0),
                                                  child: Row(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: <Widget>[
                                                      Row(
                                                        children: <Widget>[
                                                          Container(
                                                            height: 20.0,
                                                            width: 20.0,
                                                            child: Center(
                                                                child: BlocProvider.of<GlobalBloc>(context)
                                                                            .cartBloc
                                                                            .cart
                                                                            .saveKurir ==
                                                                        'jne'
                                                                    ? Icon(
                                                                        Icons
                                                                            .check,
                                                                        color: Colors
                                                                            .white,
                                                                        size:
                                                                            15.0,
                                                                      )
                                                                    : Container()),
                                                            decoration:
                                                                BoxDecoration(
                                                              color: BlocProvider.of<GlobalBloc>(
                                                                              context)
                                                                          .cartBloc
                                                                          .cart
                                                                          .saveKurir ==
                                                                      'jne'
                                                                  ? ColorStyle
                                                                      .primaryColor
                                                                  : Colors
                                                                      .transparent,
                                                              border: Border.all(
                                                                  width: 1.0,
                                                                  color: BlocProvider.of<GlobalBloc>(context)
                                                                              .cartBloc
                                                                              .cart
                                                                              .saveKurir ==
                                                                          'jne'
                                                                      ? ColorStyle
                                                                          .primaryColor
                                                                      : Colors
                                                                          .grey),
                                                              borderRadius:
                                                                  const BorderRadius
                                                                      .all(const Radius
                                                                          .circular(
                                                                      20.0)),
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 10.0),
                                                            child: Text(
                                                              'JNE',
                                                              style: TextStyle(
                                                                color:
                                                                    Colors.grey,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontSize: 16.0,
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      Container(
                                                        child: BlocProvider.of<GlobalBloc>(
                                                                            context)
                                                                        .cartBloc
                                                                        .cart
                                                                        .saveService ==
                                                                    null ||
                                                                BlocProvider.of<GlobalBloc>(
                                                                            context)
                                                                        .cartBloc
                                                                        .cart
                                                                        .saveKurir !=
                                                                    'jne'
                                                            ? Container()
                                                            : Text(
                                                                'Rp. ' +
                                                                    BlocProvider.of<GlobalBloc>(
                                                                            context)
                                                                        .cartBloc
                                                                        .cart
                                                                        .saveService
                                                                        .cost[0]
                                                                        .value
                                                                        .toString(),
                                                                style:
                                                                    TextStyle(
                                                                  color: ColorStyle
                                                                      .primaryColor,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontSize:
                                                                      16.0,
                                                                ),
                                                              ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              InkWell(
                                                onTap: () {
                                                  if (BlocProvider.of<
                                                                  GlobalBloc>(
                                                              context)
                                                          .cartBloc
                                                          .cart
                                                          .saveAddress !=
                                                      null) {
                                                    saveKotaProv(
                                                        BlocProvider.of<
                                                                    GlobalBloc>(
                                                                context)
                                                            .cartBloc
                                                            .cart
                                                            .saveAddress
                                                            .city,
                                                        BlocProvider.of<
                                                                    GlobalBloc>(
                                                                context)
                                                            .cartBloc
                                                            .cart
                                                            .saveAddress
                                                            .province,
                                                        'tiki');
                                                    Navigator.of(context).push(
                                                        PageRouteBuilder(
                                                            pageBuilder: (_, __,
                                                                    ___) =>
                                                                new OpsiPengiriman()));
                                                  } else {
                                                    saveKotaProv(
                                                        _city.cityId,
                                                        _province.provinceId,
                                                        'tiki');
                                                    Navigator.of(context).push(
                                                        PageRouteBuilder(
                                                            pageBuilder: (_, __,
                                                                    ___) =>
                                                                new OpsiPengiriman()));
                                                  }
                                                },
                                                child: Container(
                                                  margin: EdgeInsets.only(
                                                      top: 10.0, left: 20.0),
                                                  child: Row(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: <Widget>[
                                                      Row(
                                                        children: <Widget>[
                                                          Container(
                                                            height: 20.0,
                                                            width: 20.0,
                                                            child: Center(
                                                                child: BlocProvider.of<GlobalBloc>(context)
                                                                            .cartBloc
                                                                            .cart
                                                                            .saveKurir ==
                                                                        'tiki'
                                                                    ? Icon(
                                                                        Icons
                                                                            .check,
                                                                        color: Colors
                                                                            .white,
                                                                        size:
                                                                            15.0,
                                                                      )
                                                                    : Container()),
                                                            decoration:
                                                                BoxDecoration(
                                                              color: BlocProvider.of<GlobalBloc>(
                                                                              context)
                                                                          .cartBloc
                                                                          .cart
                                                                          .saveKurir ==
                                                                      'tiki'
                                                                  ? ColorStyle
                                                                      .primaryColor
                                                                  : Colors
                                                                      .transparent,
                                                              border: Border.all(
                                                                  width: 1.0,
                                                                  color: BlocProvider.of<GlobalBloc>(
                                                                                  context)
                                                                              .cartBloc
                                                                              .cart
                                                                              .saveKurir ==
                                                                          'tiki'
                                                                      ? ColorStyle
                                                                          .primaryColor
                                                                      : Colors
                                                                          .grey),
                                                              borderRadius:
                                                                  const BorderRadius
                                                                      .all(const Radius
                                                                          .circular(
                                                                      20.0)),
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 10.0),
                                                            child: Text(
                                                              'TIKI',
                                                              style: TextStyle(
                                                                color:
                                                                    Colors.grey,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontSize: 16.0,
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      Container(
                                                        child: BlocProvider.of<GlobalBloc>(
                                                                            context)
                                                                        .cartBloc
                                                                        .cart
                                                                        .saveService ==
                                                                    null ||
                                                                BlocProvider.of<GlobalBloc>(
                                                                            context)
                                                                        .cartBloc
                                                                        .cart
                                                                        .saveKurir !=
                                                                    'tiki'
                                                            ? Container()
                                                            : Text(
                                                                'Rp. ' +
                                                                    BlocProvider.of<GlobalBloc>(
                                                                            context)
                                                                        .cartBloc
                                                                        .cart
                                                                        .saveService
                                                                        .cost[0]
                                                                        .value
                                                                        .toString(),
                                                                style:
                                                                    TextStyle(
                                                                  color: ColorStyle
                                                                      .primaryColor,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontSize:
                                                                      16.0,
                                                                ),
                                                              ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              InkWell(
                                                onTap: () {
                                                  BlocProvider.of<GlobalBloc>(
                                                          context)
                                                      .cartBloc
                                                      .addKurirItem('Cargo');
                                                  BlocProvider.of<GlobalBloc>(
                                                          context)
                                                      .cartBloc
                                                      .addServiceItem(
                                                          DataService(
                                                        cost: [
                                                          Cost(
                                                            etd: '',
                                                            note: '',
                                                            value: 0,
                                                          )
                                                        ],
                                                        description: '',
                                                        service: 'a',
                                                      ));
                                                },
                                                child: Container(
                                                  margin: EdgeInsets.only(
                                                      top: 10.0, left: 20.0),
                                                  child: Row(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: <Widget>[
                                                      Row(
                                                        children: <Widget>[
                                                          Container(
                                                            height: 20.0,
                                                            width: 20.0,
                                                            child: Center(
                                                                child: BlocProvider.of<GlobalBloc>(context)
                                                                            .cartBloc
                                                                            .cart
                                                                            .saveKurir ==
                                                                        'Cargo'
                                                                    ? Icon(
                                                                        Icons
                                                                            .check,
                                                                        color: Colors
                                                                            .white,
                                                                        size:
                                                                            15.0,
                                                                      )
                                                                    : Container()),
                                                            decoration:
                                                                BoxDecoration(
                                                              color: BlocProvider.of<GlobalBloc>(
                                                                              context)
                                                                          .cartBloc
                                                                          .cart
                                                                          .saveKurir ==
                                                                      'Cargo'
                                                                  ? ColorStyle
                                                                      .primaryColor
                                                                  : Colors
                                                                      .transparent,
                                                              border: Border.all(
                                                                  width: 1.0,
                                                                  color: BlocProvider.of<GlobalBloc>(
                                                                                  context)
                                                                              .cartBloc
                                                                              .cart
                                                                              .saveKurir ==
                                                                          'cargo'
                                                                      ? ColorStyle
                                                                          .primaryColor
                                                                      : Colors
                                                                          .grey),
                                                              borderRadius:
                                                                  const BorderRadius
                                                                      .all(const Radius
                                                                          .circular(
                                                                      20.0)),
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 10.0),
                                                            child: Text(
                                                              'Cargo',
                                                              style: TextStyle(
                                                                color:
                                                                    Colors.grey,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontSize: 16.0,
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      Container(
                                                        child: BlocProvider.of<GlobalBloc>(
                                                                            context)
                                                                        .cartBloc
                                                                        .cart
                                                                        .saveService ==
                                                                    null ||
                                                                BlocProvider.of<GlobalBloc>(
                                                                            context)
                                                                        .cartBloc
                                                                        .cart
                                                                        .saveKurir !=
                                                                    'Cargo'
                                                            ? Container()
                                                            : Text(
                                                                'Rp. ' +
                                                                    BlocProvider.of<GlobalBloc>(
                                                                            context)
                                                                        .cartBloc
                                                                        .cart
                                                                        .saveService
                                                                        .cost[0]
                                                                        .value
                                                                        .toString(),
                                                                style:
                                                                    TextStyle(
                                                                  color: ColorStyle
                                                                      .primaryColor,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontSize:
                                                                      16.0,
                                                                ),
                                                              ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              BlocProvider.of<GlobalBloc>(
                                                              context)
                                                          .cartBloc
                                                          .cart
                                                          .saveService ==
                                                      null
                                                  ? Align(
                                                      alignment:
                                                          Alignment.centerLeft,
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .only(top: 8.0),
                                                        child: Text(
                                                          'Silahkan pilih Pengiriman Anda',
                                                          style: TextStyle(
                                                              color: Colors.red,
                                                              fontSize: 12),
                                                        ),
                                                      ),
                                                    )
                                                  : Container(),
                                            ],
                                          ),
                                        ),
                                      ),
                                    )
                                  : Container(),
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    if (isSelected == false) {
                                      isSelected = true;
                                      print(isSelected.toString());
                                    } else {
                                      isSelected = false;
                                    }
                                  });
                                },
                                child: Container(
                                  margin: EdgeInsets.only(top: 10.0),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            height: 20.0,
                                            width: 20.0,
                                            child: Center(
                                                child: isSelected
                                                    ? Icon(
                                                        Icons.check,
                                                        color: Colors.white,
                                                        size: 10.0,
                                                      )
                                                    : Container()),
                                            decoration: BoxDecoration(
                                              color: isSelected
                                                  ? ColorStyle.primaryColor
                                                  : Colors.transparent,
                                              border: Border.all(
                                                  width: 1.0,
                                                  color: isSelected
                                                      ? ColorStyle.primaryColor
                                                      : Colors.grey),
                                              borderRadius: const BorderRadius
                                                      .all(
                                                  const Radius.circular(20.0)),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 10.0),
                                            child: Text(
                                              'Kirim sebagai Dropshipper',
                                              style: TextStyle(
                                                color: Colors.grey,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16.0,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      // Container(
                                      //   child: isSelected
                                      //       ? Text(
                                      //           'Rp. ' + '_item.buttonText',
                                      //           style: TextStyle(
                                      //             color: Colors.green,
                                      //             fontWeight: FontWeight.bold,
                                      //             fontSize: 20.0,
                                      //           ),
                                      //         )
                                      //       : Container(),
                                      // )
                                    ],
                                  ),
                                ),
                              ),
                              isSelected == false
                                  ? Text('')
                                  : Form(
                                      key: _formKeyDrop,
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Column(
                                          children: <Widget>[
                                            InkWell(
                                              onTap: () {
                                                setState(() {
                                                  if (kodeBoking == false) {
                                                    kodeBoking = true;
                                                  } else {
                                                    kodeBoking = false;
                                                  }
                                                });
                                              },
                                              child: Container(
                                                margin:
                                                    EdgeInsets.only(top: 10.0),
                                                child: Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: <Widget>[
                                                    Row(
                                                      children: <Widget>[
                                                        Container(
                                                          height: 20.0,
                                                          width: 20.0,
                                                          child: Center(
                                                              child: kodeBoking
                                                                  ? Icon(
                                                                      Icons
                                                                          .check,
                                                                      color: Colors
                                                                          .white,
                                                                      size:
                                                                          10.0,
                                                                    )
                                                                  : Container()),
                                                          decoration:
                                                              BoxDecoration(
                                                            color: kodeBoking
                                                                ? ColorStyle
                                                                    .primaryColor
                                                                : Colors
                                                                    .transparent,
                                                            border: Border.all(
                                                                width: 1.0,
                                                                color: kodeBoking
                                                                    ? ColorStyle
                                                                        .primaryColor
                                                                    : Colors
                                                                        .grey),
                                                            borderRadius:
                                                                const BorderRadius
                                                                        .all(
                                                                    const Radius
                                                                            .circular(
                                                                        20.0)),
                                                          ),
                                                        ),
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 10.0),
                                                          child: Text(
                                                            'Memiliki Kode boking',
                                                            style: TextStyle(
                                                              color:
                                                                  Colors.grey,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              fontSize: 16.0,
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            kodeBoking
                                                ? InputTextForm(
                                                    autovalidate: _autovalidate,
                                                    textController:
                                                        _kodeBooking,
                                                    label: 'Kode Boking',
                                                    hint: 'Kode Boking',
                                                  )
                                                : Text(''),
                                            InputTextForm(
                                              autovalidate: _autovalidate,
                                              textController: _dropName,
                                              label: 'Nama Lengkap',
                                              hint: 'Nama Lengkap',
                                            ),
                                            InputTextForm(
                                              autovalidate: _autovalidate,
                                              textController: _dropPhone,
                                              label: 'No. Telepon',
                                              hint: 'No. Telepon',
                                            ),
                                          ],
                                        ),
                                      )),
                              userLevel == '4'
                                  ? InkWell(
                                      onTap: () {
                                        if (userLevel != '4') {
                                          setState(() {
                                            if (reseller == false) {
                                              reseller = true;
                                              setState(() {
                                                valueReseller = 'on';
                                              });
                                            } else {
                                              reseller = false;
                                              setState(() {
                                                valueReseller = 'off';
                                              });
                                            }
                                          });
                                        }
                                      },
                                      child: Container(
                                        margin: EdgeInsets.only(top: 10.0),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                Container(
                                                  height: 20.0,
                                                  width: 20.0,
                                                  child: Center(
                                                      child: reseller
                                                          ? Icon(
                                                              Icons.check,
                                                              color:
                                                                  Colors.white,
                                                              size: 10.0,
                                                            )
                                                          : Container()),
                                                  decoration: BoxDecoration(
                                                    color: reseller
                                                        ? ColorStyle
                                                            .primaryColor
                                                        : Colors.transparent,
                                                    border: Border.all(
                                                        width: 1.0,
                                                        color: reseller
                                                            ? ColorStyle
                                                                .primaryColor
                                                            : Colors.grey),
                                                    borderRadius:
                                                        const BorderRadius.all(
                                                            const Radius
                                                                    .circular(
                                                                20.0)),
                                                  ),
                                                ),
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      left: 10.0),
                                                  child: Text(
                                                    'Reseller',
                                                    style: TextStyle(
                                                      color: Colors.grey,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 16.0,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  : Container(),
                              BlocProvider.of<GlobalBloc>(context)
                                          .cartBloc
                                          .cart
                                          .saveAddress ==
                                      null
                                  ? Column(
                                      children: <Widget>[
                                        Padding(
                                            padding:
                                                EdgeInsets.only(top: 80.0)),
                                        InkWell(
                                          onTap: () {
                                            if (isSelected == true) {
                                              final formState =
                                                  _formKey.currentState;
                                              final formStateDrop =
                                                  _formKeyDrop.currentState;
                                              if (formState.validate() &&
                                                  formStateDrop.validate() &&
                                                  BlocProvider.of<GlobalBloc>(
                                                              context)
                                                          .cartBloc
                                                          .cart
                                                          .saveService !=
                                                      null) {
                                                _submitStore();
                                                submit(
                                                    BlocProvider.of<GlobalBloc>(
                                                            context)
                                                        .cartBloc
                                                        .cart
                                                        .saveService
                                                        .cost[0]
                                                        .value
                                                        .toString(),
                                                    BlocProvider.of<GlobalBloc>(
                                                            context)
                                                        .cartBloc
                                                        .cart
                                                        .saveKurir,
                                                    BlocProvider.of<GlobalBloc>(
                                                            context)
                                                        .cartBloc
                                                        .cart
                                                        .saveService
                                                        .service);
                                                showDialog(
                                                  context: context,
                                                  builder:
                                                      (BuildContext context) {
                                                    return AlertDialog(
                                                      shape: RoundedRectangleBorder(
                                                          borderRadius:
                                                              new BorderRadius
                                                                      .circular(
                                                                  10.0)),
                                                      content: new Text(
                                                        'Apakah data anda sudah benar?',
                                                        textAlign:
                                                            TextAlign.left,
                                                      ),
                                                      actions: <Widget>[
                                                        FlatButton(
                                                          child: Text("Tidak"),
                                                          onPressed: () {
                                                            Navigator.of(
                                                                    context)
                                                                .pop();
                                                          },
                                                        ),
                                                        FlatButton(
                                                          child:
                                                              new Text("Iya"),
                                                          onPressed: () {
                                                            submit(
                                                                BlocProvider.of<
                                                                            GlobalBloc>(
                                                                        context)
                                                                    .cartBloc
                                                                    .cart
                                                                    .saveService
                                                                    .cost[0]
                                                                    .value
                                                                    .toString(),
                                                                BlocProvider.of<
                                                                            GlobalBloc>(
                                                                        context)
                                                                    .cartBloc
                                                                    .cart
                                                                    .saveKurir,
                                                                BlocProvider.of<
                                                                            GlobalBloc>(
                                                                        context)
                                                                    .cartBloc
                                                                    .cart
                                                                    .saveService
                                                                    .service);
                                                            _completeLogin();
                                                            setState(() {});
                                                          },
                                                        ),
                                                      ],
                                                    );
                                                  },
                                                );
                                              } else {
                                                setState(() {
                                                  // isLoading = false;
                                                  _autovalidate = true;
                                                });
                                              }
                                            } else {
                                              final formState =
                                                  _formKey.currentState;
                                              if (formState.validate() &&
                                                  BlocProvider.of<GlobalBloc>(
                                                              context)
                                                          .cartBloc
                                                          .cart
                                                          .saveService !=
                                                      null) {
                                                _submitStore();
                                                submit(
                                                    BlocProvider.of<GlobalBloc>(
                                                            context)
                                                        .cartBloc
                                                        .cart
                                                        .saveService
                                                        .cost[0]
                                                        .value
                                                        .toString(),
                                                    BlocProvider.of<GlobalBloc>(
                                                            context)
                                                        .cartBloc
                                                        .cart
                                                        .saveKurir,
                                                    BlocProvider.of<GlobalBloc>(
                                                            context)
                                                        .cartBloc
                                                        .cart
                                                        .saveService
                                                        .service);
                                                showDialog(
                                                  context: context,
                                                  builder:
                                                      (BuildContext context) {
                                                    return AlertDialog(
                                                      shape: RoundedRectangleBorder(
                                                          borderRadius:
                                                              new BorderRadius
                                                                      .circular(
                                                                  10.0)),
                                                      content: new Text(
                                                        'Apakah data anda sudah benar?',
                                                        textAlign:
                                                            TextAlign.left,
                                                      ),
                                                      actions: <Widget>[
                                                        FlatButton(
                                                          child: Text("Tidak"),
                                                          onPressed: () {
                                                            Navigator.of(
                                                                    context)
                                                                .pop();
                                                          },
                                                        ),
                                                        FlatButton(
                                                          child:
                                                              new Text("Iya"),
                                                          onPressed: () {
                                                            submit(
                                                                BlocProvider.of<
                                                                            GlobalBloc>(
                                                                        context)
                                                                    .cartBloc
                                                                    .cart
                                                                    .saveService
                                                                    .cost[0]
                                                                    .value
                                                                    .toString(),
                                                                BlocProvider.of<
                                                                            GlobalBloc>(
                                                                        context)
                                                                    .cartBloc
                                                                    .cart
                                                                    .saveKurir,
                                                                BlocProvider.of<
                                                                            GlobalBloc>(
                                                                        context)
                                                                    .cartBloc
                                                                    .cart
                                                                    .saveService
                                                                    .service);
                                                            _completeLogin();
                                                            setState(() {});
                                                          },
                                                        ),
                                                      ],
                                                    );
                                                  },
                                                );
                                              } else {
                                                setState(() {
                                                  // isLoading = false;
                                                  _autovalidate = true;
                                                });
                                              }
                                            }
                                          },
                                          child: Container(
                                            height: 55.0,
                                            width: mediaQD.size.width,
                                            color: ColorStyle.primaryColor,
                                            child: Center(
                                              child: Text(
                                                "Go to Payment",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.w700,
                                                    fontSize: 16.5,
                                                    letterSpacing: 1.0),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                  : Padding(
                                      padding: const EdgeInsets.only(top: 50.0),
                                      child: InkWell(
                                        onTap: () {
                                          if (userLevel == '4' &&
                                              BlocProvider.of<GlobalBloc>(
                                                          context)
                                                      .cartBloc
                                                      .cart
                                                      .saveService !=
                                                  null) {
                                            if (isSelected == true &&
                                                BlocProvider.of<GlobalBloc>(
                                                            context)
                                                        .cartBloc
                                                        .cart
                                                        .saveService !=
                                                    null) {
                                              final formState =
                                                  _formKeyDrop.currentState;
                                              if (formState.validate() &&
                                                  BlocProvider.of<GlobalBloc>(
                                                              context)
                                                          .cartBloc
                                                          .cart
                                                          .saveService !=
                                                      null) {
                                                _submitStore2();
                                                submit(
                                                    BlocProvider.of<GlobalBloc>(
                                                            context)
                                                        .cartBloc
                                                        .cart
                                                        .saveService
                                                        .cost[0]
                                                        .value
                                                        .toString(),
                                                    BlocProvider.of<GlobalBloc>(
                                                            context)
                                                        .cartBloc
                                                        .cart
                                                        .saveKurir,
                                                    BlocProvider.of<GlobalBloc>(
                                                            context)
                                                        .cartBloc
                                                        .cart
                                                        .saveService
                                                        .service);
                                                showDialog(
                                                  context: context,
                                                  builder:
                                                      (BuildContext context) {
                                                    return AlertDialog(
                                                      shape: RoundedRectangleBorder(
                                                          borderRadius:
                                                              new BorderRadius
                                                                      .circular(
                                                                  10.0)),
                                                      content: new Text(
                                                        'Apakah data anda sudah benar?',
                                                        textAlign:
                                                            TextAlign.left,
                                                      ),
                                                      actions: <Widget>[
                                                        FlatButton(
                                                          child: Text("Tidak"),
                                                          onPressed: () {
                                                            Navigator.of(
                                                                    context)
                                                                .pop();
                                                          },
                                                        ),
                                                        FlatButton(
                                                          child:
                                                              new Text("Iya"),
                                                          onPressed: () {
                                                            submit(
                                                                BlocProvider.of<
                                                                            GlobalBloc>(
                                                                        context)
                                                                    .cartBloc
                                                                    .cart
                                                                    .saveService
                                                                    .cost[0]
                                                                    .value
                                                                    .toString(),
                                                                BlocProvider.of<
                                                                            GlobalBloc>(
                                                                        context)
                                                                    .cartBloc
                                                                    .cart
                                                                    .saveKurir,
                                                                BlocProvider.of<
                                                                            GlobalBloc>(
                                                                        context)
                                                                    .cartBloc
                                                                    .cart
                                                                    .saveService
                                                                    .service);
                                                            _resellerPage();
                                                            setState(() {});
                                                          },
                                                        ),
                                                      ],
                                                    );
                                                  },
                                                );
                                              } else {
                                                setState(() {
                                                  // isLoading = false;
                                                  _autovalidate = true;
                                                });
                                              }
                                            } else {
                                              _submitStore2();
                                              submit(
                                                  BlocProvider.of<GlobalBloc>(
                                                          context)
                                                      .cartBloc
                                                      .cart
                                                      .saveService
                                                      .cost[0]
                                                      .value
                                                      .toString(),
                                                  BlocProvider.of<GlobalBloc>(
                                                          context)
                                                      .cartBloc
                                                      .cart
                                                      .saveKurir,
                                                  BlocProvider.of<GlobalBloc>(
                                                          context)
                                                      .cartBloc
                                                      .cart
                                                      .saveService
                                                      .service);
                                              showDialog(
                                                context: context,
                                                builder:
                                                    (BuildContext context) {
                                                  return AlertDialog(
                                                    shape: RoundedRectangleBorder(
                                                        borderRadius:
                                                            new BorderRadius
                                                                    .circular(
                                                                10.0)),
                                                    content: new Text(
                                                      'Apakah data anda sudah benar?',
                                                      textAlign: TextAlign.left,
                                                    ),
                                                    actions: <Widget>[
                                                      FlatButton(
                                                        child: Text("Tidak"),
                                                        onPressed: () {
                                                          Navigator.of(context)
                                                              .pop();
                                                        },
                                                      ),
                                                      FlatButton(
                                                        child: new Text("Iya"),
                                                        onPressed: () {
                                                          submit(
                                                              BlocProvider.of<
                                                                          GlobalBloc>(
                                                                      context)
                                                                  .cartBloc
                                                                  .cart
                                                                  .saveService
                                                                  .cost[0]
                                                                  .value
                                                                  .toString(),
                                                              BlocProvider.of<
                                                                          GlobalBloc>(
                                                                      context)
                                                                  .cartBloc
                                                                  .cart
                                                                  .saveKurir,
                                                              BlocProvider.of<
                                                                          GlobalBloc>(
                                                                      context)
                                                                  .cartBloc
                                                                  .cart
                                                                  .saveService
                                                                  .service);
                                                          _resellerPage();
                                                          setState(() {});
                                                        },
                                                      ),
                                                    ],
                                                  );
                                                },
                                              );
                                            }
                                          } else {
                                            if (isSelected == true &&
                                                BlocProvider.of<GlobalBloc>(
                                                            context)
                                                        .cartBloc
                                                        .cart
                                                        .saveService !=
                                                    null) {
                                              final formState =
                                                  _formKeyDrop.currentState;
                                              if (formState.validate() &&
                                                  BlocProvider.of<GlobalBloc>(
                                                              context)
                                                          .cartBloc
                                                          .cart
                                                          .saveService !=
                                                      null) {
                                                _submitStore2();
                                                submit(
                                                    BlocProvider.of<GlobalBloc>(
                                                            context)
                                                        .cartBloc
                                                        .cart
                                                        .saveService
                                                        .cost[0]
                                                        .value
                                                        .toString(),
                                                    BlocProvider.of<GlobalBloc>(
                                                            context)
                                                        .cartBloc
                                                        .cart
                                                        .saveKurir,
                                                    BlocProvider.of<GlobalBloc>(
                                                            context)
                                                        .cartBloc
                                                        .cart
                                                        .saveService
                                                        .service);
                                                showDialog(
                                                  context: context,
                                                  builder:
                                                      (BuildContext context) {
                                                    return AlertDialog(
                                                      shape: RoundedRectangleBorder(
                                                          borderRadius:
                                                              new BorderRadius
                                                                      .circular(
                                                                  10.0)),
                                                      content: new Text(
                                                        'Apakah data anda sudah benar?',
                                                        textAlign:
                                                            TextAlign.left,
                                                      ),
                                                      actions: <Widget>[
                                                        FlatButton(
                                                          child: Text("Tidak"),
                                                          onPressed: () {
                                                            Navigator.of(
                                                                    context)
                                                                .pop();
                                                          },
                                                        ),
                                                        FlatButton(
                                                          child:
                                                              new Text("Iya"),
                                                          onPressed: () {
                                                            submit(
                                                                BlocProvider.of<
                                                                            GlobalBloc>(
                                                                        context)
                                                                    .cartBloc
                                                                    .cart
                                                                    .saveService
                                                                    .cost[0]
                                                                    .value
                                                                    .toString(),
                                                                BlocProvider.of<
                                                                            GlobalBloc>(
                                                                        context)
                                                                    .cartBloc
                                                                    .cart
                                                                    .saveKurir,
                                                                BlocProvider.of<
                                                                            GlobalBloc>(
                                                                        context)
                                                                    .cartBloc
                                                                    .cart
                                                                    .saveService
                                                                    .service);
                                                            _completeLogin();
                                                            setState(() {});
                                                          },
                                                        ),
                                                      ],
                                                    );
                                                  },
                                                );
                                              } else {
                                                setState(() {
                                                  // isLoading = false;
                                                  _autovalidate = true;
                                                });
                                              }
                                            } else if (BlocProvider.of<
                                                        GlobalBloc>(context)
                                                    .cartBloc
                                                    .cart
                                                    .saveService !=
                                                null) {
                                              _submitStore2();
                                              submit(
                                                  BlocProvider.of<GlobalBloc>(
                                                          context)
                                                      .cartBloc
                                                      .cart
                                                      .saveService
                                                      .cost[0]
                                                      .value
                                                      .toString(),
                                                  BlocProvider.of<GlobalBloc>(
                                                          context)
                                                      .cartBloc
                                                      .cart
                                                      .saveKurir,
                                                  BlocProvider.of<GlobalBloc>(
                                                          context)
                                                      .cartBloc
                                                      .cart
                                                      .saveService
                                                      .service);
                                              showDialog(
                                                context: context,
                                                builder:
                                                    (BuildContext context) {
                                                  return AlertDialog(
                                                    shape: RoundedRectangleBorder(
                                                        borderRadius:
                                                            new BorderRadius
                                                                    .circular(
                                                                10.0)),
                                                    content: new Text(
                                                      'Apakah data anda sudah benar?',
                                                      textAlign: TextAlign.left,
                                                    ),
                                                    actions: <Widget>[
                                                      FlatButton(
                                                        child: Text("Tidak"),
                                                        onPressed: () {
                                                          Navigator.of(context)
                                                              .pop();
                                                        },
                                                      ),
                                                      FlatButton(
                                                        child: new Text("Iya"),
                                                        onPressed: () {
                                                          submit(
                                                              BlocProvider.of<
                                                                          GlobalBloc>(
                                                                      context)
                                                                  .cartBloc
                                                                  .cart
                                                                  .saveService
                                                                  .cost[0]
                                                                  .value
                                                                  .toString(),
                                                              BlocProvider.of<
                                                                          GlobalBloc>(
                                                                      context)
                                                                  .cartBloc
                                                                  .cart
                                                                  .saveKurir,
                                                              BlocProvider.of<
                                                                          GlobalBloc>(
                                                                      context)
                                                                  .cartBloc
                                                                  .cart
                                                                  .saveService
                                                                  .service);
                                                          _completeLogin();
                                                          setState(() {});
                                                        },
                                                      ),
                                                    ],
                                                  );
                                                },
                                              );
                                            }
                                          }
                                        },
                                        child: Container(
                                          height: 55.0,
                                          width: mediaQD.size.width,
                                          color: ColorStyle.primaryColor,
                                          child: Center(
                                            child: Text(
                                              userLevel == '4' ||
                                                      userLevel == '5'
                                                  ? 'Submit'
                                                  : "Go to Payment",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w700,
                                                  fontSize: 16.5,
                                                  letterSpacing: 1.0),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                            ],
                          )),
                    ),
                  )
                : Container(
                    child: Center(
                      child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(
                              ColorStyle.primaryColor)),
                    ),
                  ),
          );
        });
  }
}
