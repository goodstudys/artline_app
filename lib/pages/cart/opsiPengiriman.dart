import 'dart:async';
import 'dart:convert';
import 'package:artline/bloc/global/BlocProvider.dart';
import 'package:artline/bloc/global/GlobalBloc.dart';
import 'package:artline/components/textFormatIDR.dart';
import 'package:artline/env.dart';
import 'package:artline/model/dataService.dart';
import 'package:artline/storage/storage.dart';
import 'package:artline/theme/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class OpsiPengiriman extends StatefulWidget {
  @override
  _OpsiPengirimanState createState() => _OpsiPengirimanState();
}

class _OpsiPengirimanState extends State<OpsiPengiriman> {
  DataStore storage = DataStore();
  // RefreshController _refreshController =
  //     RefreshController(initialRefresh: false);

  // List data;
  // bool isLoading = false;
  // List<DataAddress> addressApi;
  // List<DataProvince> listApiProvince;
  // List<DataCity> listApiCity;
  // DataCity cit;
  // DataProvince prov;

  // void _onRefresh() async {
  //   await Future.delayed(Duration(milliseconds: 1000));
  //   setState(() {
  //     getData().then((s) => setState(() {
  //           isLoading = true;
  //         }));
  //   });
  //   if (mounted)
  //     setState(() {
  //       getData().then((s) => setState(() {
  //             isLoading = true;
  //           }));
  //     });
  //   _refreshController.refreshCompleted();
  // }

  // void _onLoading() async {
  //   await Future.delayed(Duration(milliseconds: 1000));
  //   setState(() {
  //     getData().then((s) => setState(() {
  //           isLoading = true;
  //         }));
  //   });
  //   if (mounted)
  //     setState(() {
  //       getData().then((s) => setState(() {
  //             isLoading = true;
  //           }));
  //     });
  //   _refreshController.loadComplete();
  // }

  // Future<bool> nav() {
  //   return Navigator.of(context).pushReplacement(
  //       MaterialPageRoute(builder: (BuildContext context) => Delivery()));
  // }

  // startTime() async {
  //   return Timer(Duration(milliseconds: 1450), navigator);
  // }

  // void navigator() {
  //   Navigator.of(context).pushReplacement(
  //       PageRouteBuilder(pageBuilder: (_, __, ___) => new Delivery()));
  // }

  bool isLoading = false;
  List data;
  List<DataService> listService;
  String kurir;
  Future<String> getData() async {
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var $getBerat = BlocProvider.of<GlobalBloc>(context)
        .cartBloc
        .cart
        .totalWeight
        .toString();
    var $getcityId = await storage.getDataString('simpanKota');
    var $getKurir = await storage.getDataString('simpanKurir');
    var res = await http.post(url('api/getService'), headers: {
      'Authorization': $tokenType + ' ' + $accesToken,
      'Accept': 'application/json'
    }, body: {
      'tujuan': $getcityId,
      'berat': $getBerat,
      'kurir': $getKurir
    });
    setState(() {
      var content = json.decode(res.body);
      print(content);
      kurir = $getKurir;
      data = content['result'];
      listService = List<DataService>.from(content['result'][0]['costs']
          .map((item) => DataService.fromJson(item)));
      // data = content['address'];
      // addressApi = List<DataAddress>.from(
      //     content['address'].map((item) => DataAddress.fromJson(item)));
    });
    return 'success!';
  }

  // Future<String> getData2() async {
  //   var $tokenType = await storage.getDataString('token_type');
  //   var $accesToken = await storage.getDataString('access_token');
  //   var res = await http.get(url('api/formDelivery'),
  //       headers: {'Authorization': $tokenType + ' ' + $accesToken});

  //   setState(() {
  //     var content = json.decode(res.body);
  //     listApiProvince = List<DataProvince>.from(content['provinces']
  //             ['rajaongkir']['results']
  //         .map((item) => DataProvince.fromJson(item)));
  //     listApiCity = List<DataCity>.from(content['city']['rajaongkir']['results']
  //         .map((item) => DataCity.fromJson(item)));
  //   });

  //   //print(data);
  //   return 'success!';
  // }

  @override
  void initState() {
    super.initState();
    getData().then((s) => setState(() {
          isLoading = true;
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pilih Pengiriman'),
      ),
      body: isLoading
          ? Container(
              child: ListView.builder(
                itemCount: listService == null ? 0 : listService.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    child: InkWell(
                      onTap: () {
                        BlocProvider.of<GlobalBloc>(context)
                            .cartBloc
                            .addKurirItem(kurir);
                        BlocProvider.of<GlobalBloc>(context)
                            .cartBloc
                            .addServiceItem(listService[index]);
                        Navigator.of(context).pop(true);
                      },
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(top: 13.0, left: 13.0),
                                child: Text(listService[index].service),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 13.0, left: 8.0),
                                child: Text('( ' +
                                    listService[index].cost[0].etd +
                                    ' hari )'),
                              )
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 13.0, left: 13.0),
                            child: TextFormatIDR(
                              thisText: listService[index].cost[0].value,
                              thisStyle: TextStyle(),
                            ),
                          ),
                          Divider(
                            color: Colors.blue,
                          )
                        ],
                      ),
                    ),
                  );
                },
              ),
            )
          : Container(
              child: Center(
                child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(ColorStyle.primaryColor)),
              ),
            ),
    );
  }
}
