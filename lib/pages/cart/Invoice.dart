import 'dart:convert';

import 'package:artline/bloc/global/BlocProvider.dart';
import 'package:artline/bloc/global/GlobalBloc.dart';
import 'package:artline/components/cardInventory.dart';
import 'package:artline/components/textFormatIDR.dart';
import 'package:artline/env.dart';
import 'package:artline/pages/BottomNavigationBar.dart';
import 'package:artline/pages/profile/confirmation.dart';
import 'package:artline/storage/storage.dart';
import 'package:artline/theme/style.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class InvoiceLayout extends StatefulWidget {
  @override
  _InvoiceLayoutState createState() => _InvoiceLayoutState();
}

class _InvoiceLayoutState extends State<InvoiceLayout> {
  DataStore storage = DataStore();

  bool isLoading = false;
  String userLevel;

  // Future<bool> nav() {
  //   // return Navigator.of(context).pushReplacement(MaterialPageRoute(
  //   //     builder: (BuildContext context) => BottomNavigation()));
  //   return ,
  //   );
  // }
  Future<bool> _onWillPop() async {
    return (await showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            // title: new Text('Apakah anda ingin keluar dar halaman invoice?'),
            content: new Text('Apakah anda ingin keluar dari halaman invoice?'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('Tidak'),
              ),
              new FlatButton(
                onPressed: () {
                  clear();
                  clearShared();
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              BottomNavigation()),
                      ModalRoute.withName('/home'));
                },
                child: new Text('Iya'),
              ),
            ],
          ),
        )) ??
        false;
  }

  void clear() {
    BlocProvider.of<GlobalBloc>(context).cartBloc.clearCart();
  }

  clearShared() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('billProduct');
    prefs.remove('billNama');
    prefs.remove('billProvinsi');
    prefs.remove('billKota');
    prefs.remove('billAlamat');
    prefs.remove('billKecamatan');
    prefs.remove('billKelurahan');
    prefs.remove('billKodePos');
    prefs.remove('billTelp');
    prefs.remove('billEmail');
    prefs.remove('ongkir');
    prefs.remove('mustPay');
    prefs.remove('payment');
    prefs.remove('tujuan');
    prefs.remove('berat');
    prefs.remove('payOngkir');
  }

  void saveIdOrder(String id) async {
    storage.setDataString('idOrder', id);
  }

  var data;
  List listProduk;
  List produk;
  var prod;
  Future<String> getData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    prod = pref.getString('billProduct');
    var $tokenType = await storage.getDataString('token_type');
    var $userLevel = await storage.getDataString('userLevel');
    var $accesToken = await storage.getDataString('access_token');
    var res = await http.get(url('api/getLastOrder'), headers: {
      'Authorization': $tokenType + ' ' + $accesToken,
      'Accept': 'application/json'
    });
    setState(() {
      var content = json.decode(res.body);
      data = content['data'];
      listProduk =content['data']['detail'];
      userLevel = $userLevel;
    });
    return 'success!';
  }

  @override
  void initState() {
    super.initState();
    getData().then((s) => setState(() {
          isLoading = true;
        }));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
          appBar: AppBar(
            // leading: InkWell(
            //     onTap: () {
            //       Navigator.of(context).pop(false);
            //     },
            //     child: Icon(Icons.arrow_back)),
            elevation: 0.0,
            title: Text(
              "Invoice",
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 18.0,
                  color: Colors.black54,
                  fontFamily: "Gotik"),
            ),
            centerTitle: true,
            backgroundColor: Colors.white,
            iconTheme: IconThemeData(color: ColorStyle.primaryColor),
          ),
          body: isLoading
              ? Container(
                  height: MediaQuery.of(context).size.height,
                  child: Stack(
                    children: <Widget>[
                      SingleChildScrollView(
                        padding: EdgeInsets.only(bottom: 80.0),
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 25.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 25.0),
                                  child: Text(
                                    'Nomor pesanan telah dibuat',
                                    style: TextStyle(
                                      color: Colors.black,
                                      //fontWeight: FontWeight.bold,
                                      fontSize: 18.0,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 8.0, left: 25.0),
                                  child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      margin: EdgeInsets.only(
                                          bottom: 8.0, left: 0.0, right: 13.0),
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                          boxShadow: [
                                            BoxShadow(
                                                color: Colors.black12,
                                                blurRadius: 5,
                                                spreadRadius: 2,
                                                offset: Offset(5, 5))
                                          ]),
                                      child: Material(
                                          color: Colors.transparent,
                                          child: Padding(
                                              padding:
                                                  const EdgeInsets.all(13.0),
                                              child: Text(
                                                data['orderCode'],
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.bold),
                                              )))),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 8.0, left: 25.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        'Alamat Pengiriman',
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 18.0,
                                        ),
                                      ),
                                      Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          margin: EdgeInsets.only(
                                              bottom: 8.0,
                                              left: 0.0,
                                              right: 13.0),
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              boxShadow: [
                                                BoxShadow(
                                                    color: Colors.black12,
                                                    blurRadius: 5,
                                                    spreadRadius: 2,
                                                    offset: Offset(5, 5))
                                              ]),
                                          child: Material(
                                              color: Colors.transparent,
                                              child: Padding(
                                                  padding:
                                                      const EdgeInsets.all(8.0),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    children: <Widget>[
                                                      Text(
                                                        data['billName'],
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                      Text(data['billPhone']),
                                                      Text(data['billAddress']),
                                                      Text(data['billCity'] +
                                                          ', ' +
                                                          data[
                                                              'billKecamatan'] +
                                                          ', ' +
                                                          data['billProvince']),
                                                      Text(data['billKodePos'])
                                                    ],
                                                  )))),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 8.0, left: 25.0, right: 20.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text(
                                            'Daftar Barang',
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 18.0,
                                            ),
                                          ),
                                          Text(
                                            'On Proccess',
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 18.0,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 8.0, left: 13.0),
                                  child: ListView.builder(
                                    physics: NeverScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    itemCount:
                                        listProduk == null ? 0 : listProduk.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return CardInventory(
                                        type: 'listbarang',
                                        image: host +
                                            'upload/users/products/' +
                                            listProduk[index]['image'],
                                        name: listProduk[index]['name'],
                                        price: listProduk[index]['price'],
                                        discAmount: listProduk[index]['discAmount'],
                                        discPercent: listProduk[index]['discPercent'],
                                        // totalPrice: int.parse(listProduk[index]['price']),
                                        qty: listProduk[index]['quantity'],
                                      );
                                    },
                                  ),
                                ),
                                // userLevel == '4' || userLevel == '5'
                                //     ? Container()
                                //     : 
                                    Padding(
                                        padding: const EdgeInsets.only(
                                            top: 8.0, left: 25.0),
                                        child: Container(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            margin: EdgeInsets.only(
                                                bottom: 8.0,
                                                left: 0.0,
                                                right: 13.0),
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius:
                                                    BorderRadius.circular(10.0),
                                                boxShadow: [
                                                  BoxShadow(
                                                      color: Colors.black12,
                                                      blurRadius: 5,
                                                      spreadRadius: 2,
                                                      offset: Offset(5, 5))
                                                ]),
                                            child: Material(
                                                color: Colors.transparent,
                                                child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            13.0),
                                                    child: Row(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: <Widget>[
                                                        Text(
                                                          'Biaya Ongkir',
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        ),
                                                        TextFormatIDR(
                                                          thisText: 
                                                              data[
                                                                  'kurirRate'],
                                                          thisStyle: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        )
                                                      ],
                                                    )))),
                                      ),
                                // userLevel == '4' || userLevel == '5'
                                //     ? Container()
                                //     :
                                     Padding(
                                        padding: const EdgeInsets.only(
                                            top: 8.0, left: 25.0),
                                        child: Container(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            margin: EdgeInsets.only(
                                                bottom: 8.0,
                                                left: 0.0,
                                                right: 13.0),
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius:
                                                    BorderRadius.circular(10.0),
                                                boxShadow: [
                                                  BoxShadow(
                                                      color: Colors.black12,
                                                      blurRadius: 5,
                                                      spreadRadius: 2,
                                                      offset: Offset(5, 5))
                                                ]),
                                            child: Material(
                                                color: Colors.transparent,
                                                child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            13.0),
                                                    child: Row(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: <Widget>[
                                                        Text(
                                                          'Total',
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        ),
                                                        TextFormatIDR(
                                                          thisText:
                                                              data['total'],
                                                          thisStyle: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        )
                                                      ],
                                                    )))),
                                      ),
                                // Padding(
                                //   padding: const EdgeInsets.only(
                                //       top: 8.0, left: 25.0),
                                //   child: Column(
                                //     crossAxisAlignment:
                                //         CrossAxisAlignment.start,
                                //     children: <Widget>[
                                //       Text(
                                //         'Pengiriman',
                                //         style: TextStyle(
                                //           color: Colors.black,
                                //           fontWeight: FontWeight.w500,
                                //           fontSize: 18.0,
                                //         ),
                                //       ),
                                //       // namaShipping == null ? Text('...') : Text(namaShipping)
                                //     ],
                                //   ),
                                // ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 8.0),
                                  child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    height: MediaQuery.of(context).size.height /
                                        4.5,
                                    decoration: BoxDecoration(
                                        color: Colors.grey[300],
                                        boxShadow: [
                                          BoxShadow(
                                              color: Colors.black38,
                                              blurRadius: 5)
                                        ]),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          top: 15.0, left: 25.0, right: 25.0),
                                      child: Text(
                                          //  \n\njumlah barang dan transaksi akan menyesuaikan dengan stok yang ada
                                          'Segera lakukan pembayaran dikarenakan Stok diatas dapat dapat berubah sewaktu-waktu. \n\nPesanan anda akan dibatalkan otomatis jika belum melakukan pembayaran selama 1x24 jam.'),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      userLevel == '4' || userLevel == '5'
                          ? Container()
                          : Positioned(
                              bottom: 15.0,
                              left: 8,
                              right: 8,
                              child: Center(
                                child: InkWell(
                                  onTap: () {
                                    saveIdOrder(data['id'].toString());
                                    clear();
                                    clearShared();
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                Confirmation()));
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: ColorStyle.primaryColor,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(25.0))),
                                    height: 50.0,
                                    width:
                                        MediaQuery.of(context).size.width / 1.2,
                                    child: Center(
                                        child: Text(
                                      'Konfirmasi Pembayaran',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20.0),
                                    )),
                                  ),
                                ),
                              ),
                            )
                    ],
                  ),
                )
              : Container(
                  child: Center(
                    child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(ColorStyle.primaryColor)),
                  ),
                )),
    );
  }
}
