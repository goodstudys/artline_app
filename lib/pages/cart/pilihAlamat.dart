import 'dart:async';
import 'dart:convert';
import 'package:artline/bloc/global/BlocProvider.dart';
import 'package:artline/bloc/global/GlobalBloc.dart';
import 'package:artline/components/cardAddress.dart';
import 'package:artline/env.dart';
import 'package:artline/model/dataAddress.dart';
import 'package:artline/model/dataCity.dart';
import 'package:artline/model/dataProvince.dart';
import 'package:artline/pages/profile/newAddress.dart';
import 'package:artline/storage/storage.dart';
import 'package:artline/theme/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pull_to_refresh/pull_to_refresh.dart';

class PilihAlamat extends StatefulWidget {
  @override
  _PilihAlamatState createState() => _PilihAlamatState();
}

class _PilihAlamatState extends State<PilihAlamat> {
  DataStore storage = DataStore();
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  List data;
  bool isLoading = false;
  List<DataAddress> addressApi;
  List<DataProvince> listApiProvince;
  List<DataCity> listApiCity;
  DataCity cit;
  DataProvince prov;

  void _onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      getData().then((s) => setState(() {
            isLoading = true;
          }));
    });
    if (mounted)
      setState(() {
        getData().then((s) => setState(() {
              isLoading = true;
            }));
      });
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      getData().then((s) => setState(() {
            isLoading = true;
          }));
    });
    if (mounted)
      setState(() {
        getData().then((s) => setState(() {
              isLoading = true;
            }));
      });
    _refreshController.loadComplete();
  }

  // Future<bool> nav() {
  //   return Navigator.of(context).pushReplacement(
  //       MaterialPageRoute(builder: (BuildContext context) => Delivery()));
  // }

  // startTime() async {
  //   return Timer(Duration(milliseconds: 1450), navigator);
  // }

  // void navigator() {
  //   Navigator.of(context).pushReplacement(
  //       PageRouteBuilder(pageBuilder: (_, __, ___) => new Delivery()));
  // }

  Future<String> getData() async {
    setState(() {
      isLoading = true;
    });
    getData2();
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var res = await http.get(url('api/getAddress'), headers: {
      'Authorization': $tokenType + ' ' + $accesToken,
      'Accept': 'application/json'
    });
    setState(() {
      var content = json.decode(res.body);
      data = content['address'];
      addressApi = List<DataAddress>.from(
          content['address'].map((item) => DataAddress.fromJson(item)));
    });
        setState(() {
      isLoading = false;
    });
    return 'success!';
  }

  Future<String> getData2() async {
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var res = await http.get(url('api/formDelivery'),
        headers: {'Authorization': $tokenType + ' ' + $accesToken});

    setState(() {
      var content = json.decode(res.body);
      listApiProvince = List<DataProvince>.from(content['provinces']
              ['rajaongkir']['results']
          .map((item) => DataProvince.fromJson(item)));
      listApiCity = List<DataCity>.from(content['city']['rajaongkir']['results']
          .map((item) => DataCity.fromJson(item)));
    });

    //print(data);
    return 'success!';
  }

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pilih Alamat'),
        // leading: InkWell(
        //     onTap: () {
        //       Navigator.of(context).pushReplacement(MaterialPageRoute(
        //           builder: (BuildContext context) => Delivery()));
        //     },
        //     child: Icon(Icons.arrow_back)),
      ),
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        header: WaterDropHeader(),
        footer: CustomFooter(
          builder: (BuildContext context, LoadStatus mode) {
            Widget body;
            if (mode == LoadStatus.idle) {
              body = Text("pull up load");
            } else if (mode == LoadStatus.loading) {
              body = CupertinoActivityIndicator();
            } else if (mode == LoadStatus.failed) {
              body = Text("Load Failed!Click retry!");
            } else if (mode == LoadStatus.canLoading) {
              body = Text("release to load more");
            } else {
              body = Text("No more Data");
            }
            return Container(
              height: 55.0,
              child: Center(child: body),
            );
          },
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: isLoading ||
                addressApi != null ||
                listApiProvince != null ||
                listApiCity != null
            ? SingleChildScrollView(
                child: Container(
                  child: Column(
                    children: <Widget>[
                      listApiCity == null || listApiProvince == null
                          ? Container(
                              child: Center(
                                child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(ColorStyle.primaryColor)),
                              ),
                            )
                          : ListView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: data == null ? 0 : data.length,
                              itemBuilder: (BuildContext context, int index) {
                                if (listApiCity != null ||
                                    listApiProvince != null) {
                                  listApiCity.forEach((f) {
                                    if (f.cityId == addressApi[index].city) {
                                      cit = f;
                                    }
                                  });
                                  listApiProvince.forEach((p) {
                                    if (p.provinceId ==
                                        addressApi[index].province) {
                                      prov = p;
                                    }
                                  });
                                }
                                return CardAddress(
                                  type: 'pilihAlamat',
                                  tap: () {
                                    if (BlocProvider.of<GlobalBloc>(context)
                                                .cartBloc
                                                .cart
                                                .saveAddress ==
                                            null ||
                                        BlocProvider.of<GlobalBloc>(context)
                                                .cartBloc
                                                .cart
                                                .saveAddress
                                                .city ==
                                            addressApi[index].city) {
                                      BlocProvider.of<GlobalBloc>(context)
                                          .cartBloc
                                          .addAddress
                                          .add(addressApi[index]);
                                      // startTime();
                                      Navigator.of(context).pop(true);
                                      print('GAAAKKKCLEARRRRR');
                                    } else if (BlocProvider.of<GlobalBloc>(
                                                context)
                                            .cartBloc
                                            .cart
                                            .saveAddress
                                            .city !=
                                        addressApi[index].city) {
                                      BlocProvider.of<GlobalBloc>(context)
                                          .cartBloc
                                          .clearOpsiPengiriman(null);
                                      BlocProvider.of<GlobalBloc>(context)
                                          .cartBloc
                                          .addAddress
                                          .add(addressApi[index]);
                                      // startTime();
                                      Navigator.of(context).pop(true);
                                      print('CLEARRRRR');
                                    }
                                  },
                                  address: addressApi[index].address,
                                  cityName: cit,
                                  city: addressApi[index].city,
                                  provinceName: prov,
                                  kecamatan: addressApi[index].kecamatan,
                                  kodePos: addressApi[index].kodePos,
                                  name: addressApi[index].name,
                                  phone: addressApi[index].phone,
                                  province: addressApi[index].province,
                                );
                              },
                            ),
                      Container(
                          margin: EdgeInsets.only(
                              bottom: 8.0, left: 13.0, right: 13.0, top: 13.0),
                          padding: EdgeInsets.all(13.0),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10.0),
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.black12,
                                    blurRadius: 5,
                                    spreadRadius: 2,
                                    offset: Offset(5, 5))
                              ]),
                          child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                onTap: () {
                                  Navigator.of(context).push(PageRouteBuilder(
                                      pageBuilder: (_, __, ___) =>
                                          new NewAddress()));
                                },
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'Tambahkan Alamat Baru',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      '+',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    )
                                  ],
                                ),
                              )))
                    ],
                  ),
                ),
              )
            : Container(
                child: Center(
                  child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(ColorStyle.primaryColor)),
                ),
              ),
      ),
    );
  }
}
