import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:artline/components/textFormRadius.dart';
import 'package:artline/pages/BottomNavigationBar.dart';
import 'package:artline/storage/storage.dart';
import 'package:artline/theme/style.dart';
import 'package:http/http.dart' as http;
import 'package:artline/env.dart';
import 'package:flutter/material.dart';
import 'package:artline/pages/auth/LoginAnimation.dart';
import 'package:artline/pages/auth/Signup.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

Map<String, String> requestHeaders = Map();

/// Component Widget this layout UI
class _LoginScreenState extends State<LoginScreen>
    with TickerProviderStateMixin {
  //Animation Declaration
  AnimationController sanimationController;
  TextEditingController _emailControl = new TextEditingController();
  TextEditingController _passwordControl = new TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  DataStore dataStore = DataStore();
  bool _autovalidate = false;
  bool _hidepass = true;
  var tap = 0;
  bool _isLoading = false;

  void showPop(String txt) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(10.0)),
          content: new Text(
            txt,
            textAlign: TextAlign.left,
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _showhide() {
    setState(() {
      _hidepass = !_hidepass;
    });
  }

  _login() async {
    setState(() {
      _isLoading = true;
    });
    try {
      final getToken = await http.post(url('api/login'), body: {
        "email": _emailControl.text,
        "password": _passwordControl.text,
      });

      var getTokenDecode = json.decode(getToken.body);

      if (getToken.statusCode == 200) {
        if (getTokenDecode['error'] == 'Unautorized') {
          showPop(getTokenDecode['message']);
          setState(() {
            _isLoading = false;
          });
        } else if (getTokenDecode['status'] == 'success') {
          dataStore.setDataString('access_token', getTokenDecode['token']);
          dataStore.setDataString('token_type', getTokenDecode['token_type']);
        }
        dynamic tokenType = getTokenDecode['token_type'];
        dynamic accessToken = getTokenDecode['token'];
        requestHeaders['Accept'] = 'application/json';
        requestHeaders['Authorization'] = '$tokenType $accessToken';

        try {
          // final getUser =
          //     await http.post(url("api/details"), headers: requestHeaders);

          // print('getUser ' + getUser.body);
          var user = getTokenDecode['user'];
          if (user != null) {
            // dynamic datauser = json.decode(getUser.body);
            dataStore.setDataString("id", user['id'].toString());
            dataStore.setDataString("name", user['displayName']);
            dataStore.setDataString("email", user['email']);
            dataStore.setDataString('userPointer', user['rowPointer']);
            dataStore.setDataString('userImage', user['image']);
            dataStore.setDataString('userLevel', user['level']);
            setState(() {
              _isLoading = false;
            });
            // playAnimation();
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (BuildContext context) => BottomNavigation()));
          } else {
            showPop('Request failed with status: ${user.statusCode}');
            setState(() {
              _isLoading = false;
            });
          }
        } on SocketException catch (_) {
          showPop('Connection Timed Out');
          setState(() {
            _isLoading = false;
          });
        } catch (e) {
          print(e);
          setState(() {
            _isLoading = false;
          });
        }
      } else if (getToken.statusCode == 401) {
        showPop('Incorrect email or password');
        setState(() {
          _isLoading = false;
        });
      } else {
        showPop('Request failed with status: ${getToken.statusCode}');
        setState(() {
          _isLoading = false;
        });
      }
      // print(datajson.toString());

    } on SocketException catch (_) {
      showPop('Connection Timed Out');
      setState(() {
        _isLoading = false;
      });
    } catch (e) {
      print(e);
      setState(() {
        _isLoading = false;
      });
      // showInSnackBar(e);
    }
  }

  @override

  /// set state animation controller
  void initState() {
    sanimationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 800))
          ..addStatusListener((statuss) {
            if (statuss == AnimationStatus.dismissed) {
              setState(() {
                tap = 0;
              });
            }
          });
    super.initState();
  }

  /// Dispose animation controller
  @override
  void dispose() {
    super.dispose();
    sanimationController.dispose();
  }

  /// Playanimation set forward reverse
  Future<Null> playAnimation() async {
    try {
      await sanimationController.forward();
      await sanimationController.reverse();
    } on TickerCanceled {}
  }

  /// Component Widget layout UI
  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQD = MediaQuery.of(context);
    // mediaQD.devicePixelRatio;
    mediaQD.size.width;
    mediaQD.size.height;
    return Stack(
      children: <Widget>[
        Scaffold(
          backgroundColor: Colors.white,
          body: Container(
            /// Set Background image in layout (Click to open code)
            decoration: BoxDecoration(
                image: DecorationImage(
              image: AssetImage("assets/img/loginscreenbackground.png"),
              fit: BoxFit.cover,
            )),
            child: Container(
              /// Set gradient color in image (Click to open code)
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Color.fromRGBO(0, 0, 0, 0.0),
                    Color.fromRGBO(0, 0, 0, 0.3)
                  ],
                  begin: FractionalOffset.topCenter,
                  end: FractionalOffset.bottomCenter,
                ),
              ),

              /// Set component layout
              child: ListView(
                children: <Widget>[
                  Stack(
                    alignment: AlignmentDirectional.bottomCenter,
                    children: <Widget>[
                      Container(
                        alignment: AlignmentDirectional.topCenter,
                        child: Column(
                          children: <Widget>[
                            /// padding logo
                            Padding(
                              padding: EdgeInsets.only(top: mediaQD.padding.top + 25.0)
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10.0),
                              child: Image(
                                image: AssetImage(
                                    "assets/img/artline_logo.png"),
                                height: 50.0,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 60.0, bottom: 20.0),
                              child: Container(
                                width: mediaQD.size.width / 1.2,
                                child: Text(
                                  'Login',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    color: ColorStyle.fontColorDark,
                                    letterSpacing: 0.2,
                                    fontFamily: "Roboto",
                                    fontSize: 30.0,
                                    fontWeight: FontWeight.w900,
                                  ),
                                )
                              )
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 5.0, bottom: 15.0),
                              width: mediaQD.size.width / 1.2,
                              child: Text(
                                "Login with your registered email and password",
                                style: TextStyle(
                                  color:
                                      ColorStyle.fontSecondaryColorDark,
                                  fontSize: 13.0,
                                  fontWeight: FontWeight.w400,
                                  letterSpacing: 1,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            // /// ButtonCustomFacebook
                            // Padding(
                            //     padding:
                            //         EdgeInsets.symmetric(vertical: 30.0)),
                            // ButtonCustomFacebook(),

                            // /// ButtonCustomGoogle
                            // Padding(
                            //     padding:
                            //         EdgeInsets.symmetric(vertical: 7.0)),
                            // ButtonCustomGoogle(),

                            /// Set Text
                            // Padding(
                            //     padding:
                            //         EdgeInsets.symmetric(vertical: 30.0)),
                            // Text(
                            //   "OR",
                            //   style: TextStyle(
                            //       fontWeight: FontWeight.w900,
                            //       color: Colors.white,
                            //       letterSpacing: 0.2,
                            //       fontFamily: 'Sans',
                            //       fontSize: 17.0),
                            // ),
                            Form(
                                key: _formKey,
                                child: Column(
                                  children: <Widget>[
                                    /// TextFromField Email
                                    Padding(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 10.0)),
                                    TextFromField(
                                      controller: _emailControl,
                                      autovalidate: _autovalidate,
                                      icon: Icons.email,
                                      password: false,
                                      email: "Email",
                                      inputType: TextInputType.emailAddress,
                                    ),

                                    /// TextFromField Password
                                    Padding(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 5.0)),
                                    TextFromField(
                                      controller: _passwordControl,
                                      autovalidate: _autovalidate,
                                      icon: Icons.vpn_key,
                                      password: true,
                                      tapHide: () {
                                        _showhide();
                                      },
                                      obscure: _hidepass,
                                      email: "Password",
                                      inputType: TextInputType.text,
                                    ),
                                  ],
                                )),

                            /// Button Signup
                            FlatButton(
                                padding: EdgeInsets.only(top: 20.0),
                                onPressed: () {
                                  Navigator.of(context).pushReplacement(
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              new Signup()));
                                },
                                child: Text(
                                  "Not Have Acount? Sign Up",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 13.0,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: "Sans"),
                                )),
                            Padding(
                              padding: EdgeInsets.only(
                                  top: mediaQD.padding.top + 100.0,
                                  bottom: 0.0),
                            )
                          ],
                        ),
                      ),

                      /// Set Animaion after user click buttonLogin
                      tap == 0
                          ? InkWell(
                              splashColor: Colors.yellow,
                              onTap: () async {
                                final formState = _formKey.currentState;
                                if (formState.validate()) {
                                  _login();
                                } else {
                                  setState(() {
                                    // isLoading = false;
                                    _autovalidate = true;
                                  });
                                }
                              },
                              // () {
                              //   setState(() {
                              //     tap = 1;
                              //   });
                              //   _login();
                              //   // new LoginAnimation(
                              //   //   animationController: sanimationController.view,
                              //   // );
                              //   // playAnimation();
                              //   return tap;
                              // },
                              child: ButtonBlackBottom(),
                            )
                          : new LoginAnimation(
                              animationController: sanimationController.view,
                            )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
        _isLoading
            ? Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              )
            : Center(),
      ],
    );
  }
}

///buttonCustomFacebook class
class ButtonCustomFacebook extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30.0),
      child: Container(
        alignment: FractionalOffset.center,
        height: 49.0,
        width: 500.0,
        decoration: BoxDecoration(
          color: Color.fromRGBO(107, 112, 248, 1.0),
          borderRadius: BorderRadius.circular(40.0),
          boxShadow: [BoxShadow(color: Colors.black26, blurRadius: 15.0)],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/img/icon_facebook.png",
              height: 25.0,
            ),
            Padding(padding: EdgeInsets.symmetric(horizontal: 7.0)),
            Text(
              "Login With Facebook",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 15.0,
                  fontWeight: FontWeight.w500,
                  fontFamily: 'Sans'),
            ),
          ],
        ),
      ),
    );
  }
}

///buttonCustomGoogle class
class ButtonCustomGoogle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30.0),
      child: Container(
        alignment: FractionalOffset.center,
        height: 49.0,
        width: 500.0,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 10.0)],
          borderRadius: BorderRadius.circular(40.0),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/img/google.png",
              height: 25.0,
            ),
            Padding(padding: EdgeInsets.symmetric(horizontal: 7.0)),
            Text(
              "Login With Google",
              style: TextStyle(
                  color: Colors.black26,
                  fontSize: 15.0,
                  fontWeight: FontWeight.w500,
                  fontFamily: 'Sans'),
            )
          ],
        ),
      ),
    );
  }
}

///ButtonBlack class
class ButtonBlackBottom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(30.0),
      child: Container(
        height: 55.0,
        width: 600.0,
        child: Text(
          "Login",
          style: TextStyle(
              color: Colors.white,
              letterSpacing: 0.2,
              fontFamily: "Sans",
              fontSize: 18.0,
              fontWeight: FontWeight.w800),
        ),
        alignment: FractionalOffset.center,
        decoration: BoxDecoration(
            boxShadow: [BoxShadow(color: Colors.black38, blurRadius: 15.0)],
            borderRadius: BorderRadius.circular(30.0),
            gradient: LinearGradient(
                colors: <Color>[Color(0xFF121940), Color(0xFF6E48AA)])),
      ),
    );
  }
}
