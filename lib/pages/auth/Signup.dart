import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:artline/components/dialogSuccess.dart';
import 'package:artline/components/textFormRadius.dart';
import 'package:artline/env.dart';
import 'package:artline/storage/storage.dart';
import 'package:flutter/material.dart';
import 'package:artline/pages/auth/Login.dart';
import 'package:artline/pages/auth/LoginAnimation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;
import 'package:mime/mime.dart';
import 'package:http_parser/http_parser.dart';

class Signup extends StatefulWidget {
  Signup({this.radius = 45.0});
  final double radius;
  @override
  _SignupState createState() => _SignupState();
}

class _SignupState extends State<Signup> with TickerProviderStateMixin {
  //Animation Declaration
  AnimationController sanimationController;
  AnimationController animationControllerScreen;
  Animation animationScreen;

  TextEditingController _emailControl = new TextEditingController();
  TextEditingController _passwordControl = new TextEditingController();
  TextEditingController _firstName = new TextEditingController();
  TextEditingController _lastName = new TextEditingController();
  TextEditingController _displayName = new TextEditingController();
  TextEditingController _company = new TextEditingController();
  TextEditingController _phone = new TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  DataStore storage = DataStore();
  bool _autovalidate = false;
  var tap = 0;
  bool _isUploading = false;
  File _image;
  bool _hidepass = true;
  String base64Image;

  void _showhide() {
    setState(() {
      _hidepass = !_hidepass;
    });
  }

  Future getImage(ImageSource media) async {
    try {
      File image = await ImagePicker.pickImage(
        source: media,
        maxWidth: 800,
      );
      setState(() {
        _image = image;
      });
      // if (_image != null) {
      //   uploadImage(_image);
      //   // Closes the bottom sheet
      //   // Navigator.pop(context);
      // }
    } catch (error) {
      // Navigator.pop(context);
      print('Failed!  ' + error.toString());
    }
  }

  void myAlert() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
            title: Text('Please choose media to select'),
            content: Container(
              height: MediaQuery.of(context).size.height / 6,
              child: Column(
                children: <Widget>[
                  FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                      getImage(ImageSource.gallery);
                    },
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.image),
                        Text('From Gallery'),
                      ],
                    ),
                  ),
                  FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                      getImage(ImageSource.camera);
                    },
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.camera),
                        Text('From Camera'),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  Map<String, String> baseHeaders = {
    "Content-type": "application/json",
    "accept": "application/json",
  };
  Future<dynamic> uploadImage(File image) async {
    setState(() {
      _isUploading = true;
    });
    // baseHeaders.addAll({
    //   "Authorization": tokenType + ' ' + accessToken,
    // });
    // Find the mime type of the selected file by looking at the header bytes of the file
    final mimeTypeData =
        lookupMimeType(image.path, headerBytes: [0xFF, 0xD8]).split('/');
    // Intilize the multipart request
    final request =
        http.MultipartRequest('POST', Uri.parse(url('api/register')))
          ..fields['firstName'] = _firstName.text
          ..fields['lastName'] = _firstName.text
          ..fields['phone'] = _phone.text
          ..fields['displayName'] = _displayName.text
          ..fields['company'] = _company.text
          ..fields['email'] = _emailControl.text
          ..fields['password'] = _passwordControl.text
          ..files.add(await http.MultipartFile.fromPath('image', image.path,
              contentType: MediaType(mimeTypeData[0], mimeTypeData[1])));
    // Set Header
    request.headers.addAll(baseHeaders);

    try {
      final streamedResponse = await request.send();
      final response = await http.Response.fromStream(streamedResponse);
      var content = json.decode(response.body);
      final int statusCode = response.statusCode;
      if (statusCode == 204) {
        throw new Exception("No record found.");
      } else if (statusCode == 400) {
        throw new Exception("Invalid request parameter");
      } else if (statusCode == 401) {
        throw new Exception(
            "Unauthorized Access, please check your credentials");
      } else if (statusCode == 500) {
        throw new Exception("Server error");
      } else if (json == null) {
        throw new Exception("Content not found");
      }
      if (content['status'] == 'success') {
        setState(() {
          _isUploading = false;
        });
        showDialog(
            context: context,
            builder: (context) {
              Future.delayed(Duration(seconds: 5), () {
                Navigator.of(context).pop(true);
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                    builder: (BuildContext context) => LoginScreen()));
              });
              return DialogSuccess(
                type: true,
                txtHead: content['status'] + '!!!',
                txtSub: content['message'],
              );
            });
      } else if (content['status'] == 'error') {
        setState(() {
          _isUploading = false;
        });
        showDialog(
            context: context,
            builder: (context) {
              Future.delayed(Duration(seconds: 5), () {
                Navigator.of(context).pop(true);
                // Navigator.of(context).pushReplacement(MaterialPageRoute(
                //     builder: (BuildContext context) => LoginScreen()));
              });
              return DialogSuccess(
                type: false,
                txtHead: content['status'] + '!!!',
                txtSub: content['message'],
              );
            });
      }

      return print(content);
    } catch (e) {
      print(e);
      return null;
    }
  }

  /// Set AnimationController to initState
  @override
  void initState() {
    sanimationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 800))
          ..addStatusListener((statuss) {
            if (statuss == AnimationStatus.dismissed) {
              setState(() {
                tap = 0;
              });
            }
          });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    sanimationController.dispose();
  }

  /// Component Widget layout UI
  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    mediaQueryData.size.height;
    mediaQueryData.size.width;

    return Scaffold(
      body: Stack(
        children: <Widget>[
          _isUploading
              ? Container(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                )
              : Container(
                  /// Set Background image in layout
                  decoration: BoxDecoration(
                      image: DecorationImage(
                    image: AssetImage("assets/img/loginscreenbackground.png"),
                    fit: BoxFit.cover,
                  )),
                  child: Container(
                    /// Set gradient color in image
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Color.fromRGBO(0, 0, 0, 0.2),
                          Color.fromRGBO(0, 0, 0, 0.3)
                        ],
                        begin: FractionalOffset.topCenter,
                        end: FractionalOffset.bottomCenter,
                      ),
                    ),

                    /// Set component layout
                    child: ListView(
                      padding: EdgeInsets.all(0.0),
                      children: <Widget>[
                        Stack(
                          alignment: AlignmentDirectional.bottomCenter,
                          children: <Widget>[
                            Column(
                              children: <Widget>[
                                Container(
                                  alignment: AlignmentDirectional.topCenter,
                                  child: Column(
                                    children: <Widget>[
                                      /// padding logo
                                      Padding(
                                          padding: EdgeInsets.only(
                                              top: mediaQueryData.padding.top +
                                                  40.0)),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Image(
                                            image: AssetImage(
                                                "assets/img/artline_logo.png"),
                                            height: 70.0,
                                          ),
                                          /*Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10.0)),
                                    /// Animation text Artline Shop accept from login layout
                                    Hero(
                                      tag: "Artline",
                                      child: Text(
                                        "Artline Shop",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w900,
                                            letterSpacing: 0.6,
                                            fontFamily: "Sans",
                                            color: Colors.white,
                                            fontSize: 20.0),
                                      ),
                                    ),*/
                                        ],
                                      ),
                                      Form(
                                          key: _formKey,
                                          child: Column(
                                            children: <Widget>[
                                              Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 20.0)),
                                              Stack(children: <Widget>[
                                                _isUploading
                                                    ? Container(
                                                        padding:
                                                            EdgeInsets.only(
                                                                top: 6.0,
                                                                right: 6.0),
                                                        child: ClipRRect(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(widget
                                                                        .radius),
                                                            child: Container(
                                                                width: 75,
                                                                height: 75,
                                                                padding:
                                                                    EdgeInsets
                                                                        .all(
                                                                            20.0),
                                                                color: Colors
                                                                    .white,
                                                                child:
                                                                    CircularProgressIndicator())),
                                                      )
                                                    : Container(
                                                        padding:
                                                            EdgeInsets.only(
                                                                top: 6.0,
                                                                right: 6.0),
                                                        child: Container(
                                                            padding:
                                                                EdgeInsets.all(
                                                                    2.0),
                                                            height: 75.0,
                                                            width: 75.0,
                                                            decoration:
                                                                new BoxDecoration(
                                                              color:
                                                                  Colors.white,
                                                              shape: BoxShape
                                                                  .circle,
                                                            ),
                                                            child: ClipRRect(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(widget
                                                                            .radius),
                                                                child: _image ==
                                                                        null
                                                                    ? Icon(Icons
                                                                        .camera_alt)
                                                                    : Image
                                                                        .file(
                                                                        _image,
                                                                        //placeholder: 'assets/img/default-image.png',
                                                                        height:
                                                                            75.0,
                                                                        width:
                                                                            75.0,
                                                                        fit: BoxFit
                                                                            .cover,
                                                                      )))),
                                                Positioned(
                                                  top: 0.0,
                                                  right: 0.0,
                                                  child: InkWell(
                                                    onTap: () {
                                                      myAlert();
                                                    },
                                                    child: Container(
                                                      padding:
                                                          EdgeInsets.all(6.0),
                                                      decoration: BoxDecoration(
                                                        color: Colors.indigo,
                                                        shape: BoxShape.circle,
                                                      ),
                                                      constraints:
                                                          BoxConstraints(
                                                        minWidth: 20.0,
                                                        minHeight: 20.0,
                                                      ),
                                                      child: Center(
                                                        child: Icon(
                                                          Icons.camera_alt,
                                                          color: Colors.white,
                                                          size: 20,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ]),

                                              Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 5.0)),

                                              /// TextFromField Email
                                              TextFromField(
                                                controller: _firstName,
                                                autovalidate: _autovalidate,
                                                icon: Icons.account_circle,
                                                password: false,
                                                email: "First Name",
                                                inputType: TextInputType.text,
                                              ),
                                              Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 5.0)),

                                              /// TextFromField Email
                                              TextFromField(
                                                controller: _lastName,
                                                autovalidate: _autovalidate,
                                                icon: Icons.account_circle,
                                                password: false,
                                                email: "Last Name",
                                                inputType: TextInputType.text,
                                              ),
                                              Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 5.0)),

                                              /// TextFromField Email
                                              TextFromField(
                                                controller: _displayName,
                                                autovalidate: _autovalidate,
                                                icon:
                                                    Icons.perm_contact_calendar,
                                                password: false,
                                                email: "Display Name",
                                                inputType:
                                                    TextInputType.emailAddress,
                                              ),
                                              Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 5.0)),

                                              /// TextFromField Email
                                              TextFromField(
                                                controller: _company,
                                                autovalidate: _autovalidate,
                                                icon: Icons.account_circle,
                                                password: false,
                                                email: "Company",
                                                inputType: TextInputType.text,
                                              ),
                                              Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 5.0)),

                                              /// TextFromField Email
                                              TextFromField(
                                                controller: _phone,
                                                autovalidate: _autovalidate,
                                                icon: Icons.phone,
                                                password: false,
                                                email: "phone",
                                                inputType: TextInputType.phone,
                                              ),
                                              Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 5.0)),

                                              /// TextFromField Email
                                              TextFromField(
                                                controller: _emailControl,
                                                autovalidate: _autovalidate,
                                                icon: Icons.email,
                                                password: false,
                                                email: "Email",
                                                inputType:
                                                    TextInputType.emailAddress,
                                              ),
                                              Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 5.0)),

                                              /// TextFromField Password
                                              TextFromField(
                                                controller: _passwordControl,
                                                autovalidate: _autovalidate,
                                                icon: Icons.vpn_key,
                                                password: true,
                                                tapHide: () {
                                                  _showhide();
                                                },
                                                obscure: _hidepass,
                                                email: "Password",
                                                inputType: TextInputType.text,
                                              ),
                                            ],
                                          )),

                                      /// Button Login
                                      FlatButton(
                                          padding: EdgeInsets.only(top: 20.0),
                                          onPressed: () {
                                            Navigator.of(context)
                                                .pushReplacement(
                                                    MaterialPageRoute(
                                                        builder: (BuildContext
                                                                context) =>
                                                            new LoginScreen()));
                                          },
                                          child: Text(
                                            " Have Acount? Sign In",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 13.0,
                                                fontWeight: FontWeight.w600,
                                                fontFamily: "Sans"),
                                          )),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            top: mediaQueryData.padding.top +
                                                105.0,
                                            bottom: 0.0),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),

                            /// Set Animaion after user click buttonLogin
                            // tap == 0
                            //     ? InkWell(
                            //         splashColor: Colors.yellow,
                            //         onTap: () {
                            //           setState(() {
                            //             tap = 1;
                            //           });
                            //           _playAnimation();
                            //           return tap;
                            //         },
                            //         child: ButtonBlackBottom(),
                            //       )
                            //     : new LoginAnimation(
                            //         animationController: sanimationController.view,
                            //       )
                            tap == 0
                                ? InkWell(
                                    splashColor: Colors.yellow,
                                    onTap: () async {
                                      final formState = _formKey.currentState;
                                      if (formState.validate()) {
                                        uploadImage(_image);
                                      } else {
                                        setState(() {
                                          // isLoading = false;
                                          _autovalidate = true;
                                        });
                                      }
                                    },
                                    // () {
                                    //   setState(() {
                                    //     tap = 1;
                                    //   });
                                    //   _login();
                                    //   // new LoginAnimation(
                                    //   //   animationController: sanimationController.view,
                                    //   // );
                                    //   // playAnimation();
                                    //   return tap;
                                    // },
                                    child: ButtonBlackBottom(),
                                  )
                                : new LoginAnimation(
                                    animationController:
                                        sanimationController.view,
                                  )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
        ],
      ),
    );
  }
}

///ButtonBlack class
class ButtonBlackBottom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(30.0),
      child: Container(
        height: 55.0,
        width: 600.0,
        child: Text(
          "Sign Up",
          style: TextStyle(
              color: Colors.white,
              letterSpacing: 0.2,
              fontFamily: "Sans",
              fontSize: 18.0,
              fontWeight: FontWeight.w800),
        ),
        alignment: FractionalOffset.center,
        decoration: BoxDecoration(
            boxShadow: [BoxShadow(color: Colors.black38, blurRadius: 15.0)],
            borderRadius: BorderRadius.circular(30.0),
            gradient: LinearGradient(
                colors: <Color>[Color(0xFF121940), Color(0xFF6E48AA)])),
      ),
    );
  }
}
