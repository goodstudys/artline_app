import 'dart:async';
import 'dart:convert';
import 'package:artline/components/inputTextForm.dart';
import 'package:artline/env.dart';
import 'package:artline/model/dataCity.dart';
import 'package:artline/model/dataProvince.dart';
import 'package:artline/pages/profile/SettingAcount.dart';
import 'package:artline/storage/storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class EditProfile extends StatefulWidget {
  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  DataStore storage = DataStore();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _firstName = TextEditingController();
  final TextEditingController _lastName = TextEditingController();
  final TextEditingController _displayName = TextEditingController();
  final TextEditingController _company = TextEditingController();
  final TextEditingController _phone = TextEditingController();
  final TextEditingController _email = TextEditingController();

  List data;
  bool isLoading = false;
  bool _autovalidate = false;
  String getType;

  List<String> type = ['BILLING', 'SHOPPING'];
  List<DataProvince> listApiProvince;
  List<DataCity> listApiCity;
  List<DataCity> listCity = [];

  // Timer _timer;

  /// Custom Text Header for Dialog after user succes payment
  var _txtCustomHead = TextStyle(
    color: Colors.black54,
    fontSize: 23.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Gotik",
  );

  /// Custom Text Description for Dialog after user succes payment
  var _txtCustomSub = TextStyle(
    color: Colors.black38,
    fontSize: 15.0,
    fontWeight: FontWeight.w500,
    fontFamily: "Gotik",
  );

  _showDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => SimpleDialog(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 30.0, right: 60.0, left: 60.0),
            color: Colors.white,
            child: Image.asset(
              "assets/img/checklist.png",
              height: 110.0,
              color: Colors.lightGreen,
            ),
          ),
          Center(
              child: Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: Text(
              "Sukses !!!",
              style: _txtCustomHead,
            ),
          )),
          Center(
              child: Padding(
            padding: const EdgeInsets.only(top: 30.0, bottom: 40.0),
            child: Text(
              "Edit Profile Tersimpan",
              style: _txtCustomSub,
            ),
          )),
        ],
      ),
    );
  }

  void navigator() {
    Navigator.of(context).pushReplacement(PageRouteBuilder(
      pageBuilder: (_, __, ___) => new SettingAccount(),
      transitionsBuilder: (_, Animation<double> animation, __, Widget widget) {
        return Opacity(
          opacity: animation.value,
          child: widget,
        );
      },
      transitionDuration: Duration(milliseconds: 1500),
    ));
  }

  submit() async {
    var $tokenType = await storage.getDataString('token_type');
    var userPointer = await storage.getDataString('userPointer');
    var $accesToken = await storage.getDataString('access_token');
    storage.setDataString("name", _displayName.text);
    storage.setDataString("email", _email.text);
    var res =
        await http.post(url('api/updateProfile/' + userPointer), headers: {
      'Authorization': $tokenType + ' ' + $accesToken
    }, body: {
      'firstName': _firstName.text,
      'lastName': _lastName.text,
      'displayName': _displayName.text,
      'company': _company.text,
      'phone': _phone.text,
      'email': _email.text,
    });
    var data = json.decode(res.body);
    setState(() {
      print(data);
    });
  }

  // Future<String> getData2() async {
  //   var $tokenType = await storage.getDataString('token_type');
  //   var $accesToken = await storage.getDataString('access_token');
  //   var userPointer = await storage.getDataString('userPointer');
  //   var res = await http.get(url('api/editProfile/' + userPointer),
  //       headers: {'Authorization': $tokenType + ' ' + $accesToken});

  //   setState(() {
  //     var content = json.decode(res.body);
  //     // print(content['provinces']['rajaongkir']['results']);
  //     listApiProvince = List<DataProvince>.from(content['provinces']
  //             ['rajaongkir']['results']
  //         .map((item) => DataProvince.fromJson(item)));
  //     listApiCity = List<DataCity>.from(content['city']['rajaongkir']['results']
  //         .map((item) => DataCity.fromJson(item)));
  //   });

  //   //print(data);
  //   return 'success!';
  // }

  Future<String> getData() async {
    // getData2();
    var $tokenType = await storage.getDataString('token_type');
    var userPointer = await storage.getDataString('userPointer');
    var $accesToken = await storage.getDataString('access_token');
    var res = await http.get(url('api/editProfile/' + userPointer),
        headers: {'Authorization': $tokenType + ' ' + $accesToken});

    setState(() {
      var content = json.decode(res.body);
      print(content['data']['type']);
      _firstName.text = content['data']['firstName'];
      _lastName.text = content['data']['lastName'];
      _displayName.text = content['data']['displayName'];
      _company.text = content['data']['company'];
      _phone.text = content['data']['phone'];
      _email.text = content['data']['email'];
    });

    //print(data);
    return 'success!';
  }

  @override
  void initState() {
    super.initState();
    getData().then((s) => setState(() {
          isLoading = true;
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Profile'),
      ),
      body: isLoading
          ? Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(
                      top: 13.0, right: 13.0, left: 13.0, bottom: 85.0),
                  child: Form(
                      key: _formKey,
                      child: ListView(
                        children: <Widget>[
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'First Name',
                            textController: _firstName,
                          ),
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'Last Name',
                            textController: _lastName,
                          ),
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'Display Name',
                            textController: _displayName,
                          ),
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'Company',
                            textController: _company,
                          ),
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'Phone',
                            textController: _phone,
                          ),
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'Email',
                            textController: _email,
                          ),
                        ],
                      )),
                ),
                Positioned(
                  bottom: 13.0,
                  left: 13.0,
                  right: 13.0,
                  child: InkWell(
                    onTap: () {
                      final formState = _formKey.currentState;
                      if (formState.validate()) {
                        submit();
                        _showDialog(context);
                        navigator();
                        // showDialog(
                        //   context: context,
                        //   builder: (BuildContext context) {
                        //     return AlertDialog(
                        //       shape: RoundedRectangleBorder(
                        //           borderRadius:
                        //               new BorderRadius.circular(10.0)),
                        //       content: new Text(
                        //         'Apakah data anda sudah benar?',
                        //         textAlign: TextAlign.left,
                        //       ),
                        //       actions: <Widget>[
                        //         FlatButton(
                        //           child: Text("No"),
                        //           onPressed: () {
                        //             Navigator.of(context).pop();
                        //           },
                        //         ),
                        //         FlatButton(
                        //           child: new Text("Yes"),
                        //           onPressed: () {
                        //             submit();
                        //             _scaffoldKey.currentState
                        //                 .showSnackBar(SnackBar(
                        //               content:
                        //                   Text("Item berhasil ditambahkan"),
                        //               duration: Duration(seconds: 2),
                        //               backgroundColor: Colors.indigo,
                        //             ));
                        //             back();
                        //             setState(() {});
                        //           },
                        //         ),
                        //       ],
                        //     );
                        //   },
                        // );
                      } else {
                        setState(() {
                          // isLoading = false;
                          _autovalidate = true;
                        });
                      }
                    },
                    child: Container(
                      height: 55.0,
                      width: 300.0,
                      decoration: BoxDecoration(
                          color: Colors.indigoAccent,
                          borderRadius:
                              BorderRadius.all(Radius.circular(40.0))),
                      child: Center(
                        child: Text(
                          "Submit",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w700,
                              fontSize: 16.5,
                              letterSpacing: 1.0),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )
          : Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
    );
  }
}
