import 'dart:convert';
import 'package:artline/components/cardOrder.dart';
import 'package:artline/env.dart';
import 'package:artline/pages/profile/MyOrders.dart';
import 'package:artline/storage/storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pull_to_refresh/pull_to_refresh.dart';

class OrderProses extends StatefulWidget {
  @override
  _OrderProsesState createState() => _OrderProsesState();
}

class _OrderProsesState extends State<OrderProses> {
  DataStore storage = DataStore();
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  List data;
  List prod;
  bool isLoading = false;

  void _onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      getData().then((s) => setState(() {
            isLoading = true;
          }));
    });
    if (mounted)
      setState(() {
        getData().then((s) => setState(() {
              isLoading = true;
            }));
      });
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      getData().then((s) => setState(() {
            isLoading = true;
          }));
    });
    if (mounted)
      setState(() {
        getData().then((s) => setState(() {
              isLoading = true;
            }));
      });
    _refreshController.loadComplete();
  }

  Future<String> getData() async {
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
        var $userLevel = await storage.getDataString('userLevel');
    String apiLink;
    if ($userLevel == '5') {
      apiLink = 'api/getSalesOrder';
    } else {
      apiLink = 'api/getOrder';
    }
    var res = await http.get(url(apiLink), headers: {
      'Authorization': $tokenType + ' ' + $accesToken,
      'Accept': 'application/json'
    });
    if(mounted)
    setState(() {
      var content = json.decode(res.body);
      data = content['proses'];
    });
    return 'success!';
  }

  void saveIdOrder(String id) async {
    storage.setDataString('idOrder', id);
  }

  @override
  void initState() {
    // if (!mounted) {
    //   getData().then((s) => setState(() {
    //         isLoading = true;
    //       }));
    // }else{
    //   return;
    // }
        getData().then((s) {
      if (mounted)
        setState(() {
          isLoading = true;
        });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        header: WaterDropHeader(),
        footer: CustomFooter(
          builder: (BuildContext context, LoadStatus mode) {
            Widget body;
            if (mode == LoadStatus.idle) {
              body = Text("pull up load");
            } else if (mode == LoadStatus.loading) {
              body = CupertinoActivityIndicator();
            } else if (mode == LoadStatus.failed) {
              body = Text("Load Failed!Click retry!");
            } else if (mode == LoadStatus.canLoading) {
              body = Text("release to load more");
            } else {
              body = Text("No more Data");
            }
            return Container(
              height: 55.0,
              child: Center(child: body),
            );
          },
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: isLoading
            ? Container(
                child: data == null
                    ? Text('')
                    : ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: data == null ? 0 : data.length,
                        itemBuilder: (BuildContext context, int index) {
                          return CardOrder(
                            type: 'proses',
                            tapdetail: () {
                              saveIdOrder(data[index]['rowPointer']);
                              Navigator.of(context).push(PageRouteBuilder(
                                  pageBuilder: (_, __, ___) => new Order()));
                            },
                            orderNumber: data[index]['orderCode'],
                            konfirm: data[index]['confirmationsisActive'].toString(),
                            resi: data[index]['noResi'],
                            name: data[index]['detail'][0]['name'],
                            discAmount: data[index]['detail'][0]['discAmount'],
                            discPercent: data[index]['detail'][0]
                                ['discPercent'],
                            price: data[index]['detail'][0]['price'],
                            qty: data[index]['detail'][0]['quantity'].toString(),
                            image: data[index]['detail'][0]['image'],
                            jumlah: data[index]['detail'].length - 1,
                            tap: () {
                              saveIdOrder(data[index]['rowPointer']);
                              Navigator.of(context).push(PageRouteBuilder(
                                  pageBuilder: (_, __, ___) => new Order()));
                            },
                          );
                        },
                      ),
              )
            : Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ));
  }
}
