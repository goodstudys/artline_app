import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:artline/components/inputTextForm.dart';
import 'package:artline/env.dart';
import 'package:artline/model/dataCity.dart';
import 'package:artline/model/dataProvince.dart';
import 'package:artline/pages/profile/SettingAcount.dart';
import 'package:artline/storage/storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mime/mime.dart';

class EditReseller extends StatefulWidget {
  @override
  _EditResellerState createState() => _EditResellerState();
}

class _EditResellerState extends State<EditReseller> {
  DataStore storage = DataStore();
  int keperluan;
  final TextEditingController _otherKeperluan = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _ktp = TextEditingController();
  final TextEditingController _npwp = TextEditingController();
  final TextEditingController _storeAddress = TextEditingController();
  final TextEditingController _storeCity = TextEditingController();
  final TextEditingController _phone = TextEditingController();
  final TextEditingController _position = TextEditingController();

  List data;
  bool isLoading = false;
  bool _autovalidate = false;
  String getType;
  String userLevel;
  File _imageKTP;
  File _imageNPWP;

  List<String> type = ['BILLING', 'SHOPPING'];
  List<DataProvince> listApiProvince;
  List<DataCity> listApiCity;
  List<DataCity> listCity = [];

  // Timer _timer;

  /// Custom Text Header for Dialog after user succes payment
  var _txtCustomHead = TextStyle(
    color: Colors.black54,
    fontSize: 23.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Gotik",
  );

  /// Custom Text Description for Dialog after user succes payment
  var _txtCustomSub = TextStyle(
    color: Colors.black38,
    fontSize: 15.0,
    fontWeight: FontWeight.w500,
    fontFamily: "Gotik",
  );
  Future getImage(ImageSource media, String target) async {
    try {
      File image = await ImagePicker.pickImage(
        source: media,
        maxWidth: 800,
      );
      setState(() {
        if (target == 'ktp') {
          _imageKTP = image;
        } else if (target == 'npwp') {
          _imageNPWP = image;
        }
      });
    } catch (error) {
      // Navigator.pop(context);
      print('Failed!  ' + error.toString());
    }
  }

  void uploadImage(String target) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
            title: Text('Please choose media to select'),
            content: Container(
              height: MediaQuery.of(context).size.height / 6,
              child: Column(
                children: <Widget>[
                  FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                      getImage(ImageSource.gallery, target);
                    },
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.image),
                        Text('From Gallery'),
                      ],
                    ),
                  ),
                  FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                      getImage(ImageSource.camera, target);
                    },
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.camera),
                        Text('From Camera'),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  _showDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => SimpleDialog(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 30.0, right: 60.0, left: 60.0),
            color: Colors.white,
            child: Image.asset(
              "assets/img/checklist.png",
              height: 110.0,
              color: Colors.lightGreen,
            ),
          ),
          Center(
              child: Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: Text(
              "Sukses !!!",
              style: _txtCustomHead,
            ),
          )),
          Center(
              child: Padding(
            padding: const EdgeInsets.only(top: 30.0, bottom: 40.0),
            child: Text(
              "Edit Profile Tersimpan",
              style: _txtCustomSub,
            ),
          )),
        ],
      ),
    );
  }

  void navigator() {
    Navigator.of(context).pushReplacement(PageRouteBuilder(
      pageBuilder: (_, __, ___) => new SettingAccount(),
      transitionsBuilder: (_, Animation<double> animation, __, Widget widget) {
        return Opacity(
          opacity: animation.value,
          child: widget,
        );
      },
      transitionDuration: Duration(milliseconds: 1500),
    ));
  }

  submit(BuildContext context) async {
    var tokenType = await storage.getDataString('token_type');
    var userPointer = await storage.getDataString('userPointer');
    var accessToken = await storage.getDataString('access_token');
    Map<String, String> baseHeaders = {
      "Content-type": "application/json",
      "accept": "application/json",
      "Authorization": tokenType + ' ' + accessToken,
    };
    // Find the mime type of the selected file by looking at the header bytes of the file
    final mimeTypeKTP =
        lookupMimeType(_imageKTP.path, headerBytes: [0xFF, 0xD8]).split('/');
    List<String> mimeTypeNPWP;
    if (_npwp.text != null && _npwp.text != '' && _imageNPWP.path != '') {
      mimeTypeNPWP =
          lookupMimeType(_imageNPWP.path, headerBytes: [0xFF, 0xD8]).split('/');
    }
    // Intilize the multipart request
    final request =
        http.MultipartRequest('POST', Uri.parse(url('api/requestReseller')));
    // Set Header
    request.headers.addAll(baseHeaders);
    // Attach the file in the request
    final fileKTP = await http.MultipartFile.fromPath(
        'ktp_image', _imageKTP.path,
        contentType: MediaType(mimeTypeKTP[0], mimeTypeKTP[1]));
    request.fields['shopfor'] =
        keperluan == 5 ? _otherKeperluan.text : keperluan.toString();
    request.fields['ktp'] = _ktp.text;
    request.fields['npwp'] = _npwp.text;
    request.fields['npwp_image'] = '';
    request.fields['work_address'] = _storeAddress.text;
    request.fields['work_city'] = _storeCity.text;
    request.fields['work_phone'] = _phone.text;
    request.fields['position'] = _position.text;
    request.files.add(fileKTP);
    if (_npwp.text != null && _npwp.text != '' && _imageNPWP.path != '') {
      final fileNPWP = await http.MultipartFile.fromPath(
          'ktp_image', _imageNPWP.path,
          contentType: MediaType(mimeTypeNPWP[0], mimeTypeNPWP[1]));
      request.files.add(fileNPWP);
    }
    try {
      final streamedResponse = await request.send();
      final response = await http.Response.fromStream(streamedResponse);
      var content = json.decode(response.body);
      final int statusCode = response.statusCode;
      print(statusCode);
      print(content);
      if (statusCode == 204) {
        throw new Exception("No record found.");
      } else if (statusCode == 400) {
        throw new Exception("Invalid request parameter");
      } else if (statusCode == 401) {
        throw new Exception(
            "Unauthorized Access, please check your credentials");
      } else if (statusCode == 500) {
        throw new Exception("Server error");
      } else if (json == null) {
        throw new Exception("Content not found");
      }
      setState(() {
        if (content['status'] == 'success') {
          _showDialog(context);
          Navigator.of(context).pop();
        }
      });
      return print(content);
    } catch (e) {
      print(e);
      return null;
    }
    /*
    var res = await http.post(url('api/requestReseller'), headers: {
      'Authorization': $tokenType + ' ' + $accesToken
    }, body: {
      'shopfor': keperluan == 5 ? _otherKeperluan.text : keperluan.toString(),
      'ktp': _ktp.text,
      'ktp_image': '',
      'npwp': _npwp.text,
      'npwp_image': '',
      'work_address': _storeAddress.text,
      'work_city': _storeCity.text,
      'work_phone': _phone.text,
      'position': _position.text,
    });
    print(res.statusCode);
    var data = json.decode(res.body);
    setState(() {
      print(data);
    });
    _showDialog(context);
    Navigator.of(context).pop();
    */
  }

  Future<String> getData() async {
    // getData2();
    var $tokenType = await storage.getDataString('token_type');
    var userPointer = await storage.getDataString('userPointer');
    var $accesToken = await storage.getDataString('access_token');
    var level = await storage.getDataString('userLevel');
    var res = await http.get(url('api/editProfile/' + userPointer),
        headers: {'Authorization': $tokenType + ' ' + $accesToken});

    setState(() {
      var content = json.decode(res.body);
      print(content['data']['type']);
      _ktp.text = content['data']['firstName'];
      _npwp.text = content['data']['lastName'];
      _storeAddress.text = content['data']['displayName'];
      _storeCity.text = content['data']['company'];
      _phone.text = content['data']['phone'];
      _position.text = content['data']['email'];
      userLevel = level;
    });

    //print(data);
    return 'success!';
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      isLoading = false;
    });
    /* getData().then((s) => setState(() {
          isLoading = true;
        })); */
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:
            Text(userLevel == '4' ? 'Edit Reseller' : 'Register as Reseller'),
      ),
      body: !isLoading
          ? Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(bottom: 85.0),
                  child: Form(
                      key: _formKey,
                      child: ListView(
                        children: <Widget>[
                          Container(
                            padding: const EdgeInsets.only(
                                left: 13.0, right: 13.0, top: 13.0),
                            color: Colors.yellow[50],
                            child: Column(
                              children: [
                                DropdownButtonFormField(
                                    value: keperluan,
                                    autovalidate: _autovalidate,
                                    hint: Text('Keperluan Belanja'),
                                    items: [
                                      DropdownMenuItem(
                                        child: Text(
                                            'Toko Offline (Offline Store)'),
                                        value: 1,
                                      ),
                                      DropdownMenuItem(
                                        child:
                                            Text('Toko Online (Online Store)'),
                                        value: 2,
                                      ),
                                      DropdownMenuItem(
                                        child: Text(
                                            'Supplier/Perusahaan pengadaan barang'),
                                        value: 3,
                                      ),
                                      DropdownMenuItem(
                                        child: Text('Lembaga Pendidikan'),
                                        value: 4,
                                      ),
                                      DropdownMenuItem(
                                        child: Text('Lain-lain'),
                                        value: 5,
                                      )
                                    ],
                                    onChanged: (value) {
                                      setState(() {
                                        keperluan = value;
                                      });
                                    }),
                                keperluan == 5
                                    ? InputTextForm(
                                        autovalidate: _autovalidate,
                                        label: 'Lain-lain',
                                        textController: _otherKeperluan,
                                      )
                                    : Container(),
                              ],
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 13.0),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                      child: TextFormField(
                                        controller: _ktp,
                                        autovalidateMode: _autovalidate
                                            ? AutovalidateMode.always
                                            : AutovalidateMode.disabled,
                                        autocorrect: true,
                                        decoration: InputDecoration(
                                          labelText: 'KTP',
                                          hintStyle:
                                              TextStyle(color: Colors.black54),
                                          hintText: 'KTP',
                                        ),
                                        keyboardType: TextInputType.number,
                                        validator: (input) {
                                          if (input.isEmpty) {
                                            return 'Silahkan isi KTP anda';
                                          } else if (_imageKTP == null) {
                                            return 'Foto KTP diperlukan';
                                          } else {
                                            return null;
                                          }
                                        },
                                      ),
                                    ),
                                    _imageKTP == null
                                        ? IconButton(
                                            icon: Icon(
                                              Icons.image,
                                            ),
                                            onPressed: () {
                                              uploadImage('ktp');
                                            },
                                          )
                                        : InkWell(
                                            onTap: () {
                                              uploadImage('ktp');
                                            },
                                            child: SizedBox(
                                              width: 45.0,
                                              height: 45.0,
                                              child: Image.file(
                                                _imageKTP,
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          )
                                  ],
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                      child: TextFormField(
                                        controller: _npwp,
                                        autovalidateMode: _autovalidate
                                            ? AutovalidateMode.always
                                            : AutovalidateMode.disabled,
                                        autocorrect: true,
                                        decoration: InputDecoration(
                                          labelText: 'NPWP (Opsional)',
                                          hintStyle:
                                              TextStyle(color: Colors.black54),
                                          hintText: 'NPWP (Opsional)',
                                        ),
                                        keyboardType: TextInputType.number,
                                        validator: (input) {
                                          if (input.isNotEmpty &&
                                              _imageNPWP == null) {
                                            return 'Foto NPWP diperlukan';
                                          } else {
                                            return null;
                                          }
                                        },
                                      ),
                                    ),
                                    _imageNPWP == null
                                        ? IconButton(
                                            icon: Icon(
                                              Icons.image,
                                            ),
                                            onPressed: () {
                                              if (_npwp.text.isNotEmpty) {
                                                uploadImage('npwp');
                                              }
                                            },
                                          )
                                        : InkWell(
                                            onTap: () {
                                              uploadImage('npwp');
                                            },
                                            child: SizedBox(
                                              width: 45.0,
                                              height: 45.0,
                                              child: Image.file(
                                                _imageNPWP,
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          )
                                  ],
                                ),
                                InputTextForm(
                                  autovalidate: _autovalidate,
                                  label: 'Alamat Tempat Usaha',
                                  textController: _storeAddress,
                                ),
                                InputTextForm(
                                  autovalidate: _autovalidate,
                                  label: 'Kota Tempat Usaha',
                                  textController: _storeCity,
                                ),
                                InputTextForm(
                                  autovalidate: _autovalidate,
                                  label: 'Nomor Telepon Perusahaan',
                                  textController: _phone,
                                  inputType: TextInputType.phone,
                                ),
                                InputTextForm(
                                  autovalidate: _autovalidate,
                                  label: 'Jabatan',
                                  textController: _position,
                                ),
                              ],
                            ),
                          ),
                        ],
                      )),
                ),
                Positioned(
                  bottom: 13.0,
                  left: 13.0,
                  right: 13.0,
                  child: InkWell(
                    onTap: () {
                      final formState = _formKey.currentState;
                      if (formState.validate()) {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0)),
                              content: Text(
                                'Apakah data anda sudah benar?',
                                textAlign: TextAlign.left,
                              ),
                              actions: <Widget>[
                                FlatButton(
                                  child: Text("No"),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                                FlatButton(
                                  child: new Text("Yes"),
                                  onPressed: () {
                                    submit(context);
                                    // Navigator.of(context).pop();
                                    // _showDialog(context);
                                    // navigator();
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      } else {
                        setState(() {
                          _autovalidate = true;
                        });
                      }
                    },
                    child: Container(
                      height: 55.0,
                      width: 300.0,
                      decoration: BoxDecoration(
                          color: Colors.indigoAccent,
                          borderRadius:
                              BorderRadius.all(Radius.circular(40.0))),
                      child: Center(
                        child: Text(
                          "Submit",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w700,
                              fontSize: 16.5,
                              letterSpacing: 1.0),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )
          : Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
    );
  }
}
