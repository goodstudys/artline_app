import 'dart:async';
import 'dart:convert';
import 'package:artline/components/dialogSuccess.dart';
import 'package:artline/components/inputTextForm.dart';
import 'package:artline/env.dart';
import 'package:artline/model/dataCity.dart';
import 'package:artline/model/dataProvince.dart';
import 'package:artline/storage/storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ChangePass extends StatefulWidget {
  @override
  _ChangePassState createState() => _ChangePassState();
}

class _ChangePassState extends State<ChangePass> {
  DataStore storage = DataStore();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _passLama = TextEditingController();
  final TextEditingController _passBaru = TextEditingController();

  List data;
  bool isLoading = false;
  bool _autovalidate = false;
  String getType;

  List<String> type = ['billing', 'shopping'];
  List<DataProvince> listApiProvince;
  List<DataCity> listApiCity;
  List<DataCity> listCity = [];

  submit() async {
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var id = await storage.getDataString('id');
    var res = await http.post(url('api/changePassword/' + id), headers: {
      'Authorization': $tokenType + ' ' + $accesToken
    }, body: {
      'cur_pass': _passLama.text,
      'password': _passBaru.text,
    });
    var data = json.decode(res.body);
    setState(() {
      print(data);
      if (data['status'] == 'success') {
        showDialog(
            context: context,
            builder: (context) {
              Future.delayed(Duration(seconds: 3), () {
                Navigator.of(context).pop(true);
                Navigator.of(context).pop(true);
              });
              return DialogSuccess(
                type: true,
                txtHead: 'success !!!',
                txtSub: "Password Baru Tersimpan",
              );
            });
      } else if (data['status'] == 'error') {
        showDialog(
            context: context,
            builder: (context) {
              Future.delayed(Duration(seconds: 3), () {
                Navigator.of(context).pop(true);
              });
              return DialogSuccess(
                type: false,
                txtHead: data['status'],
                txtSub: data['message'],
              );
            });
      }
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Change Password'),
        ),
        body:
            //  isLoading
            //     ?
            Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(
                  top: 13.0, right: 13.0, left: 13.0, bottom: 85.0),
              child: Form(
                  key: _formKey,
                  child: ListView(
                    children: <Widget>[
                      InputTextForm(
                        autovalidate: _autovalidate,
                        label: 'Masukkan Password Lama',
                        textController: _passLama,
                        password: true,
                        inputType: TextInputType.visiblePassword,
                      ),
                      InputTextForm(
                        autovalidate: _autovalidate,
                        label: 'Masukkan Password Baru',
                        textController: _passBaru,
                        password: true,
                        inputType: TextInputType.visiblePassword,
                      ),
                    ],
                  )),
            ),
            Positioned(
              bottom: 13.0,
              left: 13.0,
              right: 13.0,
              child: InkWell(
                onTap: () {
                  final formState = _formKey.currentState;
                  if (formState.validate()) {
                    submit();
                    // _showDialog(context);
                    // startTime();
                    // showDialog(
                    //   context: context,
                    //   builder: (BuildContext context) {
                    //     return AlertDialog(
                    //       shape: RoundedRectangleBorder(
                    //           borderRadius:
                    //               new BorderRadius.circular(10.0)),
                    //       content: new Text(
                    //         'Apakah data anda sudah benar?',
                    //         textAlign: TextAlign.left,
                    //       ),
                    //       actions: <Widget>[
                    //         FlatButton(
                    //           child: Text("No"),
                    //           onPressed: () {
                    //             Navigator.of(context).pop();
                    //           },
                    //         ),
                    //         FlatButton(
                    //           child: new Text("Yes"),
                    //           onPressed: () {
                    //             submit();
                    //             _scaffoldKey.currentState
                    //                 .showSnackBar(SnackBar(
                    //               content:
                    //                   Text("Item berhasil ditambahkan"),
                    //               duration: Duration(seconds: 2),
                    //               backgroundColor: Colors.indigo,
                    //             ));
                    //             back();
                    //             setState(() {});
                    //           },
                    //         ),
                    //       ],
                    //     );
                    //   },
                    // );
                  } else {
                    setState(() {
                      // isLoading = false;
                      _autovalidate = true;
                    });
                  }
                },
                child: Container(
                  height: 55.0,
                  width: 300.0,
                  decoration: BoxDecoration(
                      color: Colors.indigoAccent,
                      borderRadius: BorderRadius.all(Radius.circular(40.0))),
                  child: Center(
                    child: Text(
                      "Submit",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                          fontSize: 16.5,
                          letterSpacing: 1.0),
                    ),
                  ),
                ),
              ),
            ),
          ],
        )
        // : Container(
        //     child: Center(
        //       child: CircularProgressIndicator(),
        //     ),
        //   ),
        );
  }
}
