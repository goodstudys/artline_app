import 'package:artline/components/textFormatIDR.dart';
import 'package:artline/env.dart';
import 'package:artline/storage/storage.dart';
import 'package:artline/theme/style.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Order extends StatefulWidget {
  @override
  _OrderState createState() => _OrderState();
}

class _OrderState extends State<Order> {
  DataStore storage = DataStore();
  bool isLoading = false;
  static var _txtCustom = TextStyle(
    color: Colors.black54,
    fontSize: 15.0,
    fontWeight: FontWeight.w500,
    fontFamily: "Gotik",
  );

  // /// Create Big Circle for Data Order Not Success
  // var _bigCircleNotYet = Padding(
  //   padding: const EdgeInsets.only(top: 8.0),
  //   child: Container(
  //     height: 20.0,
  //     width: 20.0,
  //     decoration: BoxDecoration(
  //       color: Colors.lightGreen,
  //       shape: BoxShape.circle,
  //     ),
  //   ),
  // );

  // /// Create Circle for Data Order Success
  // var _bigCircle = Padding(
  //   padding: const EdgeInsets.only(top: 8.0),
  //   child: Container(
  //     height: 20.0,
  //     width: 20.0,
  //     decoration: BoxDecoration(
  //       color: Colors.lightGreen,
  //       shape: BoxShape.circle,
  //     ),
  //     child: Center(
  //       child: Icon(
  //         Icons.check,
  //         color: Colors.white,
  //         size: 14.0,
  //       ),
  //     ),
  //   ),
  // );

  // /// Create Small Circle
  // var _smallCircle = Padding(
  //   padding: const EdgeInsets.only(top: 8.0),
  //   child: Container(
  //     height: 3.0,
  //     width: 3.0,
  //     decoration: BoxDecoration(
  //       color: Colors.lightGreen,
  //       shape: BoxShape.circle,
  //     ),
  //   ),
  // );
  var data;
  List produk;
  Future<String> getData() async {
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var $orderId = await storage.getDataString('idOrder');
    var res = await http.get(url('api/getOrderDetail/' + $orderId), headers: {
      'Authorization': $tokenType + ' ' + $accesToken,
      'Accept': 'application/json'
    });
    setState(() {
      var content = json.decode(res.body);
      data = content['data'];
      produk = content['data']['detail'];
      print(content['data']['detail']);
      print(content['data']['noResi']);
    });
    return 'success!';
  }

  @override
  void initState() {
    super.initState();
    getData().then((s) => setState(() {
          isLoading = true;
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          "Order Detail",
          style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 20.0,
              color: Colors.black54,
              fontFamily: "Gotik"),
        ),
        centerTitle: true,
        iconTheme: IconThemeData(color: ColorStyle.primaryColor),
        elevation: 0.0,
      ),
      body: isLoading
          ? SingleChildScrollView(
              child: Container(
                color: Colors.white,
                width: 800.0,
                child: Padding(
                  padding: const EdgeInsets.only(top: 20.0, left: 25.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        data == null ? '' : "Order ID: " + data['orderCode'],
                        style: _txtCustom,
                      ),
                      Padding(padding: EdgeInsets.only(top: 7.0)),
                      Text(
                        data == null
                            ? ''
                            : data['noResi'] == null
                                ? 'No Resi: ----'
                                : "No Resi: " + data['noResi'],
                        style: _txtCustom,
                      ),
                      Padding(padding: EdgeInsets.only(top: 30.0)),
                      Text(
                        "Orders",
                        style: _txtCustom.copyWith(
                            color: Colors.black54,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w600),
                      ),
                      Padding(padding: EdgeInsets.only(top: 20.0)),
                      Container(
                          margin: EdgeInsets.only(right: 25.0),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                BorderRadius.all(Radius.circular(5.0)),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black.withOpacity(0.1),
                                blurRadius: 4.5,
                                spreadRadius: 1.0,
                              )
                            ],
                          ),
                          child: ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemCount: produk == null ? 0 : produk.length,
                              itemBuilder: (BuildContext context, index) {
                                return DataTransaction(
                                  // qty: int.parse(produk[index]['quantity']),
                                  qty: produk[index]['quantity'],
                                  item: produk[index]['name'],
                                  price: int.parse(produk[index]['price']),
                                  discAmount: produk[index]['discAmount'],
                                  discPercent: produk[index]['discPercent'],
                                );
                              })
                          // Column(
                          //   children: <Widget>[

                          //     DataTransaction(
                          //       date: "2 x",
                          //       item:
                          //           "BMV 527 Eco Whiteboard Marker 2mm Green",
                          //       price: "Rp. 8.000",
                          //     ),
                          //     DataTransaction(
                          //       date: "1 x",
                          //       item: "BMV 527 Eco Whiteboard Marker 2mm Red",
                          //       price: "Rp. 34.000",
                          //     ),
                          //     DataTransaction(
                          //       date: "12 x",
                          //       item:
                          //           "BMV Supreme Marker 1.0mm Bullet Light Blue",
                          //       price: "Rp. 15.000",
                          //     ),
                          //     DataTransaction(
                          //       date: "15 x",
                          //       item:
                          //           "BMV Supreme Marker 1.0mm Bullet Light Pink",
                          //       price: "Rp. 8.000",
                          //     ),
                          //     DataTransaction(
                          //       date: "100 x",
                          //       item:
                          //           "BMV Supreme Whiteboard Marker 1.5mm Red ",
                          //       price: "Rp. 8.000",
                          //     ),
                          //   ],
                          // ),
                          ),
                      Padding(padding: EdgeInsets.only(top: 20.0)),
                      Container(
                        margin: EdgeInsets.only(right: 25.0),
                        padding: EdgeInsets.all(13.0),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.1),
                              blurRadius: 4.5,
                              spreadRadius: 1.0,
                            )
                          ],
                        ),
                        child: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  'Pengiriman',
                                  style: TxtStyle.txtCustomSub.copyWith(
                                      color: Colors.black54, fontSize: 12),
                                ),
                                TextFormatIDR(
                                  thisText: data['kurirRate'],
                                  // thisText: data['kurirRate'],
                                  thisStyle: TxtStyle.txtCustomSub.copyWith(
                                    color: Colors.black54,
                                    fontSize: 12.0,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 13.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  'Total',
                                  style: TxtStyle.txtCustomSub.copyWith(
                                      color: Colors.black54,
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold),
                                ),
                                TextFormatIDR(
                                  thisText: data['total'],
                                  // thisText: data['total'],
                                  thisStyle: TxtStyle.txtCustomSub.copyWith(
                                      color: Colors.black54,
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                      // Row(
                      //   crossAxisAlignment: CrossAxisAlignment.start,
                      //   children: <Widget>[
                      //     Column(
                      //       children: <Widget>[
                      //         _bigCircleNotYet,
                      //         _smallCircle,
                      //         _smallCircle,
                      //         _smallCircle,
                      //         _smallCircle,
                      //         _smallCircle,
                      //         _bigCircle,
                      //         _smallCircle,
                      //         _smallCircle,
                      //         _smallCircle,
                      //         _smallCircle,
                      //         _smallCircle,
                      //         _bigCircle,
                      //         _smallCircle,
                      //         _smallCircle,
                      //         _smallCircle,
                      //         _smallCircle,
                      //         _smallCircle,
                      //         _bigCircle,
                      //       ],
                      //     ),
                      //     Column(
                      //       children: <Widget>[
                      //         DataTransaction(
                      //           date: "Jan 01",
                      //           item:
                      //               "Buy BMV 527 Eco Whiteboard Marker 2mm Green",
                      //           price: "Rp. 8.000",
                      //         ),
                      //         DataTransaction(
                      //           date: "Feb 12",
                      //           item:
                      //               "Buy BMV 527 Eco Whiteboard Marker 2mm Red",
                      //           price: "Rp. 34.000",
                      //         ),
                      //         DataTransaction(
                      //           date: "Martch 21",
                      //           item:
                      //               "Buy BMV Supreme Marker 1.0mm Bullet Light Blue",
                      //           price: "Rp. 15.000",
                      //         ),
                      //         DataTransaction(
                      //           date: "Oct 16",
                      //           item:
                      //               "Buy BMV Supreme Marker 1.0mm Bullet Light Pink",
                      //           price: "Rp. 8.000",
                      //         ),
                      //         DataTransaction(
                      //           date: "Dec 01",
                      //           item:
                      //               "Buy BMV Supreme Whiteboard Marker 1.5mm Red ",
                      //           price: "Rp. 8.000",
                      //         ),
                      //       ],
                      //     )
                      //     // Column(
                      //     //   crossAxisAlignment: CrossAxisAlignment.start,
                      //     //   children: <Widget>[
                      //     //     QeueuItem(
                      //     //       icon: "assets/img/bag.png",
                      //     //       txtHeader: "Ready to Pickup",
                      //     //       txtInfo: "Order from BMVShop",
                      //     //       time: "11:0",
                      //     //       paddingValue: 55.0,
                      //     //     ),
                      //     //     Padding(padding: EdgeInsets.only(top: 50.0)),
                      //     //     QeueuItem(
                      //     //       icon: "assets/img/courier.png",
                      //     //       txtHeader: "Order Processed",
                      //     //       txtInfo: "We are preparing your order",
                      //     //       time: "9:50",
                      //     //       paddingValue: 16.0,
                      //     //     ),
                      //     //     Padding(padding: EdgeInsets.only(top: 50.0)),
                      //     //     QeueuItem(
                      //     //       icon: "assets/img/payment.png",
                      //     //       txtHeader: "Payment Confirmed",
                      //     //       txtInfo: "Awaiting Confirmation",
                      //     //       time: "8:20",
                      //     //       paddingValue: 55.0,
                      //     //     ),
                      //     //     Padding(padding: EdgeInsets.only(top: 50.0)),
                      //     //     QeueuItem(
                      //     //       icon: "assets/img/order.png",
                      //     //       txtHeader: "Order Placed",
                      //     //       txtInfo: "We have received your order",
                      //     //       time: "8:00",
                      //     //       paddingValue: 19.0,
                      //     //     ),
                      //     //   ],
                      //     // ),
                      //   ],
                      // ), /////
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 30.0, bottom: 30.0, left: 0.0, right: 25.0),
                        child: Container(
                          height: 130.0,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black12.withOpacity(0.1),
                                  blurRadius: 4.5,
                                  spreadRadius: 1.0,
                                )
                              ]),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Image.asset("assets/img/house.png"),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    "Delivery Address",
                                    style: _txtCustom.copyWith(
                                        fontWeight: FontWeight.w700),
                                  ),
                                  Padding(padding: EdgeInsets.only(top: 5.0)),
                                  Text(
                                    data['billName'],
                                    style: _txtCustom.copyWith(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 12.0,
                                        color: Colors.black38),
                                  ),
                                  Padding(padding: EdgeInsets.only(top: 2.0)),
                                  Text(
                                    data['billAddress'] +
                                        '\n' +
                                        data['billCity'] +
                                        ', ' +
                                        data['billKecamatan'] +
                                        ', ' +
                                        " - Indonesia\n" +
                                        data['billKodePos'],
                                    style: _txtCustom.copyWith(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 12.0,
                                        color: Colors.black38),
                                    overflow: TextOverflow.ellipsis,
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          : Container(
              child: Center(
                child: CircularProgressIndicator(
                    valueColor:
                        AlwaysStoppedAnimation<Color>(ColorStyle.primaryColor)),
              ),
            ),
    );
  }
}

/// Constructor Data Orders
class QeueuItem extends StatelessWidget {
  static var _txtCustomOrder = TextStyle(
    color: Colors.black45,
    fontSize: 13.5,
    fontWeight: FontWeight.w600,
    fontFamily: "Gotik",
  );

  final String icon, txtHeader, txtInfo, time;
  final double paddingValue;

  QeueuItem(
      {this.icon, this.txtHeader, this.txtInfo, this.time, this.paddingValue});

  Widget build(BuildContext context) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    return Padding(
      padding: const EdgeInsets.only(left: 13.0),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Image.asset(icon),
              Padding(
                padding: EdgeInsets.only(
                    left: 8.0,
                    right: mediaQueryData.padding.right + paddingValue),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(txtHeader, style: _txtCustomOrder),
                    Text(
                      txtInfo,
                      style: _txtCustomOrder.copyWith(
                          fontWeight: FontWeight.w400,
                          fontSize: 12.0,
                          color: Colors.black38),
                    ),
                  ],
                ),
              ),
              Text(
                time,
                style: _txtCustomOrder..copyWith(fontWeight: FontWeight.w400),
              )
            ],
          ),
        ],
      ),
    );
  }
}

/// Constructor for Transactions Data
class DataTransaction extends StatelessWidget {
  final String item, discAmount, discPercent;
  final int qty, price;

  DataTransaction(
      {this.item, this.price, this.qty, this.discAmount, this.discPercent});

  Widget build(BuildContext context) {
    int amopuntCalculate;
    if (discAmount != '0') {
      amopuntCalculate = price - int.parse(discAmount);
    } else if (discPercent != '0') {
      amopuntCalculate = (price - (price * int.parse(discPercent) ~/ 100));
    }
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 0.0),
                child: Text(
                  qty.toString() + 'x',
                  style: TxtStyle.txtCustomSub.copyWith(
                      color: Colors.black38,
                      fontSize: 11.0,
                      fontWeight: FontWeight.w500),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 2,
                child: Text(
                  item,
                  style: TxtStyle.txtCustomSub
                      .copyWith(color: Colors.black54, fontSize: 12),
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              TextFormatIDR(
                thisStyle: TxtStyle.txtCustomSub.copyWith(
                  color: Colors.redAccent,
                  fontSize: 12.0,
                ),
                thisText: discAmount != '0'
                    ? amopuntCalculate
                    : discPercent != '0' ? amopuntCalculate : price,
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: Divider(
            height: 0.5,
            color: Colors.black12,
          ),
        ),
      ],
    );
  }
}
