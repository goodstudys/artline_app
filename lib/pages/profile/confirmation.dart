import 'dart:async';
import 'dart:convert';
import 'package:artline/env.dart';
import 'package:artline/theme/style.dart';
import 'package:http/http.dart' as http;
import 'package:artline/components/inputTextForm.dart';
import 'package:artline/pages/profile/orders.dart';
import 'package:artline/storage/storage.dart';
import 'package:flutter/material.dart';

class Confirmation extends StatefulWidget {
  @override
  _ConfirmationState createState() => _ConfirmationState();
}

class _ConfirmationState extends State<Confirmation> {
  DataStore storage = DataStore();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _tujuan = TextEditingController();
  final TextEditingController _bank = TextEditingController();
  final TextEditingController _akun = TextEditingController();
  final TextEditingController _nama = TextEditingController();
  final TextEditingController _jumlah = TextEditingController();

  List data;
  bool isLoading = false;
  bool _autovalidate = false;
  String getType;
  String orderId;

  _showDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => SimpleDialog(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 30.0, right: 60.0, left: 60.0),
            color: Colors.white,
            child: Image.asset(
              "assets/img/checklist.png",
              height: 110.0,
              color: Colors.lightGreen,
            ),
          ),
          Center(
              child: Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: Text(
              "Sukses !!!",
              style: TxtStyle.txtCustomHead,
            ),
          )),
          Center(
              child: Padding(
            padding: const EdgeInsets.only(top: 30.0, bottom: 40.0),
            child: Text(
              "Konfirmasi Pembayaran Terkirim",
              style: TxtStyle.txtCustomSub,
            ),
          )),
        ],
      ),
    );
  }

  startTime() async {
    return Timer(Duration(milliseconds: 1450), navigator);
  }

  void navigator() {
    Navigator.of(context).pushReplacement(
        PageRouteBuilder(pageBuilder: (_, __, ___) => new Orders()));
  }

  submit() async {
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var res = await http.post(url('api/confirmation'), headers: {
      'Authorization': $tokenType + ' ' + $accesToken
    }, body: {
      'order': orderId,
      'tujuan': _tujuan.text,
      'bank': _bank.text,
      'akun': _akun.text,
      'nama': _nama.text,
      'jumlah': _jumlah.text,
    });
    var data = json.decode(res.body);
    setState(() {
      print(data);
    });
  }

  Future<String> getData() async {
    var idOR = await storage.getDataString('idOrder');

    setState(() {
      orderId = idOR;
    });

    //print(data);
    return 'success!';
  }

  @override
  void initState() {
    super.initState();
    getData().then((s) => setState(() {
          isLoading = true;
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Konfirmasi Pembayaran'),
      ),
      body: isLoading
          ? Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(
                      top: 13.0, right: 13.0, left: 13.0, bottom: 85.0),
                  child: Form(
                      key: _formKey,
                      child: ListView(
                        children: <Widget>[
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'Nama Pengirim di Rekening Bank',
                            textController: _nama,
                          ),
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'Transfer dari Bank',
                            textController: _bank,
                          ),
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'Masukkan No. Rekening Anda',
                            textController: _akun,
                          ),
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'Tujuan Transfer',
                            textController: _tujuan,
                          ),
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'Jumlah Transfer',
                            textController: _jumlah,
                          ),
                        ],
                      )),
                ),
                Positioned(
                  bottom: 13.0,
                  left: 13.0,
                  right: 13.0,
                  child: InkWell(
                    onTap: () {
                      final formState = _formKey.currentState;
                      if (formState.validate()) {
                        submit();
                        _showDialog(context);
                        startTime();
                        // showDialog(
                        //   context: context,
                        //   builder: (BuildContext context) {
                        //     return AlertDialog(
                        //       shape: RoundedRectangleBorder(
                        //           borderRadius:
                        //               new BorderRadius.circular(10.0)),
                        //       content: new Text(
                        //         'Apakah data anda sudah benar?',
                        //         textAlign: TextAlign.left,
                        //       ),
                        //       actions: <Widget>[
                        //         FlatButton(
                        //           child: Text("No"),
                        //           onPressed: () {
                        //             Navigator.of(context).pop();
                        //           },
                        //         ),
                        //         FlatButton(
                        //           child: new Text("Yes"),
                        //           onPressed: () {
                        //             submit();
                        //             _scaffoldKey.currentState
                        //                 .showSnackBar(SnackBar(
                        //               content:
                        //                   Text("Item berhasil ditambahkan"),
                        //               duration: Duration(seconds: 2),
                        //               backgroundColor: Colors.indigo,
                        //             ));
                        //             back();
                        //             setState(() {});
                        //           },
                        //         ),
                        //       ],
                        //     );
                        //   },
                        // );
                      } else {
                        setState(() {
                          // isLoading = false;
                          _autovalidate = true;
                        });
                      }
                    },
                    child: Container(
                      height: 55.0,
                      width: 300.0,
                      decoration: BoxDecoration(
                          color: Colors.indigoAccent,
                          borderRadius:
                              BorderRadius.all(Radius.circular(40.0))),
                      child: Center(
                        child: Text(
                          "Submit",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w700,
                              fontSize: 16.5,
                              letterSpacing: 1.0),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )
          : Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
    );
  }
}
