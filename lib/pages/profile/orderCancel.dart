import 'dart:convert';
import 'package:artline/storage/storage.dart';
import 'package:artline/components/cardOrder.dart';
import 'package:artline/env.dart';
import 'package:artline/pages/profile/MyOrders.dart';
import 'package:artline/pages/profile/confirmation.dart';
import 'package:artline/theme/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pull_to_refresh/pull_to_refresh.dart';

class OrderCancel extends StatefulWidget {
  @override
  _OrderCancelState createState() => _OrderCancelState();
}

class _OrderCancelState extends State<OrderCancel> {
  DataStore storage = DataStore();
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  List data;
  List prod;
  bool isLoading = false;

  void _onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      getData().then((s) => setState(() {
            isLoading = true;
          }));
    });
    if (mounted)
      setState(() {
        getData().then((s) => setState(() {
              isLoading = true;
            }));
      });
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      getData().then((s) => setState(() {
            isLoading = true;
          }));
    });
    if (mounted)
      setState(() {
        getData().then((s) => setState(() {
              isLoading = true;
            }));
      });
    _refreshController.loadComplete();
  }

  Future<String> getData() async {
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var $userLevel = await storage.getDataString('userLevel');
    String apiLink;
    if ($userLevel == '5') {
      apiLink = 'api/getSalesOrder';
    } else {
      apiLink = 'api/getOrder';
    }
    var res = await http.get(url(apiLink), headers: {
      'Authorization': $tokenType + ' ' + $accesToken,
      'Accept': 'application/json'
    });
    if (mounted)
      setState(() {
        var content = json.decode(res.body);
        data = content['cancel'];
      });
    return 'success!';
  }

  void saveIdOrder(String id) async {
    storage.setDataString('idOrder', id);
  }

  @override
  void initState() {
    // if (!mounted) {
    //   getData().then((s) => setState(() {
    //         isLoading = true;
    //       }));
    // } else {
    //   return;
    // }
    getData().then((s) {
      if (mounted)
        setState(() {
          isLoading = true;
        });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        header: WaterDropHeader(),
        footer: CustomFooter(
          builder: (BuildContext context, LoadStatus mode) {
            Widget body;
            if (mode == LoadStatus.idle) {
              body = Text("pull up load");
            } else if (mode == LoadStatus.loading) {
              body = CupertinoActivityIndicator();
            } else if (mode == LoadStatus.failed) {
              body = Text("Load Failed!Click retry!");
            } else if (mode == LoadStatus.canLoading) {
              body = Text("release to load more");
            } else {
              body = Text("No more Data");
            }
            return Container(
              height: 55.0,
              child: Center(child: body),
            );
          },
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: isLoading
            ? Container(
                child: data == null
                    ? Text('')
                    : ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: data == null ? 0 : data.length,
                        itemBuilder: (BuildContext context, int index) {
                          return CardOrder(
                            type: 'cancel',
                            tapdetail: () {
                              saveIdOrder(data[index]['rowPointer'].toString());
                              Navigator.of(context).push(PageRouteBuilder(
                                  pageBuilder: (_, __, ___) => new Order()));
                            },
                            orderNumber: data[index]['orderCode'],
                            konfirm:
                                data[index]['confirmationsisActive'].toString(),
                            name: data[index]['detail'][0]['name'],
                            price: data[index]['detail'][0]['price'].toString(),
                            resi: data[index]['noResi'],
                            discAmount: data[index]['detail'][0]['discAmount'],
                            discPercent: data[index]['detail'][0]
                                ['discPercent'],
                            jumlah: data[index]['detail'].length - 1,
                            qty:
                                data[index]['detail'][0]['quantity'].toString(),
                            image: data[index]['detail'][0]['image'],
                            tap: () {
                              saveIdOrder(data[index]['id'].toString());
                              Navigator.of(context).push(PageRouteBuilder(
                                  pageBuilder: (_, __, ___) =>
                                      new Confirmation()));
                            },
                          );
                        },
                      ),
                // ListView(
                //   children: <Widget>[
                //     Container(
                //         margin: EdgeInsets.only(
                //             bottom: 8.0, left: 13.0, right: 13.0, top: 13.0),
                //         padding: EdgeInsets.all(13.0),
                //         decoration: BoxDecoration(
                //             color: Colors.white,
                //             borderRadius: BorderRadius.circular(10.0),
                //             boxShadow: [
                //               BoxShadow(
                //                   color: Colors.black12,
                //                   blurRadius: 5,
                //                   spreadRadius: 2,
                //                   offset: Offset(5, 5))
                //             ]),
                //         child: Material(
                //             color: Colors.transparent,
                //             child: InkWell(
                //               child: Column(
                //                 children: <Widget>[
                //                   Row(
                //                     crossAxisAlignment: CrossAxisAlignment.start,
                //                     mainAxisAlignment:
                //                         MainAxisAlignment.spaceBetween,
                //                     children: <Widget>[
                //                       Text('ARTL00008Y47356N7YT'),
                //                       Text('Belum Bayar')
                //                     ],
                //                   ),
                //                   Row(
                //                     crossAxisAlignment: CrossAxisAlignment.center,
                //                     mainAxisAlignment:
                //                         MainAxisAlignment.spaceBetween,
                //                     children: <Widget>[
                //                       Image.asset(
                //                         'assets/imgItem/ink4.jpg',
                //                         width: 100.0,
                //                       ),
                //                       Column(
                //                         children: <Widget>[
                //                           Text('nama PRODUK'),
                //                           Text('2x'),
                //                           Text('Rp. 10.000')
                //                         ],
                //                       )
                //                     ],
                //                   ),
                //                   Row(
                //                     crossAxisAlignment: CrossAxisAlignment.center,
                //                     mainAxisAlignment:
                //                         MainAxisAlignment.spaceBetween,
                //                     children: <Widget>[
                //                       Container(
                //                         width:
                //                             MediaQuery.of(context).size.width / 3,
                //                         child: Text(
                //                             'Bayar Sebelum 28-04-2020 dengan transfer bank bca'),
                //                       ),
                //                       Container(
                //                         width:
                //                             MediaQuery.of(context).size.width / 2,
                //                         child: FlatButton(
                //                             padding: EdgeInsets.all(8.0),
                //                             color: Colors.indigo,
                //                             onPressed: () {},
                //                             child: Text(
                //                               'Konfirmasi Pembayaran',
                //                               style: TextStyle(color: Colors.white),
                //                             )),
                //                       )
                //                     ],
                //                   )
                //                 ],
                //               ),
                //             )))
                //   ],
                // ),
              )
            : Container(
                child: Center(
                  child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(ColorStyle.primaryColor)),
                ),
              ));
  }
}
