import 'package:artline/pages/profile/orderDelivered.dart';
import 'package:artline/pages/profile/orderNew.dart';
import 'package:artline/pages/profile/orderProses.dart';
import 'package:artline/pages/profile/orderCancel.dart';
import 'package:artline/theme/style.dart';
import 'package:flutter/material.dart';

class Orders extends StatefulWidget {
  @override
  _OrdersState createState() => _OrdersState();
}

class _OrdersState extends State<Orders> with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 4);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 4,
        child: Scaffold(
          appBar: AppBar(
            title: Text('My Orders'),
            elevation: 0,
            bottom: TabBar(
                controller: _tabController,
                isScrollable: true,
                labelPadding: EdgeInsets.only(left: 23.0, right: 23.0),
                indicatorPadding: EdgeInsets.only(left: 23.0, right: 23.0),
                labelColor: ColorStyle.primaryColor,
                unselectedLabelColor: Colors.black,
                indicatorSize: TabBarIndicatorSize.label,
                indicator: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10)),
                    color: Colors.white),
                tabs: [
                  Tab(
                    child: Align(
                      alignment: Alignment.center,
                      child: Text("Belum Bayar"),
                    ),
                  ),
                  Tab(
                    child: Align(
                      alignment: Alignment.center,
                      child: Text("Proses"),
                    ),
                  ),
                  Tab(
                    child: Align(
                      alignment: Alignment.center,
                      child: Text("Selesai"),
                    ),
                  ),
                  Tab(
                    child: Align(
                      alignment: Alignment.center,
                      child: Text("Batal"),
                    ),
                  )
                ]),
          ),
          body: TabBarView(
              controller: _tabController,
              physics: NeverScrollableScrollPhysics(),
              children: [
                OrderNew(),
                OrderProses(),
                OrderDelivered(),
                OrderCancel(),
              ]),
        ));
  }
}
