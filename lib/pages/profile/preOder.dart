import 'package:artline/pages/profile/preorderDelivered.dart';
import 'package:artline/pages/profile/preorderNew.dart';
import 'package:artline/pages/profile/preorderProses.dart';
import 'package:flutter/material.dart';

class MyPreorder extends StatefulWidget {
  @override
  _MyPreorderState createState() => _MyPreorderState();
}

class _MyPreorderState extends State<MyPreorder> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            title: Text('My PreOrder'),
            elevation: 0,
            bottom: TabBar(
                labelColor: Colors.indigo,
                unselectedLabelColor: Colors.black,
                indicatorSize: TabBarIndicatorSize.label,
                indicator: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10)),
                    color: Colors.white),
                tabs: [
                  Tab(
                    child: Align(
                      alignment: Alignment.center,
                      child: Text("Belum Bayar"),
                    ),
                  ),
                  Tab(
                    child: Align(
                      alignment: Alignment.center,
                      child: Text("Proses"),
                    ),
                  ),
                  Tab(
                    child: Align(
                      alignment: Alignment.center,
                      child: Text("Selesai"),
                    ),
                  ),
                ]),
          ),
          body: TabBarView(children: [
            PreorderNew(),
            PreorderProses(),
            PreorderDelivered(),
          ]),
        ));
  }
}
