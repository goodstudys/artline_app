import 'dart:convert';
import 'package:artline/model/dataNotif.dart';
import 'package:artline/storage/storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:artline/env.dart';

class Notif extends StatefulWidget {
  @override
  _NotifState createState() => _NotifState();
}

class _NotifState extends State<Notif> {
  DataStore storage = DataStore();
  List<DataNotif> datalist = [];
  bool isLoading = true;

  void loadData() async {
    setState(() {
      isLoading = true;
    });
    try {
      var userLevel = await storage.getDataString('userLevel');
      String topic = 'artlineapp_resellers';
      if (userLevel == '4') {
        topic = 'artlineapp_resellers';
      }
      final res = await http.get(urlNotif('notifs/' + topic), headers: headersNotif);
      if (res.statusCode == 200) {
        final body = json.decode(res.body);
        if (body['status'] == 'success') {
          setState(() {
            datalist = List<DataNotif>.from(body['content']['data'].map((e) =>  DataNotif.fromJson(e)).toList());
            isLoading = false;
          });
        } else if (body['status'] == 'error' && body['content'] == 'No record found.') {
          setState(() {
            datalist = [];
            isLoading = false;
          });
        } else {
          setState(() {
            datalist = [];
            isLoading = false;
          });
        }
      }
    } catch(e) {
      debugPrint(e.toString());
      setState(() {
        datalist = [];
        isLoading = false;
      });
    }
  }
  @override
  void initState() {
    super.initState();
    loadData();
  }

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Notification",
            style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 18.0,
                color: Colors.black54,
                fontFamily: "Gotik"),
          ),
          iconTheme: IconThemeData(
            color: const Color(0xFF6991C7),
          ),
          centerTitle: true,
          elevation: 0.0,
          backgroundColor: Colors.white,
        ),
        body: !isLoading && datalist.length > 0 ? ListView.separated(
          itemCount: datalist.length,
          padding: const EdgeInsets.all(5.0),
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(
                '${datalist[index].title}',
                style: TextStyle(
                  fontSize: 17.5,
                  color: Colors.black87,
                  fontWeight: FontWeight.w600),
              ),
              subtitle: Padding(
                padding: const EdgeInsets.only(top: 6.0),
                child: Container(
                  width: 440.0,
                  child: Text(
                    '${datalist[index].message}',
                    style: new TextStyle(
                        fontSize: 15.0,
                        fontStyle: FontStyle.italic,
                        color: Colors.black38),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
              leading: Container(
                height: 40.0,
                width: 40.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(60.0)),
                  image: DecorationImage(
                    image: AssetImage('assets/img/product-placeholder.jpg'),
                    fit: BoxFit.cover
                  )
                ),
                child: datalist[index].image != null && datalist[index].image != '' ? FadeInImage.assetNetwork(
                  placeholder: 'assets/img/product-placeholder.jpg',
                  image: '${datalist[index].image}',
                  fit: BoxFit.cover,
                  alignment: Alignment.center
                ) : Container(),
              ),
            );
          },
          separatorBuilder: (context, index) {
            return Divider(height: 4.0);
          },
        ) : !isLoading && datalist.length == 0 ? NoItemNotifications()
        : Center(
          child: CircularProgressIndicator(),
        ));
  }
}

void _onTapItem(BuildContext context, DataNotif post) {
  Scaffold.of(context).showSnackBar(
      new SnackBar(content: new Text(post.id.toString() + ' - ' + post.title)));
}

class NoItemNotifications extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQueryData = MediaQuery.of(context);
    return Container(
      width: 500.0,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
                padding:
                    EdgeInsets.only(top: mediaQueryData.padding.top + 100.0)),
            Image.asset(
              "assets/img/noNotification.png",
              height: 200.0,
            ),
            Padding(padding: EdgeInsets.only(bottom: 30.0)),
            Text(
              "Not Have Notification",
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 18.5,
                  color: Colors.black54,
                  fontFamily: "Gotik"),
            ),
          ],
        ),
      ),
    );
  }
}
