import 'dart:convert';
import 'package:artline/components/dialogSuccess.dart';
import 'package:artline/components/cardOrder.dart';
import 'package:artline/env.dart';
import 'package:artline/pages/profile/MyOrders.dart';
import 'package:artline/pages/profile/confirmation.dart';
import 'package:artline/storage/storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pull_to_refresh/pull_to_refresh.dart';

class OrderNew extends StatefulWidget {
  @override
  _OrderNewState createState() => _OrderNewState();
}

class _OrderNewState extends State<OrderNew> {
  DataStore storage = DataStore();
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  List data;
  List prod;
  bool isLoading = false;

  void _onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      getData().then((s) => setState(() {
            isLoading = true;
          }));
    });
    if (mounted)
      setState(() {
        getData().then((s) => setState(() {
              isLoading = true;
            }));
      });
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      getData().then((s) => setState(() {
            isLoading = true;
          }));
    });
    if (mounted)
      setState(() {
        getData().then((s) => setState(() {
              isLoading = true;
            }));
      });
    _refreshController.loadComplete();
  }

  Future<String> cancelOrder(String orderPointer) async {
    setState(() {
      isLoading = true;
    });
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var res = await http.get(url('api/cancelOrder/' + orderPointer), headers: {
      'Authorization': $tokenType + ' ' + $accesToken,
      'Accept': 'application/json'
    });
    setState(() {
      var data = json.decode(res.body);
      if (data['status'] == 'success') {
        showDialog(
            context: context,
            builder: (context) {
              Future.delayed(Duration(seconds: 1), () {
                Navigator.of(context).pop(true);
                Navigator.of(context).pop(true);
                _onRefresh();
              });
              return DialogSuccess(
                type: true,
                txtHead: data['status'] + '!!!',
                txtSub: data['message'],
              );
            });
      } else if (data['status'] == 'danger') {
        showDialog(
            context: context,
            builder: (context) {
              Future.delayed(Duration(seconds: 1), () {
                Navigator.of(context).pop(true);
                Navigator.of(context).pop(true);
                _onRefresh();
              });
              return DialogSuccess(
                type: false,
                txtHead: data['status'] + '!!!',
                txtSub: data['message'],
              );
            });
      }
    });
    setState(() {
      isLoading = false;
    });
    return 'success!';
  }

  Future<String> getData() async {
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var $userLevel = await storage.getDataString('userLevel');
    String apiLink;
    if ($userLevel == '5') {
      apiLink = 'api/getSalesOrder';
    } else if ($userLevel == '4') {
      apiLink = 'api/getPreOrder';
    } else {
      apiLink = 'api/getOrder';
    }
    var res = await http.get(url(apiLink), headers: {
      'Authorization': $tokenType + ' ' + $accesToken,
      'Accept': 'application/json'
    });
    setState(() {
      var content = json.decode(res.body);
      print(content.toString());
      data = content['new'];
      // data = content['order'];
      // where((i) => i['status'] == 'NEW').toList();
      // prod = content['order']['detail'];
    });
    return 'success!';
  }

  void saveIdOrder(String id) async {
    storage.setDataString('idOrder', id);
  }

  @override
  void initState() {
    getData().then((s) => setState(() {
          isLoading = true;
        }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        header: WaterDropHeader(),
        footer: CustomFooter(
          builder: (BuildContext context, LoadStatus mode) {
            Widget body;
            if (mode == LoadStatus.idle) {
              body = Text("pull up load");
            } else if (mode == LoadStatus.loading) {
              body = CupertinoActivityIndicator();
            } else if (mode == LoadStatus.failed) {
              body = Text("Load Failed!Click retry!");
            } else if (mode == LoadStatus.canLoading) {
              body = Text("release to load more");
            } else {
              body = Text("No more Data");
            }
            return Container(
              height: 55.0,
              child: Center(child: body),
            );
          },
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: isLoading
            ? Container(
                child: data == null
                    ? Text('')
                    : ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: data == null ? 0 : data.length,
                        itemBuilder: (BuildContext context, int index) {
                          return CardOrder(
                            type: 'NEW',
                            orderNumber: data[index]['orderCode'],
                            resi: data[index]['noResi'],
                            konfirm: data[index]['confirmationId'].toString(),
                            name: data[index]['detail'][0]['name'],
                            discAmount: data[index]['detail'][0]['discAmount'],
                            discPercent: data[index]['detail'][0]
                                ['discPercent'],
                            jumlah: data[index]['detail'].length - 1,
                            price: data[index]['detail'][0]['price'],
                            qty:
                                data[index]['detail'][0]['quantity'].toString(),
                            image: data[index]['detail'][0]['image'],
                            tapdetail: () {
                              saveIdOrder(data[index]['rowPointer'].toString());
                              Navigator.of(context).push(PageRouteBuilder(
                                  pageBuilder: (_, __, ___) => new Order()));
                            },
                            tapCancel: () {
                              showDialog(
                                context: context,
                                builder: (context) => new AlertDialog(
                                  // title: new Text('Apakah anda ingin keluar dar halaman invoice?'),
                                  content: new Text(
                                      'Apakah anda ingin membatalkan pesanan anda?'),
                                  actions: <Widget>[
                                    new FlatButton(
                                      onPressed: () =>
                                          Navigator.of(context).pop(false),
                                      child: new Text('Tidak'),
                                    ),
                                    new FlatButton(
                                      onPressed: () {
                                        cancelOrder(data[index]['rowPointer']);
                                        Navigator.of(context).pop(false);
                                      },
                                      child: new Text('Iya'),
                                    ),
                                  ],
                                ),
                              );
                            },
                            tap: () {
                              saveIdOrder(data[index]['id'].toString());
                              Navigator.of(context).push(PageRouteBuilder(
                                  pageBuilder: (_, __, ___) =>
                                      new Confirmation()));
                            },
                          );
                        },
                      ),
              )
            : Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ));
  }
}
