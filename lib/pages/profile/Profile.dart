import 'dart:io';
import 'dart:convert';
import 'package:artline/pages/profile/editReseller.dart';
import 'package:artline/storage/storage.dart';
import 'package:http_parser/http_parser.dart';
import 'package:artline/env.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mime/mime.dart';
import 'package:artline/pages/profile/editProfile.dart';
import 'package:artline/pages/profile/orders.dart';
import 'package:artline/pages/profile/AboutApps.dart';
import 'package:artline/pages/profile/CallCenter.dart';
import 'package:artline/pages/profile/Notification.dart';
import 'package:artline/pages/profile/SettingAcount.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

/// Custom Font
var _txt = TextStyle(
  color: Colors.black,
  fontFamily: "Sans",
);

var _txtName = _txt.copyWith(fontWeight: FontWeight.w700, fontSize: 17.0);

var _txtEdit = _txt.copyWith(color: Colors.black26, fontSize: 15.0);

var _txtCategory = _txt.copyWith(
    fontSize: 14.5, color: Colors.black54, fontWeight: FontWeight.w500);

class _ProfileState extends State<Profile> {
  String userName = '';
  String userEmail = '';
  String userImage = '';
  String userPointer = '';
  String tokenType = '';
  String accessToken = '';
  String userLevel = '';
  bool isLoading = true;
  bool _isUploading = false;
  File _image;
  DataStore storage = DataStore();

  Map<String, String> baseHeaders = {
    "Content-type": "application/json",
    "accept": "application/json",
  };
  Future<dynamic> uploadImage(File image) async {
    setState(() {
      _isUploading = true;
    });
    baseHeaders.addAll({
      "Authorization": tokenType + ' ' + accessToken,
    });
    // Find the mime type of the selected file by looking at the header bytes of the file
    final mimeTypeData =
        lookupMimeType(image.path, headerBytes: [0xFF, 0xD8]).split('/');
    // Intilize the multipart request
    final request = http.MultipartRequest(
        'POST', Uri.parse(url('api/updatePhoto/' + userPointer)));
    // Set Header
    request.headers.addAll(baseHeaders);
    // Attach the file in the request
    final file = await http.MultipartFile.fromPath('image', image.path,
        contentType: MediaType(mimeTypeData[0], mimeTypeData[1]));
    // Explicitly pass the extension of the image with request body
    // Since image_picker has some bugs due which it mixes up
    // image extension with file name like this filenamejpge
    // Which creates some problem at the server side to manage
    // or verify the file extension

    request.fields['ext'] = mimeTypeData[1];
    request.fields['data'] = '{"users": {"crmcode": "aaa"}}';
    request.files.add(file);
    try {
      final streamedResponse = await request.send();
      final response = await http.Response.fromStream(streamedResponse);
      var content = json.decode(response.body);
      final int statusCode = response.statusCode;
      if (statusCode == 204) {
        throw new Exception("No record found.");
      } else if (statusCode == 400) {
        throw new Exception("Invalid request parameter");
      } else if (statusCode == 401) {
        throw new Exception(
            "Unauthorized Access, please check your credentials");
      } else if (statusCode == 500) {
        throw new Exception("Server error");
      } else if (json == null) {
        throw new Exception("Content not found");
      }
      setState(() {
        if (content['status'] == 'success') {
          storage.setDataString('userImage', content['imageName']);
          userImage = content['imageName'];
          _isUploading = false;
        }
      });
      return print(content); // Navigator.pushReplacement(
      //   context,
      //   MaterialPageRoute(
      //       builder: (BuildContext context) {
      //         return BottomNavigation(
      //           currentP: 3,
      //         );
      //       },
      //       settings: RouteSettings(
      //           arguments: BottomNavigation(
      //         currentP: 3,
      //       ))),
      // );
      // Navigator.of(context).pushReplacement(PageRouteBuilder(
      //     pageBuilder: (_, __, ___) => new BottomNavigation(
      //           currentP: 3,
      //         )));
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future getImage(ImageSource media) async {
    try {
      File image = await ImagePicker.pickImage(
        source: media,
        maxWidth: 800,
      );
      setState(() {
        _image = image;
      });
      if (_image != null) {
        uploadImage(_image);
        // Closes the bottom sheet
        // Navigator.pop(context);
      }
    } catch (error) {
      // Navigator.pop(context);
      print('Failed!  ' + error.toString());
    }
  }

  void myAlert() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
            title: Text('Please choose media to select'),
            content: Container(
              height: MediaQuery.of(context).size.height / 6,
              child: Column(
                children: <Widget>[
                  FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                      getImage(ImageSource.gallery);
                    },
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.image),
                        Text('From Gallery'),
                      ],
                    ),
                  ),
                  FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                      getImage(ImageSource.camera);
                    },
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.camera),
                        Text('From Camera'),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  void getUserData() async {
    setState(() {
      isLoading = true;
    });
    var name = await storage.getDataString('name');
    var email = await storage.getDataString('email');
    var img = await storage.getDataString('userImage');
    var pointer = await storage.getDataString('userPointer');
    var tokentp = await storage.getDataString('token_type');
    var token = await storage.getDataString('access_token');
    var level = await storage.getDataString('userLevel');
    setState(() {
      userName = name;
      userEmail = email;
      userImage = img;
      userPointer = pointer;
      tokenType = tokentp;
      accessToken = token;
      userLevel = level;
      isLoading = false;
    });
    print(name);
  }

  @override
  void initState() {
    getUserData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQD = MediaQuery.of(context);
    return !isLoading ? SingleChildScrollView(
      child: Stack(
      children: <Widget>[
        Container(
          color: Colors.white,
          child: Column(
            children: [
              Stack(
                children: <Widget>[
                  /// Setting Header Banner
                  Container(
                    height: mediaQD.size.height / 3,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("assets/img/headerProfile.png"),
                        fit: BoxFit.contain,
                        alignment: Alignment.topCenter
                      )
                    ),
                  ),
                  /// Calling _profile variable
                  Padding(
                    padding: EdgeInsets.only(
                      top: mediaQD.size.height / 3 - 100,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Stack(children: <Widget>[
                              _isUploading ? Container(
                                padding: EdgeInsets.only(top: 6.0, right: 6.0),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(45.0),
                                  child: Container(
                                    width: 100,
                                    height: 100,
                                    padding: EdgeInsets.all(20.0),
                                    color: Colors.white,
                                    child: CircularProgressIndicator()
                                  )
                                ),
                              ) : userImage == '' ? Container(
                                padding: EdgeInsets.only(top: 6.0, right: 6.0),
                                child: Container(
                                  padding: EdgeInsets.all(2.0),
                                  height: 100.0,
                                  width: 100.0,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.circle,
                                    border: Border.all(color: Colors.black26, width: 2),
                                    image: DecorationImage(
                                      image: AssetImage('assets/img/product-placeholder.jpg'),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                )
                              ) : Container(
                                padding: EdgeInsets.only(top: 6.0, right: 6.0),
                                child: Container(
                                  padding: EdgeInsets.all(2.0),
                                  height: 100.0,
                                  width: 100.0,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.circle,
                                    border: Border.all(color: Colors.black26, width: 2),
                                  ),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(45.0),
                                    child: FadeInImage.assetNetwork(
                                      image: '${host}upload/user/$userImage',
                                      placeholder: 'assets/img/product-placeholder.jpg',
                                      height: 100.0,
                                      width: 100.0,
                                      fit: BoxFit.cover,
                                    )
                                  )
                                )
                              ),
                              Positioned(
                                top: 0.0,
                                right: 0.0,
                                child: InkWell(
                                  onTap: () {
                                    myAlert();
                                  },
                                  child: Container(
                                    padding: EdgeInsets.all(6.0),
                                    decoration: BoxDecoration(
                                      color: Colors.indigo,
                                      shape: BoxShape.circle,
                                    ),
                                    constraints: BoxConstraints(
                                      minWidth: 20.0,
                                      minHeight: 20.0,
                                    ),
                                    child: Center(
                                      child: Icon(
                                        Icons.camera_alt,
                                        color: Colors.white,
                                        size: 20,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ]),
                            Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: Text(
                                userName,
                                style: _txtName,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 0.0),
                              child: InkWell(
                                onTap: () {
                                  Navigator.of(context).push(
                                      PageRouteBuilder(
                                          pageBuilder: (_, __, ___) =>
                                              new EditProfile()));
                                },
                                child: Text(
                                  "Edit Profile",
                                  style: _txtEdit,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(),
                      ],
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Column(
                  /// Setting Category List
                  children: <Widget>[
                    userLevel != '4' ? RaisedButton(
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                      color: Colors.yellow[800],
                      onPressed: () {
                        Navigator.of(context).push(PageRouteBuilder(
                          pageBuilder: (_, __, ___) => EditReseller())
                        );
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                        child: Text(
                          'Start as Artline Reseller and earn money!',
                          style: TextStyle(
                            color: Colors.white
                          ),
                        ),
                      ),
                    ) : Container(),
                    /// Call category class
                    Category(
                      txt: "Notification",
                      padding: 35.0,
                      image: "assets/icon/notification.png",
                      tap: () {
                        Navigator.of(context).push(PageRouteBuilder(
                            pageBuilder: (_, __, ___) => new Notif()));
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 20.0, left: 85.0, right: 30.0),
                      child: Divider(
                        color: Colors.black12,
                        height: 2.0,
                      ),
                    ),
                    Category(
                      txt: "My Orders",
                      padding: 23.0,
                      image: "assets/icon/truck.png",
                      tap: () {
                        Navigator.of(context).push(PageRouteBuilder(
                            pageBuilder: (_, __, ___) => new Orders()));
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 20.0, left: 85.0, right: 30.0),
                      child: Divider(
                        color: Colors.black12,
                        height: 2.0,
                      ),
                    ),
                    Category(
                      txt: "Setting Acount",
                      padding: 30.0,
                      image: "assets/icon/setting.png",
                      tap: () {
                        Navigator.of(context).push(PageRouteBuilder(
                            pageBuilder: (_, __, ___) =>
                                new SettingAccount()));
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 20.0, left: 85.0, right: 30.0),
                      child: Divider(
                        color: Colors.black12,
                        height: 2.0,
                      ),
                    ),
                    Category(
                      txt: "Call Center",
                      padding: 30.0,
                      image: "assets/icon/callcenter.png",
                      tap: () {
                        Navigator.of(context).push(PageRouteBuilder(
                            pageBuilder: (_, __, ___) =>
                                new CallCenter()));
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 20.0, left: 85.0, right: 30.0),
                      child: Divider(
                        color: Colors.black12,
                        height: 2.0,
                      ),
                    ),
                    Category(
                      padding: 38.0,
                      txt: "About Apps",
                      image: "assets/icon/aboutapp.png",
                      tap: () {
                        Navigator.of(context).push(PageRouteBuilder(
                            pageBuilder: (_, __, ___) =>
                                new AboutApps()));
                      },
                    ),
                    Padding(padding: EdgeInsets.only(bottom: 20.0)),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    )) : Container(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}

/// Component category class to set list
class Category extends StatelessWidget {
  final String txt, image;
  final GestureTapCallback tap;
  final double padding;

  Category({this.txt, this.image, this.tap, this.padding});

  Widget build(BuildContext context) {
    return InkWell(
      onTap: tap,
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 15.0, left: 30.0),
            child: Row(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(right: padding),
                  child: Image.asset(
                    image,
                    height: 25.0,
                  ),
                ),
                Text(
                  txt,
                  style: _txtCategory,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
