import 'dart:convert';
import 'package:artline/components/cardInventory.dart';
import 'package:artline/components/textFormatIDR.dart';
import 'package:artline/env.dart';
import 'package:artline/storage/storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class InvoiceOrder extends StatefulWidget {
  final String rowPointer;
  InvoiceOrder({this.rowPointer});
  @override
  _InvoiceOrderState createState() => _InvoiceOrderState();
}

class _InvoiceOrderState extends State<InvoiceOrder> {
  DataStore storage = DataStore();

  bool isLoading = false;

  void saveIdOrder(String id) async {
    storage.setDataString('idOrder', id);
  }

  var data;
  List produk;
  var prod;
  Future<String> getData() async {
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var res = await http.get(url('api/getOrderDetail/' + widget.rowPointer),
        headers: {
          'Authorization': $tokenType + ' ' + $accesToken,
          'Accept': 'application/json'
        });
    setState(() {
      var content = json.decode(res.body);
      data = content['data'];
      produk = content['data']['detail'];
    });
    return 'success!';
  }

  @override
  void initState() {
    super.initState();
    getData().then((s) => setState(() {
          isLoading = true;
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          // leading: InkWell(
          //     onTap: () {
          //       Navigator.of(context).pop(false);
          //     },
          //     child: Icon(Icons.arrow_back)),
          elevation: 0.0,
          title: Text(
            "Invoice",
            style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 18.0,
                color: Colors.black54,
                fontFamily: "Gotik"),
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Color(0xFF6991C7)),
        ),
        body: isLoading
            ? Container(
                height: MediaQuery.of(context).size.height,
                child: Stack(
                  children: <Widget>[
                    SingleChildScrollView(
                      padding: EdgeInsets.only(bottom: 80.0),
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 25.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(left: 25.0),
                                child: Text(
                                  'Nomor pesanan telah dibuat',
                                  style: TextStyle(
                                    color: Colors.black,
                                    //fontWeight: FontWeight.bold,
                                    fontSize: 18.0,
                                  ),
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 8.0, left: 25.0),
                                child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: EdgeInsets.only(
                                        bottom: 8.0, left: 0.0, right: 13.0),
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        boxShadow: [
                                          BoxShadow(
                                              color: Colors.black12,
                                              blurRadius: 5,
                                              spreadRadius: 2,
                                              offset: Offset(5, 5))
                                        ]),
                                    child: Material(
                                        color: Colors.transparent,
                                        child: Padding(
                                            padding: const EdgeInsets.all(13.0),
                                            child: Text(
                                              data['orderCode'],
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            )))),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 8.0, left: 25.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      'Alamat Pengiriman',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 18.0,
                                      ),
                                    ),
                                    Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        margin: EdgeInsets.only(
                                            bottom: 8.0,
                                            left: 0.0,
                                            right: 13.0),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            boxShadow: [
                                              BoxShadow(
                                                  color: Colors.black12,
                                                  blurRadius: 5,
                                                  spreadRadius: 2,
                                                  offset: Offset(5, 5))
                                            ]),
                                        child: Material(
                                            color: Colors.transparent,
                                            child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: <Widget>[
                                                    Text(
                                                      data['billName'],
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                    Text(data['billPhone']),
                                                    Text(data['billAddress']),
                                                    Text(data['billCity'] +
                                                        ', ' +
                                                        data['billKecamatan'] +
                                                        ', ' +
                                                        data['billProvince']),
                                                    Text(data['billKodePos'])
                                                  ],
                                                )))),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 8.0, left: 25.0, right: 20.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          'Daftar Barang',
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 18.0,
                                          ),
                                        ),
                                        Text(
                                          'On Proccess',
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 18.0,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 8.0, left: 13.0),
                                child: ListView.builder(
                                  physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: produk == null ? 0 : produk.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return CardInventory(
                                      type: 'listbarang',
                                      image: host +
                                          'upload/users/products/' +
                                          produk[index]['image'],
                                      name: produk[index]['name'],
                                      // price: produk[index]['price'],
                                      discAmount: produk[index]['price'],
                                      // discAmount: '40000',
                                      totalPrice:
                                          int.parse(produk[index]['price']),
                                      qty: int.parse(produk[index]['quantity']),
                                      
                                    );
                                  },
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 8.0, left: 25.0),
                                child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: EdgeInsets.only(
                                        bottom: 8.0, left: 0.0, right: 13.0),
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        boxShadow: [
                                          BoxShadow(
                                              color: Colors.black12,
                                              blurRadius: 5,
                                              spreadRadius: 2,
                                              offset: Offset(5, 5))
                                        ]),
                                    child: Material(
                                        color: Colors.transparent,
                                        child: Padding(
                                            padding: const EdgeInsets.all(13.0),
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: <Widget>[
                                                Text(
                                                  'Biaya Ongkir',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                TextFormatIDR(
                                                  thisText: int.parse(
                                                      data['kurirRate']),
                                                  thisStyle: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold),
                                                )
                                              ],
                                            )))),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 8.0, left: 25.0),
                                child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: EdgeInsets.only(
                                        bottom: 8.0, left: 0.0, right: 13.0),
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        boxShadow: [
                                          BoxShadow(
                                              color: Colors.black12,
                                              blurRadius: 5,
                                              spreadRadius: 2,
                                              offset: Offset(5, 5))
                                        ]),
                                    child: Material(
                                        color: Colors.transparent,
                                        child: Padding(
                                            padding: const EdgeInsets.all(13.0),
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: <Widget>[
                                                Text(
                                                  'Total',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                TextFormatIDR(
                                                  thisText:
                                                      int.parse(data['total']),
                                                  thisStyle: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold),
                                                )
                                              ],
                                            )))),
                              ),
                              // Padding(
                              //   padding: const EdgeInsets.only(
                              //       top: 8.0, left: 25.0),
                              //   child: Column(
                              //     crossAxisAlignment:
                              //         CrossAxisAlignment.start,
                              //     children: <Widget>[
                              //       Text(
                              //         'Pengiriman',
                              //         style: TextStyle(
                              //           color: Colors.black,
                              //           fontWeight: FontWeight.w500,
                              //           fontSize: 18.0,
                              //         ),
                              //       ),
                              //       // namaShipping == null ? Text('...') : Text(namaShipping)
                              //     ],
                              //   ),
                              // ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    // Positioned(
                    //   bottom: 15.0,
                    //   left: 8,
                    //   right: 8,
                    //   child: Center(
                    //     child: InkWell(
                    //       onTap: () {
                    //         saveIdOrder(data['id'].toString());
                    //         clear();
                    //         clearShared();
                    //         Navigator.of(context).pushReplacement(
                    //             MaterialPageRoute(
                    //                 builder: (BuildContext context) =>
                    //                     Confirmation()));
                    //       },
                    //       child: Container(
                    //         decoration: BoxDecoration(
                    //             color: Colors.indigo,
                    //             borderRadius:
                    //                 BorderRadius.all(Radius.circular(25.0))),
                    //         height: 50.0,
                    //         width: MediaQuery.of(context).size.width / 1.2,
                    //         child: Center(
                    //             child: Text(
                    //           'Konfirmasi Pembayaran',
                    //           style: TextStyle(
                    //               color: Colors.white,
                    //               fontWeight: FontWeight.bold,
                    //               fontSize: 20.0),
                    //         )),
                    //       ),
                    //     ),
                    //   ),
                    // )
                  ],
                ),
              )
            : Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ));
  }
}
