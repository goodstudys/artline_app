import 'dart:async';
import 'dart:convert';
import 'package:artline/components/dialogSuccess.dart';
import 'package:artline/components/inputTextForm.dart';
import 'package:artline/env.dart';
import 'package:artline/model/dataCity.dart';
import 'package:artline/model/dataProvince.dart';
import 'package:artline/storage/storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class NewAddress extends StatefulWidget {
  @override
  _NewAddressState createState() => _NewAddressState();
}

class _NewAddressState extends State<NewAddress> {
  DataStore storage = DataStore();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _name = TextEditingController();
  final TextEditingController _telp = TextEditingController();
  final TextEditingController _kecamatan = TextEditingController();
  final TextEditingController _kelurahan = TextEditingController();
  final TextEditingController _kodepos = TextEditingController();
  final TextEditingController _label = TextEditingController();
  final TextEditingController _alamat = TextEditingController();

  List data;
  bool isLoading = false;
  bool _autovalidate = false;
  String getType;
  DataCity _city;
  DataProvince _province;

  List<String> type = ['billing', 'shopping'];
  List<DataProvince> listApiProvince;
  List<DataCity> listApiCity;
  List<DataCity> listCity = [];

  Timer _timer;

  void back() {
    _timer = Timer.periodic(
        Duration(seconds: 3),
        (Timer t) => setState(() {
              Navigator.pop(context, true);
            }));
  }

  startTime() async {
    return Timer(Duration(milliseconds: 1450), navigator);
  }

  void navigator() {
    // Navigator.of(context).pushReplacement(
    //     PageRouteBuilder(pageBuilder: (_, __, ___) => new MyAddress()));
    Navigator.pop(context, true);
  }

  submit() async {
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var res = await http.post(url('api/createAddress'), headers: {
      'Authorization': $tokenType + ' ' + $accesToken
    }, body: {
      'type': getType,
      'label': _label.text,
      'name': _name.text,
      'phone': _telp.text,
      'address': _alamat.text,
      'city': _city.cityId,
      'province': _province.provinceId,
      'kecamatan': _kecamatan.text,
      'kelurahan': _kelurahan.text,
      'kodepos': _kodepos.text,
    });
    var data = json.decode(res.body);
    setState(() {
      if (data['status'] == 'success') {
        showDialog(
            context: context,
            builder: (context) {
              Future.delayed(Duration(seconds: 1), () {
                Navigator.of(context).pop(true);
                Navigator.of(context).pop(true);
              });
              return DialogSuccess(
                type: true,
                txtHead: data['status'] + '!!!',
                txtSub: data['message'],
              );
            });
      } else if (data['status'] == 'danger') {
        showDialog(
            context: context,
            builder: (context) {
              Future.delayed(Duration(seconds: 1), () {
                Navigator.of(context).pop(true);
              });
              return DialogSuccess(
                type: false,
                txtHead: data['status'] + '!!!',
                txtSub: data['message'],
              );
            });
      }
    });
  }

  Future<String> getData() async {
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var res = await http.get(url('api/formDelivery'),
        headers: {'Authorization': $tokenType + ' ' + $accesToken});

    setState(() {
      var content = json.decode(res.body);
      // print(content['provinces']['rajaongkir']['results']);
      listApiProvince = List<DataProvince>.from(content['provinces']
              ['rajaongkir']['results']
          .map((item) => DataProvince.fromJson(item)));
      listApiCity = List<DataCity>.from(content['city']['rajaongkir']['results']
          .map((item) => DataCity.fromJson(item)));
    });

    //print(data);
    return 'success!';
  }

  @override
  void initState() {
    super.initState();
    getData().then((s) => setState(() {
          isLoading = true;
        }));
  }

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Alamat Baru'),
      ),
      body: isLoading
          ? Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(
                      top: 13.0, right: 13.0, left: 13.0, bottom: 85.0),
                  child: Form(
                      key: _formKey,
                      child: ListView(
                        children: <Widget>[
                          DropdownButtonFormField(
                              hint: Text("Pilih Type Alamat"),
                              value: getType,
                              items: type.map((value) {
                                return DropdownMenuItem<String>(
                                  child: Text(value),
                                  value: value,
                                );
                              }).toList(),
                              onChanged: (value) {
                                setState(() {
                                  getType = value;
                                });
                              }),
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'Label',
                            textController: _label,
                          ),
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'Nama',
                            textController: _name,
                          ),
                          DropdownButtonFormField(
                            autovalidate: _autovalidate,
                            value: _province,
                            validator: (value) {
                              if (value == null) {
                                return 'Silahkan pilih Provinsi';
                              } else {
                                return null;
                              }
                            },
                            decoration: InputDecoration(
                                hintStyle: TextStyle(color: Colors.black54),
                                filled: false,
                                hintText: 'Pilih Provinsi',
                                labelText: _province == null
                                    ? 'Pilih Provinsi'
                                    : 'Provinsi',
                                // border: InputBorder.none,
                                hoverColor: Colors.white),
                            items: listApiProvince.map((DataProvince value) {
                              return DropdownMenuItem<DataProvince>(
                                value: value,
                                child: Text(
                                  value.province,
                                ),
                              );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                _province = value;
                                listCity = [];
                                _city = null;
                              });
                              setState(() {
                                listApiCity.forEach((f) {
                                  if (f.provinceId.toString() ==
                                      value.provinceId.toString()) {
                                    listCity.add(f);
                                  }
                                });
                              });
                            },
                          ),
                          DropdownButtonFormField(
                            autovalidate: _autovalidate,
                            value: _city,
                            validator: (value) {
                              if (value == null) {
                                return 'Silahkan pilih Kota';
                              } else {
                                return null;
                              }
                            },
                            decoration: InputDecoration(
                                hintStyle: TextStyle(color: Colors.black54),
                                filled: false,
                                hintText: 'Pilih Kota',
                                labelText:
                                    _city == null ? 'Pilih Kota' : 'Kota',
                                hoverColor: Colors.white),
                            items: listCity.map((DataCity value) {
                              return DropdownMenuItem<DataCity>(
                                value: value,
                                child: Text(
                                  value.cityName,
                                  // style: TextStyle(
                                  //   fontWeight: FontWeight.bold,
                                  //   fontSize: 18.0,
                                  //   color: Colors.black,
                                  // ),
                                ),
                              );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                _city = value;
                              });
                            },
                          ),
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'Telepon',
                            textController: _telp,
                          ),
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'Alamat Lengkap',
                            textController: _alamat,
                          ),
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'Kecamatan',
                            textController: _kecamatan,
                          ),
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'Kelurahan',
                            textController: _kelurahan,
                          ),
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'Kode Pos',
                            textController: _kodepos,
                          ),
                        ],
                      )),
                ),
                Positioned(
                  bottom: 13.0,
                  left: 13.0,
                  right: 13.0,
                  child: InkWell(
                    onTap: () {
                      final formState = _formKey.currentState;
                      if (formState.validate()) {
                        submit();
                        // _scaffoldKey.currentState.showSnackBar(SnackBar(
                        //   content: Text("Alamat Baru Tersimpan"),
                        //   duration: Duration(seconds: 2),
                        //   backgroundColor: Colors.indigo,
                        // ));
                        // back();
                        // _showDialog(context);
                        // Navigator.of(context).pop(context);
                        // showDialog(
                        //   context: context,
                        //   builder: (BuildContext context) {
                        //     return AlertDialog(
                        //       shape: RoundedRectangleBorder(
                        //           borderRadius:
                        //               new BorderRadius.circular(10.0)),
                        //       content: new Text(
                        //         'Apakah data anda sudah benar?',
                        //         textAlign: TextAlign.left,
                        //       ),
                        //       actions: <Widget>[
                        //         FlatButton(
                        //           child: Text("No"),
                        //           onPressed: () {
                        //             Navigator.of(context).pop();
                        //           },
                        //         ),
                        //         FlatButton(
                        //           child: new Text("Yes"),
                        //           onPressed: () {
                        //             submit();
                        //             _scaffoldKey.currentState
                        //                 .showSnackBar(SnackBar(
                        //               content:
                        //                   Text("Item berhasil ditambahkan"),
                        //               duration: Duration(seconds: 2),
                        //               backgroundColor: Colors.indigo,
                        //             ));
                        //             back();
                        //             setState(() {});
                        //           },
                        //         ),
                        //       ],
                        //     );
                        //   },
                        // );
                      } else {
                        setState(() {
                          // isLoading = false;
                          _autovalidate = true;
                        });
                      }
                    },
                    child: Container(
                      height: 55.0,
                      width: 300.0,
                      decoration: BoxDecoration(
                          color: Colors.indigoAccent,
                          borderRadius:
                              BorderRadius.all(Radius.circular(40.0))),
                      child: Center(
                        child: Text(
                          "Submit",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w700,
                              fontSize: 16.5,
                              letterSpacing: 1.0),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )
          : Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
    );
  }
}
