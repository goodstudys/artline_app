import 'dart:async';
import 'dart:convert';
import 'package:artline/components/inputTextForm.dart';
import 'package:artline/env.dart';
import 'package:artline/model/dataCity.dart';
import 'package:artline/model/dataProvince.dart';
import 'package:artline/pages/profile/myAddress.dart';
import 'package:artline/storage/storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class EditAddress extends StatefulWidget {
  final String rowPointer;
  EditAddress({this.rowPointer});
  @override
  _EditAddressState createState() => _EditAddressState();
}

class _EditAddressState extends State<EditAddress> {
  DataStore storage = DataStore();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _name = TextEditingController();
  final TextEditingController _telp = TextEditingController();
  final TextEditingController _kecamatan = TextEditingController();
  final TextEditingController _kelurahan = TextEditingController();
  final TextEditingController _kodepos = TextEditingController();
  final TextEditingController _label = TextEditingController();
  final TextEditingController _alamat = TextEditingController();

  List data;
  bool isLoading = false;
  bool _autovalidate = false;
  String getType;
  DataCity _city;
  DataProvince _province;
  List<DataCity> listCityManeh = [];
  String prov, cit;

  List<String> type = ['BILLING', 'SHOPPING'];
  List<DataProvince> listApiProvince;
  List<DataCity> listApiCity;
  List<DataCity> listCity = [];

  /// Custom Text Header for Dialog after user succes payment
  var _txtCustomHead = TextStyle(
    color: Colors.black54,
    fontSize: 23.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Gotik",
  );

  /// Custom Text Description for Dialog after user succes payment
  var _txtCustomSub = TextStyle(
    color: Colors.black38,
    fontSize: 15.0,
    fontWeight: FontWeight.w500,
    fontFamily: "Gotik",
  );

  _showDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => SimpleDialog(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 30.0, right: 60.0, left: 60.0),
            color: Colors.white,
            child: Image.asset(
              "assets/img/checklist.png",
              height: 110.0,
              color: Colors.lightGreen,
            ),
          ),
          Center(
              child: Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: Text(
              "Sukses !!!",
              style: _txtCustomHead,
            ),
          )),
          Center(
              child: Padding(
            padding: const EdgeInsets.only(top: 30.0, bottom: 40.0),
            child: Text(
              "Alamat Baru Tersimpan",
              style: _txtCustomSub,
            ),
          )),
        ],
      ),
    );
  }

  startTime() async {
    return Timer(Duration(milliseconds: 1450), navigator);
  }

  void navigator() {
    Navigator.of(context).pushReplacement(
        PageRouteBuilder(pageBuilder: (_, __, ___) => new MyAddress()));
  }

  submit() async {
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var res = await http
        .post(url('api/updateAddress/' + widget.rowPointer), headers: {
      'Authorization': $tokenType + ' ' + $accesToken
    }, body: {
      // 'type': getType,
      'label': _label.text,
      // 'name': _name.text,
      // 'phone': _telp.text,
      'address': _alamat.text,
      'city': _city.cityId,
      'province': _province.provinceId,
      'kecamatan': _kecamatan.text,
      'kelurahan': _kelurahan.text,
      'kodepos': _kodepos.text,
    });
    var data = json.decode(res.body);
    setState(() {
      print(data);
    });
  }

  Future<String> getData2() async {
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var res = await http.get(url('api/formDelivery'),
        headers: {'Authorization': $tokenType + ' ' + $accesToken});

    setState(() {
      var content = json.decode(res.body);
      listApiProvince = List<DataProvince>.from(content['provinces']
              ['rajaongkir']['results']
          .map((item) => DataProvince.fromJson(item)));
      listApiCity = List<DataCity>.from(content['city']['rajaongkir']['results']
          .map((item) => DataCity.fromJson(item)));
      if (listApiCity != null || listApiProvince != null) {
        listApiCity.forEach((f) {
          if (f.cityId == cit) {
            _city = f;
          }
        });
        listApiProvince.forEach((p) {
          if (p.provinceId == prov) {
            _province = p;
          }
        });
        listApiCity.forEach((f) {
          if (f.provinceId.toString() == _province.provinceId.toString()) {
            setState(() {
              listCity.add(f);
            });
          }
        });
      }
    });

    //print(data);
    return 'success!';
  }

  Future<String> getData() async {
    getData2();
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var res = await http.get(url('api/getAddressDetail/' + widget.rowPointer),
        headers: {'Authorization': $tokenType + ' ' + $accesToken});

    setState(() {
      var content = json.decode(res.body);
      print(content['address']['type']);
      getType = content['address']['type'];
      _label.text = content['address']['label'];
      _name.text = content['address']['name'];
      if (listApiCity != null || listApiProvince != null) {
        listApiCity.forEach((f) {
          if (f.cityId == content['address']['city']) {
            _city = f;
          }
        });
        listApiProvince.forEach((p) {
          if (p.provinceId == content['address']['province']) {
            setState(() {
              _province = p;
            });
          }
        });

        listApiCity.forEach((f) {
          if (f.provinceId.toString() == _province.provinceId.toString()) {
            setState(() {
              listCity.add(f);
            });
          }
        });
      }
      prov = content['address']['province'];
      cit = content['address']['city'];
      _telp.text = content['address']['phone'];
      _alamat.text = content['address']['address'];
      _kecamatan.text = content['address']['kecamatan'];
      _kelurahan.text = content['address']['kelurahan'];
      _kodepos.text = content['address']['kodePos'];
    });

    //print(data);
    return 'success!';
  }

  @override
  void initState() {
    super.initState();
    getData().then((s) => setState(() {
          isLoading = true;
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Alamat'),
      ),
      body: isLoading || listApiProvince != null || listApiCity != null
          ? Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(
                      top: 13.0, right: 13.0, left: 13.0, bottom: 85.0),
                  child: Form(
                      key: _formKey,
                      child: ListView(
                        children: <Widget>[
                          DropdownButtonFormField(
                              hint: Text("Pilih Type Alamat"),
                              value: getType,
                              items: type.map((value) {
                                return DropdownMenuItem<String>(
                                  child: Text(value),
                                  value: value,
                                );
                              }).toList(),
                              onChanged: (value) {
                                setState(() {
                                  getType = value;
                                });
                              }),
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'Label',
                            textController: _label,
                          ),
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'Nama',
                            textController: _name,
                          ),
                          listApiProvince == null
                              ? Text('')
                              : DropdownButtonFormField(
                                  autovalidate: _autovalidate,
                                  value: _province,
                                  validator: (value) {
                                    if (value == null) {
                                      return 'Silahkan pilih Provinsi';
                                    } else {
                                      return null;
                                    }
                                  },
                                  decoration: InputDecoration(
                                      hintStyle:
                                          TextStyle(color: Colors.black54),
                                      filled: false,
                                      hintText: 'Pilih Provinsi',
                                      labelText: _province == null
                                          ? 'Pilih Provinsi'
                                          : 'Provinsi',
                                      // border: InputBorder.none,
                                      hoverColor: Colors.white),
                                  items:
                                      listApiProvince.map((DataProvince value) {
                                    return DropdownMenuItem<DataProvince>(
                                      value: value,
                                      child: Text(
                                        value.province,
                                      ),
                                    );
                                  }).toList(),
                                  onChanged: (value) {
                                    setState(() {
                                      _province = value;
                                      listCityManeh = [];
                                      _city = null;
                                    });
                                    setState(() {
                                      listApiCity.forEach((f) {
                                        if (f.provinceId.toString() ==
                                            value.provinceId.toString()) {
                                          listCityManeh.add(f);
                                        }
                                      });
                                    });
                                  },
                                ),
                          listCityManeh.toString() == '[]'
                              ? Container()
                              : DropdownButtonFormField(
                                  autovalidate: _autovalidate,
                                  value: _city,
                                  // validator: (value) {
                                  //   if (value == null) {
                                  //     return 'Silahkan pilih Kota';
                                  //   }else{
                                  //     return '';
                                  //   }
                                  // },
                                  decoration: InputDecoration(
                                      hintStyle:
                                          TextStyle(color: Colors.black54),
                                      filled: false,
                                      hintText: 'Pilih Kota',
                                      labelText:
                                          _city == null ? 'Pilih Kota' : 'Kota',
                                      hoverColor: Colors.white),
                                  items: listCityManeh.map((DataCity user) {
                                    return new DropdownMenuItem<DataCity>(
                                      value: user,
                                      child: new Text(
                                        user.cityName,
                                        style:
                                            new TextStyle(color: Colors.black),
                                      ),
                                    );
                                  }).toList(),
                                  onChanged: (DataCity newValue) {
                                    setState(() {
                                      _city = newValue;
                                    });
                                  },
                                ),
                          listApiCity == null ||
                                  listCityManeh.toString() != '[]'
                              ? Container()
                              : DropdownButtonFormField(
                                  autovalidate: _autovalidate,
                                  value: _city,
                                  validator: (value) {
                                    if (value == null) {
                                      return 'Silahkan pilih Kota';
                                    } else {
                                      return null;
                                    }
                                  },
                                  decoration: InputDecoration(
                                      hintStyle:
                                          TextStyle(color: Colors.black54),
                                      filled: false,
                                      hintText: 'Pilih Kota',
                                      labelText:
                                          _city == null ? 'Pilih Kota' : 'Kota',
                                      hoverColor: Colors.white),
                                  items: listCityManeh.toString() == '[]'
                                      ? listCity.map((DataCity value) {
                                          return DropdownMenuItem<DataCity>(
                                            value: value,
                                            child: Text(
                                              value.cityName,
                                              // style: TextStyle(
                                              //   fontWeight: FontWeight.bold,
                                              //   fontSize: 18.0,
                                              //   color: Colors.black,
                                              // ),
                                            ),
                                          );
                                        }).toList()
                                      : listCityManeh.map((DataCity value) {
                                          return DropdownMenuItem<DataCity>(
                                            value: value,
                                            child: Text(
                                              value.cityName,
                                              // style: TextStyle(
                                              //   fontWeight: FontWeight.bold,
                                              //   fontSize: 18.0,
                                              //   color: Colors.black,
                                              // ),
                                            ),
                                          );
                                        }).toList(),
                                  onChanged: (value) {
                                    setState(() {
                                      _city = value;
                                    });
                                  },
                                ),
                          _city == null
                              ? Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Text(
                                    'silahkan pilih kota',
                                    style: TextStyle(color: Colors.red),
                                  ),
                                )
                              : Container(),
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'Telepon',
                            textController: _telp,
                          ),
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'Alamat Lengkap',
                            textController: _alamat,
                          ),
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'Kecamatan',
                            textController: _kecamatan,
                          ),
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'Kelurahan',
                            textController: _kelurahan,
                          ),
                          InputTextForm(
                            autovalidate: _autovalidate,
                            label: 'Kode Pos',
                            textController: _kodepos,
                          ),
                        ],
                      )),
                ),
                Positioned(
                  bottom: 13.0,
                  left: 13.0,
                  right: 13.0,
                  child: InkWell(
                    onTap: () {
                      final formState = _formKey.currentState;
                      _autovalidate = true;
                      print(_city.toString());
                      if (formState.validate() && _city != null) {
                        submit();
                        _showDialog(context);
                        startTime();
                        // showDialog(
                        //   context: context,
                        //   builder: (BuildContext context) {
                        //     return AlertDialog(
                        //       shape: RoundedRectangleBorder(
                        //           borderRadius:
                        //               new BorderRadius.circular(10.0)),
                        //       content: new Text(
                        //         'Apakah data anda sudah benar?',
                        //         textAlign: TextAlign.left,
                        //       ),
                        //       actions: <Widget>[
                        //         FlatButton(
                        //           child: Text("No"),
                        //           onPressed: () {
                        //             Navigator.of(context).pop();
                        //           },
                        //         ),
                        //         FlatButton(
                        //           child: new Text("Yes"),
                        //           onPressed: () {
                        //             submit();
                        //             _scaffoldKey.currentState
                        //                 .showSnackBar(SnackBar(
                        //               content:
                        //                   Text("Item berhasil ditambahkan"),
                        //               duration: Duration(seconds: 2),
                        //               backgroundColor: Colors.indigo,
                        //             ));
                        //             back();
                        //             setState(() {});
                        //           },
                        //         ),
                        //       ],
                        //     );
                        //   },
                        // );
                      } else {
                        setState(() {
                          // isLoading = false;
                          _autovalidate = true;
                        });
                      }
                    },
                    child: Container(
                      height: 55.0,
                      width: 300.0,
                      decoration: BoxDecoration(
                          color: Colors.indigoAccent,
                          borderRadius:
                              BorderRadius.all(Radius.circular(40.0))),
                      child: Center(
                        child: Text(
                          "Submit",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w700,
                              fontSize: 16.5,
                              letterSpacing: 1.0),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )
          : Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
    );
  }
}
