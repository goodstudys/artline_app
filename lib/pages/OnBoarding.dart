import 'package:flutter/material.dart';
import 'package:artline/Library/intro_views_flutter-2.4.0/lib/Models/page_view_model.dart';
// import 'package:artline/Library/intro_views_flutter-2.4.0/lib/intro_views_flutter.dart';
// import 'package:artline/pages/auth/ChoseLoginOrSignup.dart';
// import 'package:artline/pages/auth/Login.dart';

class OnBoarding extends StatefulWidget {
  @override
  _OnBoardingState createState() => _OnBoardingState();
}

var _fontHeaderStyle = TextStyle(
  fontFamily: "Popins",
  fontSize: 21.0,
  fontWeight: FontWeight.w800,
  color: Colors.black87,
  letterSpacing: 1.5
);

var _fontDescriptionStyle = TextStyle(
  fontFamily: "Sans",
  fontSize: 14.0,
  color: Colors.black26,
  fontWeight: FontWeight.w400
);

///
/// Page View Model for on boarding
///
final pages = [
  new PageViewModel(
      pageColor:  Colors.white,
      iconColor: Colors.black,
      bubbleBackgroundColor: Colors.black,
      title: Text(
        'Cari alat tulis?',style: _fontHeaderStyle,
      ),
      body: Text(
        'berbagai macam alat tulis tersedia sesuai dengan kebutuhan anda',textAlign: TextAlign.center,
        style: _fontDescriptionStyle
      ),
      mainImage: Image.asset(
        'assets/imgIllustration/IlustrasiOnBoarding1.png',
        height: 80.0,
        width: 80.0,
        alignment: Alignment.center,
      )),

  new PageViewModel(
      pageColor:  Colors.white,
      iconColor: Colors.black,
      bubbleBackgroundColor: Colors.black,
      title: Text(
        'Mudah untuk mencari',style: _fontHeaderStyle,
      ),
      body: Text(
          'Cari dan pilih alat tulis dengan mudah dan cepat',textAlign: TextAlign.center,
          style: _fontDescriptionStyle
      ),
      mainImage: Image.asset(
        'assets/imgIllustration/IlustrasiOnBoarding2.png',
        height: 80.0,
        width: 80.0,
        alignment: Alignment.center,
      )),

  new PageViewModel(
      pageColor:  Colors.white,
      iconColor: Colors.black,
      bubbleBackgroundColor: Colors.black,
      title: Text(
        'Belanja sekarang',style: _fontHeaderStyle,
      ),
      body: Text(
          'Kami memiliki banyak jenis alat tulis yang memiliki kegunaan spesifik yang mempermudah Anda ',textAlign: TextAlign.center,
          style: _fontDescriptionStyle
      ),
      mainImage: Image.asset(
        'assets/imgIllustration/IlustrasiOnBoarding3.png',
        height: 80.0,
        width: 80.0,
        alignment: Alignment.center,
      )),

];

class _OnBoardingState extends State<OnBoarding> {
  @override
  Widget build(BuildContext context) {
    return Container();
    // IntroViewsFlutter(
    //   pages,
      
    //   pageButtonsColor: Colors.black45,
    //   skipText: Text("SKIP",style: _fontDescriptionStyle.copyWith(color: Colors.deepPurpleAccent,fontWeight: FontWeight.w800,letterSpacing: 1.0),),
    //   doneText: Text("DONE",style: _fontDescriptionStyle.copyWith(color: Colors.deepPurpleAccent,fontWeight: FontWeight.w800,letterSpacing: 1.0),),
    //   onTapDoneButton: (){
    //     Navigator.of(context).pushReplacement(PageRouteBuilder(pageBuilder: (_,__,___)=> new LoginScreen(),
    //     transitionsBuilder: (_,Animation<double> animation,__,Widget widget){
    //       return Opacity(
    //         opacity: animation.value,
    //         child: widget,
    //       );
    //     },
    //     transitionDuration: Duration(milliseconds: 1500),
    //     ));
    //   },
    // );
  }
}

