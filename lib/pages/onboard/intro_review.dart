import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:artline/pages/onboard/intro.dart';
import 'package:artline/pages/onboard/color_palette.dart';
import 'package:artline/pages/auth/login.dart';

class IntroPage extends StatefulWidget {
  @override
  _IntroPageState createState() => _IntroPageState();
}

var _fontHeaderStyle = TextStyle(
    fontFamily: "Popins",
    fontSize: 21.0,
    fontWeight: FontWeight.w800,
    color: Colors.black87,
    letterSpacing: 1.5);
var _fontDescriptionStyle = TextStyle(
    fontFamily: "Sans",
    fontSize: 14.0,
    color: Colors.black26,
    fontWeight: FontWeight.w400);

class _IntroPageState extends State<IntroPage> {
  final List<Intro> introList = [
    Intro(
      image: "assets/imgIllustration/IlustrasiOnBoarding1.png",
      title: "Cari alat tulis?",
      description:
          "berbagai macam alat tulis tersedia sesuai dengan kebutuhan anda",
    ),
    Intro(
      image: "assets/imgIllustration/IlustrasiOnBoarding2.png",
      title: "Mudah untuk mencari",
      description: "Cari dan pilih alat tulis dengan mudah dan cepat'",
    ),
    Intro(
      image: "assets/imgIllustration/IlustrasiOnBoarding1.png",
      title: "Belanja sekarang",
      description:
          "Kami memiliki banyak jenis alat tulis yang memiliki kegunaan spesifik yang mempermudah Anda.",
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Swiper.children(
        index: 0,
        autoplay: false,
        loop: false,
        pagination: SwiperPagination(
          margin: EdgeInsets.only(bottom: 20.0),
          builder: DotSwiperPaginationBuilder(
            color: ColorPalette.dotColor,
            activeColor: Colors.deepPurpleAccent,
            size: 10.0,
            activeSize: 10.0,
          ),
        ),
        control: SwiperControl(iconNext: null, iconPrevious: null),
        children: _buildPage(context),
      ),
    );
  }

  List<Widget> _buildPage(BuildContext context) {
    List<Widget> widgets = [];
    for (int i = 0; i < introList.length; i++) {
      Intro intro = introList[i];
      widgets.add(Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              top: MediaQuery.of(context).size.height / 6,
            ),
            child: ListView(
              children: <Widget>[
                Image.asset(
                  intro.image,
                  height: MediaQuery.of(context).size.height / 2.5,
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height / 12.0,
                  ),
                ),
                Center(
                  child: Text(
                    intro.title,
                    style: _fontHeaderStyle,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height / 20.0,
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.height / 20.0,
                  ),
                  child: Text(
                    intro.description,
                    style: _fontDescriptionStyle,
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
          intro.title == "Belanja sekarang"
              ? Container(
                  alignment: Alignment.bottomRight,
                  child: FlatButton(
                    onPressed: () {
                      Navigator.of(context).pushReplacement(PageRouteBuilder(
                        pageBuilder: (_, __, ___) => new LoginScreen(),
                        transitionsBuilder: (_, Animation<double> animation, __,
                            Widget widget) {
                          return Opacity(
                            opacity: animation.value,
                            child: widget,
                          );
                        },
                        transitionDuration: Duration(milliseconds: 500),
                      ));
                    },
                    child: Opacity(
                      opacity: 1.0,

                      child: Text(
                        "DONE",
                        style: _fontDescriptionStyle.copyWith(
                            color: Colors.deepPurpleAccent,
                            fontWeight: FontWeight.w800,
                            letterSpacing: 1.0),
                      ), //Text
                    ), //Opacity
                  ),
                )
              : Container(
                  child: Text(''),
                ),
        ],
      ));
    }
    return widgets;
  }
}
