import 'package:flutter/material.dart';
import 'package:artline/pages/brand/BrandLayout.dart';
import 'package:artline/pages/cart/cartLayout.dart';
import 'package:artline/pages/home/Home.dart';
import 'package:artline/pages/profile/Profile.dart';

class BottomNavigation extends StatefulWidget {
  final int currentP;
  BottomNavigation({this.currentP = 0});
  @override
  _BottomNavigationState createState() => _BottomNavigationState(cur: currentP);
}

class _BottomNavigationState extends State<BottomNavigation> {
  int cur;
  _BottomNavigationState({this.cur});
//  int currentIndex = cur;
  /// Set a type current number a layout class
  Widget callPage(int current) {
    switch (current) {
      case 0:
        return new Menu();
      case 1:
        return new Brand();
      case 2:
        return new CartLayout();
      case 3:
        return new Profile();
        break;
      default:
        return Menu();
    }
  }

  /// Build BottomNavigationBar Widget
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: callPage(cur),
      bottomNavigationBar: Theme(
          data: Theme.of(context).copyWith(
              canvasColor: Colors.white,
              textTheme: Theme.of(context).textTheme.copyWith(
                  caption: TextStyle(color: Colors.black26.withOpacity(0.15)))),
          child: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            currentIndex: cur,
            fixedColor: Color(0xFF6991C7),
            onTap: (value) {
              cur = value;
              setState(() {});
            },
            items: [
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.home,
                    size: 23.0,
                  ),
                  label: "Home"
                  //  title: Text(
                  //   "Home",
                  //   style: TextStyle(fontFamily: "Berlin", letterSpacing: 0.5),
                  //  )
                  ),
              BottomNavigationBarItem(icon: Icon(Icons.shop), label: "Category"
                  //  title: Text(
                  //   "Category",
                  //   style: TextStyle(fontFamily: "Berlin", letterSpacing: 0.5),
                  //  )
                  ),
              BottomNavigationBarItem(
                icon: Icon(Icons.shopping_cart),
                label: "Cart",
                //  title: Text(
                //   "Cart",
                //   style: TextStyle(fontFamily: "Berlin", letterSpacing: 0.5),
                //  )
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.person,
                  size: 24.0,
                ),
                label: "Account",
                //  title: Text(
                //   "Acount",
                //   style: TextStyle(fontFamily: "Berlin", letterSpacing: 0.5),
                //  )
              ),
            ],
          )),
    );
  }
}
