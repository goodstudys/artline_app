import 'package:artline/bloc/cart/Cart.dart';
import 'package:artline/bloc/global/BlocProvider.dart';
import 'package:artline/bloc/global/GlobalBloc.dart';
import 'package:artline/model/dataCart.dart';
import 'package:artline/pages/cart/CartLayout.dart';
import 'package:artline/pages/home/searchView.dart';
import 'package:flutter/material.dart';
import 'package:artline/pages/profile/Notification.dart';
import 'package:artline/pages/home/Search.dart';
import 'package:artline/storage/storage.dart';

class AppbarGradient extends StatefulWidget {
  @override
  _AppbarGradientState createState() => _AppbarGradientState();
}

class _AppbarGradientState extends State<AppbarGradient> {
  TextEditingController searchCtr = TextEditingController();
  CartData cart;
  DataStore storage = DataStore();
  String countNotice = "4";
  void loadCart() async {
    var cartbloc = BlocProvider.of<GlobalBloc>(context).cartBloc;
    if (cartbloc.cart.listCart.length == 0) {
      if (await storage.checkData('listcart')) {
        var listcart = await storage.fetchObject('listcart');
        if (listcart['cart'].length > 0) {
          var shopcart = CartResponse.fromJson(listcart['cart']).results;
          cartbloc.updateCart(shopcart);
        }
      }
    }
  }

  void saveCari(String cari) async {
    storage.setDataString('cari', cari);
  }

  @override
  void initState() {
    loadCart();
    super.initState();
  }

  /// Build Appbar in layout home
  @override
  Widget build(BuildContext context) {
    /// Create responsive height and padding
    final double statusBarHeight = MediaQuery.of(context).padding.top;

    /// Create component in appbar
    return Container(
      padding: EdgeInsets.only(top: statusBarHeight),
      height: 58.0 + statusBarHeight,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            const Color(0xFFA3BDED),
            const Color(0xFF0066CC),
          ],
          begin: const FractionalOffset(0.0, 0.0),
          end: const FractionalOffset(1.0, 0.0),
          stops: [0.0, 1.0],
          tileMode: TileMode.clamp
        )
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          /// if user click shape white in appbar navigate to search layout
          InkWell(
            onTap: () {
              Navigator.of(context).push(PageRouteBuilder(
                  pageBuilder: (_, __, ___) => SearchAppbar(),

                  /// transtation duration in animation
                  transitionDuration: Duration(milliseconds: 750),

                  /// animation route to search layout
                  transitionsBuilder:
                      (_, Animation<double> animation, __, Widget child) {
                    return Opacity(
                      opacity: animation.value,
                      child: child,
                    );
                  }));
            },

            /// Create shape background white in appbar (background Artline Shop text)
            child: Container(
              height: 37.0,
              width: 222.0,
              padding: EdgeInsets.only(left: 10.0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                shape: BoxShape.rectangle
              ),
              child: Theme(
                data: ThemeData(hintColor: Colors.transparent),
                child: TextField(
                  controller: searchCtr,
                  onChanged: (value) {
                    saveCari(value);
                  },
                  onSubmitted: (value) {
                    saveCari(value);
                    searchCtr.clear();
                    Navigator.of(context).push(
                      PageRouteBuilder(pageBuilder: (_, __, ___) => SearchView())
                    );
                  },
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    suffixIcon: InkWell(
                      onTap: () {
                        searchCtr.clear();
                        Navigator.of(context).push(PageRouteBuilder(pageBuilder: (_, __, ___) => SearchView()));
                      },
                      child: Icon(
                        Icons.search,
                      )
                    ),
                    hintText: "Find product...",
                    hintStyle: TextStyle(
                      fontSize: 14.0,
                      height: 1.3,
                      color: Colors.black54,
                      fontFamily: "Gotik",
                      fontWeight: FontWeight.w400
                    )
                  ),
                )
              )
                /*Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Icon(
                      Icons.search,
                      color: Colors.black45,
                    )
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 3.0, left: 10.0,),
                    child: Text(
                      "Artline Shop",
                      style: TextStyle(
                          fontFamily: "Popins",
                          color: Colors.black12,
                          fontWeight: FontWeight.w900,
                          letterSpacing: 0.0,
                          fontSize: 16.4),
                    ),
                  ),
                ],
              ),
              */
            ),
          ),

          /// Icon chat (if user click navigate to chat layout)
          InkWell(
            onTap: () {
              Navigator.of(context).push(PageRouteBuilder(
                  pageBuilder: (_, __, ___) => new CartLayout()));
            },
            child: Stack(
              alignment: AlignmentDirectional(-1.0, -0.8),
              children: <Widget>[
                IconButton(
                  iconSize: 28.0,
                    onPressed: null,
                    icon: Icon(

                      Icons.shopping_cart,
                      color: Colors.white,
                    )),
                CircleAvatar(
                  radius: 10.0,
                  backgroundColor: Colors.red,
                  child: StreamBuilder(
                    stream: BlocProvider.of<GlobalBloc>(context).cartBloc.cartStream,
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) {
                        return Text(
                          '0',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                          )
                        );
                      } else {
                        cart = snapshot.data;
                        if (cart.listCart.length > 0) {
                          return Text(
                            cart.listCart.length.toString(),
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 13.0,
                            )
                          );
                        } else {
                          return Text(
                            '0',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 13.0,
                            )
                          );
                        }
                      }
                    }
                  ),
                ),
              ],
            ),
          ),

          /// Icon notification (if user click navigate to notification layout)
          InkWell(
            onTap: () {
              Navigator.of(context).push(
                  PageRouteBuilder(pageBuilder: (_, __, ___) => new Notif()));
            },
            child: Stack(
              alignment: AlignmentDirectional(-3.0, -3.0),
              children: <Widget>[
                Image.asset(
                  "assets/img/notifications-button.png",
                  height: 24.0,
                ),
                /*
                CircleAvatar(
                  radius: 8.6,
                  backgroundColor: Colors.redAccent,
                  child: Text(
                    countNotice,
                    style: TextStyle(fontSize: 13.0, color: Colors.white),
                  ),
                )
                */
              ],
            ),
          ),
        ],
      ),
    );
  }
}
