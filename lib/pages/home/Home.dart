import 'dart:convert';
import 'dart:io';
import 'package:artline/Library/carousel_pro/carousel_pro.dart';
import 'package:artline/bloc/global/BlocProvider.dart';
import 'package:artline/bloc/global/GlobalBloc.dart';
import 'package:artline/components/gridItemCard.dart';
import 'package:artline/env.dart';
import 'package:artline/model/dataProduct.dart';
import 'package:artline/storage/storage.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:artline/ListItem/HomeGridItemRecomended.dart';
import 'package:artline/pages/home/AppbarGradient.dart';
import 'package:artline/pages/home/CategoryDetail.dart';
import 'package:artline/pages/home/DetailProduct.dart';
import 'package:http/http.dart' as http;

class Menu extends StatefulWidget {
  @override
  _MenuState createState() => _MenuState();
}

/// Component all widget in home
class _MenuState extends State<Menu> with TickerProviderStateMixin {
  /// Declare class GridItem from HomeGridItemReoomended.dart in folder ListItem
  GridItem gridItem;

  bool isStarted = false;
  var hourssub, minutesub, secondsub;
  String userLevel;
  bool isLoading = false;
  DataStore storage = DataStore();
  List data = [];
  List dataPromotion;
  List dataCategories;
  List<DataProduct> dataRecom;
  Future<String> getData() async {
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var leveluser = await storage.getDataString('userLevel');
    var res = await http.get(url('api/home'), headers: {
      'Authorization': $tokenType + ' ' + $accesToken,
      'Accept': 'application/json'
    });
    if (mounted)
      setState(() {
        var content = json.decode(res.body);
        data = content['slider'];
        dataPromotion = content['promotion'];
        dataCategories = content['categories'];
        // dataRecom = content['recomProducts'];
        dataRecom = List<DataProduct>.from(
            content['recomProducts'].map((item) => DataProduct.fromJson(item)));
        userLevel = leveluser;
      });
    return 'success!';
  }

  void saveIdProduct(String id) async {
    storage.setDataString('idProd', id);
    BlocProvider.of<GlobalBloc>(context).cartBloc.cart.getGroup = null;
  }

  void saveIdCategory(String id, String name) async {
    storage.setDataString('idCat', id);
    storage.setDataString('nameCat', name);
  }

  // Firebase Cloud Messaging
  // int _bottomNavBarSelectedIndex = 0;
  // bool _newNotification = false;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  /*
  final Map<String, MessageBean> _items = <String, MessageBean>{};
  MessageBean _itemForMessage(Map<String, dynamic> message) {
    //If the message['data'] is non-null, we will return its value, else return map message object
    final dynamic data = message['data'] ?? message;
    final String itemId = data['id'];
    final MessageBean item = _items.putIfAbsent(
        itemId, () => MessageBean(itemId: itemId))
      ..status = data['status'];
    return item;
  }

  //PRIVATE METHOD TO HANDLE TAPPED EVENT ON THE BOTTOM BAR ITEM
  void _onItemTapped(int index) {
    setState(() {
      _bottomNavBarSelectedIndex = index;
      if (index == 1){
        _newNotification = false ;
      }
    });
  }
  */

  Future<void> _showNotif(message) async {
    return showDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(message['data']['title']),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                message['data']['image'] != null &&
                        message['data']['image'] != ''
                    ? Container(
                        child: FadeInImage.assetNetwork(
                            placeholder: 'assets/img/product-placeholder.jpg',
                            image: message['data']['image'],
                            fit: BoxFit.contain,
                            alignment: Alignment.topCenter),
                      )
                    : Container(),
                Text(message['data']['message']),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                'Close',
                style: TextStyle(color: Colors.grey[700]),
              ),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
            TextButton(
              child: Text('Next'),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            ),
          ],
        );
      },
    ).then((value) => value ? _navigateToItemDetail(message) : '');
  }

  void _navigateToItemDetail(Map<String, dynamic> message) {
    String status = message['data']['status'];
    if (status.indexOf('product') > -1) {
      var splits = status.split('/');
      saveIdProduct(splits[1]);
      Navigator.of(context).push(PageRouteBuilder(
          pageBuilder: (_, __, ___) => new DetailProduk(),
          transitionsBuilder:
              (_, Animation<double> animation, __, Widget child) {
            return Opacity(
              opacity: animation.value,
              child: child,
            );
          },
          transitionDuration: Duration(milliseconds: 850)));
    } else if (status.indexOf('category') > -1) {
      var splits = status.split('/');
      if (splits[1] != null && splits[2] != null) {
        saveIdCategory(splits[1], splits[2]);
        Navigator.of(context).push(PageRouteBuilder(
            pageBuilder: (_, __, ___) => CategoryDetail(),
            transitionDuration: Duration(milliseconds: 500),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            }));
      }
    }
  }

  void subscribeCheck() async {
    // check storage
    if (await storage.checkData('subscribe')) {
      if (await storage.getDataBool('subscribe') == false) {
        subscribeNotif();
      }
    } else {
      subscribeNotif();
    }
  }

  void subscribeNotif() async {
    try {
      final idnum = await storage.getDataString('id');
      final res = await http.get(urlNotif('subscriptions/subscription' + idnum),
          headers: headersNotif);
      if (res.statusCode == 200) {
        final body = json.decode(res.body);
        if (body['status'] == 'error' &&
            body['content'] == 'No record found.') {
          // not subscribed
          String topic = 'artlineapp_members';
          if (userLevel == '4') {
            topic = 'artlineapp_resellers';
          }
          // register to database
          // Getting the token from FCM
          final String token = await _firebaseMessaging.getToken();
          debugPrint(token);
          if (token != null) {
            var body = {
              'subscriptions': {
                'user_id': idnum,
                'token': token,
                'notification_key_name': '',
                'notification_key': '',
                'device': Platform.isIOS ? 'iOS' : 'Android', // temporary
                'topic': topic,
              }
            };
            try {
              final regres = await http.post(urlNotif('subscriptions'),
                  headers: headersNotif, body: utf8.encode(json.encode(body)));
              if (regres.statusCode == 200) {
                final regbody = json.decode(regres.body);
                if (regbody['status'] == 'success') {
                  _firebaseMessaging.subscribeToTopic(topic);
                  storage.setDataBool("subscribe", true);
                  debugPrint('Subscribe Success');
                }
              }
            } catch (e) {
              debugPrint(e.toString());
            }
          }
        } else if (body['status'] == 'success') {
          storage.setDataBool("subscribe", true);
          debugPrint('Push Notif already subscribed');
        }
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    getData().then((s) {
      if (mounted)
        setState(() {
          isLoading = true;
        });
    });
    // Firebase check subscribtion
    subscribeCheck();
    // Firebase get message
    if (Platform.isAndroid) {
      _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
          debugPrint("onMessage: $message");
          _showNotif(message);
        },
        onLaunch: (Map<String, dynamic> message) async {
          debugPrint("onLaunch: $message");
          _navigateToItemDetail(message);
        },
        onResume: (Map<String, dynamic> message) async {
          debugPrint("onResume: $message");
          _navigateToItemDetail(message);
        },
      );
    }

    //Needed by iOS only
    if (Platform.isIOS) {
      _firebaseMessaging.requestNotificationPermissions(
          const IosNotificationSettings(sound: true, badge: true, alert: true));
      _firebaseMessaging.onIosSettingsRegistered
          .listen((IosNotificationSettings settings) {
        debugPrint("Settings registered: $settings");
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    /*
    double aspecttRatio;
    print(MediaQuery.of(context).size.height);
    if (MediaQuery.of(context).size.height <= 715.0) {
      aspecttRatio = 1150;
    } else {
      aspecttRatio = 1150;
    }
    */
    final children = [];

    for (var post in data) {
      children.add(
        NetworkImage(host + 'upload/users/sliders/' + post['image']),
      );
    }

    MediaQueryData mediaQueryData = MediaQuery.of(context);

    /// ImageSlider in header
    var imageSlider = isLoading
        ? Container(
            height: 166.0,
            child: new Carousel(
                boxFit: BoxFit.cover,
                dotColor: Color(0xFF6991C7).withOpacity(0.8),
                dotSize: 5.5,
                dotSpacing: 16.0,
                dotBgColor: Colors.transparent,
                showIndicator: true,
                overlayShadow: true,
                overlayShadowColors: Colors.white.withOpacity(0.9),
                overlayShadowSize: 0.9,
                images: children),
          )
        : Text('');

    var promoHorizontalList = isLoading
        ? dataPromotion == null
            ? Text('')
            : Container(
                alignment: Alignment.centerLeft,
                // color: Colors.red,
                height: 230.0,
                padding: EdgeInsets.only(bottom: 0.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                        padding:
                            EdgeInsets.only(left: 13.0, top: 15.0, bottom: 3.0),
                        child: Text(
                          "Promotions",
                          style: TextStyle(
                              fontSize: 15.0,
                              fontFamily: "Sans",
                              fontWeight: FontWeight.w700),
                        )),
                    Expanded(
                      child: ListView.builder(
                        shrinkWrap: true,
                        padding: EdgeInsets.only(top: 5.0),
                        scrollDirection: Axis.horizontal,
                        itemCount:
                            dataPromotion == null ? 0 : dataPromotion.length,
                        itemBuilder: (BuildContext context, int index) {
                          //Padding(padding: EdgeInsets.only(left: 20.0));
                          return Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: InkWell(
                                onTap: () {
                                  // Navigator.of(context).push(PageRouteBuilder(
                                  //     pageBuilder: (_, __, ___) => new ItemList(),
                                  //     transitionDuration: Duration(milliseconds: 500),
                                  //     transitionsBuilder: (_, Animation<double> animation,
                                  //         __, Widget child) {
                                  //       return Opacity(
                                  //         opacity: animation.value,
                                  //         child: child,
                                  //       );
                                  //     }));
                                  // ScopedModel.of<CartModel>(context).testClick(listWeek[index].title);
                                },
                                child: Image.network(host +
                                    'upload/users/promotions/' +
                                    dataPromotion[index]['image'])),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              )
        : Text('');
    var categoryImageBottom = isLoading
        ? Container(
            height: 180.0,
            color: Colors.white,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 13.0, top: 10.0),
                    child: Text(
                      "Category",
                      style: TextStyle(
                          fontSize: 17.0,
                          fontWeight: FontWeight.w700,
                          fontFamily: "Sans"),
                    ),
                  ),
                  Expanded(
                    child: GridView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount:
                            dataCategories == null ? 0 : dataCategories.length,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 1,
                            crossAxisSpacing: 3.0,
                            mainAxisSpacing: 0.5),
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            child: Padding(
                              padding: EdgeInsets.only(left: 13.0, top: 5.0),
                              child: CategoryItemValue(
                                image: host +
                                    'upload/users/categories/' +
                                    dataCategories[index]['image'],
                                title: dataCategories[index]['name'],
                                tap: () {
                                  saveIdCategory(
                                      dataCategories[index]['id'].toString(),
                                      dataCategories[index]['name']);
                                  Navigator.of(context).push(PageRouteBuilder(
                                      pageBuilder: (_, __, ___) =>
                                          new CategoryDetail(),
                                      transitionDuration:
                                          Duration(milliseconds: 500),
                                      transitionsBuilder: (_,
                                          Animation<double> animation,
                                          __,
                                          Widget child) {
                                        return Opacity(
                                          opacity: animation.value,
                                          child: child,
                                        );
                                      }));
                                },
                              ),
                            ),
                          );
                        }),
                  ),
                ]))
        : Text('');

    ///  Grid item in bottom of Category
    var gridProduct = isLoading || dataRecom.toString() != '[]'
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 13.0, top: 10.0),
                child: Text(
                  "Recomended",
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 17.0,
                  ),
                ),
              ),
              GridView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  padding: EdgeInsets.only(
                      left: 13.0, right: 13.0, top: 5.0, bottom: 20.0),
                  shrinkWrap: true,
                  itemCount: dataRecom == null ? 0 : dataRecom.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisSpacing: 10.0,
                    mainAxisSpacing: 15.0,
                    childAspectRatio: userLevel == '4' ? 0.51 : 0.6,
                    crossAxisCount: 2,
                  ),
                  itemBuilder: (BuildContext context, int index) {
                    return GridItemCard(
                      item: dataRecom[index],
                      tap: () {
                        saveIdProduct(dataRecom[index].rowPointer);
                        Navigator.of(context).push(PageRouteBuilder(
                            pageBuilder: (_, __, ___) => new DetailProduk(),
                            transitionsBuilder: (_, Animation<double> animation,
                                __, Widget child) {
                              return Opacity(
                                opacity: animation.value,
                                child: child,
                              );
                            },
                            transitionDuration: Duration(milliseconds: 850)));
                      },
                      userLevel: userLevel,
                    );
                  })
            ],
          )
        : Text('');

    return Scaffold(
      /// Use Stack to costume a appbar
      body: isLoading
          ? Stack(
              children: <Widget>[
                SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(
                              top: mediaQueryData.padding.top + 58.5)),
                      imageSlider,
                      // promoHorizontalList,
                      categoryImageBottom,
                      Padding(
                        padding: EdgeInsets.only(bottom: 10.0),
                      ),
                      dataRecom.toString() == '[]'
                          ? Padding(padding: EdgeInsets.all(0.0))
                          : gridProduct,
                    ],
                  ),
                ),

                /// Get a class AppbarGradient
                /// This is a Appbar in home activity
                AppbarGradient(),
              ],
            )
          : Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
    );
  }
}

/// Component FlashSaleItem
class FlashSaleItem extends StatelessWidget {
  final String image;
  final String title;
  final int normalprice;
  final int discountprice;
  final String ratingvalue;
  final String place;
  final String stock;
  final int colorLine;
  final double widthLine;
  final tapFlash;
  final formatnya;

  FlashSaleItem(
      {this.image,
      this.title,
      this.normalprice,
      this.discountprice,
      this.ratingvalue,
      this.place,
      this.stock,
      this.colorLine,
      this.widthLine,
      this.tapFlash,
      this.formatnya});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              InkWell(
                onTap: tapFlash,
                child: Container(
                  height: 305.0,
                  width: 145.0,
                  color: Colors.white,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 140.0,
                        width: 145.0,
                        child: Image.asset(
                          image,
                          fit: BoxFit.cover,
                        ),
                      ),
                      Padding(
                        padding:
                            EdgeInsets.only(left: 8.0, right: 3.0, top: 15.0),
                        child: Text(title,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontSize: 10.5,
                                fontWeight: FontWeight.w500,
                                fontFamily: "Sans")),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 10.0, top: 5.0),
                        child: Text(formatnya.format(normalprice),
                            style: TextStyle(
                                fontSize: 10.5,
                                decoration: TextDecoration.lineThrough,
                                color: Colors.black54,
                                fontWeight: FontWeight.w600,
                                fontFamily: "Sans")),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 10.0, top: 5.0),
                        child: Text(formatnya.format(discountprice),
                            style: TextStyle(
                                fontSize: 12.0,
                                color: Color(0xFF7F7FD5),
                                fontWeight: FontWeight.w800,
                                fontFamily: "Sans")),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0, top: 5.0),
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.star,
                              size: 11.0,
                              color: Colors.yellow,
                            ),
                            Icon(
                              Icons.star,
                              size: 11.0,
                              color: Colors.yellow,
                            ),
                            Icon(
                              Icons.star,
                              size: 11.0,
                              color: Colors.yellow,
                            ),
                            Icon(
                              Icons.star,
                              size: 11.0,
                              color: Colors.yellow,
                            ),
                            Icon(
                              Icons.star_half,
                              size: 11.0,
                              color: Colors.yellow,
                            ),
                            Text(
                              ratingvalue,
                              style: TextStyle(
                                  fontSize: 10.0,
                                  fontWeight: FontWeight.w500,
                                  fontFamily: "Sans",
                                  color: Colors.black38),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0, top: 5.0),
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.location_on,
                              size: 11.0,
                              color: Colors.black38,
                            ),
                            Text(
                              place,
                              style: TextStyle(
                                  fontSize: 10.0,
                                  fontWeight: FontWeight.w500,
                                  fontFamily: "Sans",
                                  color: Colors.black38),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0, left: 10.0),
                        child: Text(
                          stock,
                          style: TextStyle(
                              fontSize: 10.0,
                              fontWeight: FontWeight.w500,
                              fontFamily: "Sans",
                              color: Colors.black),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 4.0, left: 10.0),
                        child: Container(
                          height: 5.0,
                          width: widthLine,
                          decoration: BoxDecoration(
                              color: Color(colorLine),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4.0)),
                              shape: BoxShape.rectangle),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}

/// Component category item bellow FlashSale
class CategoryItemValue extends StatelessWidget {
  final image, title;
  final GestureTapCallback tap;

  CategoryItemValue({
    this.image,
    this.title,
    this.tap,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: tap,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(3.0)),
          image: DecorationImage(image: NetworkImage(image), fit: BoxFit.cover),
        ),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(3.0)),
            color: Colors.black.withOpacity(0.25),
          ),
          child: Center(
              child: Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontFamily: "Berlin",
              fontSize: 18.5,
              letterSpacing: 0.7,
              fontWeight: FontWeight.w800,
            ),
          )),
        ),
      ),
    );
  }
}
