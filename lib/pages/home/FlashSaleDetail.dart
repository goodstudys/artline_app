import 'package:artline/Library/carousel_pro/carousel_pro.dart';
import 'package:artline/components/favoriteItem.dart';
import 'package:flutter/material.dart';
import 'package:artline/pages/cart/cartLayout.dart';
import 'package:artline/pages/home/ChatItem.dart';
import 'package:artline/repository/products.dart';
import 'package:intl/intl.dart';

class FlashSaleDetail extends StatefulWidget {

  final Product itemSale;
  FlashSaleDetail(this.itemSale);
  _FlashSaleDetailState createState() => _FlashSaleDetailState(itemSale);
}

class _FlashSaleDetailState extends State<FlashSaleDetail> {
  /// Declare class in FlashSaleItem.dart
  final Product itemSale;
  _FlashSaleDetailState(this.itemSale);

  // static BuildContext ctx;
  int valueItemChart = 0;
  final GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();

  /// Create a bottomSheet "ViewMore" in description
  void _bottomSheet() {
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return SingleChildScrollView(
            child: Container(
              color: Colors.black26,
              child: Padding(
                padding: const EdgeInsets.only(top: 2.0),
                child: Container(
                  height: 1500.0,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20.0),
                          topRight: Radius.circular(20.0))),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(padding: EdgeInsets.only(top: 20.0)),
                      Center(
                          child: Text(
                        "Description",
                        style: _subHeaderCustomStyle,
                      )),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 20.0, left: 20.0, right: 20.0, bottom: 20.0),
                        child: Text(itemSale.description, style: _detailText),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  /// Variable for custom Text
  static var _customTextStyle = TextStyle(
    color: Colors.black87,
    fontFamily: "Gotik",
    fontSize: 17.0,
    fontWeight: FontWeight.w800,
  );

  /// Variable Custom Text for Header text
  static var _subHeaderCustomStyle = TextStyle(
      color: Colors.black54,
      fontWeight: FontWeight.w700,
      fontFamily: "Gotik",
      fontSize: 16.0);

  /// Variable Custom Text for Detail text
  static var _detailText = TextStyle(
      fontFamily: "Gotik",
      color: Colors.black54,
      letterSpacing: 0.3,
      wordSpacing: 0.5);

  /// Variable Component UI use in bottom layout "Top Rated Products"
  var _suggestedItem = Padding(
    padding:
        const EdgeInsets.only(left: 15.0, right: 20.0, top: 30.0, bottom: 20.0),
    child: Container(
      height: 280.0,
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Top Rated Products",
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontFamily: "Gotik",
                    fontSize: 15.0),
              ),
              InkWell(
                onTap: () {},
                child: Text(
                  "See All",
                  style: TextStyle(
                      color: Colors.indigoAccent.withOpacity(0.8),
                      fontFamily: "Gotik",
                      fontWeight: FontWeight.w700),
                ),
              )
            ],
          ),
          Expanded(
            child: ListView.builder(
                padding: EdgeInsets.only(top: 20.0, bottom: 2.0),
                scrollDirection: Axis.horizontal,
                itemCount: 4,
                itemBuilder: (BuildContext context, int index) {
                  return FavoriteItem(
                      image: loadProducts[index].img,
                      title: loadProducts[index].title,
                      salary: loadProducts[index].price.toString(),
                      rating: loadProducts[index].rattingValue,
                      tekan: loadProducts[index]
                      /*sale: "923 Sale",*/
                      );
                }
                /*FavoriteItem(
                  image: "assets/imgItem/pic2.jpg",
                  title: "Artline Supreme Marker 1.0mm Bullet Pink",
                  Salary: "Rp. 16.000",
                  Rating: "4.2",
                  /*sale: "892 Sale",*/
                ),
                Padding(padding: EdgeInsets.only(left: 10.0)),
                FavoriteItem(
                  image: "assets/imgItem/pic20.jpg",
                  title: "Artline 527 Eco Whiteboard Marker 2mm Red",
                  Salary: "Rp. 4.000",
                  Rating: "4.8",
                  /*sale: "110 Sale",*/
                ),
                Padding(padding: EdgeInsets.only(left: 10.0)),
                FavoriteItem(
                  image: "assets/imgItem/pen2.jpg",
                  title: "Artline 200 Fineliner Pen 0.4mm Red T Shirt",
                  Salary: "\$ 8",
                  Rating: "4.4",
                  /*sale: "210 Sale",*/
                ),
                Padding(padding: EdgeInsets.only(right: 10.0)),*/

                ),
          ),
        ],
      ),
    ),
  );

  /// Component any widget for FlashSaleDetail
  Widget build(BuildContext context) {
    double presentase= (itemSale.discountprice / itemSale.price ) ;
    int total = (presentase * 100.0).toInt();
    int menjadi = (-total + 100 );
    final NumberFormat formatter = NumberFormat.currency(
        symbol: 'Rp. ',
        decimalDigits: 0,
        locale: Localizations.localeOf(context).toString());
    return Scaffold(
      key: _key,
      appBar: AppBar(
        actions: <Widget>[
          InkWell(
            onTap: () {
              Navigator.of(context).push(
                  PageRouteBuilder(pageBuilder: (_, __, ___) => new CartLayout()));
            },
            child: Stack(
              alignment: AlignmentDirectional(-1.0, -0.8),
              children: <Widget>[
                IconButton(
                    onPressed: null,
                    icon: Icon(
                      Icons.shopping_cart,
                      color: Colors.black26,
                    )),
                CircleAvatar(
                  radius: 10.0,
                  backgroundColor: Colors.red,
                  child: Text('1',
                    // ScopedModel.of<CartModel>(context, rebuildOnChange: true)
                    //     .cartListing
                    //     .length
                    //     .toString(),
                    style: TextStyle(color: Colors.white, fontSize: 13.0),
                  ),
                ),
              ],
            ),
          ),
        ],
        elevation: 0.5,
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text(
          "Produk Detail",
          style: TextStyle(
            fontWeight: FontWeight.w500,
            color: Colors.black54,
            fontSize: 17.0,
            fontFamily: "Gotik",
          ),
        ),
      ),
      body: Column(
        children: <Widget>[
          Flexible(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  /// ImageSlider in header
                  Container(
                      height: 300.0,
                      child: Hero(
                        tag: "hero-flashsale-${itemSale.id}",
                        child: Material(
                          child: new Carousel(
                            dotColor: Colors.black26,
                            dotIncreaseSize: 1.7,
                            dotBgColor: Colors.transparent,
                            autoplay: false,
                            boxFit: BoxFit.cover,
                            images: [
                              AssetImage(itemSale.img),
                              AssetImage(itemSale.img),
                              AssetImage(itemSale.img),
                            ],
                          ),
                        ),
                      )),

                  ///Label FlashSale in bottom header
                  Container(
                    height: 50.0,
                    width: 1000.0,
                    color: Colors.redAccent,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Padding(padding: EdgeInsets.only(left: 20.0)),
                            Image.asset(
                              "assets/icon/flashSaleIcon.png",
                              height: 25.0,
                            ),
                            Padding(padding: EdgeInsets.only(left: 10.0)),
                            Text(
                              "Flash Sale",
                              style: _customTextStyle.copyWith(
                                  color: Colors.white),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 15.0),
                          child: Text(
                            menjadi.toString() + "%"
                            ,
                            style: _customTextStyle.copyWith(
                                color: Colors.white, fontSize: 13.5),
                          ),
                        )
                      ],
                    ),
                  ),

                  /// White Background for Title, Price and Ratting
                  Container(
                    decoration: BoxDecoration(color: Colors.white, boxShadow: [
                      BoxShadow(
                        color: Color(0xFF656565).withOpacity(0.15),
                        blurRadius: 1.0,
                        spreadRadius: 0.2,
                      )
                    ]),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0, top: 10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            itemSale.title,
                            style: _customTextStyle,
                            overflow: TextOverflow.ellipsis,
                          ),
                          Padding(padding: EdgeInsets.only(top: 5.0)),
                          Text(
                            formatter.format(itemSale.price),
                            style: _customTextStyle.copyWith(
                                decoration: TextDecoration.lineThrough,
                                fontSize: 13.0,
                                color: Colors.black26),
                          ),
                          Padding(padding: EdgeInsets.only(top: 5.0)),
                          Text(
                            formatter.format(itemSale.discountprice),
                            style: _customTextStyle.copyWith(
                                color: Colors.redAccent, fontSize: 20.0),
                          ),
                          Padding(padding: EdgeInsets.only(top: 10.0)),
                          Divider(
                            color: Colors.black12,
                            height: 1.0,
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(top: 10.0, bottom: 10.0),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  height: 30.0,
                                  width: 75.0,
                                  decoration: BoxDecoration(
                                    color: Colors.lightGreen,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20.0)),
                                  ),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        itemSale.rattingValue.toString(),
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      Padding(
                                          padding: EdgeInsets.only(left: 8.0)),
                                      Icon(
                                        Icons.star,
                                        color: Colors.white,
                                        size: 19.0,
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 10.0),
                                  child: Text(
                                    itemSale.rattingValue.toString(),
                                    style: TextStyle(
                                        color: Colors.black26,
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.w500),
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),

                  /// Detail Product
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Container(
                      height: 205.0,
                      width: 600.0,
                      decoration:
                          BoxDecoration(color: Colors.white, boxShadow: [
                        BoxShadow(
                          color: Color(0xFF656565).withOpacity(0.15),
                          blurRadius: 1.0,
                          spreadRadius: 0.2,
                        )
                      ]),
                      child: Padding(
                        padding: EdgeInsets.only(top: 20.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 20.0),
                              child: Text(
                                "Detail Product",
                                style: _subHeaderCustomStyle,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 20.0,
                                  right: 20.0,
                                  bottom: 10.0,
                                  left: 20.0),
                              child: Text(
                                itemSale.description,
                                style: _detailText,
                                //textDirection: TextDirection.ltr,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),

                  /// Description
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Container(
                      height: 205.0,
                      width: 600.0,
                      decoration:
                          BoxDecoration(color: Colors.white, boxShadow: [
                        BoxShadow(
                          color: Color(0xFF656565).withOpacity(0.15),
                          blurRadius: 1.0,
                          spreadRadius: 0.2,
                        )
                      ]),
                      child: Padding(
                        padding: EdgeInsets.only(top: 20.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 20.0),
                              child: Text(
                                "Description",
                                style: _subHeaderCustomStyle,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 15.0,
                                  right: 20.0,
                                  bottom: 10.0,
                                  left: 20.0),
                              child: Text(itemSale.description,
                                  style: _detailText),
                            ),
                            Center(
                              child: InkWell(
                                onTap: () {
                                  _bottomSheet();
                                },
                                child: Text(
                                  "View More",
                                  style: TextStyle(
                                    color: Colors.indigoAccent,
                                    fontSize: 15.0,
                                    fontFamily: "Gotik",
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),

                  ///Call a variable suggested Item(Top Rated Product Card) in bottom of description
                  _suggestedItem
                ],
              ),
            ),
          ),

          /// If user click icon chart SnackBar show
          /// this code to show a SnackBar
          /// and Increase a valueItemChart + 1
          InkWell(
            onTap: () {
              // ScopedModel.of<CartModel>(context).addCart(itemSale);

              var snackbar = SnackBar(
                content: Text("Item Added"),
              );
              /*setState(() {
                valueItemChart++;
              });*/
              _key.currentState.showSnackBar(snackbar);
            },

            /// Shopping Cart in bottom layout
            child: Padding(
              padding: const EdgeInsets.only(bottom: 5.0),
              child: Container(
                color: Colors.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      height: 40.0,
                      width: 60.0,
                      decoration: BoxDecoration(
                          color: Colors.white12.withOpacity(0.1),
                          border: Border.all(color: Colors.black12)),
                      child: Center(
                        child: Image.asset(
                          "assets/icon/shopping-cart.png",
                          height: 23.0,
                        ),
                      ),
                    ),

                    /// Icon Message in bottom layout with Flexible
                    InkWell(
                      onTap: () {
                        Navigator.of(context).push(PageRouteBuilder(
                            pageBuilder: (_, ___, ____) => new ChatItem()));
                      },
                      child: Container(
                        height: 40.0,
                        width: 60.0,
                        decoration: BoxDecoration(
                            color: Colors.white12.withOpacity(0.1),
                            border: Border.all(color: Colors.black12)),
                        child: Center(
                          child: Image.asset("assets/icon/message.png",
                              height: 20.0),
                        ),
                      ),
                    ),

                    /// Button Pay
                    InkWell(
                      onTap: () {
                        // Navigator.of(context).push(PageRouteBuilder(
                        //     pageBuilder: (_, __, ___) => new delivery()));
                      },
                      child: Container(
                        height: 45.0,
                        width: 200.0,
                        decoration: BoxDecoration(
                          color: Colors.indigoAccent,
                        ),
                        child: Center(
                          child: Text(
                            "Pay",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
