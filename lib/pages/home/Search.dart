import 'dart:convert';

import 'package:artline/bloc/global/BlocProvider.dart';
import 'package:artline/bloc/global/GlobalBloc.dart';
import 'package:artline/components/favoriteItem.dart';
import 'package:artline/env.dart';
import 'package:artline/model/dataProduct.dart';
import 'package:artline/pages/home/CategoryDetail.dart';
import 'package:artline/pages/home/searchView.dart';
import 'package:artline/storage/storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:artline/repository/products.dart';
import 'package:artline/pages/home/DetailProduct.dart';

class SearchAppbar extends StatefulWidget {
  @override
  _SearchAppbarState createState() => _SearchAppbarState();
}

class _SearchAppbarState extends State<SearchAppbar> {
  TextEditingController searchCtr = TextEditingController();
  
  void saveIdCategory(String id, String name) async {
    storage.setDataString('idCat', id);
    storage.setDataString('nameCat', name);
  }

  void saveIdProduct(String id) async {
    storage.setDataString('idProd', id);
    BlocProvider.of<GlobalBloc>(context).cartBloc.cart.getGroup = null;
  }

  void saveCari(String cari) async {
    storage.setDataString('cari', cari);
  }

  List<Product> filteredUsers = List();

  void filterSearchResults(String query) {
    List<Product> dummySearchList = List();
    dummySearchList.addAll(loadProducts);
    setState(() {
      if (query.isNotEmpty) {
        List<Product> dummyListData = List();
        dummySearchList.forEach((item) {
          if (item.title.toLowerCase().contains(query.toLowerCase()) ||
              item.kategori.toLowerCase().contains(query.toLowerCase())) {
            dummyListData.add(item);
          }
        });
        filteredUsers.clear();
        filteredUsers.addAll(dummyListData);
        return;
      } else if (query.isEmpty) {
        filteredUsers.clear();
        Text("cobalahi");
        return;
      } else {
        filteredUsers.clear();
        Text("data");
        return;
      }
    });
  }

  DataStore storage = DataStore();

  bool isLoadCat = true;
  bool isLoadRecom = true;
  List data = [];
  List<DataProduct> recomProducts = [];
  Future<String> getData() async {
    setState(() {
      isLoadRecom = true;
    });
    await getDataCategories();
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var res = await http.get(url('api/getRecom'), headers: {
      'Authorization': $tokenType + ' ' + $accesToken,
      'Accept': 'application/json'
    });
    setState(() {
      var content = json.decode(res.body);
      data = content['data'];
      recomProducts = List<DataProduct>.from(content['data'].map((item) => DataProduct.fromJson(item)));
    });
    return 'success!';
  }

  List dataCategories = [];
  Future<String> getDataCategories() async {
    setState(() {
      isLoadCat = true;
    });
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var res = await http.get(url('api/getCategories'), headers: {
      'Authorization': $tokenType + ' ' + $accesToken,
      'Accept': 'application/json'
    });
    setState(() {
      var content = json.decode(res.body);
      dataCategories = content['data'];
    });
    return 'success!';
  }

  @override
  void initState() {
    super.initState();
    getData().then((s) => setState(() {
      isLoadRecom = false;
    }));
    getDataCategories().then((s) => setState(() {
      isLoadCat = false;
    }));
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Color(0xFF6991C7),
        ),
        title: Text(
          "Search",
          style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 18.0,
              color: Colors.black54,
              fontFamily: "Gotik"),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.0,
      ),
      body: Container(
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 50.0, left: 20.0),
                    child: Text(
                      "Hello,",
                      style: TextStyle(
                          letterSpacing: 0.1,
                          fontWeight: FontWeight.w600,
                          fontSize: 27.0,
                          color: Colors.black54,
                          fontFamily: "Gotik"),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 20.0, left: 20.0),
                    child: Text(
                      "What would you like to search ?",
                      style: TextStyle(
                        letterSpacing: 0.1,
                        fontSize: 20.0,
                        color: Colors.black54,
                        fontFamily: "Gotik"),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 15.0, right: 20.0, left: 20.0),
                    child: Container(
                      height: 50.0,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black.withOpacity(0.1),
                                blurRadius: 15.0,
                                spreadRadius: 0.0)
                          ]),
                      child: Center(
                        child: Padding(
                          padding:
                              const EdgeInsets.only(left: 15.0),
                          child: Theme(
                            data: ThemeData(hintColor: Colors.transparent),
                            child: TextField(
                              controller: searchCtr,
                              onChanged: (value) {
                                saveCari(value);
                              },
                              onSubmitted: (value) {
                                saveCari(value);
                                searchCtr.clear();
                                Navigator.of(context).push(
                                  PageRouteBuilder(pageBuilder: (_, __, ___) => SearchView())
                                );
                              },
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  suffixIcon: InkWell(
                                    onTap: () {
                                      searchCtr.clear();
                                      Navigator.of(context).push(
                                          PageRouteBuilder(
                                              pageBuilder: (_, __, ___) =>
                                                  new SearchView()));
                                    },
                                    child: Icon(Icons.search,)
                                  ),
                                  hintText: "Find product...",
                                  hintStyle: TextStyle(
                                      color: Colors.black54,
                                      fontFamily: "Gotik",
                                      fontWeight: FontWeight.w400)),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListView(
                      children: <Widget>[
                        !isLoadCat ? Container(
                          color: Colors.white,
                          margin: EdgeInsets.only(top: 5.0),
                          padding: EdgeInsets.only(bottom: 13.0, top: 13.0),
                          height: 120.0,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Expanded(
                                child: GridView.builder(
                                  shrinkWrap: true,
                                  scrollDirection: Axis.horizontal,
                                  itemCount: dataCategories.length,
                                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 2,
                                    childAspectRatio: 0.2,
                                    crossAxisSpacing: 10.0,
                                    mainAxisSpacing: 10.0
                                  ),
                                  padding: EdgeInsets.only(
                                      top: 4.0,
                                      left: 20.0,
                                      right: 20.0,
                                      bottom: 5.0),
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return InkWell(
                                      onTap: () {
                                        saveIdCategory(
                                          dataCategories[index]['id'].toString(),
                                          dataCategories[index]['name']
                                        );
                                        Navigator.of(context).push(
                                            PageRouteBuilder(
                                                pageBuilder: (_, __, ___) =>
                                                    new CategoryDetail(),
                                                transitionsBuilder: (_,
                                                    Animation<double>
                                                        animation,
                                                    __,
                                                    Widget child) {
                                                  return Opacity(
                                                    opacity: animation.value,
                                                    child: child,
                                                  );
                                                },
                                                transitionDuration: Duration(
                                                    milliseconds: 850)));
                                      },
                                      child: Container(
                                        height: 18.5,
                                        // width: 90.0,
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(20.0)),
                                          boxShadow: [
                                            BoxShadow(
                                              color: Colors.black
                                                  .withOpacity(0.1),
                                              blurRadius: 4.5,
                                              spreadRadius: 1.0,
                                            )
                                          ],
                                        ),
                                        child: Center(
                                          child: Text(
                                            dataCategories[index]['name'],
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                color: Colors.black54,
                                                fontFamily: "Sans"),
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ],
                          ),
                        ) : Container(
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        ),
                        !isLoadRecom ? Padding(
                          padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                          child: Container(
                            height: 300.0,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 20.0),
                                  child: Text(
                                    "Favorite",
                                    style: TextStyle(
                                        fontFamily: "Gotik",
                                        color: Colors.black26),
                                  ),
                                ),
                                Expanded(
                                  child: ListView.builder(
                                      padding: EdgeInsets.only(bottom: 2.0),
                                      scrollDirection: Axis.horizontal,
                                      itemCount: 4,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return FavoriteItem(
                                          image: host + 'upload/users/products/' + data[index]['image1'],
                                          title: data[index]['name'],
                                          salary: data[index]['price'],
                                          rating: loadProducts[index].rattingValue,
                                          tekan: loadProducts[index],
                                          item: recomProducts[index],
                                          tap: () {
                                            saveIdProduct(
                                                data[index]['rowPointer']);
                                            Navigator.of(context).push(
                                                PageRouteBuilder(
                                                    pageBuilder: (_, __, ___) =>
                                                        new DetailProduk(),
                                                    transitionDuration:
                                                        Duration(
                                                            milliseconds: 900),

                                                    /// Set animation Opacity in route to detailProduk layout
                                                    transitionsBuilder: (_,
                                                        Animation<double>
                                                            animation,
                                                        __,
                                                        Widget child) {
                                                      return Opacity(
                                                        opacity:
                                                            animation.value,
                                                        child: child,
                                                      );
                                                    }));
                                          },
                                          /*sale: "923 Sale",*/
                                        );
                                      }),
                                ),
                              ],
                            ),
                          ),
                        ) : Container(
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        ),
                      ],
                    ),
                  )
                  // : Expanded(
                  //     child: ListView.builder(
                  //       padding: EdgeInsets.all(10.0),
                  //       itemCount: filteredUsers.length,
                  //       itemBuilder: (BuildContext context, int index) {
                  //         return InkWell(
                  //           onTap: filteredUsers[index].discountprice == 0
                  //               ? () {
                  //                   Navigator.of(context).push(
                  //                       PageRouteBuilder(
                  //                           pageBuilder: (_, __, ___) =>
                  //                               new DetailProduk(),
                  //                           transitionDuration:
                  //                               Duration(milliseconds: 900),

                  //                           /// Set animation Opacity in route to detailProduk layout
                  //                           transitionsBuilder: (_,
                  //                               Animation<double> animation,
                  //                               __,
                  //                               Widget child) {
                  //                             return Opacity(
                  //                               opacity: animation.value,
                  //                               child: child,
                  //                             );
                  //                           }));
                  //                   saveIdProduct('1');
                  //                 }
                  //               : () {
                  //                   Navigator.of(context).push(
                  //                       PageRouteBuilder(
                  //                           pageBuilder: (_, __, ___) =>
                  //                               new FlashSaleDetail(
                  //                                   filteredUsers[index]),
                  //                           transitionDuration:
                  //                               Duration(milliseconds: 900),

                  //                           /// Set animation Opacity in route to detailProduk layout
                  //                           transitionsBuilder: (_,
                  //                               Animation<double> animation,
                  //                               __,
                  //                               Widget child) {
                  //                             return Opacity(
                  //                               opacity: animation.value,
                  //                               child: child,
                  //                             );
                  //                           }));
                  //                 },
                  //           child: Card(
                  //             child: Padding(
                  //               padding: EdgeInsets.all(10.0),
                  //               child: Column(
                  //                 mainAxisAlignment:
                  //                     MainAxisAlignment.start,
                  //                 crossAxisAlignment:
                  //                     CrossAxisAlignment.start,
                  //                 children: <Widget>[
                  //                   Text(
                  //                     filteredUsers[index].title,
                  //                     style: TextStyle(
                  //                       fontSize: 16.0,
                  //                       color: Colors.black,
                  //                     ),
                  //                   ),
                  //                   SizedBox(
                  //                     height: 5.0,
                  //                   ),
                  //                   Text(
                  //                     filteredUsers[index]
                  //                         .kategori
                  //                         .toLowerCase(),
                  //                     style: TextStyle(
                  //                       fontSize: 14.0,
                  //                       color: Colors.grey,
                  //                     ),
                  //                   ),
                  //                 ],
                  //               ),
                  //             ),
                  //           ),
                  //         );
                  //       },
                  //     ),
                  //   )
                ],
              ),
            )
    );
  }
}

/// Popular Keyword Item class
class KeywordItem extends StatelessWidget {
  final String title, title2;

  KeywordItem({this.title, this.title2});

  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 4.0, left: 3.0),
          child: Container(
            height: 29.5,
            width: 90.0,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(20.0)),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.1),
                  blurRadius: 4.5,
                  spreadRadius: 1.0,
                )
              ],
            ),
            child: Center(
              child: Text(
                title,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(color: Colors.black54, fontFamily: "Sans"),
              ),
            ),
          ),
        ),
        Padding(padding: EdgeInsets.only(top: 15.0)),
        Container(
          height: 29.5,
          width: 90.0,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.1),
                blurRadius: 4.5,
                spreadRadius: 1.0,
              )
            ],
          ),
          child: Center(
            child: Text(
              title2,
              style: TextStyle(
                color: Colors.black54,
                fontFamily: "Sans",
              ),
            ),
          ),
        ),
      ],
    );
  }
}
