import 'dart:convert';
import 'package:artline/ListItem/CategoryItem.dart';
import 'package:artline/bloc/global/BlocProvider.dart';
import 'package:artline/bloc/global/GlobalBloc.dart';
import 'package:artline/components/gridItemCard.dart';
import 'package:artline/components/loadingItemCard.dart';
import 'package:artline/env.dart';
import 'package:artline/model/dataProduct.dart';
import 'package:artline/storage/storage.dart';
import 'package:flutter/material.dart';
import 'package:artline/repository/products.dart';
import 'package:artline/pages/home/DetailProduct.dart';
import 'package:http/http.dart' as http;

class SearchView extends StatefulWidget {
  @override
  _SearchViewState createState() => _SearchViewState();
}

class _SearchViewState extends State<SearchView> {
  DataStore storage = DataStore();
  var imageNetwork = NetworkImage(
      "https://firebasestorage.googleapis.com/v0/b/beauty-look.appspot.com/o/Screenshot_20181005-213931.png?alt=media&token=e6287f67-5bc0-4225-8e96-1623dc9dc42f");

  bool imageLoad = true;
  bool isLoading = true;
  String userLevel;
  String searchString = '';

  List<Product> get flash {
    return loadProducts.where((p) => p.discountprice != 0).toList();
  }

  void saveIdProduct(String id) async {
    storage.setDataString('idProd', id);
    BlocProvider.of<GlobalBloc>(context).cartBloc.cart.getGroup = null;
  }

  List<DataProduct> data;
  Future<String> getData() async {
    setState(() {
      isLoading = true;
    });
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var $userLevel = await storage.getDataString('userLevel');
    var cari = await storage.getDataString('cari');
    setState(() {
      searchString = cari;
    });
    var res = await http.post(url('api/search'), headers: {
      'Authorization': $tokenType + ' ' + $accesToken,
      'Accept': 'application/json'
    }, body: {
      'cari': cari
    });

    setState(() {
      var content = json.decode(res.body);
      userLevel = $userLevel;
      data = List<DataProduct>.from(
          content['products'].map((item) => DataProduct.fromJson(item)));
    });
    return 'success!';
  }

  @override
  void initState() {
    imageNetwork.resolve(ImageConfiguration()).addListener(
        ImageStreamListener((ImageInfo image, bool synchronousCall) {
      if (mounted) {
        setState(() {
          imageLoad = false;
        });
      }
    }));
    getData().then((s) => setState(() {
          isLoading = false;
        }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    /// Item Search in bottom of appbar
    /*
    var _search = InkWell(
      onTap: () {
        Navigator.of(context).push(PageRouteBuilder(
            pageBuilder: (_, __, ___) => SearchAppbar(),
            transitionDuration: Duration(milliseconds: 750),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            }));
      },
      child: Container(
        margin: EdgeInsets.only(left: MediaQuery.of(context).padding.left + 15),
        height: 37.0,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            shape: BoxShape.rectangle),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(padding: EdgeInsets.only(left: 17.0)),
            Image.asset(
              "assets/img/search2.png",
              height: 22.0,
            ),
            Padding(
                padding: EdgeInsets.only(
              left: 17.0,
            )),
            Padding(
              padding: EdgeInsets.only(top: 3.0),
              child: Text(
                "Search Item",
                style: TextStyle(
                    fontFamily: "Popins",
                    color: Colors.black12,
                    fontWeight: FontWeight.w900,
                    letterSpacing: 0.0,
                    fontSize: 16.4),
              ),
            ),
          ],
        ),
      ),
    );
    */

    /// Grid Item a product
    var _grid = SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            imageLoad
                ? _imageLoading(context)
                : GridView.count(
                    shrinkWrap: true,
                    padding:
                        EdgeInsets.symmetric(horizontal: 7.0, vertical: 10.0),
                    crossAxisSpacing: 10.0,
                    mainAxisSpacing: 15.0,
                    childAspectRatio: 0.55,
                    crossAxisCount: 2,
                    primary: false,
                    children: List.generate(

                        /// Get data in flashSaleItem.dart (ListItem folder)
                        // ScopedModel.of<CartModel>(context,
                        //                     rebuildOnChange: true)
                        //                 .kategoriProduk.length,
                        data == null ? 0 : data.length,
                        (index) => GridItemCard(
                              item: data[index],
                              tap: () {
                                saveIdProduct(data[index].rowPointer);
                                Navigator.of(context).push(PageRouteBuilder(
                                    pageBuilder: (_, __, ___) =>
                                        new DetailProduk(),
                                    transitionsBuilder: (_,
                                        Animation<double> animation,
                                        __,
                                        Widget child) {
                                      return Opacity(
                                        opacity: animation.value,
                                        child: child,
                                      );
                                    },
                                    transitionDuration:
                                        Duration(milliseconds: 850)));
                              },
                              userLevel: userLevel,
                            )),
                  )
          ],
        ),
      ),
    );

    return Scaffold(
      /// Appbar item
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          isLoading ? 'Loading...' : 'Search for "' + searchString + '"',
          style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 16.0,
              color: Colors.black54,
              fontFamily: "Gotik"),
        ),
        iconTheme: IconThemeData(
          color: Color(0xFF6991C7),
        ),
        elevation: 1.0,
      ),
      body: !isLoading ? SingleChildScrollView(
        child: Container(
          child: Column(
            /// Calling search and grid variable
            children: <Widget>[
              // _search,
              _grid,
            ],
          ),
        )
      ) : Container(
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }
}

// /
// /
// / Calling imageLoading animation for set a grid layout
// /
// /
Widget _imageLoading(BuildContext context) {
  return GridView.count(
    shrinkWrap: true,
    padding: EdgeInsets.symmetric(horizontal: 7.0, vertical: 10.0),
    crossAxisSpacing: 10.0,
    mainAxisSpacing: 15.0,
    childAspectRatio: 0.545,
    crossAxisCount: 2,
    primary: false,
    children: List.generate(
      /// Get data in PromotionDetail.dart (ListItem folder)
      itemDiscount.length,
      (index) => LoadingMenuItemCard(),
    ),
  );
}
