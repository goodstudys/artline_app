import 'dart:convert';
import 'package:artline/bloc/global/BlocProvider.dart';
import 'package:artline/bloc/global/GlobalBloc.dart';
import 'package:artline/components/discountItem.dart';
import 'package:artline/components/loadingItemCard.dart';
import 'package:artline/components/loadingItemCardDiscount.dart';
import 'package:artline/env.dart';
import 'package:artline/model/dataProduct.dart';
import 'package:artline/model/dataSlider.dart';
import 'package:artline/pages/home/subDetail.dart';
import 'package:artline/storage/storage.dart';
import 'package:artline/Library/carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:artline/ListItem/CategoryItem.dart';
import 'package:artline/pages/home/DetailProduct.dart';
import 'package:artline/pages/home/Search.dart';
import 'package:http/http.dart' as http;
import 'package:artline/repository/products.dart';
import 'package:artline/pages/home/itemList.dart';

class CategoryDetail extends StatefulWidget {
  // final CategoryList listCategory;
  // final String id;
  // CategoryDetail(this.listCategory, this.id);
  @override
  _CategoryDetailState createState() => _CategoryDetailState();
}

/// if user click icon in category layout navigate to CategoryDetail Layout
class _CategoryDetailState extends State<CategoryDetail> {
  DataStore storage = DataStore();
  bool isLoading = true;

  /// custom text variable is make it easy a custom textStyle black font
  static TextStyle _customTextStyleBlack = TextStyle(
    fontFamily: "Gotik",
    color: Colors.black,
    fontWeight: FontWeight.w700,
    fontSize: 15.0
  );

  /// Custom text blue in variable
  static TextStyle _customTextStyleBlue = TextStyle(
    fontFamily: "Gotik",
    color: Color(0xFF6991C7),
    fontWeight: FontWeight.w700,
    fontSize: 15.0
  );

  List<Product> get flash {
    return loadProducts.where((p) => p.discountprice != 0).toList();
  }

  List subCategories;
  List productsSale;
  List productsNew;
  List productsHotDeal;
  String nameAppbar, userLevel;
  List<DataProduct> listSale;
  List<DataProduct> listNew;
  List<DataProduct> listHotDeal;
  List<DataSlider> listSlider = [];
  void getData() async {
    setState(() {
      isLoading = true;
    });
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var $userLevel = await storage.getDataString('userLevel');
    var idcat = await storage.getDataString('idCat');
    var namecat = await storage.getDataString('nameCat');
    var res = await http.get(url('api/getDetailCategories/' + idcat), headers: {
      'Authorization': $tokenType + ' ' + $accesToken,
      'Accept': 'application/json'
    });
    var content = json.decode(res.body);
    if (content['subCategories'] != null && content['subCategories'].length > 1) {
      setState(() {
        userLevel = $userLevel;
        subCategories = content['subCategories'];
        productsSale = content['productsSale'];
        productsNew = content['productsNew'];
        productsHotDeal = content['productsHotDeal'];
        nameAppbar = namecat;
        if (content['productsSale'] != null) {
          listSale = List<DataProduct>.from(
              content['productsSale'].map((item) => DataProduct.fromJson(item)));
        }
        if (content['productsNew'] != null) {
          listNew = List<DataProduct>.from(
              content['productsNew'].map((item) => DataProduct.fromJson(item)));
        }
        if (content['productsHotDeal'] != null) {
          listHotDeal = List<DataProduct>.from(content['productsHotDeal'].map((item) => DataProduct.fromJson(item)));
        }
        isLoading = false;
      });
    } else {
      saveSubProduct("0");
      setState(() {
        isLoading = false;
      });
      Navigator.of(context).pushReplacement(PageRouteBuilder(
        pageBuilder: (_, __, ___) =>
            new SubDetail(title: namecat,),
        transitionsBuilder: (_,
            Animation<double> animation,
            __,
            Widget child) {
          return Opacity(
            opacity: animation.value,
            child: child,
          );
        },
        transitionDuration:
            Duration(milliseconds: 850)));
    }
  }

  void saveIdProduct(String id) async {
    storage.setDataString('idProd', id);
    BlocProvider.of<GlobalBloc>(context).cartBloc.cart.getGroup = null;
  }

  void saveFlagProduct(String id) async {
    storage.setDataString('flagProd', id);
  }

  void saveSubProduct(String id) async {
    storage.setDataString('subProd', id);
  }

  void getBanner() async {
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var idcat = await storage.getDataString('idCat');
    var res = await http.get(url('api/slider/category/' + idcat), headers: {
      'Authorization': $tokenType + ' ' + $accesToken,
      'Accept': 'application/json'
    });
    var content = json.decode(res.body);
    if (content['status'] == 'success') {
      setState(() {
        if (content['data'] == null) {
          listSlider = [];
        } else {
          listSlider = List<DataSlider>.from(content['data'].map((item) => DataSlider.fromJson(item)));
        }
      });
    }
  }

  @override
  void initState() {
    getData();
    getBanner();
    super.initState();
  }

  /// All Widget Component layout
  @override
  Widget build(BuildContext context) {
    /// imageSlider in header layout category detail
    var _imageSlider = Padding(
      padding: EdgeInsets.only(top: 0.0, bottom: 0.0),
      child: Container(
        height: 180.0,
        child: new Carousel(
          boxFit: BoxFit.cover,
          dotColor: Colors.transparent,
          dotSize: 5.5,
          dotSpacing: 16.0,
          dotBgColor: Colors.transparent,
          showIndicator: false,
          overlayShadow: false,
          overlayShadowColors: Colors.white.withOpacity(0.9),
          overlayShadowSize: 0.9,
          images: listSlider.map((item) => NetworkImage(host + 'upload/users/slider_category/' + item.image)).toList(),
        ),
      ),
    );

    /// Variable Category (Sub Category)
    var _subCategory = subCategories == null
        ? Container(
            child: Text(''),
          )
        : Container(
            child: Column(
              children: <Widget>[
                Padding(
                  padding:
                      const EdgeInsets.only(right: 20.0, left: 20.0, top: 35.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Sub Category",
                        style: _customTextStyleBlack,
                      ),
                    ],
                  ),
                ),
                Container(
                  color: Colors.white,
                  margin: EdgeInsets.only(top: 5.0),
                  padding: EdgeInsets.only(bottom: 15.0),
                  height: 120.0,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        child: GridView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: subCategories.length,
                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            childAspectRatio: 0.18,
                            crossAxisSpacing: 8.0,
                            mainAxisSpacing: 0.8
                          ),
                          padding: EdgeInsets.only(top: 4.0, left: 12.0, bottom: 5.0),
                          itemBuilder: (BuildContext context, int index) {
                            return Padding(
                              padding: const EdgeInsets.only(left: 8.0),
                              child: InkWell(
                                onTap: () {
                                  saveSubProduct(subCategories[index]['id'].toString());
                                  Navigator.of(context).push(PageRouteBuilder(
                                      pageBuilder: (_, __, ___) =>
                                          new SubDetail(title: subCategories[index]['name'],),
                                      transitionsBuilder: (_,
                                          Animation<double> animation,
                                          __,
                                          Widget child) {
                                        return Opacity(
                                          opacity: animation.value,
                                          child: child,
                                        );
                                      },
                                      transitionDuration:
                                          Duration(milliseconds: 850)));
                                },
                                child: Container(
                                  padding: EdgeInsets.only(left: 10.0, right: 10.0),
                                  height: 18.5,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20.0)),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black.withOpacity(0.1),
                                        blurRadius: 4.5,
                                        spreadRadius: 1.0,
                                      )
                                    ],
                                  ),
                                  child: Center(
                                    child: Text(
                                      subCategories[index]['name'],
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          color: Colors.black54,
                                          fontFamily: "Sans"),
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          );

    /// Variable item Discount with Card
    var _itemDiscount = productsSale == null
        ? Container(
            child: Text(''),
          )
        : Container(
            child: Column(
              children: <Widget>[
                Padding(
                  padding:
                      const EdgeInsets.only(right: 20.0, left: 20.0, top: 30.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Item Discount",
                        style: _customTextStyleBlack,
                      ),
                      InkWell(
                        onTap: () {
                          saveFlagProduct('discount');
                          Navigator.of(context).push(PageRouteBuilder(
                              pageBuilder: (_, __, ___) => new ItemList(title: "Item Discount",)));
                        },
                        child: Text("See More", style: _customTextStyleBlue),
                      ),
                    ],
                  ),
                ),
                SingleChildScrollView(
                  child: Container(
                    margin: EdgeInsets.only(right: 10.0),
                    height: userLevel == '4' ? 330.0 : 310,
                    child: isLoading
                        ? _loadingImageAnimationDiscount(context)
                        : ListView.builder(
                          padding: EdgeInsets.only(left: 10.0, bottom: 15.0),
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (BuildContext context, int index) =>
                              DiscountItem(
                                item: listSale[index],
                                tap: () {
                                  saveIdProduct(listSale[index].rowPointer);
                                  Navigator.of(context).push(
                                    PageRouteBuilder(
                                      pageBuilder: (_, __, ___) => DetailProduk(),
                                      transitionsBuilder: (_, Animation<double> animation, __, Widget child) {
                                        return Opacity(
                                          opacity: animation.value,
                                          child: child,
                                        );
                                      },
                                      transitionDuration: Duration(milliseconds: 850)
                                    )
                                  );
                                },
                                userLevel: userLevel,
                              ),
                            itemCount: listSale == null ? 0 : listSale.length,
                          ),
                  ),
                )
              ],
            ),
          );

    /// Variable Paling Sering Dicari with Card
    var _itemPopular = productsHotDeal == null
        ? Container(
            child: Text(''),
          )
        : Padding(
            padding: const EdgeInsets.only(top: 30.0),
            child: Container(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 20.0, left: 20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Paling Sering Dicari",
                          style: _customTextStyleBlack,
                        ),
                        InkWell(
                          onTap: () {
                            saveFlagProduct('popular');
                            Navigator.of(context).push(PageRouteBuilder(
                                pageBuilder: (_, __, ___) => new ItemList(title: "Paling Sering Dicari",)));
                          },
                          child: Text("See More", style: _customTextStyleBlue),
                        ),
                      ],
                    ),
                  ),
                  SingleChildScrollView(
                    child: Container(
                      margin: EdgeInsets.only(right: 10.0),
                      height: userLevel == '4' ? 330.0 : 310,

                      ///
                      ///
                      /// check the condition if image data from server firebase loaded or no
                      /// if image true (image still downloading from server)
                      /// Card to set card loading animation
                      ///
                      ///
                      child: isLoading
                          ? _loadingImageAnimation(context)
                          : ListView.builder(
                            padding: EdgeInsets.only(left: 10.0, bottom: 15.0),
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (BuildContext context, int index) =>
                                DiscountItem(
                                  item: listHotDeal[index],
                                  tap: () {
                                    saveIdProduct(listHotDeal[index].rowPointer);
                                    Navigator.of(context).push(
                                      PageRouteBuilder(
                                        pageBuilder: (_, __, ___) => DetailProduk(),
                                        transitionsBuilder: (_, Animation<double> animation, __, Widget child) {
                                          return Opacity(
                                            opacity: animation.value,
                                            child: child,
                                          );
                                        },
                                        transitionDuration: Duration(milliseconds: 850)
                                      )
                                    );
                                  },
                                  userLevel: userLevel,
                                ),
                              itemCount: listHotDeal == null ? 0 : listHotDeal.length,
                            ),
                    ),
                  )
                ],
              ),
            ),
          );

    /// Variable New Items with Card
    var _itemNew = productsNew == null
        ? Container(
            child: Text(''),
          )
        : Container(
          padding: const EdgeInsets.only(bottom: 15.0),
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20.0, left: 20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "New Items",
                      style: _customTextStyleBlack,
                    ),
                    InkWell(
                      onTap: () {
                        saveFlagProduct('new');
                        Navigator.of(context).push(PageRouteBuilder(
                            pageBuilder: (_, __, ___) => new ItemList(title: "New Items",)));
                      },
                      child: Text("See More", style: _customTextStyleBlue),
                    ),
                  ],
                ),
              ),
              SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.only(bottom: 15.0),
                  height: userLevel == '4' ? 330.0 : 310,

                  ///
                  ///
                  /// check the condition if image data from server firebase loaded or no
                  /// if image true (image still downloading from server)
                  /// Card to set card loading animation
                  ///
                  ///
                  child: isLoading
                      ? _loadingImageAnimation(context)
                      : ListView.builder(
                        padding: EdgeInsets.only(left: 10.0, bottom: 15.0),
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (BuildContext context, int index) =>
                            DiscountItem(
                              item: listNew[index],
                              tap: () {
                                saveIdProduct(listNew[index].rowPointer);
                                Navigator.of(context).push(
                                  PageRouteBuilder(
                                    pageBuilder: (_, __, ___) => DetailProduk(),
                                    transitionsBuilder: (_, Animation<double> animation, __, Widget child) {
                                      return Opacity(
                                        opacity: animation.value,
                                        child: child,
                                      );
                                    },
                                    transitionDuration: Duration(milliseconds: 850)
                                  )
                                );
                              },
                              userLevel: userLevel,
                            ),
                          itemCount: listNew == null ? 0 : listNew.length,
                        ),
                ),
              )
            ],
          ),
        );

    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            onPressed: () {
              Navigator.of(context).push(PageRouteBuilder(
                  pageBuilder: (_, __, ___) => new SearchAppbar()));
            },
            icon: Icon(Icons.search, color: Color(0xFF6991C7)),
          ),
        ],
        centerTitle: true,
        title: Text(
          nameAppbar == null ? ' ...' : nameAppbar,
          style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 16.0,
              color: Colors.black54,
              fontFamily: "Gotik"),
        ),
        iconTheme: IconThemeData(
          color: Color(0xFF6991C7),
        ),
        elevation: 1,
      ),

      /// For call a variable include to body
      body: !isLoading ? SingleChildScrollView(
        child: Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              listSlider.length > 0 ? _imageSlider : Container(),
              _subCategory,
              _itemDiscount,
              _itemPopular,
              _itemNew
            ],
          ),
        ),
      ) : Container(
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }
}

///
///
/// Calling imageLoading animation for set a grid layout
///
///
Widget _loadingImageAnimation(BuildContext context) {
  return ListView.builder(
    scrollDirection: Axis.horizontal,
    itemBuilder: (BuildContext context, int index) => LoadingMenuItemCard(),
    itemCount: itemDiscount.length,
  );
}

///
///
/// Calling imageLoading animation for set a grid layout
///
///
Widget _loadingImageAnimationDiscount(BuildContext context) {
  return ListView.builder(
    scrollDirection: Axis.horizontal,
    itemBuilder: (BuildContext context, int index) => LoadingMenuItemDiscountCard(),
    itemCount: itemDiscount.length,
  );
}
