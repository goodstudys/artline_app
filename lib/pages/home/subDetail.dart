import 'dart:convert';

import 'package:artline/ListItem/CategoryItem.dart';
import 'package:artline/bloc/global/BlocProvider.dart';
import 'package:artline/bloc/global/GlobalBloc.dart';
import 'package:artline/components/gridItemCard.dart';
import 'package:artline/components/loadingItemCard.dart';
import 'package:artline/env.dart';
import 'package:artline/model/dataProduct.dart';
import 'package:artline/pages/home/Search.dart';
import 'package:artline/storage/storage.dart';
import 'package:flutter/material.dart';
import 'package:artline/pages/home/DetailProduct.dart';
import 'package:http/http.dart' as http;

class SubDetail extends StatefulWidget {
  final String title;

  SubDetail({
    @required this.title,
  });

  @override
  _SubDetailState createState() => _SubDetailState();
}

class _SubDetailState extends State<SubDetail> {
  DataStore storage = DataStore();
  var imageNetwork = NetworkImage(
      "https://firebasestorage.googleapis.com/v0/b/beauty-look.appspot.com/o/Screenshot_20181005-213931.png?alt=media&token=e6287f67-5bc0-4225-8e96-1623dc9dc42f");

  bool isLoading = true;
  String userLevel;

  void saveIdProduct(String id) async {
    storage.setDataString('idProd', id);
    BlocProvider.of<GlobalBloc>(context).cartBloc.cart.getGroup = null;
  }

  List<DataProduct> listNew;
  Future<String> getData() async {
    setState(() {
      isLoading = true;
    });
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var $userLevel = await storage.getDataString('userLevel');
    var subProd = await storage.getDataString('subProd');
    var urls = 'api/getProductsSubCategories/' + subProd;
    if (subProd == "0") {
      var idcat = await storage.getDataString('idCat');
      urls = 'api/allProductsCategory/' + idcat;
    }
    var res = await http.get(url(urls), headers: {
      'Authorization': $tokenType + ' ' + $accesToken,
      'Accept': 'application/json'
    });
    var content = json.decode(res.body);
    setState(() {
      userLevel = $userLevel;
      if (content['data'] != null) {
        listNew = List<DataProduct>.from(
            content['data'].map((item) => DataProduct.fromJson(item)));
      }
      isLoading = false;
    });
    return 'success!';
  }

  @override
  void initState() {
    /*
    imageNetwork.resolve(ImageConfiguration()).addListener(
        ImageStreamListener((ImageInfo image, bool synchronousCall) {
      if (mounted) {
        setState(() {
          imageLoad = false;
        });
      }
    }));
    */
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    /// Item Search in bottom of appbar
    ///
    /*
    double aspecttRatio;
    if (MediaQuery.of(context).size.height <= 715.0) {
      aspecttRatio = 950;
    } else {
      aspecttRatio = 1150;
    }
    */
    /*
    var _search = InkWell(
      onTap: () {
        Navigator.of(context).push(PageRouteBuilder(
            pageBuilder: (_, __, ___) => SearchAppbar(),

            /// transtation duration in animation
            transitionDuration: Duration(milliseconds: 750),

            /// animation route to search layout
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            }));
      },
      child: Container(
        margin: EdgeInsets.only(left: MediaQuery.of(context).padding.left + 15),
        height: 37.0,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            shape: BoxShape.rectangle),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(padding: EdgeInsets.only(left: 17.0)),
            Image.asset(
              "assets/img/search2.png",
              height: 22.0,
            ),
            Padding(
                padding: EdgeInsets.only(
              left: 17.0,
            )),
            Padding(
              padding: EdgeInsets.only(top: 3.0),
              child: Text(
                "Search Item",
                style: TextStyle(
                    fontFamily: "Popins",
                    color: Colors.black12,
                    fontWeight: FontWeight.w900,
                    letterSpacing: 0.0,
                    fontSize: 16.4),
              ),
            ),
          ],
        ),
      ),
    );
    */

    /// Grid Item a product
    var _grid = SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ///
          ///
          /// check the condition if image data from server firebase loaded or no
          /// if image true (image still downloading from server)
          /// Card to set card loading animation
          ///
          ///
          listNew == null
              ? Container(
                  child: Center(
                    child: Text(''),
                  ),
                )
              : isLoading
                  ? _imageLoading(context)
                  : GridView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      padding: EdgeInsets.symmetric(
                          horizontal: 7.0, vertical: 10.0),
                      shrinkWrap: true,
                      itemCount: listNew == null ? 0 : listNew.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisSpacing: 10.0,
                        mainAxisSpacing: 15.0,
                        childAspectRatio: userLevel == '4' ? 0.51 : 0.6,
                        crossAxisCount: 2,
                      ),
                      itemBuilder: (BuildContext context, int index) {
                        return GridItemCard(
                          item: listNew[index],
                          tap: () {
                            saveIdProduct(listNew[index].rowPointer);
                            Navigator.of(context).push(PageRouteBuilder(
                                pageBuilder: (_, __, ___) =>
                                    new DetailProduk(),
                                transitionsBuilder: (_,
                                    Animation<double> animation,
                                    __,
                                    Widget child) {
                                  return Opacity(
                                    opacity: animation.value,
                                    child: child,
                                  );
                                },
                                transitionDuration:
                                    Duration(milliseconds: 850)));
                          },
                          userLevel: userLevel,
                        );
                      })
          // GridView.count(
          //     shrinkWrap: true,
          //     padding: EdgeInsets.symmetric(
          //         horizontal: 7.0, vertical: 10.0),
          //     crossAxisSpacing: 10.0,
          //     mainAxisSpacing: 15.0,
          //     childAspectRatio: 0.6,
          //     crossAxisCount: 2,
          //     primary: false,
          //     children: List.generate(
          //         listNew == null ? 0 : listNew.length,
          //         (index) => GridItemCard(
          //               item: listNew[index],
          //               tap: () {
          //                 saveIdProduct(listNew[index].rowPointer);
          //                 Navigator.of(context).push(PageRouteBuilder(
          //                     pageBuilder: (_, __, ___) =>
          //                         new DetailProduk(),
          //                     transitionsBuilder: (_,
          //                         Animation<double> animation,
          //                         __,
          //                         Widget child) {
          //                       return Opacity(
          //                         opacity: animation.value,
          //                         child: child,
          //                       );
          //                     },
          //                     transitionDuration:
          //                         Duration(milliseconds: 850)));
          //               },
          //               userLevel: userLevel,
          //             )),
          //   )
        ],
      ),
    );

    return Scaffold(
      backgroundColor: Colors.white,

      /// Appbar item
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          widget.title.length > 0 ? widget.title : "Products",
          style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 16.0,
              color: Colors.black54,
              fontFamily: "Gotik"),
        ),
        iconTheme: IconThemeData(
          color: Color(0xFF6991C7),
        ),
        elevation: 1,
        actions: <Widget>[
          IconButton(
            onPressed: () {
              Navigator.of(context).push(PageRouteBuilder(
                pageBuilder: (_, __, ___) => SearchAppbar(),

                /// transtation duration in animation
                transitionDuration: Duration(milliseconds: 750),

                /// animation route to search layout
                transitionsBuilder:
                    (_, Animation<double> animation, __, Widget child) {
                  return Opacity(
                    opacity: animation.value,
                    child: child,
                  );
                }));
            },
            icon: Icon(Icons.search, color: Color(0xFF6991C7)),
          ),
        ],
      ),
      body: isLoading == true
          ? Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          : SingleChildScrollView(
              child: Container(
                child: listNew == null
                    ? Center(
                        child: Text('Data Produk Kosong'),
                      )
                    : Column(
                        /// Calling search and grid variable
                        children: <Widget>[
                          // _search,
                          _grid,
                        ],
                      ),
              ),
            ),
    );
  }
}

// /
// /
// / Calling imageLoading animation for set a grid layout
// /
// /
Widget _imageLoading(BuildContext context) {
  return GridView.count(
    shrinkWrap: true,
    padding: EdgeInsets.symmetric(horizontal: 7.0, vertical: 10.0),
    crossAxisSpacing: 10.0,
    mainAxisSpacing: 15.0,
    childAspectRatio: 0.545,
    crossAxisCount: 2,
    primary: false,
    children: List.generate(
      /// Get data in PromotionDetail.dart (ListItem folder)
      itemDiscount.length,
      (index) => LoadingMenuItemCard(),
    ),
  );
}
