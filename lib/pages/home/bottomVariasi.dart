import 'package:artline/bloc/global/BlocProvider.dart';
import 'package:artline/bloc/global/GlobalBloc.dart';
import 'package:artline/components/textFormatIDR.dart';
import 'package:artline/env.dart';
import 'package:artline/model/dataProduct.dart';
import 'package:artline/model/group.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class BottomVariasi extends StatefulWidget {
  final DataProduct item;
  final double amountFromPercent;
  final String userLevel;
  final List<GroupSelected> listGroups;
  BottomVariasi({this.item, this.amountFromPercent, this.listGroups, this.userLevel});
  @override
  _BottomVariasiState createState() => _BottomVariasiState(
      detailProduct: item,
      amountFromPercent: amountFromPercent,
      listGroups: listGroups,
      userLevel: userLevel);
}

class _BottomVariasiState extends State<BottomVariasi> {
  DataProduct detailProduct;
  String userLevel;
  double amountFromPercent;
  bool sama = false;
  int quantity = 1;
  List<GroupSelected> listGroups;
  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: duration, gravity: gravity);
  }

  _BottomVariasiState(
      {this.detailProduct,
      this.amountFromPercent,
      this.listGroups,
      this.userLevel});

  /// Custom Text black
  static var _customTextStyle = TextStyle(
    color: Colors.black,
    fontFamily: "Gotik",
    fontSize: 17.0,
    fontWeight: FontWeight.w800,
  );

  /// Custom Text for Header title
  static var _subHeaderCustomStyle = TextStyle(
      color: Colors.black54,
      fontWeight: FontWeight.w700,
      fontFamily: "Gotik",
      fontSize: 16.0);

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQD = MediaQuery.of(context);
    return StreamBuilder(
        stream: BlocProvider.of<GlobalBloc>(context).cartBloc.cartStream,
        builder: (context, snapshot) {
          GroupSelected dataGroup =
              BlocProvider.of<GlobalBloc>(context).cartBloc.cart.getGroup;
          String groupHarga = '';
          String groupDiscAmount = '';
          String groupDiscPercent = '';
          String groupRowPointer = '';
          String namaGroup = '';
          // int groupAmount = 0;
          double groupAmountFromPercent = 0.0;
          if (BlocProvider.of<GlobalBloc>(context).cartBloc.cart.getGroup !=
              null) {
            // listGroups.forEach((f) {
            //   if (f.pointer == groupRowPointer) {
            //     setState(() {
            //       sama = true;
            //     });
            //   } else {
            //     setState(() {
            //       sama = false;
            //     });
            //   }
            // });
            groupRowPointer = dataGroup.pointer;
            groupDiscAmount = dataGroup.discAmount;
            groupDiscPercent = dataGroup.discPercent;
            groupHarga = dataGroup.price;
            // if (dataGroup.discAmount != '0') {
            //   double presentase = (int.parse(dataGroup.discAmount) /
            //       int.parse(dataGroup.price));
            //   groupAmount = (presentase * 100.0).toInt();
            // }
            if (dataGroup.discPercent != '0') {
              double awal = int.parse(dataGroup.price) *
                  int.parse(dataGroup.discPercent) /
                  100;
              groupAmountFromPercent = int.parse(dataGroup.price) - awal;
            }
            namaGroup = dataGroup.name;
          }
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.indigo,
                            ),
                            borderRadius:
                                BorderRadius.all(Radius.circular(13.0))),
                        child: Image.network(
                          host +
                              'upload/users/products/' +
                              detailProduct.image1,
                          width:mediaQD.size.width/4,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 13.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Container(
                              width: mediaQD.size.width / 2,
                              child: Text(
                                detailProduct.name + ' ' + namaGroup,
                                style: _customTextStyle,
                                overflow: TextOverflow.ellipsis,
                                softWrap: true,
                                maxLines: 3,
                              ),
                            ),
                            userLevel == '4' &&
                                    detailProduct.wholesalePrice != null
                                ? TextFormatIDR(
                                    thisText:
                                        int.parse(detailProduct.wholesalePrice),
                                    thisStyle: _customTextStyle,
                                  )
                                : groupHarga != ''
                                    ? BlocProvider.of<GlobalBloc>(context)
                                                    .cartBloc
                                                    .cart
                                                    .getGroup
                                                    .discAmount ==
                                                '0' &&
                                            BlocProvider.of<GlobalBloc>(context)
                                                    .cartBloc
                                                    .cart
                                                    .getGroup
                                                    .discPercent ==
                                                '0'
                                        ? TextFormatIDR(
                                            thisText: int.parse(groupHarga),
                                            thisStyle: _customTextStyle,
                                          )
                                        : Column(
                                            children: <Widget>[
                                              TextFormatIDR(
                                                thisText: int.parse(groupHarga),
                                                thisStyle:
                                                    _customTextStyle.copyWith(
                                                        decoration:
                                                            TextDecoration
                                                                .lineThrough,
                                                        fontSize: 13.0,
                                                        color: Colors.black26),
                                              ),
                                              Padding(
                                                  padding: EdgeInsets.only(
                                                      top: 5.0)),
                                              TextFormatIDR(
                                                thisText: BlocProvider.of<
                                                                    GlobalBloc>(
                                                                context)
                                                            .cartBloc
                                                            .cart
                                                            .getGroup
                                                            .discAmount !=
                                                        '0'
                                                    ? int.parse(groupHarga) -
                                                        int.parse(BlocProvider
                                                                .of<GlobalBloc>(
                                                                    context)
                                                            .cartBloc
                                                            .cart
                                                            .getGroup
                                                            .discAmount)
                                                    : groupAmountFromPercent,
                                                thisStyle:
                                                    _customTextStyle.copyWith(
                                                        color: Colors.redAccent,
                                                        fontSize: 20.0),
                                              ),
                                            ],
                                          )
                                    : detailProduct.discAmount == '0' &&
                                            detailProduct.discPercent == '0'
                                        ? TextFormatIDR(
                                            thisText:
                                                int.parse(detailProduct.price),
                                            thisStyle: _customTextStyle,
                                          )
                                        // Text(
                                        //   formatter
                                        //       .format(int.parse(detailProduct.price)),
                                        //   style:
                                        //       _customTextStyle,
                                        // )
                                        : Column(
                                            children: <Widget>[
                                              TextFormatIDR(
                                                thisText: int.parse(
                                                    detailProduct.price),
                                                thisStyle:
                                                    _customTextStyle.copyWith(
                                                        decoration:
                                                            TextDecoration
                                                                .lineThrough,
                                                        fontSize: 13.0,
                                                        color: Colors.black26),
                                              ),
                                              Padding(
                                                  padding: EdgeInsets.only(
                                                      top: 5.0)),
                                              TextFormatIDR(
                                                thisText: detailProduct
                                                            .discAmount !=
                                                        '0'
                                                    ? int.parse(detailProduct
                                                            .price) -
                                                        int.parse(detailProduct
                                                            .discAmount)
                                                    : amountFromPercent,
                                                thisStyle:
                                                    _customTextStyle.copyWith(
                                                        color: Colors.redAccent,
                                                        fontSize: 20.0),
                                              ),
                                            ],
                                          ),
                            // Text('5')
                          ],
                        ),
                      )
                    ],
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Icon(
                      Icons.close,
                      color: Colors.indigo,
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 13.0, bottom: 13.0),
                child: new Divider(
                  height: 1.5,
                ),
              ),
              Text(
                "Pilihan",
                style: _subHeaderCustomStyle,
              ),
              listGroups == []
                  ? Container()
                  : Container(
                      height: mediaQD.size.height / 4.3,
                      child: Wrap(
                        spacing: 8.0, // gap between adjacent chips
                        runSpacing: 4.0, // gap between lines
                        children: listGroups.map((entry) {
                          return InkWell(
                            onTap: () {
                              BlocProvider.of<GlobalBloc>(context)
                                  .cartBloc
                                  .tambahGroup
                                  .add(entry);
                              setState(() {
                                sama = true;
                              });
                            },
                            child: Chip(
                              padding: EdgeInsets.all(0),
                              backgroundColor: groupRowPointer == entry.pointer ? Colors.indigoAccent : Colors.grey[300],
                              label: Text(
                                entry.name,
                                style: TextStyle(
                                  color: groupRowPointer == entry.pointer ? Colors.white : Colors.black
                                )
                              ),
                            )
                          );
                        }).toList(),
                      ),
                      /*
                      GridView.builder(
                          shrinkWrap: true,
                          itemCount: listGroups == [] || listGroups == null
                              ? 0
                              : listGroups.length,
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            childAspectRatio: 2,
                            crossAxisCount: 4,
                            crossAxisSpacing: 1.0,
                            mainAxisSpacing: 1.0,
                          ),
                          itemBuilder: (BuildContext context, int index) {
                            return InkWell(
                              onTap: () {
                                BlocProvider.of<GlobalBloc>(context)
                                    .cartBloc
                                    .tambahGroup
                                    .add(listGroups[index]);
                                setState(() {
                                  sama = true;
                                });
                              },
                              child: Container(
                                height: 50.0,
                                padding: EdgeInsets.all(13.0),
                                decoration: BoxDecoration(
                                    color: groupRowPointer ==
                                            listGroups[index].pointer
                                        ? Colors.indigoAccent
                                        : Colors.grey[300],
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(13.0))),
                                child: Center(
                                  child: Text(
                                    listGroups[index].name,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        color: groupRowPointer ==
                                                listGroups[index].pointer
                                            ? Colors.white
                                            : Colors.black),
                                  ),
                                ),
                              ),
                            );
                          }),
                      */
                    ),
              Padding(
                padding: const EdgeInsets.only(top: 13.0),
                child: new Divider(
                  height: 1.5,
                ),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Jumlah Barang'),
                  Container(
                    width: 130.0,
                    decoration: BoxDecoration(
                        color: Colors.white70,
                        border: Border.all(
                            color: Colors.black12.withOpacity(0.1))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        /// Decrease of value item
                        InkWell(
                          onTap: () {
                            setState(() {
                              if (quantity == 1 || quantity <= 1) {
                                return;
                              } else {
                                quantity--;
                              }
                            });
                          },
                          child: Container(
                            height: 30.0,
                            width: 30.0,
                            decoration: BoxDecoration(
                                border: Border(
                                    right: BorderSide(
                                        color: Colors.black12
                                            .withOpacity(0.1)))),
                            child: Center(child: Text("-")),
                          ),
                        ),
                        Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 18.0),
                            child: Text(quantity.toString())),

                        /// Increasing value of item
                        InkWell(
                          onTap: () {
                            setState(() {
                              quantity++;
                              // _controller.text = (currentValue)
                              //     .toString(); // incrementing value
                            });
                          },
                          child: Container(
                            height: 30.0,
                            width: 28.0,
                            decoration: BoxDecoration(
                                border: Border(
                                    left: BorderSide(
                                        color: Colors.black12
                                            .withOpacity(0.1)))),
                            child: Center(child: Text("+")),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              InkWell(
                onTap: () {
                  if (sama == true) {
                    if (detailProduct.wholesalePrice != null &&
                        userLevel == '4') {
                      BlocProvider.of<GlobalBloc>(context)
                          .cartBloc
                          .addItem
                          .add(DataProduct(
                              brand: detailProduct.brand,
                              category: detailProduct.category,
                              colorHex: detailProduct.colorHex,
                              colorName: detailProduct.colorName,
                              dateCreated: detailProduct.dateCreated,
                              discAmount: groupDiscAmount,
                              discPercent: groupDiscPercent,
                              description: detailProduct.description,
                              flag: detailProduct.flag,
                              id: detailProduct.id,
                              image1: detailProduct.image1,
                              name:
                                  detailProduct.name + ' ' + namaGroup,
                              price: detailProduct.wholesalePrice,
                              sku: detailProduct.sku + '-' + namaGroup,
                              rowPointer: detailProduct.rowPointer,
                              weight: detailProduct.weight,
                              dimensions: detailProduct.dimensions,
                              subcategory: detailProduct.subcategory,
                              tags: detailProduct.tags,
                              qty: quantity));
                      Navigator.pop(context);
                      showToast("Dimasukkan ke Keranjang",
                          duration: Toast.LENGTH_LONG,
                          gravity: Toast.BOTTOM);
                    } else {
                      BlocProvider.of<GlobalBloc>(context)
                          .cartBloc
                          .addItem
                          .add(DataProduct(
                              brand: detailProduct.brand,
                              category: detailProduct.category,
                              colorHex: detailProduct.colorHex,
                              colorName: detailProduct.colorName,
                              dateCreated: detailProduct.dateCreated,
                              discAmount: detailProduct.discAmount,
                              discPercent: detailProduct.discPercent,
                              description: detailProduct.description,
                              flag: detailProduct.flag,
                              id: detailProduct.id,
                              image1: detailProduct.image1,
                              name:
                                  detailProduct.name + ' ' + namaGroup,
                              price: groupHarga,
                              sku: detailProduct.sku + '-' + namaGroup,
                              rowPointer: detailProduct.rowPointer,
                              weight: detailProduct.weight,
                              dimensions: detailProduct.dimensions,
                              subcategory: detailProduct.subcategory,
                              tags: detailProduct.tags,
                              qty: quantity));
                      Navigator.pop(context);
                      showToast("Dimasukkan ke Keranjang",
                          duration: Toast.LENGTH_LONG,
                          gravity: Toast.BOTTOM);
                    }
                  }
                },
                child: Container(
                  height: 55.0,
                  width: mediaQD.size.width,
                  color: sama == true ? Colors.indigo : Colors.grey[350],
                  child: Center(
                      child: Text(
                    "Add To Cart",
                    style: TextStyle(
                        color:
                            sama == true ? Colors.white : Colors.grey,
                        fontWeight: FontWeight.w700),
                  )),
                )
              ),
              /*
              Container(
                width: mediaQD.size.width,
                margin: EdgeInsets.only(top: 9.0),
                child: InkWell(
                    child: FlatButton(
                        onPressed: () {
                          if (sama == true) {
                            if (detailProduct.wholesalePrice != null &&
                                userLevel == '4') {
                              BlocProvider.of<GlobalBloc>(context)
                                  .cartBloc
                                  .addItem
                                  .add(DataProduct(
                                      brand: detailProduct.brand,
                                      category: detailProduct.category,
                                      colorHex: detailProduct.colorHex,
                                      colorName: detailProduct.colorName,
                                      dateCreated: detailProduct.dateCreated,
                                      discAmount: groupDiscAmount,
                                      discPercent: groupDiscPercent,
                                      description: detailProduct.description,
                                      flag: detailProduct.flag,
                                      id: detailProduct.id,
                                      image1: detailProduct.image1,
                                      name:
                                          detailProduct.name + ' ' + namaGroup,
                                      price: detailProduct.wholesalePrice,
                                      sku: detailProduct.sku + '-' + namaGroup,
                                      rowPointer: detailProduct.rowPointer,
                                      weight: detailProduct.weight,
                                      dimensions: detailProduct.dimensions,
                                      subcategory: detailProduct.subcategory,
                                      tags: detailProduct.tags,
                                      qty: quantity));
                              Navigator.pop(context);
                              showToast("Dimasukkan ke Keranjang",
                                  duration: Toast.LENGTH_LONG,
                                  gravity: Toast.BOTTOM);
                            } else {
                              BlocProvider.of<GlobalBloc>(context)
                                  .cartBloc
                                  .addItem
                                  .add(DataProduct(
                                      brand: detailProduct.brand,
                                      category: detailProduct.category,
                                      colorHex: detailProduct.colorHex,
                                      colorName: detailProduct.colorName,
                                      dateCreated: detailProduct.dateCreated,
                                      discAmount: detailProduct.discAmount,
                                      discPercent: detailProduct.discPercent,
                                      description: detailProduct.description,
                                      flag: detailProduct.flag,
                                      id: detailProduct.id,
                                      image1: detailProduct.image1,
                                      name:
                                          detailProduct.name + ' ' + namaGroup,
                                      price: groupHarga,
                                      sku: detailProduct.sku + '-' + namaGroup,
                                      rowPointer: detailProduct.rowPointer,
                                      weight: detailProduct.weight,
                                      dimensions: detailProduct.dimensions,
                                      subcategory: detailProduct.subcategory,
                                      tags: detailProduct.tags,
                                      qty: quantity));
                              Navigator.pop(context);
                              showToast("Dimasukkan ke Keranjang",
                                  duration: Toast.LENGTH_LONG,
                                  gravity: Toast.BOTTOM);
                            }
                          }
                        },
                        child: Container(
                          padding: EdgeInsets.all(13.0),
                          color: sama == true ? Colors.indigo : Colors.grey[350],
                          child: Center(
                              child: Text(
                            "Add To Cart",
                            style: TextStyle(
                                color:
                                    sama == true ? Colors.white : Colors.grey,
                                fontWeight: FontWeight.w700),
                          )),
                        ))
                    // Container(
                    //   height: 45.0,
                    //   // margin: EdgeInsets.only(top:13.0, left: 23.0),
                    //   width: 250,
                    //   decoration: BoxDecoration(
                    //     color: Colors.indigoAccent,
                    //   ),
                    //   child: Center(
                    //     child: Text(
                    //       "Add To Cart",
                    //       style: TextStyle(
                    //           color: Colors.white, fontWeight: FontWeight.w700),
                    //     ),
                    //   ),
                    // ),
                    ),
              ),
              */
            ],
          );
        });
  }
}
