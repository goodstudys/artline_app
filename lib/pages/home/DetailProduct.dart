import 'package:artline/Library/carousel_pro/carousel_pro.dart';
import 'package:artline/bloc/cart/Cart.dart';
import 'package:artline/bloc/global/BlocProvider.dart';
import 'package:artline/bloc/global/GlobalBloc.dart';
import 'package:artline/components/favoriteItem.dart';
import 'package:artline/components/textFormatIDR.dart';
import 'package:artline/env.dart';
import 'package:artline/model/dataProduct.dart';
import 'package:artline/model/group.dart';
import 'package:artline/pages/home/bottomVariasi.dart';
import 'package:artline/storage/storage.dart';
import 'package:flutter/material.dart';
import 'package:artline/pages/cart/cartLayout.dart';
import 'package:flutter_rating/flutter_rating.dart';
import 'package:artline/repository/products.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class DetailProduk extends StatefulWidget {
  @override
  _DetailProdukState createState() => _DetailProdukState();
}

/// Detail Product for Recomended Grid in home screen
class _DetailProdukState extends State<DetailProduk> {
  DataStore storage = DataStore();
  bool isLoading = true;
  List listrelated;
  List<DataProduct> relatedProducts;
  DataProduct detailProduct;
  int percentAmount;
  String userLevel;
  String hargaGroup = '';
  List rev;
  String groupi;
  List<GroupSelected> listGroups = [];
  List listImage = [];
  List<GroupSelected> groups;
  double amountFromPercent;
  GroupSelected selectGroup;
  void bottomView() {
    showModalBottomSheet(
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      context: context,
      builder: (BuildContext context) {
        return Container(
            padding: EdgeInsets.all(13.0),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(13.0),
                    topRight: Radius.circular(13.0))),
            height: MediaQuery.of(context).size.height * 0.75,
            child: BottomVariasi(
              amountFromPercent: amountFromPercent,
              item: detailProduct,
              listGroups: listGroups,
              userLevel: userLevel,
            ));
      },
    );
  }
  // Future<String> getDataGroup(String pointer) async {
  //   var $tokenType = await storage.getDataString('token_type');
  //   var $accesToken = await storage.getDataString('access_token');
  //   var idProd = await storage.getDataString('idProd');
  //   var res = await http.post(url('api/groupSelected/' + idProd),
  //       headers: {'Authorization': $tokenType + ' ' + $accesToken},
  //       body: {'selectedgroup': pointer});

  //   setState(() {
  //     var content = json.decode(res.body);
  //     selectGroup = GroupSelected.fromJson(content['groupselect']);
  //     BlocProvider.of<GlobalBloc>(context).cartBloc.cart.addGroup(selectGroup);
  //     hargaGroup = content['groupselect']['price'];
  //   });
  //   return 'success!';
  // }

  Future<String> getData() async {
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var leveluser = await storage.getDataString('userLevel');
    var idProd = await storage.getDataString('idProd');
    var res = await http.get(url('api/getDetailProduct/' + idProd),
        headers: {'Authorization': $tokenType + ' ' + $accesToken});

    setState(() {
      var content = json.decode(res.body);
      listrelated = content['products'];
      detailProduct = DataProduct.fromJson(content['data']);
      if (content['products'] != null) {
        relatedProducts = List<DataProduct>.from(content['products'].map((item) => DataProduct.fromJson(item)));
      }
      groupi = content['group'].toString();
      if (content['group'] != []) {
        listGroups = List<GroupSelected>.from(
            content['group'].map((item) => GroupSelected.fromJson(item)));
        // listGroups.add(GroupSelected(
        //   detailId: 0,
        //   discAmount: detailProduct.discAmount,
        //   discPercent: detailProduct.discPercent,
        //   groupDetail: detailProduct.id,
        //   groupId1: 0,
        //   name: detailProduct.colorName,
        //   pointer: detailProduct.rowPointer,
        //   price: detailProduct.price,
        //   productId: detailProduct.rowPointer,
        // ));
        // groups.forEach((f) {
        //   listGroups.add(f);
        // });
      }
      if (detailProduct.image1 != null) {
        listImage.add(NetworkImage(
            host + 'upload/users/products/' + detailProduct.image1));
      }
      if (detailProduct.image2 != null) {
        listImage.add(NetworkImage(
            host + 'upload/users/products/' + detailProduct.image2));
      }
      if (detailProduct.image3 != null) {
        listImage.add(NetworkImage(
            host + 'upload/users/products/' + detailProduct.image3));
      }
      if (detailProduct.image4 != null) {
        listImage.add(NetworkImage(
            host + 'upload/users/products/' + detailProduct.image4));
      }
      if (detailProduct.discAmount != '0') {
        double presentase = (int.parse(detailProduct.discAmount) /
            int.parse(detailProduct.price));
        percentAmount = (presentase * 100.0).toInt();
      }
      if (detailProduct.discPercent != '0') {
        int awal = int.parse(detailProduct.price) *
            int.parse(detailProduct.discPercent);
        double dua = awal / 100;
        amountFromPercent = int.parse(detailProduct.price) - dua;
      }
      debugPrint('INIIIIIIII' +
          idProd +
          content['data']['review'].toString() +
          'amount price = ' +
          amountFromPercent.toString());
      if (content['data']['review'] != []) {
        rev = content['data']['review'];
      }
      userLevel = leveluser;
    });
    return 'success!';
  }

  void saveIdProduct(String id) async {
    storage.setDataString('idProd', id);
    BlocProvider.of<GlobalBloc>(context).cartBloc.cart.getGroup = null;
  }

  @override
  void initState() {
    super.initState();
    getData().then((s) => setState(() {
          isLoading = false;
        }));
  }

  double rating = 3.5;
  int starCount = 5;

  Product rated;
  // static BuildContext ctx;
  int valueItemChart = 0;
  final GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();

  /// BottomSheet for view more in specification
  void _bottomSheet() {
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      context: context,
      builder: (builder) {
        return Container(
          padding: const EdgeInsets.only(top: 5.0),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20.0),
              topRight: Radius.circular(20.0)
            )
          ),
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(top: 20.0),
            children: <Widget>[
              Center(
                  child: Text(
                "Description",
                style: _subHeaderCustomStyle,
              )),
              Padding(
                padding: const EdgeInsets.only(
                    top: 20.0, left: 20.0, right: 20.0, bottom: 20.0),
                child:
                    Text(detailProduct.description, style: _detailText),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 20.0, left: 20.0, right: 20.0, bottom: 20.0),
                child:
                    Text(detailProduct.description, style: _detailText),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 20.0, left: 20.0, right: 20.0, bottom: 20.0),
                child:
                    Text(detailProduct.description, style: _detailText),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 20.0, left: 20.0, right: 20.0, bottom: 20.0),
                child:
                    Text(detailProduct.description, style: _detailText),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 20.0, left: 20.0, right: 20.0, bottom: 20.0),
                child:
                    Text(detailProduct.description, style: _detailText),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 20.0, left: 20.0, right: 20.0, bottom: 20.0),
                child:
                    Text(detailProduct.description, style: _detailText),
              ),
              // Padding(
              //   padding: const EdgeInsets.only(left: 20.0),
              //   child: Text(
              //     "Spesifications :",
              //     style: TextStyle(
              //         fontFamily: "Gotik",
              //         fontWeight: FontWeight.w600,
              //         fontSize: 15.0,
              //         color: Colors.black,
              //         letterSpacing: 0.3,
              //         wordSpacing: 0.5),
              //   ),
              // ),
              // Padding(
              //   padding: const EdgeInsets.only(top: 20.0, left: 20.0),
              //   child: Text(
              //     " - Lorem ipsum is simply dummy  ",
              //     style: _detailText,
              //   ),
              // )
            ],
          ),
        );
      }
    );
  }

  /// Custom Text black
  static var _customTextStyle = TextStyle(
    color: Colors.black,
    fontFamily: "Gotik",
    fontSize: 17.0,
    fontWeight: FontWeight.w800,
  );

  /// Custom Text for Header title
  static var _subHeaderCustomStyle = TextStyle(
      color: Colors.black54,
      fontWeight: FontWeight.w700,
      fontFamily: "Gotik",
      fontSize: 16.0);

  /// Custom Text for Detail title
  static var _detailText = TextStyle(
      fontFamily: "Gotik",
      color: Colors.black54,
      letterSpacing: 0.3,
      wordSpacing: 0.5);

  Widget build(BuildContext context) {
    MediaQueryData mediaQD = MediaQuery.of(context);
    CartData cartBLOC = BlocProvider.of<GlobalBloc>(context).cartBloc.cart;
    return StreamBuilder(
        stream: BlocProvider.of<GlobalBloc>(context).cartBloc.cartStream,
        builder: (context, snapshot) {
          String groupHarga = '';
          String namaGroup = '';
          int groupAmount = 0;
          double groupAmountFromPercent = 0.0;
          if (cartBLOC.getGroup != null) {
            groupHarga = cartBLOC.getGroup.price;
            if (cartBLOC.getGroup.discAmount != '0') {
              double presentase = (int.parse(cartBLOC.getGroup.discAmount) / int.parse(cartBLOC.getGroup.price));
              groupAmount = (presentase * 100.0).toInt();
            }
            if (cartBLOC.getGroup.discPercent != '0') {
              double awal = int.parse(cartBLOC.getGroup.price) * int.parse(cartBLOC.getGroup.discPercent) / 100;
              groupAmountFromPercent = int.parse(cartBLOC.getGroup.price) - awal;
            }
            var grpname = cartBLOC.getGroup;
            namaGroup = grpname != null ? grpname.name : '';
          }
          return Scaffold(
            key: _key,
            appBar: AppBar(
              actions: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.of(context).push(PageRouteBuilder(
                        pageBuilder: (_, __, ___) => new CartLayout()));
                  },
                  child: Stack(
                    alignment: AlignmentDirectional(-1.0, -0.8),
                    children: <Widget>[
                      IconButton(
                          onPressed: null,
                          icon: Icon(
                            Icons.shopping_cart,
                            color: Colors.black26,
                          )),
                      CircleAvatar(
                        radius: 10.0,
                        backgroundColor: Colors.red,
                        child: Text(
                          BlocProvider.of<GlobalBloc>(context)
                              .cartBloc
                              .cart
                              .listCart
                              .length
                              .toString(),
                          // ScopedModel.of<CartModel>(context, rebuildOnChange: true)
                          //     .cartListing
                          //     .length
                          //     .toString(),
                          style: TextStyle(color: Colors.white, fontSize: 13.0),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
              elevation: 1,
              centerTitle: true,
              backgroundColor: Colors.white,
              title: Text(
                !isLoading ? detailProduct.name + ' ' + namaGroup : 'Loading...',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  color: Colors.black54,
                  fontSize: 17.0,
                  fontFamily: "Gotik",
                ),
              ),
            ),
            body: !isLoading
                ? Column(
                    children: <Widget>[
                      Flexible(
                        child: SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              /// Header image slider
                              Container(
                                height: mediaQD.size.height / 3,
                                child: Hero(
                                  tag: "hero-grid-${detailProduct.id}",
                                  child: Material(
                                    child: new Carousel(
                                      dotColor: Colors.black26,
                                      dotIncreaseSize: 1.7,
                                      dotBgColor: Colors.transparent,
                                      autoplay: false,
                                      boxFit: BoxFit.contain,
                                      images: listImage,
                                    ),
                                  ),
                                ),
                              ),
                              groupHarga != ''
                                  ? BlocProvider.of<GlobalBloc>(context)
                                                  .cartBloc
                                                  .cart
                                                  .getGroup
                                                  .discAmount ==
                                              '0' &&
                                          BlocProvider.of<GlobalBloc>(context)
                                                  .cartBloc
                                                  .cart
                                                  .getGroup
                                                  .discPercent ==
                                              '0'
                                      ? Text('')
                                      : Container(
                                          height: 50.0,
                                          width: 1000.0,
                                          color: Colors.redAccent,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Row(
                                                children: <Widget>[
                                                  Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 20.0)),
                                                  Image.asset(
                                                    "assets/icon/flashSaleIcon.png",
                                                    height: 25.0,
                                                  ),
                                                  Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 10.0)),
                                                  Text(
                                                    "Sale",
                                                    style: _customTextStyle
                                                        .copyWith(
                                                            color:
                                                                Colors.white),
                                                  ),
                                                ],
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    right: 15.0),
                                                child: groupHarga != ''
                                                    ? Text(
                                                        BlocProvider.of<GlobalBloc>(
                                                                        context)
                                                                    .cartBloc
                                                                    .cart
                                                                    .getGroup
                                                                    .discAmount !=
                                                                '0'
                                                            ? groupAmount
                                                                    .toString() +
                                                                " %"
                                                            : BlocProvider.of<
                                                                            GlobalBloc>(
                                                                        context)
                                                                    .cartBloc
                                                                    .cart
                                                                    .getGroup
                                                                    .discPercent +
                                                                " %",
                                                        style: _customTextStyle
                                                            .copyWith(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 13.5),
                                                      )
                                                    : Text(
                                                        detailProduct
                                                                    .discAmount !=
                                                                '0'
                                                            ? percentAmount
                                                                    .toString() +
                                                                " %"
                                                            : detailProduct
                                                                    .discPercent +
                                                                " %",
                                                        style: _customTextStyle
                                                            .copyWith(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 13.5),
                                                      ),
                                              )
                                            ],
                                          ),
                                        )
                                  : detailProduct.discAmount == '0' &&
                                          detailProduct.discPercent == '0'
                                      ? Text('')
                                      : Container(
                                          height: 50.0,
                                          width: 1000.0,
                                          color: Colors.redAccent,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Row(
                                                children: <Widget>[
                                                  Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 20.0)),
                                                  Image.asset(
                                                    "assets/icon/flashSaleIcon.png",
                                                    height: 25.0,
                                                  ),
                                                  Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 10.0)),
                                                  Text(
                                                    "Sale",
                                                    style: _customTextStyle
                                                        .copyWith(
                                                            color:
                                                                Colors.white),
                                                  ),
                                                ],
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    right: 15.0),
                                                child: groupHarga != ''
                                                    ? Text(
                                                        BlocProvider.of<GlobalBloc>(
                                                                        context)
                                                                    .cartBloc
                                                                    .cart
                                                                    .getGroup
                                                                    .discAmount !=
                                                                '0'
                                                            ? groupAmount
                                                                    .toString() +
                                                                " %"
                                                            : BlocProvider.of<
                                                                            GlobalBloc>(
                                                                        context)
                                                                    .cartBloc
                                                                    .cart
                                                                    .getGroup
                                                                    .discPercent +
                                                                " %",
                                                        style: _customTextStyle
                                                            .copyWith(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 13.5),
                                                      )
                                                    : Text(
                                                        detailProduct
                                                                    .discAmount !=
                                                                '0'
                                                            ? percentAmount
                                                                    .toString() +
                                                                " %"
                                                            : detailProduct
                                                                    .discPercent +
                                                                " %",
                                                        style: _customTextStyle
                                                            .copyWith(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 13.5),
                                                      ),
                                              )
                                            ],
                                          ),
                                        ),

                              /// Background white title,price and ratting
                              Container(
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    boxShadow: [
                                      BoxShadow(
                                        color:
                                            Color(0xFF656565).withOpacity(0.15),
                                        blurRadius: 1.0,
                                        spreadRadius: 0.2,
                                      )
                                    ]),
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 10.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.only(left: 13.0, right: 13.0),
                                        child: Text(
                                          detailProduct.name + ' ' + namaGroup,
                                          style: _customTextStyle.copyWith(fontSize: 19.0),
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 2,
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(left: 13.0, top: 5.0),
                                      child: groupHarga != '' ? cartBLOC.getGroup.discAmount == '0'
                                        && cartBLOC.getGroup.discPercent == '0' ? TextFormatIDR(
                                          thisText: int.parse(groupHarga),
                                          thisStyle: _customTextStyle,
                                        ) : Column(
                                          children: <Widget>[
                                            TextFormatIDR(
                                              thisText:
                                                          int.parse(groupHarga),
                                                      thisStyle: _customTextStyle
                                                          .copyWith(
                                                              decoration:
                                                                  TextDecoration
                                                                      .lineThrough,
                                                              fontSize: 13.0,
                                                              color: Colors
                                                                  .black26),
                                                    ),
                                                    Padding(
                                                        padding:
                                                            EdgeInsets.only(
                                                                top: 5.0)),
                                                    TextFormatIDR(
                                                      thisText: BlocProvider.of<
                                                                          GlobalBloc>(
                                                                      context)
                                                                  .cartBloc
                                                                  .cart
                                                                  .getGroup
                                                                  .discAmount !=
                                                              '0'
                                                          ? int.parse(
                                                                  groupHarga) -
                                                              int.parse(BlocProvider
                                                                      .of<GlobalBloc>(
                                                                          context)
                                                                  .cartBloc
                                                                  .cart
                                                                  .getGroup
                                                                  .discAmount)
                                                          : groupAmountFromPercent,
                                                      thisStyle: _customTextStyle
                                                          .copyWith(
                                                              color: Colors
                                                                  .redAccent,
                                                              fontSize: 20.0),
                                                    ),
                                                  ],
                                                )
                                          : detailProduct.discAmount == '0' &&
                                                  detailProduct.discPercent ==
                                                      '0'
                                              ? TextFormatIDR(
                                                  thisText: int.parse(
                                                      detailProduct.price),
                                                  thisStyle: _customTextStyle,
                                                )
                                              : Column(
                                                  children: <Widget>[
                                                    TextFormatIDR(
                                                      thisText: int.parse(
                                                          detailProduct.price),
                                                      thisStyle: _customTextStyle
                                                          .copyWith(
                                                              decoration:
                                                                  TextDecoration
                                                                      .lineThrough,
                                                              fontSize: 13.0,
                                                              color: Colors
                                                                  .black26),
                                                    ),
                                                    Padding(
                                                        padding:
                                                            EdgeInsets.only(
                                                                top: 5.0)),
                                                    TextFormatIDR(
                                                      thisText: detailProduct
                                                                  .discAmount !=
                                                              '0'
                                                          ? int.parse(
                                                                  detailProduct
                                                                      .price) -
                                                              int.parse(
                                                                  detailProduct
                                                                      .discAmount)
                                                          : amountFromPercent,
                                                      thisStyle: _customTextStyle
                                                          .copyWith(
                                                              color: Colors
                                                                  .redAccent,
                                                              fontSize: 20.0),
                                                    ),
                                                  ],
                                                ),
                                  ),
                                      Padding(padding: EdgeInsets.only(top: 10.0)),
                                      Divider(
                                        color: Colors.black12,
                                        height: 1.0,
                                      ),
                                      InkWell(
                                        onTap: () {
                                          bottomView();
                                          cartBLOC.getGroup = null;
                                        },
                                        // =>
                                        // showModalBottomSheet(
                                        //   isScrollControlled: true,
                                        //   backgroundColor: Colors.transparent,
                                        //   context: context,
                                        //   builder: (BuildContext context) {
                                        //     return Container(
                                        //         padding: EdgeInsets.all(13.0),
                                        //         decoration: BoxDecoration(
                                        //             color: Colors.white,
                                        //             borderRadius: BorderRadius.only(
                                        //                 topLeft:
                                        //                     Radius.circular(13.0),
                                        //                 topRight:
                                        //                     Radius.circular(13.0))),
                                        //         height: mediaQD
                                        //                 .size
                                        //                 .height *
                                        //             0.75,
                                        //         child: BottomVariasi(
                                        //           amountFromPercent:
                                        //               amountFromPercent,
                                        //           item: detailProduct,
                                        //           listGroups: listGroups,
                                        //         ));
                                        //   },
                                        // ),
                                        child: Container(
                                          padding: EdgeInsets.only(left: 13.0, right: 13.0),
                                          height: 50.0,
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Row(
                                                children: <Widget>[
                                                  Text(
                                                    "Pilih Variasi",
                                                    style: _subHeaderCustomStyle,
                                                  ),
                                                  Padding(
                                                    padding: const EdgeInsets.only(
                                                        left: 13.0),
                                                    child: Text(
                                                      "( Warna )",
                                                      style: _detailText,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Icon(
                                                Icons.arrow_forward_ios,
                                                color: Colors.black45,
                                                size: 16,
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                      /*
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 10.0, bottom: 10.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                Container(
                                                  height: 30.0,
                                                  width: 75.0,
                                                  decoration: BoxDecoration(
                                                    color: Colors.lightGreen,
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                20.0)),
                                                  ),
                                                  child: Row(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: <Widget>[
                                                      Text(
                                                        '4.8'.toString(),
                                                        style: TextStyle(
                                                            color:
                                                                Colors.white),
                                                      ),
                                                      Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 8.0)),
                                                      Icon(
                                                        Icons.star,
                                                        color: Colors.white,
                                                        size: 19.0,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                            /*Padding(
                                  padding: const EdgeInsets.only(right: 15.0),
                                  child: Text(
                                    isie.itemSale,
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 13.0,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),*/
                                          ],
                                        ),
                                      )
                                      */
                                    ],
                                  ),
                                ),
                              ),
                              // groups == null || groupi == '[]'
                              //     ? Container()
                              //     : Container(
                              //         // color: Colors.white,
                              //         margin: EdgeInsets.only(
                              //             right: 10.0, top: 5.0),
                              //         padding: EdgeInsets.only(
                              //             bottom: 8.0, top: 8.0),
                              //         height: 120.0,
                              //         child: Column(
                              //           mainAxisAlignment:
                              //               MainAxisAlignment.start,
                              //           crossAxisAlignment:
                              //               CrossAxisAlignment.start,
                              //           children: <Widget>[
                              //             Expanded(
                              //               child: GridView.builder(
                              //                 shrinkWrap: true,
                              //                 scrollDirection: Axis.horizontal,
                              //                 itemCount: groups == null
                              //                     ? 0
                              //                     : groups.length,
                              //                 gridDelegate:
                              //                     SliverGridDelegateWithFixedCrossAxisCount(
                              //                         crossAxisCount: 2,
                              //                         childAspectRatio: 0.2,
                              //                         crossAxisSpacing: 8.0,
                              //                         mainAxisSpacing: 0.8),
                              //                 padding: EdgeInsets.only(
                              //                     top: 4.0,
                              //                     left: 5.0,
                              //                     right: 5.0,
                              //                     bottom: 5.0),
                              //                 itemBuilder:
                              //                     (BuildContext context,
                              //                         int index) {
                              //                   return Padding(
                              //                     padding:
                              //                         const EdgeInsets.only(
                              //                             left: 20.0),
                              //                     child: InkWell(
                              //                       onTap: () {
                              //                         BlocProvider.of<
                              //                                     GlobalBloc>(
                              //                                 context)
                              //                             .cartBloc
                              //                             .tambahGroup
                              //                             .add(groups[index]);
                              //                       },
                              //                       child: Container(
                              //                         padding:
                              //                             const EdgeInsets.only(
                              //                                 left: 10.0),
                              //                         height: 18.5,
                              //                         // width: 90.0,
                              //                         decoration: BoxDecoration(
                              //                           color: Colors.white,
                              //                           border: namaGroup
                              //                                       .toLowerCase() ==
                              //                                   groups[index]
                              //                                       .name
                              //                                       .toLowerCase()
                              //                               ? Border.all(
                              //                                   color: Colors
                              //                                       .indigo)
                              //                               : Border.all(
                              //                                   color: Colors
                              //                                       .grey),
                              //                           borderRadius:
                              //                               BorderRadius.all(
                              //                                   Radius.circular(
                              //                                       20.0)),
                              //                           boxShadow: [
                              //                             BoxShadow(
                              //                               color: Colors.black
                              //                                   .withOpacity(
                              //                                       0.1),
                              //                               blurRadius: 4.5,
                              //                               spreadRadius: 1.0,
                              //                             )
                              //                           ],
                              //                         ),
                              //                         child: Center(
                              //                           child: Text(
                              //                             groups[index].name,
                              //                             overflow: TextOverflow
                              //                                 .ellipsis,
                              //                             style: TextStyle(
                              //                                 color: Colors
                              //                                     .black54,
                              //                                 fontFamily:
                              //                                     "Sans"),
                              //                           ),
                              //                         ),
                              //                       ),
                              //                     ),
                              //                   );
                              //                 },
                              //               ),
                              //             ),
                              //           ],
                              //         ),
                              //       ),

                              /// Background white for chose Size and Color
                              // Padding(
                              //   padding: const EdgeInsets.only(top: 10.0),
                              //   child: Container(
                              //     height: 120.0,
                              //     width: 600.0,
                              //     decoration: BoxDecoration(
                              //         color: Colors.white,
                              //         boxShadow: [
                              //           BoxShadow(
                              //             color: Color(0xFF656565)
                              //                 .withOpacity(0.15),
                              //             blurRadius: 1.0,
                              //             spreadRadius: 0.2,
                              //           )
                              //         ]),
                              //     child: Padding(
                              //       padding: const EdgeInsets.only(
                              //           top: 20.0, left: 20.0),
                              //       child: Column(
                              //         crossAxisAlignment:
                              //             CrossAxisAlignment.start,
                              //         children: <Widget>[

                              //           Padding(
                              //               padding:
                              //                   EdgeInsets.only(top: 10.0)),
                              //           Text(
                              //             "Color",
                              //             style: _subHeaderCustomStyle,
                              //           ),

                              //           // group == null
                              //           //     ? Text('')
                              //           //     : Container(
                              //           //         width: mediaQD
                              //           //                 .size
                              //           //                 .width /
                              //           //             2.5,
                              //           //         child: ListView.builder(

                              //           //           shrinkWrap: true,
                              //           //           itemCount: group == null
                              //           //               ? 0
                              //           //               : group.length,
                              //           //           itemBuilder:
                              //           //               (BuildContext context,
                              //           //                   int index) {
                              //           //             return Text(
                              //           //                 group[index]['price']);
                              //           //           },
                              //           //         ),
                              //           //       ),
                              //           //                                     group == null
                              //           // ? Text('')
                              //           // : GridView.count(
                              //           //     shrinkWrap: true,
                              //           //     // Create a grid with 2 columns. If you change the scrollDirection to
                              //           //     // horizontal, this produces 2 rows.

                              //           //     crossAxisCount: 4,
                              //           //     // Generate 100 widgets that display their index in the List.
                              //           //     children: List.generate(
                              //           //         group == null
                              //           //             ? 0
                              //           //             : group.length,
                              //           //         (index) {
                              //           //       return Center(
                              //           //         child: Text(
                              //           //           'Item $index',

                              //           //         ),
                              //           //       );
                              //           //     }),
                              //           //   ),
                              //           Row(
                              //             children: <Widget>[
                              //               // isie.warna[1].toString() == "hitam"
                              //               //     ?
                              //               RadioButtonColor(detailProduct
                              //                   .colorHex
                              //                   .toColor()),
                              //               // : Text(''),
                              //               // Padding(
                              //               //     padding: EdgeInsets.only(
                              //               //         left: 15.0)),
                              //               // isie.warna[2].toString() == "hijau"
                              //               //     ? RadioButtonColor(Colors.green)
                              //               //     : Text(''),
                              //               // Padding(
                              //               //     padding: EdgeInsets.only(
                              //               //         left: 15.0)),
                              //               // isie.warna[0].toString() == "biru"
                              //               //     ? RadioButtonColor(Colors.blue)
                              //               //     : Text(''),
                              //             ],
                              //           ),
                              //         ],
                              //       ),
                              //     ),
                              //   ),
                              // ),
                              userLevel == '4' &&
                                      detailProduct.wholesalePrice != null
                                  ? Padding(
                                      padding: const EdgeInsets.only(top: 10.0),
                                      child: Container(
                                        height: 100.0,
                                        width: 600.0,
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            boxShadow: [
                                              BoxShadow(
                                                color: Color(0xFF656565)
                                                    .withOpacity(0.15),
                                                blurRadius: 1.0,
                                                spreadRadius: 0.2,
                                              )
                                            ]),
                                        child: Padding(
                                          padding: EdgeInsets.only(top: 20.0),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 20.0),
                                                child: Text(
                                                  "Harga Grosir (${detailProduct.wholesaleType})",
                                                  style: _subHeaderCustomStyle,
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 15.0,
                                                    right: 20.0,
                                                    bottom: 10.0,
                                                    left: 20.0),
                                                child: TextFormatIDR(
                                                  thisText: int.parse(
                                                      detailProduct
                                                          .wholesalePrice),
                                                  thisStyle: _customTextStyle,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    )
                                  : Container(),

                              /// Background white for description
                              Container(
                                height: 205.0,
                                width: 600.0,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color(0xFF656565)
                                            .withOpacity(0.15),
                                        blurRadius: 1.0,
                                        spreadRadius: 0.2,
                                      )
                                    ]),
                                child: Padding(
                                  padding: EdgeInsets.only(top: 20.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 20.0),
                                        child: Text(
                                          "Description ",
                                          style: _subHeaderCustomStyle,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 15.0,
                                            right: 20.0,
                                            bottom: 10.0,
                                            left: 20.0),
                                        child: Text(
                                          detailProduct.description,
                                          style: _detailText,
                                          maxLines: 4,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ),
                                      Center(
                                        child: InkWell(
                                          onTap: () {
                                            _bottomSheet();
                                          },
                                          child: Text(
                                            "View More",
                                            style: TextStyle(
                                              color: Colors.indigoAccent,
                                              fontSize: 15.0,
                                              fontFamily: "Gotik",
                                              fontWeight: FontWeight.w700,
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),

                              /// Background white for Ratting
                              rev == null || rev.length == 0
                                  ? Text('')
                                  : Padding(
                                      padding: const EdgeInsets.only(top: 10.0),
                                      child: Container(
                                        height:
                                            mediaQD.size.height /
                                                2,
                                        // width: 600.0,
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            boxShadow: [
                                              BoxShadow(
                                                color: Color(0xFF656565)
                                                    .withOpacity(0.15),
                                                blurRadius: 1.0,
                                                spreadRadius: 0.2,
                                              )
                                            ]),
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                              top: 20.0, left: 20.0),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Text(
                                                    'Reviews',
                                                    style:
                                                        _subHeaderCustomStyle,
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 20.0,
                                                            top: 15.0,
                                                            bottom: 15.0),
                                                    child: Row(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      children: <Widget>[
                                                        // InkWell(
                                                        //   child: Padding(
                                                        //       padding:
                                                        //           EdgeInsets.only(
                                                        //               top: 2.0,
                                                        //               right: 3.0),
                                                        //       child: Text(
                                                        //         'View All',
                                                        //         style: _subHeaderCustomStyle
                                                        //             .copyWith(
                                                        //                 color: Colors
                                                        //                     .indigoAccent,
                                                        //                 fontSize:
                                                        //                     14.0),
                                                        //       )),
                                                        //   onTap: () {
                                                        //     Navigator.of(context).push(
                                                        //         PageRouteBuilder(
                                                        //             pageBuilder: (_,
                                                        //                     __,
                                                        //                     ___) =>
                                                        //                 ReviewsAll()));
                                                        //   },
                                                        // ),
                                                        // Padding(
                                                        //   padding:
                                                        //       const EdgeInsets.only(
                                                        //           right: 15.0,
                                                        //           top: 2.0),
                                                        //   child: Icon(
                                                        //     Icons.arrow_forward_ios,
                                                        //     size: 18.0,
                                                        //     color: Colors.black54,
                                                        //   ),
                                                        // )
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ),
                                              // Row(
                                              //   children: <Widget>[
                                              //     Row(
                                              //         mainAxisAlignment:
                                              //             MainAxisAlignment.start,
                                              //         children: <Widget>[
                                              //           StarRating(
                                              //             size: 25.0,
                                              //             starCount: 5,
                                              //             rating: 4.0,
                                              //             color: Colors.yellow,
                                              //           ),
                                              //           SizedBox(width: 5.0),
                                              //           Text('8 Reviews')
                                              //         ]),
                                              //   ],
                                              // ),
                                              Container(
                                                // color: Colors.red,
                                                height: mediaQD
                                                        .size
                                                        .height /
                                                    3,
                                                child: ListView.builder(
                                                  itemCount: rev == null
                                                      ? 0
                                                      : rev.length,
                                                  itemBuilder:
                                                      (BuildContext context,
                                                          int index) {
                                                    return Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 0.0,
                                                          right: 20.0,
                                                          top: 15.0,
                                                          bottom: 7.0),
                                                      child: Column(
                                                        children: <Widget>[
                                                          _line(),
                                                          _buildRating(
                                                              '18 Nov 2018',
                                                              rev[index]
                                                                  ['comments'],
                                                              (rating) {
                                                            setState(() {
                                                              this.rating =
                                                                  rating;
                                                            });
                                                          }, "assets/avatars/avatar-1.jpg")
                                                        ],
                                                      ),
                                                    );
                                                  },
                                                ),
                                                // ListView(
                                                //   children: <Widget>[
                                                //     Padding(
                                                //       padding: EdgeInsets.only(
                                                //           left: 0.0,
                                                //           right: 20.0,
                                                //           top: 15.0,
                                                //           bottom: 7.0),
                                                //       child: _line(),
                                                //     ),
                                                //     _buildRating('18 Nov 2018',
                                                //         'Item delivered in good condition. I will recommend to other buyer.',
                                                //         (rating) {
                                                //       setState(() {
                                                //         this.rating = rating;
                                                //       });
                                                //     }, "assets/avatars/avatar-1.jpg"),
                                                //     Padding(
                                                //       padding: EdgeInsets.only(
                                                //           left: 0.0,
                                                //           right: 20.0,
                                                //           top: 15.0,
                                                //           bottom: 7.0),
                                                //       child: _line(),
                                                //     ),
                                                //     _buildRating('18 Nov 2018',
                                                //         'Item delivered in good condition. I will recommend to other buyer.',
                                                //         (rating) {
                                                //       setState(() {
                                                //         this.rating = rating;
                                                //       });
                                                //     }, "assets/avatars/avatar-4.jpg"),
                                                //     Padding(
                                                //       padding: EdgeInsets.only(
                                                //           left: 0.0,
                                                //           right: 20.0,
                                                //           top: 15.0,
                                                //           bottom: 7.0),
                                                //       child: _line(),
                                                //     ),
                                                //     _buildRating('18 Nov 2018',
                                                //         'Item delivered in good condition. I will recommend to other buyer.',
                                                //         (rating) {
                                                //       setState(() {
                                                //         this.rating = rating;
                                                //       });
                                                //     }, "assets/avatars/avatar-2.jpg"),
                                                //     Padding(
                                                //         padding: EdgeInsets.only(
                                                //             bottom: 20.0)),
                                                //   ],
                                                // ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),

                              Container(
                                height: 310.0,
                                child: Column(
                                  children: <Widget>[
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    Text(
                                      "Related Products",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontFamily: "Gotik",
                                        fontSize: 15.0
                                      ),
                                    ),
                                    Expanded(
                                      child: ListView.builder(
                                        scrollDirection: Axis.horizontal,
                                        itemCount: listrelated.length,
                                        itemBuilder: (BuildContext context, int index) {
                                          return FavoriteItem(
                                            image: host +
                                                'upload/users/products/' +
                                                listrelated[index]
                                                    ['image1'],
                                            title: listrelated[index]
                                                ['name'],
                                            salary: listrelated[index]
                                                ['price'],
                                            rating: loadProducts[index]
                                                .rattingValue,
                                            tekan: loadProducts[index],
                                            id: listrelated[index]['id']
                                                .toString(),
                                                item: relatedProducts[index],
                                            tap: () {
                                              saveIdProduct(
                                                  listrelated[index]
                                                      ['rowPointer']);
                                              Navigator.of(context).push(
                                                  PageRouteBuilder(
                                                      pageBuilder: (_, __,
                                                              ___) =>
                                                          new DetailProduk(),
                                                      transitionDuration:
                                                          Duration(
                                                              milliseconds:
                                                                  900),

                                                      /// Set animation Opacity in route to detailProduk layout
                                                      transitionsBuilder: (_,
                                                          Animation<double>
                                                              animation,
                                                          __,
                                                          Widget child) {
                                                        return Opacity(
                                                          opacity: animation
                                                              .value,
                                                          child: child,
                                                        );
                                                      }));
                                            },
                                            /*sale: "923 Sale",*/
                                          );
                                        }
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            /// Button Pay
                            InkWell(
                              onTap: () {
                                bottomView();
                                cartBLOC.getGroup = null;
                              },
                              child: Container(
                                height: 55.0,
                                width: mediaQD.size.width,
                                decoration: BoxDecoration(
                                  color: Colors.indigoAccent,
                                ),
                                child: Center(
                                  child: Text(
                                    "Add To Cart",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w700,
                                      fontSize: 18.0
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      )

                      /// If user click icon chart SnackBar show
                      /// this code to show a SnackBar
                      /// and Increase a valueItemChart + 1
                      // isinay == ''
                      //     ? Padding(
                      //         padding: const EdgeInsets.only(bottom: 5.0),
                      //         child: Container(
                      //           color: Colors.white,
                      //           child: Row(
                      //             mainAxisAlignment:
                      //                 MainAxisAlignment.spaceEvenly,
                      //             children: <Widget>[
                      //               /// Button Pay
                      //               InkWell(
                      //                 onTap: () {
                      //                   if (detailProduct.wholesalePrice !=
                      //                           null &&
                      //                       userLevel == '4') {
                      //                     BlocProvider.of<GlobalBloc>(context)
                      //                         .cartBloc
                      //                         .addItem
                      //                         .add(DataProduct(
                      //                           brand: detailProduct.brand,
                      //                           category:
                      //                               detailProduct.category,
                      //                           colorHex:
                      //                               detailProduct.colorHex,
                      //                           colorName:
                      //                               detailProduct.colorName,
                      //                           dateCreated:
                      //                               detailProduct.dateCreated,
                      //                           discAmount: '0',
                      //                           discPercent: '0',
                      //                           description:
                      //                               detailProduct.description,
                      //                           flag: detailProduct.flag,
                      //                           id: detailProduct.id,
                      //                           image1: detailProduct.image1,
                      //                           name: detailProduct.name +
                      //                               ' ' +
                      //                               isinay,
                      //                           price: detailProduct
                      //                               .wholesalePrice,
                      //                           sku: detailProduct.sku +
                      //                               '-' +
                      //                               isinay,
                      //                           rowPointer:
                      //                               detailProduct.rowPointer,
                      //                           weight: detailProduct.weight,
                      //                           dimensions:
                      //                               detailProduct.dimensions,
                      //                           subcategory:
                      //                               detailProduct.subcategory,
                      //                           tags: detailProduct.tags,
                      //                         ));
                      //                     var snackbar = SnackBar(
                      //                       duration: Duration(seconds: 1),
                      //                       backgroundColor:
                      //                           Colors.indigoAccent,
                      //                       content: Text("Item Added"),
                      //                     );
                      //                     _key.currentState
                      //                         .showSnackBar(snackbar);
                      //                   } else {
                      //                     BlocProvider.of<GlobalBloc>(context)
                      //                         .cartBloc
                      //                         .addItem
                      //                         .add(detailProduct);
                      //                     var snackbar = SnackBar(
                      //                       duration: Duration(seconds: 1),
                      //                       backgroundColor:
                      //                           Colors.indigoAccent,
                      //                       content: Text("Item Added"),
                      //                     );
                      //                     _key.currentState
                      //                         .showSnackBar(snackbar);
                      //                   }
                      //                 },
                      //                 child: Container(
                      //                   height: 45.0,
                      //                   width:
                      //                       mediaQD.size.width /
                      //                           1.5,
                      //                   decoration: BoxDecoration(
                      //                     color: Colors.indigoAccent,
                      //                   ),
                      //                   child: Center(
                      //                     child: Text(
                      //                       "Add To Cart",
                      //                       style: TextStyle(
                      //                           color: Colors.white,
                      //                           fontWeight: FontWeight.w700),
                      //                     ),
                      //                   ),
                      //                 ),
                      //               ),
                      //             ],
                      //           ),
                      //         ),
                      //       )
                      //     : Padding(
                      //         padding: const EdgeInsets.only(bottom: 5.0),
                      //         child: Container(
                      //           color: Colors.white,
                      //           child: Row(
                      //             mainAxisAlignment:
                      //                 MainAxisAlignment.spaceEvenly,
                      //             children: <Widget>[
                      //               /// Button Pay
                      //               InkWell(
                      //                 onTap: () {
                      //                   if (detailProduct.wholesalePrice !=
                      //                           null &&
                      //                       userLevel == '4') {
                      //                     BlocProvider.of<GlobalBloc>(context)
                      //                         .cartBloc
                      //                         .addItem
                      //                         .add(DataProduct(
                      //                           brand: detailProduct.brand,
                      //                           category:
                      //                               detailProduct.category,
                      //                           colorHex:
                      //                               detailProduct.colorHex,
                      //                           colorName:
                      //                               detailProduct.colorName,
                      //                           dateCreated:
                      //                               detailProduct.dateCreated,
                      //                           discAmount: '0',
                      //                           discPercent: '0',
                      //                           description:
                      //                               detailProduct.description,
                      //                           flag: detailProduct.flag,
                      //                           id: detailProduct.id,
                      //                           image1: detailProduct.image1,
                      //                           name: detailProduct.name +
                      //                               ' ' +
                      //                               isinay,
                      //                           price: detailProduct
                      //                               .wholesalePrice,
                      //                           sku: detailProduct.sku +
                      //                               '-' +
                      //                               isinay,
                      //                           rowPointer:
                      //                               detailProduct.rowPointer,
                      //                           weight: detailProduct.weight,
                      //                           dimensions:
                      //                               detailProduct.dimensions,
                      //                           subcategory:
                      //                               detailProduct.subcategory,
                      //                           tags: detailProduct.tags,
                      //                         ));
                      //                     var snackbar = SnackBar(
                      //                       duration: Duration(seconds: 1),
                      //                       backgroundColor:
                      //                           Colors.indigoAccent,
                      //                       content: Text("Item Added"),
                      //                     );
                      //                     _key.currentState
                      //                         .showSnackBar(snackbar);
                      //                   } else {
                      //                     BlocProvider.of<GlobalBloc>(context)
                      //                         .cartBloc
                      //                         .addItem
                      //                         .add(DataProduct(
                      //                           brand: detailProduct.brand,
                      //                           category:
                      //                               detailProduct.category,
                      //                           colorHex:
                      //                               detailProduct.colorHex,
                      //                           colorName:
                      //                               detailProduct.colorName,
                      //                           dateCreated:
                      //                               detailProduct.dateCreated,
                      //                           discAmount:
                      //                               detailProduct.discAmount,
                      //                           discPercent:
                      //                               detailProduct.discPercent,
                      //                           description:
                      //                               detailProduct.description,
                      //                           flag: detailProduct.flag,
                      //                           id: detailProduct.id,
                      //                           image1: detailProduct.image1,
                      //                           name: detailProduct.name +
                      //                               ' ' +
                      //                               isinay,
                      //                           price: groupHarga,
                      //                           sku: detailProduct.sku +
                      //                               '-' +
                      //                               isinay,
                      //                           rowPointer:
                      //                               detailProduct.rowPointer,
                      //                           weight: detailProduct.weight,
                      //                           dimensions:
                      //                               detailProduct.dimensions,
                      //                           subcategory:
                      //                               detailProduct.subcategory,
                      //                           tags: detailProduct.tags,
                      //                         ));
                      //                     var snackbar = SnackBar(
                      //                       duration: Duration(seconds: 1),
                      //                       backgroundColor:
                      //                           Colors.indigoAccent,
                      //                       content: Text("Item Added"),
                      //                     );
                      //                     _key.currentState
                      //                         .showSnackBar(snackbar);
                      //                   }
                      //                 },
                      //                 child: Container(
                      //                   height: 45.0,
                      //                   width:
                      //                       mediaQD.size.width /
                      //                           1.5,
                      //                   decoration: BoxDecoration(
                      //                     color: Colors.indigoAccent,
                      //                   ),
                      //                   child: Center(
                      //                     child: Text(
                      //                       "Add To Cart",
                      //                       style: TextStyle(
                      //                           color: Colors.white,
                      //                           fontWeight: FontWeight.w700),
                      //                     ),
                      //                   ),
                      //                 ),
                      //               ),
                      //             ],
                      //           ),
                      //         ),
                      //       )
                    ],
                  )
                : Container(
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  ),
          );
        });
  }

  Widget _buildRating(
      String date, String details, Function changeRating, String image) {
    return ListTile(
      leading: Container(
        height: 45.0,
        width: 45.0,
        decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage(image), fit: BoxFit.cover),
            borderRadius: BorderRadius.all(Radius.circular(50.0))),
      ),
      title: Row(
        children: <Widget>[
          StarRating(
              size: 20.0,
              rating: 3.5,
              starCount: 5,
              color: Colors.yellow,
              onRatingChanged: changeRating),
          SizedBox(width: 8.0),
          Text(
            date,
            style: TextStyle(fontSize: 12.0),
          )
        ],
      ),
      subtitle: Text(
        details,
        style: _detailText,
      ),
    );
  }
}

/// RadioButton for item choose in size
class RadioButtonCustom extends StatefulWidget {
  final String txt;

  RadioButtonCustom({this.txt});

  @override
  _RadioButtonCustomState createState() => _RadioButtonCustomState(this.txt);
}

class _RadioButtonCustomState extends State<RadioButtonCustom> {
  _RadioButtonCustomState(this.txt);

  String txt;
  bool itemSelected = true;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: InkWell(
        onTap: () {
          setState(() {
            if (itemSelected == false) {
              setState(() {
                itemSelected = true;
              });
            } else if (itemSelected == true) {
              setState(() {
                itemSelected = false;
              });
            }
          });
        },
        child: Container(
          height: 37.0,
          width: 37.0,
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                  color: itemSelected ? Colors.black54 : Colors.indigoAccent),
              shape: BoxShape.circle),
          child: Center(
            child: Text(
              txt,
              style: TextStyle(
                  color: itemSelected ? Colors.black54 : Colors.indigoAccent),
            ),
          ),
        ),
      ),
    );
  }
}

/// RadioButton for item choose in color
class RadioButtonColor extends StatefulWidget {
  final Color clr;

  RadioButtonColor(this.clr);

  @override
  _RadioButtonColorState createState() => _RadioButtonColorState(this.clr);
}

class _RadioButtonColorState extends State<RadioButtonColor> {
  bool itemSelected = true;
  Color clr;

  _RadioButtonColorState(this.clr);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: InkWell(
        onTap: () {
          if (itemSelected == false) {
            setState(() {
              itemSelected = true;
            });
          } else if (itemSelected == true) {
            setState(() {
              itemSelected = false;
            });
          }
        },
        child: Container(
          height: 37.0,
          width: 37.0,
          decoration: BoxDecoration(
              color: clr,
              border: Border.all(
                  color: itemSelected ? Colors.black26 : Colors.indigoAccent,
                  width: 2.0),
              shape: BoxShape.circle),
        ),
      ),
    );
  }
}

Widget _line() {
  return Container(
    height: 0.9,
    width: double.infinity,
    color: Colors.black12,
  );
}
