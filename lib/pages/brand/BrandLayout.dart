import 'dart:convert';

import 'package:artline/env.dart';
import 'package:artline/storage/storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:artline/pages/home/Search.dart';
import 'package:artline/pages/home/CategoryDetail.dart';

class Brand extends StatefulWidget {
  @override
  _BrandState createState() => _BrandState();
}

class _BrandState extends State<Brand> {
  bool isLoading = false;
  DataStore storage = DataStore();

  List data;
  Future<String> getData() async {
    var $tokenType = await storage.getDataString('token_type');
    var $accesToken = await storage.getDataString('access_token');
    var res = await http.get(url('api/getCategories'), headers: {
      'Authorization': $tokenType + ' ' + $accesToken,
      'Accept': 'application/json'
    });
    if (mounted)
      setState(() {
        var content = json.decode(res.body);
        data = content['data'];
      });
    return 'success!';
  }

  void saveIdCategory(String id, String name) async {
    storage.setDataString('idCat', id);
    storage.setDataString('nameCat', name);
  }

  @override
  void initState() {
    super.initState();
    getData().then((s) {
      if (mounted)
        setState(() {
          isLoading = true;
        });
    });
  }

  @override
  Widget build(BuildContext context) {
    /// Component appbar
    var _appbar = AppBar(
      backgroundColor: Color(0xFFFFFFFF),
      elevation: 0.0,
      title: Padding(
        padding: const EdgeInsets.only(left: 10.0),
        child: Text(
          "Category",
          style: TextStyle(
              fontFamily: "Gotik",
              fontSize: 20.0,
              color: Colors.black54,
              fontWeight: FontWeight.w700),
        ),
      ),
      actions: <Widget>[
        InkWell(
          onTap: () {
            Navigator.of(context).push(PageRouteBuilder(
                pageBuilder: (_, __, ___) => new SearchAppbar()));
          },
          child: Padding(
            padding: const EdgeInsets.only(right: 20.0),
            child: Icon(
              Icons.search,
              size: 27.0,
              color: Colors.black54,
            ),
          ),
        )
      ],
    );

    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Scaffold(
          appBar: _appbar,
          body: isLoading
              ? Container(
                  color: Colors.white,
                  child: CustomScrollView(
                    /// Create List Menu
                    slivers: <Widget>[
                      SliverPadding(
                        padding: EdgeInsets.only(top: 0.0),
                        sliver: SliverFixedExtentList(
                            itemExtent: 145.0,
                            delegate: SliverChildBuilderDelegate(

                                /// Calling itemCard Class for constructor card
                                (context, index) => Padding(
                                      padding: const EdgeInsets.only(
                                          left: 10.0,
                                          right: 10.0,
                                          top: 5.0,
                                          bottom: 5.0),
                                      child: InkWell(
                                        onTap: () {
                                          saveIdCategory(
                                              data[index]['id'].toString(),
                                              data[index]['name']);
                                          Navigator.of(context).push(
                                            PageRouteBuilder(
                                                pageBuilder: (_, __, ___) =>
                                                    new CategoryDetail(),
                                                transitionDuration:
                                                    Duration(milliseconds: 600),
                                                transitionsBuilder: (_,
                                                    Animation<double> animation,
                                                    __,
                                                    Widget child) {
                                                  return Opacity(
                                                    opacity: animation.value,
                                                    child: child,
                                                  );
                                                }),
                                          );
                                        },
                                        child: Container(
                                          height: 130.0,
                                          width: 400.0,
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(15.0))),
                                          child: Hero(
                                            tag:
                                                'hero-tag-${data[index]['id'].toString()}',
                                            child: Material(
                                              child: DecoratedBox(
                                                decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(
                                                              15.0)),
                                                  image: DecorationImage(
                                                      image: NetworkImage(
                                                          host + 'upload/users/categories/' + data[index]['image']),
                                                      fit: BoxFit.cover),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Color(0xFFABABAB)
                                                          .withOpacity(0.3),
                                                      blurRadius: 1.0,
                                                      spreadRadius: 2.0,
                                                    ),
                                                  ],
                                                ),
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                15.0)),
                                                    color: Colors.black12
                                                        .withOpacity(0.1),
                                                  ),
                                                  child: Center(
                                                    child: Text(
                                                      data[index]['name'],
                                                      style: TextStyle(
                                                        color: Colors.white,
                                                        fontFamily: "Berlin",
                                                        fontSize: 35.0,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                childCount: data == null ? 0 : data.length)),
                      ),
                    ],
                  ),
                )
              : Container(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                )),
    );
  }
}

// /// Constructor for itemCard for List Menu
// class ItemCard extends StatelessWidget {
//   /// Declaration and Get data from BrandDataList.dart
//   final CategoryList brand;
//   ItemCard(this.brand);

//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding:
//           const EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
//       child: InkWell(
//         onTap: () {
//           Navigator.of(context).push(
//             PageRouteBuilder(
//                 pageBuilder: (_, __, ___) => new CategoryDetail(brand),
//                 transitionDuration: Duration(milliseconds: 600),
//                 transitionsBuilder:
//                     (_, Animation<double> animation, __, Widget child) {
//                   return Opacity(
//                     opacity: animation.value,
//                     child: child,
//                   );
//                 }),
//           );
//         },
//         child: Container(
//           height: 130.0,
//           width: 400.0,
//           decoration: BoxDecoration(
//               borderRadius: BorderRadius.all(Radius.circular(15.0))),
//           child: Hero(
//             tag: 'hero-tag-${brand.title}',
//             child: Material(
//               child: DecoratedBox(
//                 decoration: BoxDecoration(
//                   color: Colors.white,
//                   borderRadius: BorderRadius.all(Radius.circular(15.0)),
//                   image: DecorationImage(
//                       image: AssetImage(brand.image), fit: BoxFit.cover),
//                   boxShadow: [
//                     BoxShadow(
//                       color: Color(0xFFABABAB).withOpacity(0.3),
//                       blurRadius: 1.0,
//                       spreadRadius: 2.0,
//                     ),
//                   ],
//                 ),
//                 child: Container(
//                   decoration: BoxDecoration(
//                     borderRadius: BorderRadius.all(Radius.circular(15.0)),
//                     color: Colors.black12.withOpacity(0.1),
//                   ),
//                   child: Center(
//                     child: Text(
//                       brand.title,
//                       style: TextStyle(
//                         color: Colors.white,
//                         fontFamily: "Berlin",
//                         fontSize: 35.0,
//                       ),
//                     ),
//                   ),
//                 ),
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }
