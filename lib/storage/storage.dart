import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class DataStore {
  final String prefix = 'artlineApp/';
  Future<bool> setDataString(String name, String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(prefix + name, value);
    return true;
  }

  Future<String> getDataString(String name) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(prefix + name) ?? "Tidak ditemukan";
  }

  Future<bool> setDataInteger(String name, int value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(prefix + name, value);
    return true;
  }

  Future<int> getDataInteger(String name) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(prefix + name) ?? 0;
  }

  Future<bool> setDataBool(String name, bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(prefix + name, value);
    return true;
  }

  Future<bool> getDataBool(String name) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(prefix + name) ?? false;
  }
  Future<bool> storeObject(String name, Map<String, dynamic> value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(prefix + name, json.encode(value));
    return true;
  }
  Future<Map<String, dynamic>> fetchObject(String name) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return json.decode(prefs.getString(prefix + name)) ?? "Tidak ditemukan";
  }
  Future<bool> checkData(String name) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.containsKey(prefix + name) ?? false;
  }
  
  Future<bool> removeData(String name) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(prefix + name);
    return true;
  }
  Future<bool> clearData() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
    return true;
  }
}